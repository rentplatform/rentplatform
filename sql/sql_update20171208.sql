ALTER TABLE `ad_price_list`
ADD COLUMN `days`  int UNSIGNED NOT NULL DEFAULT 1 COMMENT '優惠天數' AFTER `end_time`,
ADD COLUMN `count`  int UNSIGNED NOT NULL DEFAULT 0 COMMENT '參與人數' AFTER `days`,
ADD COLUMN `title`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '活動名稱' AFTER `ad_id`;

ALTER TABLE `user`
ADD COLUMN `ad_percent_date`  timestamp NULL COMMENT '成為租公時取得的廣告優惠趴數到期時間' AFTER `ad_percent`;

INSERT INTO `rentplatform`.`admin_group_permission` (`gid`, `perm_id`) VALUES ('1', '58');
INSERT INTO `rentplatform`.`admin_group_permission` (`gid`, `perm_id`) VALUES ('1', '59');