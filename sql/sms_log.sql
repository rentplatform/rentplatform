/*
 Navicat MySQL Data Transfer

 Source Server         : local
 Source Server Version : 50617
 Source Host           : localhost
 Source Database       : rentplatform

 Target Server Version : 50617
 File Encoding         : utf-8

 Date: 11/01/2017 22:03:24 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `sms_log`
-- ----------------------------
DROP TABLE IF EXISTS `sms_log`;
CREATE TABLE `sms_log` (
	`bid` varchar(255) NOT NULL COMMENT '唯一識別碼，可藉由本識別碼查詢發送狀態',
	`phone_number` varchar(15) NOT NULL COMMENT '傳送人電話',
	`status` varchar(10) NOT NULL COMMENT '發送狀態',
	`cost` float NOT NULL COMMENT '花費',
	`unsend` int(11) NOT NULL COMMENT '失敗筆數',
	`send_date` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`bid`)
) ENGINE=`MyISAM` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ROW_FORMAT=DYNAMIC CHECKSUM=0 DELAY_KEY_WRITE=0;

-- ----------------------------
--  Records of `sms_log`
-- ----------------------------
BEGIN;
INSERT INTO `sms_log` VALUES ('220478cc-8506-49b2-93b7-2505f651c12e', '0980910641', '100', '1', '0', '2017-11-01 14:56:42'), ('77b6ac9d-aa9d-4095-a661-fe90ab2e230e', '+886980910641', '0\n', '1', '0', '2017-11-01 21:59:07');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
