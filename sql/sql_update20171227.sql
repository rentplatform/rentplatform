CREATE TABLE `rentplatform`.`carousel` (
	`caid` int UNSIGNED NOT NULL AUTO_INCREMENT,
	`pcimg` varchar(255) NOT NULL DEFAULT '' COMMENT 'PC版圖片',
	`mobileimg` varchar(255) NOT NULL DEFAULT '' COMMENT '手機版圖片',
	`url` text DEFAULT NULL COMMENT '連結',
	`status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '狀態, 0停用1啟用',
	`alwaysshow` tinyint(1) NOT NULL DEFAULT '0' COMMENT '狀態, 0 none,1 上架後永久顯示',
	`displayorder` int NOT NULL DEFAULT '1' COMMENT '排序',
	`start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '上架日期',
	`end_time` timestamp NULL DEFAULT NULL COMMENT '下架日期',
	`create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間',
	PRIMARY KEY (`caid`),
	INDEX `time` (`status`, start_time, end_time)
) ENGINE=`MyISAM` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT='首頁輪播圖列表';


insert into `rentplatform`.`admin_permission_list` ( `name`, `key`, `perm_id`) values ( '首頁輪播圖管理', 'carousel', '60');
insert into `rentplatform`.`admin_permission_list` ( `parent_perm_id`, `name`, `key`, `perm_id`) values ( '60', '首頁輪播圖編輯', 'carouselEdit', '61');
insert into `rentplatform`.`admin_permission_list` ( `parent_perm_id`, `name`, `key`, `perm_id`) values ( '60', '首頁輪播圖列表', 'carouselList', '62');


ALTER TABLE `rentplatform`.`carousel` ADD COLUMN `release_time` timestamp NULL DEFAULT NULL COMMENT '上次發布時間' AFTER `displayorder`, CHANGE COLUMN `start_time` `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '上架日期' AFTER `release_time`, CHANGE COLUMN `end_time` `end_time` timestamp NULL DEFAULT NULL COMMENT '下架日期' AFTER `start_time`, CHANGE COLUMN `create_time` `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間' AFTER `end_time`;
delete from `rentplatform`.`admin_permission_list` where `perm_id`='26';
ALTER TABLE `rentplatform`.`article` CHANGE COLUMN `Introduction` `introduction` varchar(255) DEFAULT NULL COMMENT '簡介';

insert into `rentplatform`.`article_type_list` ( `name` ) values ( '最新消息');
insert into `rentplatform`.`article_type_list` ( `name` ) values ( '電電玩家');
insert into `rentplatform`.`article_type_list` ( `name` ) values ( '設計達人');
insert into `rentplatform`.`article_type_list` ( `name` ) values ( '家事高手');
insert into `rentplatform`.`article_type_list` ( `name` ) values ( '共享經濟');
insert into `rentplatform`.`article_type_list` ( `name` ) values ( '活動紀錄');
insert into `rentplatform`.`article_type_list` ( `name` ) values ( '媒體報導');
delete from `rentplatform`.`admin_permission_list` where `perm_id`='30';
delete from `rentplatform`.`admin_permission_list` where `perm_id`='34';
delete from `rentplatform`.`admin_permission_list` where `perm_id`='38';
ALTER TABLE `rentplatform`.`static_page` CHANGE COLUMN `title` `title` varchar(100) COMMENT '標題';