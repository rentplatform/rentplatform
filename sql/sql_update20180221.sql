ALTER TABLE `device_list`
ADD COLUMN `notice`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '注意事項' AFTER `description`;

ALTER TABLE `rent_record`
ADD COLUMN `week`  int UNSIGNED NOT NULL DEFAULT 0 COMMENT '下單當時假日定價' AFTER `reward`,
ADD COLUMN `normal`  int UNSIGNED NOT NULL DEFAULT 0 COMMENT '下單當時平日定價' AFTER `week`;

update rent_record set week = 700,normal = 500;