$(document).ready(function() {

    $.request = function (url, data, fileTarget) {
        var formData = new FormData();
        $.each(data, function (k, r) {
            formData.append(r.name, r.value);
        });
        if (fileTarget !== undefined) {
            $.each(fileTarget, function (k, r) {
                var name = $(r).attr('name');
                if (r.files.length === 0) {
                    return;
                }
                $.each(r.files, function (_k, value) {
                    formData.append(name, value);
                })
            });
        }

        return $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false
        });
    };

    var event = {
        initDatatable: function () {
            $('.datatable-admin').each(function () {
                var linkUrl = $(this).data('linkurl');
                var ajax = $(this).data('ajax');
                var order = $(this).data('orderColumn');
                var params = {
                    dom: 'Bfrtip',
                    buttons: []
                };
                if (ajax) {
                    params.processing = true;
                    params.serverSide = true;
                    params.ajax = {
                        url: ajax,
                        type: 'POST'
                    };
                    // params.columnDefs = [
                    //     {
                    //         targets: [0],
                    //         orderable: false
                    //     }
                    // ]
                }
                if (order !== undefined) {
                    params.order = order.split(',');
                }
                if (linkUrl !== undefined) {
                    params.buttons.push({
                        className: "btn-success",
                        text: '新增資料',
                        action: function () {
                            window.location.href = linkUrl;
                        }
                    });
                }
                params.buttons.push('pageLength');
                var object = $(this).DataTable(params);
                $(this).data('object', object);
            });

        },
        initValidation: function () {
            $('.formValidation').each(function () {
                $(this).validate({
                    submitHandler:function(form) {
                        var url = $(form).attr('action');
                        var redirect = $(form).data('redirect');
                        var msg = $(form).data('msg');
                        var isUpdate = parseInt($(form).data('isUpdate')) === 1;
                        var callback = $(form).data('callback');

                        if (!msg) {
                            msg = isUpdate ? '資料修改成功' : '資料新增成功';
                        }

                        $(form).find('.alert-danger').removeClass('in').find('.txt').text('');
                        $.request(url, $(form).serializeArray(), $(form).find('input[type=file]'))
                            .always(function (res, status) {
                                res = res || {};
                                if (res.valid) {
                                    alert(msg);
                                    if (typeof callback === 'function' ) {
                                        callback(res);
                                    } else {
                                        window.location.href = redirect;
                                    }
                                    return;
                                } else {
                                    $(form).find('.alert-danger').addClass('in').find('.txt').text(res.msg || '表單提交失敗');
                                    $(form).find('input').one('focus', function () {
                                        $(form).find('.alert-danger').removeClass('in').find('.txt').text('');
                                    });
                                }
                            });
                    }
                });
            });
        },
        listDataDelete: function () {
            var $tr = $(this).parents('tr:eq(0)');
            var $table = $(this).parents('table').data('object');
            var index = $tr.index();
            var ajaxurl = $(this).data('url');
            if (confirm('確定刪除？')) {
                $.get(ajaxurl);
                $table.row($tr).remove().draw();
            }
        },
        selCountry: function () {
            var $this = $(this);
            var id = $(this).val();
            var target = $(this).data('targetCitySel');
            $.post('/api/data_api/getCityList', {cid:id}, function (res) {
                if (res && res.valid) {
                    $(target).find('option[value!=""]').remove();
                    $.each(res.data.list, function(k, r) {
                        $('<option/>').val(r.ct_id).text(r.name).appendTo(target);
                    });
                }
            }, 'json');
        }
    };

    event.initDatatable();
    event.initValidation();
    $(document).on('click', '.listDataDelete', event.listDataDelete);
    $(document).on('change', '.selCountry', event.selCountry);
});
