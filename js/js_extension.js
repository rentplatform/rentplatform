/**
 * 數字format
 * @param {int} ext 取小數點幾位，預設3位
 * @example new Number(123444232.2356502).format()
 * @return {string} 123,444,232.24
 * */
Number.prototype.format = function (ext) {
    var number = Number(this);
    ext = ext || 3;
    if (isNaN(number)) {
        return 0;
    }
    if (number % 1 !== 0) {
        number = number.toFixed(ext);
    } else {
        number = number.toString();
    }
    return number.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
};

/**
 * 無條件捨去到第幾位
 * @param {int} ext 第幾位後捨去
 * @example new Number(12.2356502).floorDec(1)
 * @return {Number} 12.2
 * */
Number.prototype.floorDec = function (ext) {
    var number = Number(this);
    if (isNaN(number)) {
        return 0;
    }
    var c = Math.pow(10, ext);
    return Math.floor(number * c) / c;
}

/**
 * @return {string} yyyy-mm-dd
 * */
Date.prototype.toFormatDate = function (tag) {
    tag = tag || '-';
    var date = this;
    if (isNaN(date)) {
        return '';
    }
    return date.getFullYear() + tag + (date.getMonth() + 1).toString().replace(/^(\d)$/, '0$1') + tag + (date.getDate()).toString().replace(/^(\d)$/, '0$1');
};

/**
 * @return {string} yyyy-mm-dd h:i:s
 * */
Date.prototype.toFormatDateTime = function (tag) {
    var date = this;
    if (isNaN(date)) {
        return '';
    }
    return date.toFormatDate(tag) + ' ' + (date.getHours()).toString().replace(/^(\d)$/, '0$1') + ':' + (date.getMinutes()).toString().replace(/^(\d)$/, '0$1') + ':' + (date.getSeconds()).toString().replace(/^(\d)$/, '0$1');
};

Date.prototype.dateDiff = function(interval,objDate){
    var dtEnd = new Date(objDate);
    if(isNaN(dtEnd)) return undefined;
    switch (interval) {
        case "s":return parseInt((dtEnd - this) / 1000);
        case "n":return parseInt((dtEnd - this) / 60000);
        case "h":return parseInt((dtEnd - this) / 3600000);
        case "d":return parseInt((dtEnd - this) / 86400000);
        case "w":return parseInt((dtEnd - this) / (86400000 * 7));
        case "m":return (dtEnd.getMonth()+1)+((dtEnd.getFullYear()-this.getFullYear())*12) - (this.getMonth()+1);
        case "y":return dtEnd.getFullYear() - this.getFullYear();
    }
}

/**
 * @example var fullName = 'Hello. My name is {0} {1}.'.format('FirstName', 'LastName');
 *          => Hello. My name is FirstName LastName
 * @return {string} format string
 * */
String.prototype.format = function (args) {
    var txt = this.toString();
    for (var i = 0; i < arguments.length; i++) {
        var exp = new RegExp('({)?\\{' + i + '\\}(?!})', 'gm');
        txt = txt.replace(exp, (arguments[i] == null ? "" : arguments[i]));
    }
    if (txt == null) {
        return "";
    }
    return txt.replace(new RegExp('({)?\\{\\d+\\}(?!})', 'gm'), "");
};

/**
 * @return {int} 小數點後擁有位數
 * */
String.prototype.decimalNum = function () {
    var number = this.toString();
    return (number.split('.')[1] || []).length;
};