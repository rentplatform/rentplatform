$(function () {
    var event = {
        clearForm: function (e) {
            if (!confirm('確定清空?')) {
                return;
            }
            $('form input, form textarea').each(function () {
                $(this).val('');
            });
        },
        backToList: function (e) {
            var isAlert = $(this).data('alert') === undefined ? true : $(this).data('alert');
            var p = $(this).data('n') || -1;
            if (isAlert) {
                if (!confirm('確定放棄?')) {
                    return;
                }
            }
            history.go(p);
            e.preventDefault();
        },
        initValidation: function () {
            $('.formValidation').each(function () {
                $(this).validate({
                    submitHandler: function (form) {
                        var url = $(form).attr('action');
                        var redirect = $(form).data('redirect');
                        var successMsg = $(form).data('msg') || '';
                        // var isUpdate = parseInt($(form).data('isUpdate')) === 1;
                        // $(form).find('.alert-danger').removeClass('in').find('.txt').text('');
                        $(form).find('[type=submit]').button('loading');
                        $.request(url, $(form).serializeArray(), $(form).find('input[type=file]'))
                            .always(function (res, status) {
                                res = res || {};
                                if (res.valid) {
                                    if (successMsg) {
                                        alert(successMsg);
                                    }
                                    if (redirect) {
                                        window.location.href = redirect;
                                    } else {
                                        window.location.reload();
                                    }
                                    return;
                                } else {
                                    $(form).find('[type=submit]').button('reset');
                                    alert(res.msg || '表單提交失敗');
                                    if (res.redirect) {
                                        window.location.href = res.redirect;
                                    }
                                    // $(form).find('.alert-danger').addClass('in').find('.txt').text(res.msg || '表單提交失敗');
                                    // $(form).find('input').one('focus', function () {
                                    //     $(form).find('.alert-danger').removeClass('in').find('.txt').text('');
                                    // });
                                }
                            });
                    }
                });
            });
        },
        selCountry: function () {
            var $this = $(this);
            var id = $(this).val();
            var target = $(this).data('targetCitySel');
            $.post('/api/data_api/getCityList', {cid:id}, function (res) {
                if (res && res.valid) {
                    $(target).find('option[value!=""]').remove();
                    $.each(res.data.list, function(k, r) {
                        $('<option/>').val(r.ct_id).text(r.name).appendTo(target);
                    });
                }
            }, 'json');
        },
        loginRedirect: function () {
            var uri = $(this).data('uri');
            var form = $('<form class="hide" action="/login" method="post">' +
                '<input type="hidden" name="redirect" value="' + uri + '" />' +
                '</form>');
            $('body').append(form);
            form.submit();
        }
    };

    /**
     * 多檔案上傳
     * @param {string} type
     * @param {input element} target
     * @param {function} callback
     * @param {function} error
     * */
    var uploadFileObj = function (type, target, callback, error) {
        callback = callback || function () {
            console.log('upload success');
        };
        error = error || function () {
        };
        var data = new FormData();
        data.append('name', 'eric');
        data.append('method', 'post');
        $.each(target, function (k, r) {
            var name = $(r).attr('name');
            if (r.files.length === 0) {
                return;
            }
            $.each(r.files, function (_k, value) {
                data.append(name, value);
            })
        });
        $.ajax({
            url: '/admin/api/file/test/' + type,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: callback,
            error: function () {
                console.log('upload error');
                alert('檔案上傳失敗');
                error();
            }
        });
    };
    /**
     *
     * .done(function() { alert("success"); })
     * .fail(function() { alert("error"); })
     * .always(function() { alert("complete"); });
     * @return {$.ajax} ajax object
     * */
    var request = function (url, data, fileTarget) {
        var formData = new FormData();
        $.each(data, function (k, r) {
            formData.append(r.name, r.value);
        });
        if (fileTarget !== undefined) {
            $.each(fileTarget, function (k, r) {
                var name = $(r).attr('name');
                if (r.files.length === 0) {
                    return;
                }
                $.each(r.files, function (_k, value) {
                    formData.append(name, value);
                })
            });
        }

        return $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false
        });
    };

    $.validator.addMethod("phone", function (value, element, params) {
        if (value.length < 1) {
            return false;
        }
        if (/^09[0-9]{8}$/.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "請輸入正確的手機號碼");

    //所有input共用，開始輸入就註銷錯誤顯示
    // $(document).on('click', '.clearForm', event.clearForm);
    $.uploadFile = uploadFileObj;
    $.request = request;
    event.initValidation();

    $('.selCountry').on('change', event.selCountry);
    $('.loginRedirect').on('click', event.loginRedirect);
});