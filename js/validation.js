$(function () {
    var gfunc = {
        showError: function (t, txt) {
            t.parents('.form-group:eq(0)').addClass('has-error');
            var $e = t.parents('div:eq(0)').find('p.text-danger');
            if ($e.length > 0) {
                $e.text(txt);
            } else {
                $('<p class="text-danger"/>').text(txt).appendTo(t.parents('div:eq(0)'));
            }
        },
        reset: function (t) {
            t.parents('div:eq(0)').find('p.text-danger').remove();
        },
        removeInputError: function () {
            $(this).parents('.form-group:eq(0)').removeClass('has-error');
            gfunc.reset($(this));
        }
    };
    $.fn.validation = function () {
        var $form = $(this);
        var map = {};
        var disabledMap = [];
        var msgMap = {};
        var types = {
            length: function (t, n) {
                if (t.val().length > n) {
                    gfunc.showError(t, msgMap.length || '字數超過限制');
                    return false;
                }
                return true;
            },
            noneEmpty: function (t) {
                var v = t.val();
                if (v === '' || v === undefined) {
                    gfunc.showError(t, msgMap.noneEmpty || '此欄位不可為空');
                    return false;
                }
                return true;
            },
            isNumber: function (t) {

                if (!/^[0-9]+$/.test(t.val())) {
                    gfunc.showError(t, msgMap.isNumber || '此欄位僅限輸入數字');
                    return false;
                }
                return true;
            },
            isEmail: function (t) {
                var pattern = /^([\w]+)(.[\w]+)*@([\w]+)(.[\w]{2,3}){1,2}$/;
                if (!pattern.test(t.val())) {
                    gfunc.showError(t, msgMap.isNumber || '信箱格式不正確');
                    return false;
                }
                return true;
            }
        }
        var event = {
            add: function (name, args, msg) {
                if (disabledMap.indexOf(name) >= 0) {
                    delete disabledMap[name];
                    return;
                }
                map[name] = args;
            },
            call: function (t, method, args) {
                return types[method](t, args);
            },
            disable: function (name) {
                console.log(disabledMap.indexOf(name));
                if (disabledMap.indexOf(name) < 0) {
                    disabledMap.push(name);
                }
            },
            editMsg: function (method, msg) {
                msgMap[method] = msg;
            },
            getFormData: function () {
                var data = {};
                var arr = $form.serializeArray();
                $.each(arr, function (k, r) {
                    data[r.name] = r.value;
                });
                return data;
            },
            reopenSubmit: function () {
                $form.find('button[type=submit]').button('reset');
            },
            callback: null
        }
        //禁止submit換頁事件
        $form.find('button[type=submit]').attr('onClick', 'javascript:return false;')
        $form.find('button[type=submit], .submit').attr({
            'data-loading-text': $form.find('button[type=submit]').text(),
            'autocomplete': 'off'
        }).on('click', function () {
            $form.find('button[type=submit]').button('loading');
            var result = true;
            $.each(map, function (name, args) {
                var t = $form.find('[name="{0}"]'.format(name));
                for (var method in args) {
                    if ((typeof args[method] === 'boolean' && !args[method]) || disabledMap.indexOf(name) >= 0) {
                        continue;
                    }
                    if (types[method] === undefined) {
                        console.error('{0} is not a function'.format(method));
                        continue;
                    }
                    if (!types[method](t, args[method])) {
                        result = false;
                        console.log('error name:{0}, function:{1}'.format(name, method));
                        break;
                    }
                    gfunc.reset(t);
                }
            });
            if (result) {
                if (event.callback === null) {
                    $form.submit();
                    return;
                }
                event.callback();
            } else {
                event.reopenSubmit();
            }
        });

        $form.find('input, textarea').on('click', gfunc.removeInputError);

        return event;
    }

});