$(function () {
    var gfunc = {
        showError: function (t, txt) {
            var $e = t.parents('div:eq(0)').find('p.msg');
            if ($e.length > 0) {
                $e.text(txt);
            } else {
                $('<p class="msg" style="color:red;margin:0;"/>').text(txt).appendTo(t.parents('div:eq(0)'));
            }
        },
        reset: function (t) {
            t.parents('div:eq(0)').find('p.msg').remove();
        },
        removeInputError: function () {
            gfunc.reset($(this));
        }
    };
    $.fn.validation = function () {
        var $form = $(this);
        var map = {};
        var disabledMap = [];
        var msgMap = {};
        var types = {
            length: function (t, n) {
                var length = t.val().length;
                
                if (typeof n === 'object') {
                    if (length < n.less) {
                        gfunc.showError(t, '字數不得小於{0}位'.format(n.less));
                        return false;
                    } else if (length > n.more) {
                        gfunc.showError(t, '字數不得大於%s位'.format(n.more));
                        return false;
                    }
                } else {
                    if (length > n) {
                        gfunc.showError(t, msgMap.length || '字數超過限制');
                        return false;
                    }
                }
                return true;
            },
            noneEmpty: function (t) {
                var v = t.val();
                if (v === '' || v === undefined) {
                    gfunc.showError(t, msgMap.noneEmpty || '此欄位不可為空');
                    return false;
                }
                return true;
            },
            isNumber: function (t) {
                
                if (!/^[0-9]+$/.test(t.val())) {
                    gfunc.showError(t, msgMap.isNumber || '此欄位僅限輸入數字');
                    return false;
                }
                return true;
            },
            isMobile: function (t) {
                var pattern = /^[09]{2}[0-9]{8}$/;
                if (t.val() !== '' && !pattern.test(t.val())) {
                    gfunc.showError(t, msgMap.isMobile || '手機格式不正確');
                    return false;
                }
                return true;
            },
            isEmail: function (t) {
                var pattern = /^([\w]+)(.[\w]+)*@([\w]+)(.[\w]{2,3}){1,2}$/;
                if (t.val() !== '' && !pattern.test(t.val())) {
                    gfunc.showError(t, msgMap.isEmail || '信箱格式不正確');
                    return false;
                }
                return true;
            },
            compareOf: function (t, o) {
                if (t.val() !== $form.find('[name="{0}"]'.format(o.t)).val()) {
                    gfunc.showError(t, o.msg);
                    return false;
                }
                return true;
            },
            xssFilter: function (t) {
                var pattern = /^[^~!@#$%^&*()_+<>\\/;]+$/;
                if (t.val() !== '' && !pattern.test(t.val())) {
                    gfunc.showError(t, msgMap.xssFilter || '輸入格式不正確');
                    return false;
                }
                return true;
            }
        }
        var event = {
            form: $form,
            add: function (name, args, msg) {
                if (disabledMap.indexOf(name) >= 0) {
                    delete disabledMap[name];
                    return;
                }
                map[name] = args;
            },
            call: function (t, method, args) {
                return types[method](t, args);
            },
            disable: function (name) {
                console.log(disabledMap.indexOf(name));
                if (disabledMap.indexOf(name) < 0) {
                    disabledMap.push(name);
                }
            },
            editMsg: function (method, msg) {
                msgMap[method] = msg;
            },
            getFormData: function (ignoreList) {
                var data = {};
                var arr = $form.serializeArray();
                ignoreList = ignoreList || [];
                $.each(arr, function (k, r) {
                    if ($.inArray(r.name, ignoreList) === -1) {
                        data[r.name] = r.value;
                    }
                });
                return data;
            },
            reopenSubmit: function () {
                if ($form.find('button[type=submit]').length > 0) {
                    $form.find('button[type=submit]').button('reset');
                } else if ($form.find('.submit').length > 0){
                    $form.find('.submit').removeClass('disabled');
                }
            },
            closeSubmit: function () {
                if ($form.find('button[type=submit]').length > 0) {
                    $form.find('button[type=submit]').button('loading');
                } else if ($form.find('.submit').length > 0){
                    $form.find('.submit').addClass('disabled');
                }
            },
            callback: null
        }
        //禁止submit換頁事件
        $form.find('button[type=submit]').attr('onClick', 'javascript:return false;')
        $form.find('button[type=submit], .submit:not(.disabled)').attr({
            'data-loading-text':$form.find('button[type=submit]').text(),
            'autocomplete': 'off'
        }).on('click', function () {
            event.closeSubmit();
            var result = true;
            $.each(map, function (name, args) {
                var t = $form.find('[name="{0}"]'.format(name));
                for (var method in args) {
                    if ((typeof args[method] === 'boolean' && !args[method]) || disabledMap.indexOf(name) >= 0) {
                        continue;
                    }
                    if (types[method] === undefined) {
                        console.error('{0} is not a function'.format(method));
                        continue;
                    }
                    if (!types[method](t, args[method])) {
                        result = false;
                        console.log('error name:{0}, function:{1}'.format(name, method));
                        break;
                    }
                    gfunc.reset(t);
                }
            });
            if (result) {
                if (event.callback === null) {
                    $form.submit();
                    return ;
                }
                event.callback();
            } else {
                event.reopenSubmit();
            }
        });
        
        $form.find('input, textarea').on('click keypress', gfunc.removeInputError);

        return event;
    }
    
});