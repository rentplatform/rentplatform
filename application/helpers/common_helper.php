<?php

function count_date($start, $end, $format = '%d') {
    $a = date_create($start);
    $b = date_create($end);
    return (int)date_diff($a, $b)->format($format) + 1;
}