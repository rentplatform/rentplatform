<?php

function genTimeWhereString($data, $prefix = '') {
    $output = '';
    $timeWhere = array();
    if (!empty($data['start_time'])) {
        $timeWhere[] = " {$prefix}start_time >= '{$data['start_time']}' ";
    }
    if (!empty($data['end_time'])) {
        $timeWhere[] = " {$prefix}end_time <= '{$data['end_time']}' ";
    }
    if (count($timeWhere) > 0) {
        $output = implode('AND', $timeWhere);
        $output = "($output)";
    }
    return $output;
}

function arrayToSearchString($array) {
    $new = array();
    foreach ($array as $k => $v) {
        $new[] = "\"{$v}\"";  
    }
    return implode(',', $new);
}