<?php

function debug() {
    $args = func_get_args();
    echo '<pre>';
    foreach ($args as $k => $v) {
        var_dump($v);
    }
    die;
}

function write_log($str,$data)  //傳入資料夾名 資料
{

    $textname = $str.date("Ymd").".txt"; //檔名  filename
    $URL = FCPATH . "log/".$str."/";                         //路徑  Path
    if(!is_dir($URL))                                 // 路徑中的$str 資料夾是否存在 Folder exists in the path
        mkdir($URL,0700);

    $URL .= $textname;                           //完整路徑與檔名 The full path and filename

    $time = $str.":".date("H:i:s"); //時間 Time
    $writ_tmp = '';
    if (is_array($data)) {
        $writ_tmp = json_encode($data);
    } else {
        $writ_tmp = $data;
    }
    $write_data = $time.':  '.$writ_tmp."\n";

    $fileopen = fopen($URL, "a+");
    fseek($fileopen, 0);
    fwrite($fileopen,$write_data);                 //寫資料進去 write data
    fclose($fileopen);
}