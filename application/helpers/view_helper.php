<?php

function load_modal() {
    $m = func_get_args();
    foreach ($m as $k => $v) {
        require_once VIEWPATH . "modal/{$v}_modal.php";
    }
}