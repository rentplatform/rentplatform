<?php

class Barrister extends Ext_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Common_model');
    }

    public function index() {
        $this->mOutput['title'] = '法律顧問';
        $data = $this->Common_model->getStaticData(STATICPAGE_TYPE['BARRISTER']);
        $this->mOutput['content'] = $data ? $data['content'] : '';
        $this->loadView('page/static_page');
    }

}