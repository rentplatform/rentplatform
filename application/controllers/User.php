<?php
/**
 * User: eric
 * Date: 2017/11/4
 */
class User extends Ext_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->checkLogin(true)) {
            redirect('/login');
        }
    }

    /**
     * 個人資料頁
     */
    public function profile() {
        $uid = $this->mOutput['profile']['uid'];
        $this->mOutput['cancel'] = $this->db->where(['renter_uid' => $uid, 'status' => RENT_RECORD_STATUS['RENT_CANCEL']])->count_all_results('rent_record');
        $this->mOutput['noresponse'] = $this->db->where(['device_owner' => $uid, 'status' => RENT_RECORD_STATUS['MISS']])->count_all_results('rent_record');

        $this->loadView('user/user_profile');
    }

    /**
     * 修改個人資料頁
     */
    public function editProfile() {
        $uid = $this->mOutput['profile']['uid'];
        $this->mOutput['cancel'] = $this->db->where(['renter_uid' => $uid, 'status' => RENT_RECORD_STATUS['RENT_CANCEL']])->count_all_results('rent_record');
        $this->mOutput['noresponse'] = $this->db->where(['device_owner' => $uid, 'status' => RENT_RECORD_STATUS['MISS']])->count_all_results('rent_record');

        $this->loadView('user/user_profile_edit');
    }

    /**
     * 訊息通知頁面
     * @param int $type
     * @param int $page
     */
    public function notifications(int $type = 0, int $page = 1) {
        $limit = 10;
        $this->load->model('Notification_model');
        $this->load->model('Rent_model');
        $data = $this->Notification_model->getList($this->mOutput['profile']['uid'], $type, $page, $limit);
        $this->loadPagenation("/user/notifications/{$type}", $data['count'], $limit);
        $list = [];
        $today = date('Y/m/d');
        foreach ($data['list'] as $row) {
            $date = date('Y/m/d', strtotime($row['create_time']));
            if (!isset($list[$date])) {
                $list[$date] = [
                    'text' => $date === $today ? 'Today' : $date,
                    'list' => []
                ];
            }
            $status = $row['id'] ? $this->Rent_model->checkOrderStatus($row['id']) : 0;
            $list[$date]['list'][] = [
                'content' => $row['content'],
                'id' => $row['id'],
                'status' => $status
            ];
        }
        $this->mOutput['data'] = $list;
        $this->mOutput['type'] = $type;
        $this->loadView('user/notifications');
    }

    /**
     * 租借紀錄頁
     */
    public function rentRecord(int $type = 0, int $page = 1) {
        $limit = 10;
        $uid = $this->mOutput['profile']['uid'];
        $this->load->model('Rent_model');
        $this->load->library('encryption');
        $data = $this->Rent_model->getRentRecordList($type, $uid, $page, $limit);
        $this->loadPagenation("/user/rentRecord/{$type}", $data['count'], $limit);

        $list = [];
        $today = date('Y/m/d');
        foreach ($data['list'] as $row) {
            $date = date('Y/m/d', strtotime($row['create_time']));
            if (!isset($list[$date])) {
                $list[$date] = [
                    'text' => $date === $today ? 'Today' : $date,
                    'list' => []
                ];
            }
            switch ($row['status']) {
                case 0:
                    $row['statustxt'] = '待回覆';
                    break;
                case 1:
                    $row['statustxt'] = '租借中';
                    break;
                case 2:
                    $row['statustxt'] = '結案';
                    break;
                case 3:
                    $row['statustxt'] = '未回應已取消';
                    break;
                case 4:
                    $row['statustxt'] = $type > 0 ? '已拒絕' : '已取消';
                    break;
                case 5:
                    $row['statustxt'] = '已取消';
                    break;
                case 6:
                    $row['statustxt'] = '已接單';
                    break;
                case 7:
                    $row['statustxt'] = '處理中';
                    break;
                case 8:
                    $row['statustxt'] = '已銷單';
                    break;
                default:
                    $row['statustxt'] = '';
            }
            $row['privacyStr'] = urlencode($this->encryption->encrypt($row['ur_id']));
            $list[$date]['list'][] = $row;
        }

        $this->mOutput['data'] = $list;
        $this->mOutput['type'] = $type;
        $this->loadView('user/user_rent_record');
    }

    public function arbitration() {
        $this->loadView('user/arbitration');
    }

    /**
     * 驗證手機起始頁
     */
    public function validPhone() {
        $this->loadView('other/valid_phone');
    }

    /**
     * 驗證手機驗證碼頁面
     */
    public function validPhoneSign() {
        $valid_phone_data = $this->session->userdata('valid_phone_data');
        if (empty($valid_phone_data)) {
            redirect('/');
            return;
        }
        $this->loadView('other/valid_phone_sign');
    }

    /**
     * 手機驗證成功頁面
     */
    public function validPhoneSuccess() {
        $valid_phone_data = $this->session->userdata('valid_phone_data');
        if (empty($valid_phone_data)) {
            redirect('/');
            return;
        }
        $this->session->unset_userdata('valid_phone_data');
        if ($redirect = $this->session->userdata('phone_success_redirect')) {
            $this->session->unset_userdata('phone_success_redirect');
            redirect($redirect);
            return;
        }
        $this->loadView('other/valid_phone_success');
    }

}