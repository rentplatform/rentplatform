<?php
/**
 * Created by PhpStorm.
 * User: asdf
 * Date: 2017/11/12
 * Time: 下午 12:56
 */

class Guest extends Ext_Controller {

    public function forgetPassword() {
        $this->loadView('other/forget_pwd');
    }

    public function forgetPasswordSign() {
        $valid_pwd_data = $this->session->userdata('valid_pwd_data');
        if (empty($valid_pwd_data)) {
            redirect('/');
            return;
        }
        $this->mOutput['uid'] = $valid_pwd_data['user']['uid'];
        $this->mOutput['email'] = $valid_pwd_data['user']['username'];

        $this->loadView('other/forget_pwd_sign');
    }

}