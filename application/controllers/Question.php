<?php

class Question extends Ext_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Page_model', 'page');
    }

    public function index() {
        $data = $this->page->getQuestion();
        $output = [];
        $tmp = [];
        foreach ($data as $k => $row) {
            $tmp[] = $row;
            if (count($tmp) === 2 || $k === count($data) - 1) {
                $output[] = $tmp;
                $tmp = [];
            }
        }
        $this->mOutput['list'] = $output;
        $this->loadView('page/question');
    }

}