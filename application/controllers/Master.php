<?php

/**
 * 租公相關
 * @property Master_model $master
 * @property Common_model $common
 * @property Device_model $device
 */
class master extends Ext_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Master_model', 'master');
        $this->load->model('Common_model', 'common');
        $this->load->model('Device_model', 'device');
    }

    /**
     * 成為電租公頁面
     */
    public function apply() {
        $this->mOutput['masterGood'] = $this->common->getStaticData(STATICPAGE_TYPE['MASTER_GOOD'], true);
        $this->loadView('user/apply_for_master');
    }

    /**
     * 電租公詳情頁
     * @param int $uid
     */
    public function card(int $uid = 0) {
        $this->load->model('User_model', 'user');
        $this->load->model('Rent_model', 'rent');
        if (!$uid) {
            redirect('/');
            return;
        }
        $data = $this->user->getProfile($uid);
        //只能查租公
        if (empty($data) || $data['rent_master'] < 1) {
            redirect('/');
            return;
        }
        $data['miss'] = $this->rent->getDeviceOwnerRecordCountByStatus($uid, RENT_RECORD_STATUS['MISS']);
        $data['reject'] = $this->rent->getDeviceOwnerRecordCountByStatus($uid, RENT_RECORD_STATUS['REJECT']);

        $this->mOutput['usercard'] = $data;
        $this->mOutput['deviceList'] = $this->device->getDeviceListByUid($uid);

        $this->loadView('master/card');
    }

    /**
     * 聯絡租客頁面
     * @param $urid 租借紀錄編號
     */
    public function contactRenter($urid = 0) {
        $this->checkLogin();
        if ($this->mOutput['profile']['rent_master'] < 1) {
            redirect('/');
            return;
        }
        $this->load->model('Rent_model', 'rent');
        $this->load->model('User_model', 'user');
        $this->load->library('encryption');
        $data = $this->rent->getRentRequestOrder($urid);
        if (empty($data)) {
            redirect('/');
            return;
        }
        $create_time = new DateTime($data['create_time']);
        $create_time->add(new DateInterval("P3D"));
        $validDate = $create_time->format('Y/m/d');


        $start_time = new DateTime($data['start_time']);
        $end_time = date('Ymd', strtotime($data['end_time']));
        $normal = 0;
        $weekend = 0;
        do {
            $start_time->add(new DateInterval("P1D"));

            switch ($start_time->format('N')) {
                case 1:
                    $weekend++;
                    break;
                case 5:
                    $normal++;
                    break;
            }
        } while($start_time->format('Ymd') !== $end_time);


        $this->mOutput['data'] = $data;
        $this->mOutput['validDate'] = $validDate;
        $this->mOutput['normal'] = $normal;
        $this->mOutput['weekend'] = $weekend;
        $this->mOutput['countDate'] = count_date($data['start_time'], $data['end_time']);
        $this->mOutput['renter'] = $this->user->getProfile($data['renter_uid']);
        $this->mOutput['privacyStr'] = urlencode($this->encryption->encrypt($data['ur_id']));
        $this->loadView('master/contact_renter');

    }

    /**
     * 電器管理
     */
    public function deviceList() {
        $this->checkLogin();
        if ($this->mOutput['profile']['rent_master'] < 1) {
            redirect('/');
            return;
        }
        $uid = $this->mOutput['profile']['uid'];
        $this->mOutput['reward'] = $this->master->getRewardCount($uid);
        $this->mOutput['list'] = $this->device->getDeviceListByUid($uid);
        $this->mOutput['canEditDevice'] = count($this->master->getLocationList($uid)) > 0;

        $this->loadView('master/device_list');
    }

    /**
     * 電器新增編輯
     * @param int $deviceId
     */
    public function deviceEdit($deviceId = 0) {
        $this->checkLogin();
        $this->load->model('Rent_model');
        if ($this->mOutput['profile']['rent_master'] < 1) {
            show_404('/');
            return;
        }
        $this->mOutput['brandList'] = $this->common->getBrandList();
        $this->mOutput['deviceTypeList'] = $this->common->getDeviceTypeList();
        if ($deviceId) {
            $data = $this->device->getDevice($deviceId);
            //查無電器或是非擁有者則導向404
            if (!$data || $data['uid'] != $this->mOutput['profile']['uid']) {
                show_404();
                return;
            }
            $data['accessory'] = json_decode($data['accessory'], true);
            $this->mOutput['isRenting'] = $this->Rent_model->checkOrderStatusByDevice($deviceId);
            $this->mOutput['data'] = $data;
        }

        $this->loadView('master/device_edit');
    }

    /**
     * 電器接單頁
     * @param int $urId
     */
    public function rentRequest($urId = 0) {
        if (!$this->checkLogin(true)) {
            if ($urId) {
                $this->session->set_userdata('login_redirect', "/master/rentRequest/{$urId}");
            }
            redirect('/login');
            return;
        }
        if (!$urId) {
            show_404();
            return;
        }
        $this->load->model('Rent_model');
        $data = $this->Rent_model->getRentRequestOrder($urId);
        $data['notice'] = str_replace(PHP_EOL, '<br>', $data['notice']);
        if (empty($data) || $this->mOutput['profile']['uid'] != $data['device_owner']) {
            show_404();
            die;
        }

        $create_time = new DateTime($data['create_time']);
        $create_time->add(new DateInterval("P3D"));
        $validDate = $create_time->format('Y/m/d');


        $start_time = new DateTime($data['start_time']);
        $end_time = date('Ymd', strtotime($data['end_time']));
        $normal = 0;
        $weekend = 0;
        do {
            $start_time->add(new DateInterval("P1D"));

            switch ($start_time->format('N')) {
                case 1:
                    $weekend++;
                    break;
                case 5:
                    $normal++;
                    break;
            }
        } while($start_time->format('Ymd') !== $end_time);


        $this->mOutput['data'] = $data;
        $this->mOutput['validDate'] = $validDate;
        $this->mOutput['normal'] = $normal;
        $this->mOutput['weekend'] = $weekend;
        $this->mOutput['urId'] = $urId;
        $this->loadView('master/rent_request');
    }

    /**
     * 面交點列表
     */
    public function location() {
        $this->checkLogin();
        $this->mOutput['countryList'] = $this->common->getCountryList();
        $this->mOutput['list'] = $this->master->getLocationList($this->mOutput['profile']['uid']);
        $this->loadView('master/location');
    }

    /**
     * 驗證信用卡起始頁
     */
    public function validCredit() {
        //未登入狀態導向登入頁
        if (!$this->checkLogin(true)) {
            redirect('/login');
            return;
        }
        //手機未驗證導向手機驗證頁，半小時內驗證完會在導回來，超過則走正常流程
        if ($this->mOutput['profile']['phone_valid'] < 1) {
            $this->session->set_userdata('phone_success_redirect', '/master/validCredit');
            redirect('/user/validPhone');
            return;
        }
        $this->loadView('other/valid_credit');
    }

    /**
     * 信用卡驗證成功頁
     */
    public function validCreditSuccess() {
        $data = $this->getTradReturn();
        if ((int)$data['RtnCode'] === 1) {
            $this->load->model('User_model', 'user');
            $this->mOutput['profile']['credit_card_valid'] = 1;
            $this->mOutput['profile']['rent_master'] = 1;
            $this->session->set_userdata('profile', $this->mOutput['profile']);
            $success = true;
        } else {
            $success = false;
        }

        $this->mOutput['isSuccess'] = $success;
        $this->loadView('other/valid_credit_success');
    }

    /**
     * 接單成功頁
     */
    public function rentSuccess() {
        $data = $this->getTradReturn();
        if ((int)$data['RtnCode'] === 1) {

            $success = true;
        } else {
            $success = false;
        }

        $this->mOutput['isSuccess'] = $success;
        $this->mOutput['urid'] = $data['CustomField1'];
        $this->loadView('master/rent_request_success');
    }

    /**
     * 驗證綠界回傳值
     * @return array|void
     */
    private function getTradReturn() {
        $this->checkLogin();
        $post = $this->input->post(null, true);
        $paramsName = array(
            'MerchantID', 'MerchantTradeNo', 'StoreID', 'RtnCode', 'RtnMsg', 'TradeNo',
            'TradeAmt', 'PaymentDate', 'PaymentType', 'PaymentTypeChargeFee', 'TradeDate', 'SimulatePaid', 'CheckMacValue',
            'CustomField1', 'CustomField2', 'CustomField3', 'CustomField4'
        );
        $data = array();
        //檢查傳入參數
        foreach ($paramsName as $k => $key) {
            if (!isset($post[$key])) {
                redirect('/');
                return;
            }
            $data[$key] = $post[$key];
        }
        return $data;
    }

}