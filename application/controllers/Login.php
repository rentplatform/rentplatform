<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 登入頁控制器
 */
class Login extends Ext_Controller {

    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $redirect = $this->session->userdata('login_redirect') ? : $this->input->post('redirect');
        $this->session->unset_userdata('login_redirect');
        $this->mOutput['redirect'] = $redirect ? : '/';
        $this->loadView('other/login');
    }

    /**
     * 信箱驗證接口，sign如果不存在會導向錯誤頁
     * @param int $uid
     * @param string $sign
     */
    public function sign(int $uid, string $sign = '') {
        $this->load->model('User_model');
        if (empty($sign) || !$this->User_model->checkUserSign($uid, $sign)) {
            show_404();
            die;
        }

        $this->User_model->updateProfile($uid, array(
            'email_valid' => 1
        ));

        $this->mOutput['sign'] = true;
        $this->mOutput['uid'] = $uid;
        $this->loadView('other/login');
    }

}
