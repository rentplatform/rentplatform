<?php

class Brand extends Ext_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Common_model');
    }

    public function index() {
        $this->mOutput['title'] = '品牌合作';
        $data = $this->Common_model->getStaticData(STATICPAGE_TYPE['BRAND_COOPERATION']);
        $this->mOutput['content'] = $data ? $data['content'] : '';
        $this->loadView('page/static_page');
    }

}