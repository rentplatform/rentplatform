<?php

class Aboutus extends Ext_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Common_model');
    }

    public function index() {
        $this->mOutput['title'] = '關於我們';
        $data = $this->Common_model->getStaticData(STATICPAGE_TYPE['ABOUT_US']);
        $this->mOutput['content'] = $data ? $data['content'] : '';
        $this->loadView('page/static_page');
    }

}