<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 首頁控制器
 * @property Device_model $device
 *
 */
class Main extends Ext_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Device_model', 'device');
        $this->load->model('Page_model', 'page');
    }

    /**
     * 首頁
     * @param string $previewCode 預覽輪播圖用
     * @param string $id 輪播ID
     */
    public function index($previewCode = '', $id = '') {
        $this->load->model('Common_model');
        if ($previewCode === 'preview2234') {
            $this->mOutput['carousel'] = $this->Common_model->getPreviewCarousel($id);
        } else {
            $this->mOutput['carousel'] = $this->Common_model->getCarouselList();
        }
        $this->mOutput['newest'] = $this->device->getNewestDevice();
        $this->mOutput['hot'] = $this->device->gethotDevice();
        $this->mOutput['article'] = $this->page->getNewestArticle(1, 3);
        $this->mOutput['masterRecommend'] = $this->Common_model->getStaticData(STATICPAGE_TYPE['MASTER_RECOMMEND'], true);
        $this->mOutput['masterGood'] = $this->Common_model->getStaticData(STATICPAGE_TYPE['MASTER_GOOD'], true);
        $this->mOutput['firstLogin'] = $this->session->userdata('first_login') ? : false;
        $this->session->unset_userdata('first_login');

        $this->loadView('index');
    }

}
