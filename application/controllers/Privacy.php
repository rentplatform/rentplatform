<?php

/**
 * @property Pdf pdf
 */
class Privacy extends Ext_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Common_model');
    }

    /**
     * 隱私權政策
     */
    public function index() {
        $this->mOutput['title'] = '隱私權政策';
        $data = $this->Common_model->getStaticData(STATICPAGE_TYPE['PRIVACY']);
        $this->mOutput['content'] = $data ? $data['content'] : '';
        $this->loadView('page/static_page');
    }

    /**
     * 租借合約
     */
    public function order() {
        $this->checkLogin();
        $encrypt = $this->input->get('e', false);
        $print = $this->input->get('print') ? true : false;
        if (empty($encrypt)) {
            redirect('/');
            return false;
        }
        $this->load->library('encryption');
        $this->load->model('Rent_model', 'rent');
        $urid = $this->encryption->decrypt($encrypt);
        if (!$urid || !is_int((int)$urid)) {
            redirect('/');
            return false;
        }
        $data = $this->rent->getRentRequestOrder($urid);

        $uid = $this->mOutput['profile']['uid'];
        if (empty($data) || ($uid != $data['renter_uid'] && $uid != $data['device_owner'])) {
            redirect('/');
            return false;
        }
        $data['notice'] = str_replace(PHP_EOL, '<br>', $data['notice']);
        if (in_array($data['status'], [
            RENT_RECORD_STATUS['WAIT'],
            RENT_RECORD_STATUS['MISS'],
            RENT_RECORD_STATUS['REJECT'],
            RENT_RECORD_STATUS['RENT_CANCEL'],
        ])) {
            $hide_column = [
                'name', 'device_owner_name', 'renter_idnumber', 'renter_address', 'renter_phone', 'username', 'device_owner_name',
                'device_owner_idnumber', 'device_owner_address', 'device_owner_phone', 'device_owner_username'
            ];
            foreach ($hide_column as $k => $key) {
                $data[$key] = preg_replace('/^(.{3})(.*)/', '$1********', $data[$key]);
            }
        }
        $this->mOutput['data'] = $data;

        if (!$print) {
            $this->mOutput['encrypt'] = $encrypt;
            $this->loadView('page/privacy_order');
        } else {
            $this->load->library('Pdf');
            $html = $this->load->view('page/privacy_order_print_view.php', $this->mOutput, true);
            ob_end_clean();
            $this->pdf->AddPage();
            $this->pdf->writeHTML($html, true, false, true, false, '');

            $filename = sprintf('%s_Deal_%s.pdf', $urid, date('Ymd', strtotime($data['end_time'])));
            if (file_exists(FILE_PATH . $filename)) {
                $this->pdf->Output($filename, 'D');
            } else {
                $this->pdf->Output(FILE_PATH . $filename, 'FD');
            }
        }
    }

}