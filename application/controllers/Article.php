<?php

class Article extends Ext_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Page_model', 'page');
    }

    public function list(int $page = 1) {
        $type = $this->input->get('type') ? : 1;
        $type = urldecode($type);
        $limit = 10;
        $data = $this->page->getArticleList($type, $page, $limit);
        $this->mOutput['atypeid'] = $type;
        $this->mOutput['type'] = $this->page->getArticleTypeList();
        $this->mOutput['list'] = $data['list'];
        $this->loadPagenation("/article/list", $data['count'], $limit);

        $this->loadView('page/article');
    }

    /**
     * 專欄文章內容頁
     * @param int $atid
     * @param string $previewPassCode 後台需要預覽時才會有這個參數，固定的簡單驗證，可忽略上架中文章才可看的條件
     */
    public function content(int $atid = 0, $previewPassCode = '') {
        if (!$atid) {
            show_404();
            return;
        }
        $this->mOutput['data'] = $this->page->getArticle($atid, $previewPassCode === 'preview1234');
        if (empty($this->mOutput['data'])) {
            show_404();
            return;
        }
        $this->mOutput['type'] = $this->page->getArticleTypeList();

        $this->loadView('page/article_content');
    }

}