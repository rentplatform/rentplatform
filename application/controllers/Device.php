<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property Device_model $device
 * @property Common_model $common
 * @property Master_model $master
 */
class Device extends Ext_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Common_model', 'common');
        $this->load->model('Device_model', 'device');
        $this->load->model('Master_model', 'master');
    }

    /**
     * 家電搜尋列表頁
     * @param int $page
     */
    public function search($page = 1) {
        $limit = 10;

        $params = new \params\Device_search_params();
        $search_data = $this->device->getDeviceList($params, $page, $limit);

        $this->mOutput['params'] = $params;
        $this->mOutput['brandList'] = $this->common->getBrandList();
        $this->mOutput['deviceTypeList'] = $this->common->getDeviceTypeList();
        $this->mOutput['countryList'] = $this->common->getCountryList();
        if ($params->getCtid()) {
            $city = $this->common->getCityList($params->getCid());
        }
        $this->mOutput['cityList'] = $city ?? array();

        $this->mOutput['list'] = $search_data['list'];
        $this->mOutput['newest'] = $this->device->getNewestDevice();
        $this->mOutput['hot'] = $this->device->gethotDevice();
        $this->mOutput['count'] = $search_data['count'];

        $this->loadPagenation('/device/search', $search_data['count'], $limit);

        $this->loadView('device/device_list');
    }

    /**
     * 家電詳細內容頁
     * @param int $deviceId
     */
    public function detail(int $deviceId = 0) {
        if (!$deviceId) {
            redirect('/');
            return;
        }
        $data = $this->device->getDevice($deviceId);
        if (empty($data)) {
            redirect('/');
            return;
        }
        if ($data['accessory']) {
            $data['accessory'] = json_decode($data['accessory'], true);
        }
        $this->mOutput['data'] = $data;
        $this->mOutput['location'] = $this->master->getLocationList($data['uid']);
        $this->loadView('device/device_detail');
    }

    /**
     * 家電租借申請頁
     * @param int $deviceId
     */
    public function rent(int $deviceId = 0) {
        if (!$deviceId || !$this->checkLogin(true)) {
            redirect('/');
            return;
        }
        if (!$this->mOutput['profile']['phone_valid']) {
            $this->session->set_userdata('phone_success_redirect', "/device/rent/{$deviceId}");
            redirect('/user/validPhone');
            return;
        }
        $this->load->model('Rent_model');
        $data = $this->device->getDevice($deviceId);
        if (empty($data)) {
            redirect('/');
            return;
        }

        if ($data['accessory']) {
            $data['accessory'] = json_decode($data['accessory'], true);
        }
        $data['notice'] = str_replace(PHP_EOL, '<br>', $data['notice']);

        $startdate = [];
        $rentingdate = [];
        $n = 1;
        while ($n <= 30) {
            $time = strtotime("+{$n} days");
            $day = date('N', $time);

            if (in_array($day, [2,6])) {
                $date = date('Y-m-d', $time);
                if (!$this->Rent_model->checkRentDateValid($deviceId, $date, $date)) {
                    $startdate[$date] = sprintf('%s (%s)', $date, (int)$day === 2 ? '週二' : '週六');
                } else {
                    $rentingdate[] = $date;
                }
            }

            $n++;
        }

        $setting = new \params\Websetting_params();

        $this->mOutput['week'] = $setting->getWeekendRent();
        $this->mOutput['normal'] = $setting->getNormalRent();
        $this->mOutput['startDateList'] = $startdate;
        $this->mOutput['rentingList'] = $rentingdate;
        $this->mOutput['data'] = $data;
        $this->mOutput['location'] = $this->master->getLocationList($data['uid']);
        $this->loadView('device/rent');
    }

}