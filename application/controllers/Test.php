<?php

/**
 * 測試用控制器
 */
class Test extends Ext_Controller {

    public function view() {
        echo '<pre>';
        if ($handle = opendir(FILE_PATH)) {
            $today = date('Ymd');
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    list($filename, $ext) = explode('.', $entry);
                    list($orderId, $str, $date) = explode('_', $filename);
//                    if (($today - $date) > 7) {
                        unlink(FILE_PATH . $entry);
//                    }
                }
            }
            closedir($handle);
        }
    }

    public function ajax() {
        print_r($_POST);
    }

    public function sms() {
        $this->load->library('sms_sender');
        $this->sms_sender->sendMsg('test send msg lalala', '0980910641');
    }

    public function smsc() {
        $this->load->library('sms_sender');
        $res = $this->sms_sender->getCredit();
        echo '<pre>';
        var_dump($res);
    }

    public function insert() {
        $this->db->insert('sms_log', array(
            'bid' => '220478cc-8506-49b2-93b7-2505f651c12e',
            'phone_number' => '0980910641',
            'status' => '100',
            'cost' => 1,
            'unsend' => 0,
            'send_date' => date('Y/m/d H:i:s')
        ));
    }

    public function phpmailer() {
        $this->load->library('Mail_sender');
        $this->mail_sender->AddReceiver('erickkpk@gmail.com');
        $this->mail_sender->SetSubject('電電租會員註冊驗證信');
        $this->mail_sender->SetMessage('test4567');
        $this->mail_sender->send();
    }

    public function template() {
        $this->load->library('Msg_template');
        $this->load->model('User_model');

        $sign = md5(1 . 'eric' . '_mail_valid_sign');
        $this->User_model->insertUserMailValidSign(1, $sign);

        $mail_content = $this->msg_template->register('eric', $sign);

        $this->load->library('Mail_sender');
        $this->mail_sender->AddReceiver('kkkpk87@hotmail.com');
        $this->mail_sender->SetSubject('電電租會員註冊驗證信test');
        $this->mail_sender->SetMessage($mail_content);
        $this->mail_sender->send();
    }

    public function validtest() {
        $this->load->library('Satisfy');
        var_dump(call_user_func(array($this->satisfy, 'email'), 'abcabc.com'));
    }

    public function trad() {
        require __DIR__ . '/../libraries/api/api_base.php';
        $this->load->library('api/Trans_api', 'api');
        $this->trans_api->valid();
    }

    function autoload() {
        $p = new \params\Trad_params();
    }

    function php7() {
        $a = '2017-11-25';
        $b = '2017-11-28';
        var_dump(count_date($a, $b));
    }

    function revalid() {
        $username = 'kkkpk87@hotmail.com';
        $uid = '6';
        $sign = md5($uid . $username . '_mail_valid_sign');

        $this->load->library('Msg_template');
        $mail_content = $this->msg_template->register($uid, 'revalid name', $sign);

        $this->load->library('Mail_sender');
        $this->mail_sender->AddReceiver($username);
        $this->mail_sender->SetSubject(REGISTER_MAIL_TITLE);
        $this->mail_sender->SetMessage($mail_content);
        $this->mail_sender->send();

        $this->session->set_userdata(array(
            'valid_mail_data' => array(
                'uid' => $uid,
                'content' => $mail_content,
                'username' => $username
            )
        ));

        redirect('/register/success');
    }

    function login($uid = 1) {
        $this->load->model('User_model', 'userModel');
        $Profile = $this->userModel->getProfile($uid);
        $this->userModel->updateLastLoginTime($uid);
        $this->session->set_userdata(array(
            'isLogged' => TRUE,
            'uid' => $uid,
            'profile' => $Profile
        ));
    }

    function checkmac() {
        $data = '{"PaymentType":"Credit_CreditCard","RtnMsg":"\u4ed8\u6b3e\u6210\u529f","SimulatePaid":"1","CustomField2":"","PaymentDate":"2017\/11\/11 17:34:52","TradeNo":"1711111704428201","TradeAmt":"30","MerchantID":"3038253","StoreID":"","CustomField3":"","MerchantTradeNo":"RPT00000000000000002","CustomField4":"","CheckMacValue":"6B25E929D32689427CCC59BCA0AB5D8FDE0B6C97FCF0865357E92AC7C92CCE6D","TradeDate":"2017\/11\/11 17:04:42","CustomField1":"","RtnCode":"1","PaymentTypeChargeFee":"0"}';
        $data = json_decode($data, true);

        $macvalue = $data['CheckMacValue'];
        unset($data['CheckMacValue']);
        ksort($data);

        $datastr = '';
        foreach ($data as $k => $v) {
            $datastr .= $k . '=' . $v . '&';
        }
        $datastr = substr($datastr, 0, -1);
        $datastr = sprintf('HashKey=%s&%s&HashIV=%s', 'MOBddNi7BPP1m0hx', $datastr, '3Htu8XAIONrCB7Y0');
        $datastr = urlencode($datastr);
        $datastr = strtolower($datastr);
        $datastr = str_replace('%2d', '-', $datastr);
        $datastr = str_replace('%5f', '_', $datastr);
        $datastr = str_replace('%2e', '.', $datastr);
        $datastr = str_replace('%21', '!', $datastr);
        $datastr = str_replace('%2a', '*', $datastr);
        $datastr = str_replace('%28', '(', $datastr);
        $datastr = str_replace('%29', ')', $datastr);
        $datastr = str_replace('%20', '+', $datastr);

        $encrypt = hash('sha256', $datastr);
        $encrypt = strtoupper($encrypt);

        echo '<pre>';
        var_dump($encrypt);

        var_dump($macvalue);

    }

    public function rentSuccess() {
        $this->load->model('Device_model');
        $this->load->model('Rent_model');
        $this->load->model('Notification_model');
        $urid = 3;
        $deviceid = 3;
        //租公付款完成後更改狀態為已接單
        $this->Rent_model->updateRecord($urid, [
            'status' => 6
        ]);

        //更新電器租借次數
        $this->Device_model->updateDeviceRentTime($deviceid);


        //寄信通知租客
        $this->load->library('Msg_template');
        $this->load->library('Mail_sender');
        $order = $this->Rent_model->getRentRequestOrder($urid);
        $mail_content = $this->msg_template->rentSuccess($order);
        //新增通知
        $this->Notification_model->insert($order['renter_uid'], sprintf('%s 租公同意租借 %s 電器，交易單號： %s', $order['device_owner_name'], $order['device_name'], $order['ur_id']));

        //寄信
        $this->mail_sender->AddReceiver($order['username']);
        $this->mail_sender->SetSubject(RENT_DETAIL_MAIL_TITLE);
        $this->mail_sender->SetMessage($mail_content);
        $this->mail_sender->send();

        $this->mOutput['isSuccess'] = 1;
        $this->mOutput['urid'] = $urid;
        $this->loadView('master/rent_request_success');

    }

    public function mailContent() {
        $this->load->model('Rent_model');
        $this->load->library('Msg_template');
        $order = $this->Rent_model->getRentRequestOrder(2);
        $template = $this->msg_template->noticeEndRentingForMaster($order);
        echo $template;

    }

}
