<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends Ext_Controller {

    public function __construct() {
        parent::__construct();
    }
    
    /**
     * 註冊頁面
     */
    public function index() {
        $this->loadView('other/register');
    }

    /**
     * 註冊成功，提示收取信箱驗證信頁面
     */
    public function success() {
        $valid_mail_data = $this->session->userdata('valid_mail_data');
        if (empty($valid_mail_data)) {
            show_404();
            die;
        }
        $this->loadView('other/register_success');
    }

}
