<?php
    load_modal('phone_valid');
?>
<!--main content-->
<div class="page-heading">
    <h2>我要租家電</h2>
</div>
<div class="container">
    <div class="row">
        <!-- 側欄-->
        <div class="home-app-aside-block col-xs-12 col-sm-12 col-md-3">
            <div class="row">
                <div class="col-sm-12">
                    <div class="green-border-box">
                        <div class="latest-product-slider">
                            <?php foreach($newest as $row): ?>
                                <div class="product-slide">
                                    <a class="product-name" href="/device/detail/<?= $row['device_id'] ?>" title="<?= $row['name'] ?>">
                                        <span class="slider_slide__img-container" style="background-image: url('/upload/<?= $row['filename'] ?>')"></span>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="latest-product-slider__controller">
                            <h3>最新上架</h3>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="green-border-box">
                        <div class="hot-product-slider">
                            <?php foreach($hot as $row): ?>
                                <div class="product-slide">
                                    <a class="product-name" href="/device/detail/<?= $row['device_id'] ?>" title="<?= $row['name'] ?>">
                                       <span class="slider_slide__img-container" style="background-image: url('/upload/<?= $row['filename'] ?>')"></span>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="hot-product-slider__controller">
                            <h3>最熱門</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- 主要內容-->
        <div class="home-app-main-block col-xs-12 col-sm-12 col-md-9">
			<?php if ($count > 0): ?>
				<div class="block-title-md">有<span class="red"><?= $count ?></span><span>件家電等你來租囉！</span></div>
			<?php endif; ?>
            <!-- 搜尋家電-->
            <div class="home-app-search green-border-box">
                <button class="search-button__mobile btn" type="button" data-toggle="collapse" data-target="#collapse__search-filter" aria-expanded="false" aria-controls="collapse__search-filter">搜尋家電</button>
                <div class="search-filter collapse" id="collapse__search-filter">
                    <form class="search-filter-form" method="get" action="/device/search">
                        <div class="row">
                            <div class="hp-form-group col-xs-12 col-sm-6">
                                <select class="select_category home-app-search__select form-content-green-border" name="bid">
                                    <option value="">品類</option>
                                    <?php
                                    foreach ($brandList as $row) {
                                        $seleted = $params->getBid() && $params->getBid() == $row['bid'] ? 'selected' : '';
                                        echo sprintf('<option value="%s" %s>%s</option>', $row['bid'], $seleted, $row['name']);
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="hp-form-group col-xs-12 col-sm-6">
                                <select class="select_brand home-app-search__select form-content-green-border" name="dtid">
                                    <option value="">電器類型</option>
                                    <?php
                                    foreach ($deviceTypeList as $row) {
                                        $seleted = $params->getDtid() && $params->getDtid() == $row['dt_id'] ? 'selected' : '';
                                        echo sprintf('<option value="%s" %s>%s</option>', $row['dt_id'], $seleted, $row['name']);
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="hp-form-group col-xs-12 col-sm-3">
                                <select class="select_city home-app-search__select selCountry form-content-green-border" name="cid" data-target-city-sel=".selCity">
                                    <option value="">縣市</option>
                                    <?php
                                    foreach ($countryList as $row) {
                                        $seleted = $params->getCid() && $params->getCid() == $row['cid'] ? 'selected' : '';
                                        echo sprintf('<option value="%s" %s>%s</option>', $row['cid'], $seleted, $row['name']);
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="hp-form-group col-xs-12 col-sm-3">
                                <select class="select_town home-app-search__select form-content-green-border selCity" name="ctid">
                                    <option value="">鄉鎮市區</option>
                                    <?php
                                    foreach ($cityList as $row) {
                                        $seleted = $params->getCtid() && $params->getCtid() == $row['ct_id'] ? 'selected' : '';
                                        echo sprintf('<option value="%s" %s>%s</option>', $row['ct_id'], $seleted, $row['name']);
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="hp-form-group col-xs-12 col-sm-6">
                                <input class="form-content-green-border" type="text" name="address" value="<?= $params->getAddress() ?>" placeholder="路名">
                            </div>
                            <div class="hp-form-group col-xs-12 col-sm-12 width-100">
                                <input class="form-content-green-border" type="text" name="keyword" value="<?= $params->getKeyword() ?>" placeholder="請輸入關鍵字">
                            </div>
                        </div>
                        <div class="row">
                            <div class="hp-form-group col-sm-12">
                                <div class="filter-checkbox display-f col-sm-offset-3">
                                    <input type="checkbox" name="allow" value="1" id="current-available" <?= $params->getAllow() ? 'checked' : '' ?>>
                                    <label for="current-available">僅限現在可租</label>
                                </div>
                                <button class="home-app__search-button green-btn-md btn" type="submit">搜尋</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- 家電列表-->
            <div class="home-app-list">
                <?php foreach ($list as $row): ?>
                    <div class="home-app-list__item row green-border-box">
                        <div class="home-app-list__item__pic col-sm-3">
                            <div class="img-container" style="background-image: url('/upload/<?= $row['filename'] ?>')"></div>
                        </div>
                        <div class="home-app-list__item__info col-sm-6">
                            <a class="product-name" href="/device/detail/<?= $row['device_id'] ?>"><?= $row['name'] ?></a>
                            <div class="display-f">
                                <div class="product-category"><?= $row['device_type_name'] ?></div>
                                <!-- 可出租 data-toggle="unavailable" -->
                                <!-- 暫停出租 data-toggle="unavailable"-->
                                <!-- 出租中 data-toggle="renting"-->
                                <div class="product-status" data-toggle="<?= $row['status'] > 0 ? ($row['status'] > 1 ? 'available' : 'renting' ) : 'unavailable' ?>">
                                    <?= $row['status'] > 0 ? ($row['status'] > 1 ? '出租中' : '可出租' ) : '暫停出租' ?>
                                </div>
                            </div>
                            <div class="display-f">
                                <a class="product-lessor" href="/master/card/<?= $row['uid'] ?>">
                                    <div class="product-lessor__icon icon-sprite"></div>
                                    <div class="product-lessor__txt"><?= $row['nickname'] ?></div>
                                </a>
                                <div class="product-evaluation">
                                    <div class="product-evaluation--good">
                                        <div class="icon_positive icon-sprite"> </div>
                                        <div class="product-evaluation__num"><?= $row['good'] ?></div>
                                    </div>
                                    <div class="product-evaluation--bad">
                                        <div class="icon_negative icon-sprite"></div>
                                        <div class="product-evaluation__num"><?= $row['bad'] ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="home-app-list__item__action col-xs-12 col-sm-3">
                            <a class="green-btn-md btn" href="/device/detail/<?= $row['device_id'] ?>">瞭解更多</a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <!--頁數-->
        <div class="center-all">
            <?= $pageHtml ?>
        </div>
    </div>
</div>
<script>
    $(function(){
        $(window).resize(function(){
            var sw = $(window).width();
            if(sw>767){
                $(".search-filter").css("height","auto");
            }
        });


        $(".latest-product-slider").slick({
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 2000
        });
        $(".hot-product-slider").slick({
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 2000
        });
    })
</script>