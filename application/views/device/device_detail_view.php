<?php
    load_modal('phone_valid');
?>
<!--main content-->
<div class="page-heading">
    <h2>我要租家電</h2>
</div>
<div class="container">
    <div class="row">
        <div class="action-bar col-sm-12 hidden-xs"><a class="back green-btn-sm btn" href="javascript: history.back();">返回</a></div>
    </div>
    <div class="row">
        <!-- 產品圖-->
        <div class="home-app-detail__pic col-xs-12 col-sm-5 col-md-5">
            <div class="row">
                <div class="col-sm-12">
                    <div class="home-app-detail__slider-for">
                        <?php foreach ($data['pic'] as $row): ?>
                            <div class="product-slide"><img src="/upload/<?= $row['filename'] ?>" alt=""></div>
                        <?php endforeach; ?>
                    </div>
                    <div class="home-app-detail__slider-nav">
                        <?php foreach ($data['pic'] as $row): ?>
                            <div class="product-slide"><img src="/upload/<?= $row['filename'] ?>" alt=""></div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- 產品介紹-->
        <div class="home-app-detail__main col-xs-12 col-sm-7 col-md-6">
            <div class="row">
                <div class="home-app-detail__info col-xs-12 col-sm-12">
                    <div class="product-name"><?= $data['name'] ?></div>
                    <div class="display-f">
                        <div class="product-category"><?= $data['device_type_name'] ?>
                            <!-- 不可出租 data-toggle="unavailable"-->
                        </div>
                        <div class="product-status" data-toggle="<?= $data['status'] > 0 ? ($data['status'] > 1 ? 'available' : 'renting' ) : 'unavailable' ?>">
                            <?= $data['status'] > 0 ? ($data['status'] > 1 ? '出租中' : '可出租' ) : '暫停出租' ?>
                        </div>
                    </div>
                    <div class="display-f"><a class="product-lessor" href="/master/card/<?= $data['uid'] ?>">
                            <div class="product-lessor__icon icon-sprite"></div>
                            <div class="product-lessor__txt"><?= $data['nickname'] ?></div></a>
                        <div class="product-evaluation">
                            <div class="product-evaluation--good">
                                <div class="icon_positive icon-sprite"> </div>
                                <div class="product-evaluation__num"><?= $data['good'] ?></div>
                            </div>
                            <div class="product-evaluation--bad">
                                <div class="icon_negative icon-sprite"></div>
                                <div class="product-evaluation__num"><?= $data['bad'] ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="product-rent-price display-f">
                        <span class="product-rent-price__heading">租金: </span>
                        <span class="product-rent-price__content">
                            平日 <?= $setting->getNormalRent() ?> / 週末 <?= $setting->getWeekendRent() ?>
                        </span>
                    </div>
                    <div class="product-rent-deposit display-f">
                        <span class="product-rent-deposit__heading">押金: </span>
                        <span class="product-rent-deposit__content"><?= $data['deposit'] ?>元</span>
                    </div>
                    <div class="product-descript">
                        <h3 class="product-descript__heading">產品簡介:</h3>
                        <p class="product-descript__content"><?= $data['description'] ?></p>
                    </div>
                    <div class="product-deal-location">
                        <h3 class="product-deal-location__heading">可面交地點:</h3>
                        <ul class="product-deal-location__list">
                            <?php foreach ($location as $row): ?>
                                <li class="product-deal-location__list__item">
                                    <span class="location-name"><?= $row['placename'] ?></span>
                                    <span class="location-address"><?= $row['countryName'] . $row['cityName'] . $row['address'] ?></span>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="product-accessories">
                        <h3 class="product-accessories__heading">配件:</h3>
                        <ol class="product-accessories__list">
                            <?php foreach ($data['accessory'] as $k => $v): ?>
                                <li class="product-accessories__list__item"><?= $v ?></li>
                            <?php endforeach; ?>
                        </ol>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12"><a class="rent-button green-btn-md btn <?= $logged ? '' : 'loginRedirect' ?>" <?= $logged ? ($profile['phone_valid'] ? "href=\"/device/rent/{$data['device_id']}\"" : 'data-toggle="modal" data-target="#mobileAuth"') : "data-uri=\"/device/rent/{$data['device_id']}\"" ?>>租借</a></div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){

        $('.home-app-detail__slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.home-app-detail__slider-nav',
            mobileFirst: true
        });
        $('.home-app-detail__slider-nav').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.home-app-detail__slider-for',
            dots: false,
            centerMode: false,
            focusOnSelect: true,
        });

    });
</script>