<?php
load_modal('rent_request');
?>
<!--main content-->
<div class="page-heading">
    <h2>我要租家電</h2>
</div>
<div class="container width-900">
    <form id="rentForm">
        <div class="rent-home-app__my-app-info">
            <div class="home-app-list__item row">
                <div class="home-app-list__item__pic col-xs-4 col-sm-3">
                    <div class="img-container" style="background-image: url('/upload/<?= !empty($data['pic']) ? $data['pic'][0]['filename'] : '' ?>')"></div>
                </div>
                <div class="home-app-list__item__info col-xs-8 col-sm-6">
                    <div class="product-name"><?= $data['name'] ?></div>
                    <div class="display-f">
                        <div class="product-category"><?= $data['device_type_name'] ?></div>
                        <!-- 可出租 data-toggle="unavailable" -->
                        <!-- 暫停出租 data-toggle="unavailable"-->
                        <!-- 出租中 data-toggle="renting"-->
                        <div class="product-status"
                             data-toggle="<?= $data['status'] > 0 ? ($data['status'] > 1 ? 'available' : 'renting') : 'unavailable' ?>">
                            <?= $data['status'] > 0 ? ($data['status'] > 1 ? '出租中' : '可出租') : '暫停出租' ?>
                        </div>
                    </div>
                    <div class="display-f">
                        <a class="product-lessor display-f">
                            <div class="product-lessor__icon icon-sprite"></div>
                            <div class="product-lessor__txt"><?= $data['nickname'] ?></div>
                        </a>
                        <div class="product-evaluation">
                            <div class="product-evaluation--good">
                                <div class="icon_positive icon-sprite"></div>
                                <div class="product-evaluation__num"><?= $data['good'] ?></div>
                            </div>
                            <div class="product-evaluation--bad">
                                <div class="icon_negative icon-sprite"></div>
                                <div class="product-evaluation__num"><?= $data['bad'] ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- 選擇租借時段 -->
        <div class="block-title-md black">請選擇租借時段</div>
        <div class="select-rent-time green-border-box">
            <div class="row">
                <!-- 租借日期 -->
                <div class="col-xs-12 col-sm-6">
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <label class="control-label">租借日期</label>
                            </div>
                            <div class="col-xs-12 col-sm-8">
                                <select id="startDateSel" class="select-rent-date home-app-search__select form-content-green-border"
                                        name="start">
                                    <option value="">請選擇時段</option>
                                    <?php foreach ($startDateList as $date => $datestr): ?>
                                        <option value="<?= $date ?>"><?= $datestr ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- 歸還日期 -->
                <div class="col-xs-12 col-sm-6">
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <label class="control-label">歸還日期</label>
                            </div>
                            <div class="col-xs-12 col-sm-8">
                                <select id="endDateSel" class="select-return-date home-app-search__select form-content-green-border"
                                        name="end">
                                    <option value="">請選擇時段</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end .row -->
        </div>

        <!--選擇面交地點-->
        <div class="block-title-md black">選擇可面交地點</div>
        <div class="select-deal-location my-deal-location__list green-border-box">
            <ul class="my-deal-location__list">
                <?php foreach ($location as $k => $row): ?>
                    <li class="my-deal-location__list-item row <?= $k === 0 ? 'selected' : '' ?>">
                        <div class="col-xs-12 col-sm-12">
                            <span class="location-name"><?= $row['placename'] ?></span>
                            <span class="location-address"><?= $row['countryName'] . $row['cityName'] . $row['address'] ?></span>
                        </div>
                        <input class="hide" type="radio" name="location" value="<?= $row['tp_id'] ?>" <?= $k === 0 ? 'checked' : '' ?>>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <!--摘要-->
        <div class="rent-result green-border-box">
            <div class="block-title-md">摘要</div>
            <div class="rent-result__info-outer">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 display-f">
                        <div class="rent-result__rent-icon label_rent_term icon-sprite"></div>
                        <div class="rent-result__rent-info">
                            <div class="rent-result__rent-date rent-result__item">
                                <div class="rent-result__rent-date__title rent-result__item__title">借期:</div>
                                <div id="dateRange"
                                     class="rent-result__rent-date__content rent-result__item__content"></div>
                            </div>
                            <div class="rent-result__deal-location rent-result__item">
                                <div class="rent-result__deal-location__title rent-result__item__title">希望面交地點:</div>
                                <div id="trasactionPlace"
                                     class="rent-result__deal-location__content rent-result__item__content"><?= $location[0]['placename'] ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 display-f">
                        <div class="rent-result__rent-icon label_rent_money icon-sprite"></div>
                        <div class="rent-result__rent-info">
                            <div class="rent-result__rent-price rent-result__item">
                                <div class="rent-result__rent-price__title rent-result__item__title">租金:</div>
                                <div id="rentPrice"
                                     class="rent-result__rent-price__content rent-result__item__content"></div>
                            </div>
                            <div class="rent-result__rent-deposit rent-result__item">
                                <div class="rent-result__rent-deposit__title rent-result__item__title">押金:</div>
                                <div class="rent-result__rent-deposit__content rent-result__item__content"><?= $data['deposit'] ?>
                                    元 (歸還時退還)
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hp-button-block center-all-pc mutiple-btn">
            <a class="green-btn-md btn hidden-xs" href="javascript: history.back();">取消</a>
            <input type="hidden" name="device_id" value="<?= $data['device_id'] ?>">
            <button class="green-btn-md btn" type="button">送出預約</button>
        </div>
		<div class="note-block tac">點選「送出預約」，即表示同意以下租賃契約。</div>
		<div class="rent-info green-border-box" id="rent-contract">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<h3 class="title">電電租 家電租賃契約</h3>
					<h4 class="sub-title">立契約書人：</h4>
					<ul class="list">
						<li>承租人：<span class="red"><?= preg_replace('/^(.{3})(.*)/', '$1********', $profile['name']) ?></span><span class="black">(以下簡稱甲方)</span></li>
						<li>出租人：<span class="red"><?= preg_replace('/^(.{3})(.*)/', '$1********', $data['realname']) ?></span><span class="black">(以下簡稱乙方)</span></li>
					</ul>
					<h4 class="sub-title">承租品明細：</h4>
					<ul class="list">
						<li>出租品項：<span class="red"><?= $data['name'] ?></span></li>
						<li>承租起迄日：
							<span class="black">自</span>
							<span class="red"> 民國
								<span class="rentStart">_</span>
							</span>
							<span class="black">至</span>
							<span class="red"> 民國
								<span class="rentEnd">_</span>
							</span>
							<span class="black">， 共</span>
							<span class="red countDate">0</span>
							<span class="black">天</span>
						</li>
						<li>租金：
							<span class="red">新臺幣
								<span id="renterPrice2">0</span>
							</span>
							<span class="black">元</span>
						</li>
						<li>押金：
							<span class="red">新臺幣
								<span><?= $data['deposit'] ?></span>
							</span>
							<span class="black">元</span>
						</li>
						<li>面交時租公收取租金+押金， 共
							<span class="red">新臺幣
								<span id="totalPrice"></span>
							</span>
							<span class="black">元</span>
						</li>
						<li>歸還時確認無誤後，現場退還押金</li>
					</ul>
					<h4 class="sub-title">茲因甲乙雙方進行家電租賃乙事，雙方特立本契約書，其協議範圍為本契約書指定品項之承租事宜，雙方同意按下列之約定進行各項事宜：</h4>
					<h4 class="sub-title">第一條 租賃協議：</h4>
					<ul class="list">一、產品租金
						<li>平日（週二～週五）新臺幣<?= $normal ?>元</li>
						<li>假日（週六～次週一）新臺幣<?= $week ?>元</li>
					</ul>
					<ul class="list">
						二、甲方如需延長承租時間，應提前確認產品後續無人承租，方可續租，並需提前於歸還日前12小時提出延長承租之要求，至多以延長兩次為限。(若有不可抗力之因素致無法如期歸還者除外)
					</ul>
					<ul class="list">三、本協議書乙式貳份，甲乙雙方各執正本乙份。</ul>
					<ul class="list">四、甲方於租賃前,應主動告知乙方所清潔區域是否有人感染法定傳染病,以便乙方後續清潔事宜。</ul>
					<ul class="list">五、如有本協議未盡之事宜，得經由雙方共同合議解決。</ul>
					<h4 class="sub-title">第二條 損壞賠償：</h4>
					<ul class="list">甲方承租前，需確實檢查品項功能正常，乙方亦須主動告知產品現狀，如非正常使用以致損壞，甲方需依實際維修費用照價賠償於乙方。</ul>
					<h4 class="sub-title">第三條 產品點交表：(請務必確認產品功能正常) </h4>
					<ul class="list">
						<li>
							<div class="checkedlist hp-form-group col-sm-12">
								<div class="filter-checkbox display-f">
									<input type="checkbox" name="" checked disabled>
									<label for="current-available">
										<span class="red">（<?= $data['name'] ?>）</span>
										<span class="black">(本體+配件 共</span>
										<span class="red"><?= count($data['accessory']) + 1 ?></span>
										<span class="blaxck">項)</span>
									</label>
								</div>
							</div>
						</li>
						<li>※若產品含有電子晶片，切勿碰水，如造成損壞依賠償之規定進行。</li>
                        <?php if (!empty($data['notice'])): ?>
							<li>※其他注意事項：
								<ul class="black-border">
									<li><?= $data['notice'] ?></li>
								</ul>
							</li>
                        <?php endif; ?>
					</ul>
					<h4 class="sub-title">第四條 違約處理：</h4>
					<ul class="list">甲方租借如逾期，若未提前12小時提出延長承租之要求，則需支付產品租金兩倍罰金於乙方。(若有不可抗力之因素致無法如期歸還者除外)</ul>
					<h4 class="sub-title">第五條 歸還流程:</h4>
					<ul class="list">
						<span class="black">甲方</span>
						<span class="red"><?=  preg_replace('/^(.{3})(.*)/', '$1********', $profile['name']) ?></span>
						<span class="black">於民國 </span>
						<span class="red rentEnd">_</span>
						<span class="black">歸還產品後，乙方確認無誤即完成歸還流程。</span>
					</ul>
					<h4 class="sub-title">第六條</h4>
					<ul class="list"><span class="black">如因本契約所發生之一切爭議，如有訴訟之必要，雙方合議由臺灣臺北地方法院為第一審管轄法院。</span></ul>
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<div class="profile-info-bk">
								<h4 class="sub-title">甲方</h4>
								<ul class="list">
									<li>姓名：<span class="red"><?= preg_replace('/^(.{3})(.*)/', '$1********', $profile['name']) ?></span></li>
									<li>身份證字號：<span class="red"><?= preg_replace('/^(.{3})(.*)/', '$1********', $profile['id_number']) ?> </span></li>
									<li>地址：<span class="red"><?= preg_replace('/^(.{3})(.*)/', '$1********', $profile['address']) ?> </span></li>
									<li>行動電話：<span class="red"><?= preg_replace('/^(.{3})(.*)/', '$1********', $profile['phone_number']) ?> </span></li>
									<li>E-MAIL：<span class="red"><?= preg_replace('/^(.{3})(.*)/', '$1********', $profile['username']) ?> </span></li>
								</ul>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="profile-info-bk">
								<h4 class="sub-title">乙方</h4>
								<ul class="list">
									<li>姓名：<span class="red"><?= preg_replace('/^(.{3})(.*)/', '$1********', $data['realname']) ?></span></li>
									<li>身份證字號：<span class="red"><?= preg_replace('/^(.{3})(.*)/', '$1********', $data['id_number']) ?></span></li>
									<li>地址：<span class="red"><?= preg_replace('/^(.{3})(.*)/', '$1********', $data['address']) ?></span></li>
									<li>行動電話：<span class="red"><?= preg_replace('/^(.{3})(.*)/', '$1********', $data['phone_number']) ?></span></li>
									<li>E-MAIL：<span class="red"><?= preg_replace('/^(.{3})(.*)/', '$1********', $data['username']) ?></span></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="signUpDate tac">中華民國 <span class="year"><?= date('Y') - 1911 ?> 年 </span><span
										class="month"><?= date('m') ?> 月 </span><span class="day"><?= date('d') ?> 日</span></div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </form>
</div>
<script>
    $(function () {
        var normalRent = <?= $setting->getNormalRent() ?>;
        var weekendRent = <?= $setting->getWeekendRent() ?>;
        var rentingDate = '<?= json_encode($rentingList) ?>';
        var deposit = <?= $data['deposit'] ?? 0 ?>;

        $(".my-deal-location__list-item").click(function (e) {
            var address = $(this).find('.location-name').text();
            $(".my-deal-location__list").find(".selected").removeClass("selected");
            $(this).addClass("selected");
            $('#trasactionPlace').text(address);
            $(this).find('input[name=location]').prop('checked', true);
        });
        $('#startDateSel').on('change', function () {
            var date = $(this).val();
            var startdate = new Date(date);
            var count = 0;
            var dateStr = '{0}年{1}月{2}日'.format(
                startdate.getFullYear() - 1911,
				(startdate.getMonth() + 1).toString().replace(/^(\d)$/, '0$1'),
				(startdate.getDate()).toString().replace(/^(\d)$/, '0$1')
			);
            $('#endDateSel option:gt(0)').remove();
            $('#rentPrice, #daterange').text('');
            if (date === '') {
                return;
            }
            while (count < 8) {
                startdate.setDate(startdate.getDate() + 1);
                let d = startdate.getDay();
                let _date = startdate.toFormatDate();
                if (rentingDate.indexOf(_date) > -1) {
                    break;
                }
                if ([1, 5].indexOf(d) >= 0) {
                    let date_str = startdate.toFormatDate();
                    $('<option/>').val(date_str).text('{0}({1})'.format(date_str, d === 1 ? '週一' : '週五')).appendTo('#endDateSel');
                    count++;
                }
            }
            $('#dateRange').text('');
            $('#renterPrice2').text(0);
            $('#totalPrice').text(deposit);
            $('.rentStart').text(dateStr);
            $('.rentEnd').text('_');
            $('.countDate').text(0);
        });
        $('#endDateSel').on('change', function () {
            var startdate = $('#startDateSel').val();
            var enddate = $(this).val();
            var amount = 0;
            var weekend_num = 0;
            var normal_num = 0;

            var startobj = new Date(startdate);
            var endobj = new Date(enddate);
            var enddateStr = '{0}年{1}月{2}日'.format(
                endobj.getFullYear() - 1911,
                (endobj.getMonth() + 1).toString().replace(/^(\d)$/, '0$1'),
                (endobj.getDate()).toString().replace(/^(\d)$/, '0$1')
            );
            var range = startobj.dateDiff('d', endobj);

            var s = new Date(startdate);
            for (let i = 1; i <= range; i++) {
                s.setDate(s.getDate() + 1);
                switch (s.getDay()) {
                    case 1:
                        amount += weekendRent;
                        weekend_num++;
                        break;
                    case 5:
                        amount += normalRent;
                        normal_num++;
                        break;
                }
            }
            $('#rentPrice').text(amount > 0 ? '{0}元 (週間x{1} 週末x{2})'.format(amount, normal_num, weekend_num) : '');
            $('#dateRange').text('{0} ~ {1} 共{2}天'.format(startobj.toFormatDate(), endobj.toFormatDate(), range + 1));
            $('#renterPrice2').text(amount > 0 ? amount : '0');
            $('#totalPrice').text(deposit + amount);
            $('.rentEnd').text(enddateStr);
            $('.countDate').text(range + 1);
        });
        $('#rentForm button').on('click', function () {
            var $btn = $(this);
            var data = $('#rentForm').serializeArray();
            var valid = true;
            $('#rentForm select').each(function () {
                if ($(this).val() === '') {
                    alert('請選擇' + $(this).parents('.row:eq(0)').find('div:eq(0) label').text());
                    valid = false;
                    return false;
                }
            });
            if (valid) {
                $btn.button('loading');
                var start = $('#startDateSel').val();
                var end = $('#endDateSel').val();
                $.request('/api/User_api/rentRequest', data).always(function (res) {
                    if (res && res.valid) {
                        $('#startDateSel option').each(function () {
                            if ($(this).attr('value') >= start && $(this).attr('value') <= end) {
                                $(this).remove();
                            }
                        });
                        $('#rentRequest').modal('show');
                    } else {
                        alert(res.msg || '租借失敗, 如有問題請聯絡客服人員');
                    }
                    $btn.button('reset');
                });
            }
        });
    });
</script>