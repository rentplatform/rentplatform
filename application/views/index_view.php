
<!--main content-->
<!--輪播大圖-->
<div class="home-page__main-slider">
    <?php foreach ($carousel as $row): ?>
        <a href="<?= $row['url'] ?>">
            <div class="home-page__main-slider__slide">
                <img class="img-responsive hidden-xs" src="/upload/<?= $row['pcimg'] ?>" alt="">
                <img class="img-responsive hidden-sm hidden-md" src="/upload/<?= $row['mobileimg'] ?>" alt="">
            </div>
        </a>
    <?php endforeach; ?>
</div>
<!-- 租借按鈕-->
<div class="container">
    <div class="home-page__heading-entrance">
        <a class="button__start-tenant blue-btn-lg btn" href="/device/search">我想租用</a>
        <a class="button__start-rent green-btn-lg btn" href="<?= !empty($profile) && $profile['credit_card_valid'] == 1 ? '/master/deviceList' : '/master/apply' ?>">我想出租</a>
    </div>
    <div class="home-page__news">
        <div class="home-page__news__heading">
            <h2>最新消息</h2>
        </div>
        <div class="home-page__news__articles">
            <div class="row">
                <?php foreach($article as $row): ?>
                    <div class="col-xs-12 col-sm-4">
                        <a class="article__link" href="/article/content/<?= $row['at_id'] ?>">
                            <div class="article__pic">
                                <img class="img-responsive" src="/upload/<?= $row['pic'] ?>" alt="">
                            </div>
                            <div class="article__txt">
                                <h3><?= $row['title'] ?></h3>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <!-- 服務特色-->
    <div class="home-page__service">
        <div class="home-page__service__heading">
            <h2>服務特色</h2>
        </div>
        <div class="home-page__service__intro">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="service__intro__icon"><img class="img-responsive" src="/images/icons/icon-index-save-money.png" alt=""></div>
                    <div class="service__intro__txt">
                        <h4>精品家電租借最省錢</h4>
                        <p>提供多種精品家電，短期租賃價位便宜，不用花大錢，短期試用確認是否真的符合需求，以免花大錢買來才發現不實用。</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="service__intro__icon"><img class="img-responsive" src="/images/icons/icon-index-online-reservation.png" alt=""></div>
                    <div class="service__intro__txt">
                        <h4>線上預約最便利</h4>
                        <p>提供線上預約的方式，可以選擇想要的日期租用</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="service__intro__icon"><img class="img-responsive" src="/images/icons/icon-index-good-judgement.png" alt=""></div>
                    <div class="service__intro__txt">
                        <h4>評價機制最心安</h4>
                        <p>導入評價機制，為大家把關，各自出借家電的品質和乾淨度，確保租借的家電都是被租公和租客彼此悉心對待。</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 租借流程-->
    <div class="home-page__rent-step">
        <div class="home-page__rent-step__heading">
            <h2>租借流程</h2>
        </div>
        <div class="home-page__rent-step__picture"><img class="img-responsive" src="/images/icons/home-process.png?2" alt=""></div>
    </div>
</div>
<!-- 最新上架電器-->
<div class="container-fluid green-bg">
    <div class="container">
        <div class="home-page__product__sliders">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="home-page__new-product__heading">
                        <h2>最新上架電器</h2>
                    </div>
                    <div class="home-page__new-product__slider">
                        <?php foreach($newest as $row): ?>
                            <div class="home-page__new-product__slider_slide">
								<a href="/device/detail/<?= $row['device_id'] ?>">
                                	<div class="slider_slide__img-container" style="background-image: url('/upload/<?= $row['filename'] ?>')"></div>
								</a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="home-page__hot-product__heading">
                        <h2>最熱門電器</h2>
                    </div>
                    <div class="home-page__hot-product__slider">
                        <?php foreach($hot as $row): ?>
                            <div class="home-page__hot-product__slider_slide">
								<a href="/device/detail/<?= $row['device_id'] ?>">
                                 	<div class="slider_slide__img-container" style="background-image: url('/upload/<?= $row['filename'] ?>')"></div>
								</a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 租客推薦/租公滿意-->
<div class="container">
    <div class="home-page__recommend">
        <div class="home-page__recommend__tab">
            <ul role="tablist">
                <li class="active" role="presentation"><a href="#tenant-recommend" aria-controls="tenant-recommend" role="tab" data-toggle="tab">租客推薦</a></li>
                <li role="presentation"><a href="#landlord-recommend" aria-controls="landlord-recommend" role="tab" data-toggle="tab">租公滿意</a></li>
            </ul>
        </div>
        <div class="home-page__recommend__tab-content tab-content">
            <!--租客推薦-->
            <div class="tab-pane active fade in active" id="tenant-recommend">
                <div class="tenant-recommend__articles row">
                    <?php foreach ($masterRecommend as $row): ?>
                    <div class="col-xs-12 col-sm-4">
                        <div class="tenant-recommend__article__pic"><img class="img-responsive" src="/upload/<?= $row['pic'] ?>" alt=""></div>
                        <div class="tenant-recommend__article__txt">
                            <h4><?= $row['title'] ?></h4>
                            <p><?= $row['content'] ?></p>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <!--租公滿意-->
            <div class="tab-pane fade" id="landlord-recommend">
                <div class="landlord-recommend__articles row">
                    <?php foreach ($masterGood as $row): ?>
                        <div class="col-xs-12 col-sm-4">
                            <div class="tenant-recommend__article__pic"><img class="img-responsive" src="/upload/<?= $row['pic'] ?>" alt=""></div>
                            <div class="tenant-recommend__article__txt">
                                <h4><?= $row['title'] ?></h4>
                                <p><?= $row['content'] ?></p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
