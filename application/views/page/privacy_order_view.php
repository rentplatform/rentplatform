
<!--main content-->
<div class="page-heading">
    <h2>家電租賃契約</h2>
</div>
<div class="container width-800" id="rent-request_contact-tenant">
    <div class="row">
        <div class="block-title-lg col-sm-12">
            <h2>訂單編號： <?= $data['ur_id'] ?></h2>
            <button class="btn-print-contract green-btn-md btn" onclick="printElem('rent-contract')">列印</button>
			<?php if (date('Ymd') <= date('Ymd', strtotime($data['end_time'])) || count_date(date('Y-m-d'), $data['end_time']) < 7): ?>
				<a class="pdf-download btn-print-contract green-btn-md btn" href="/privacy/order?e=<?= urlencode($encrypt) ?>&print=1">下載PDF</a>
			<?php endif; ?>
        </div>
    </div>
    <div class="rent-info green-border-box" id="rent-contract">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h3 class="title">電電租 家電租賃契約</h3>
                <h4 class="sub-title">立契約書人：</h4>
                <ul class="list">
                    <li>承租人：<span class="red"><?= $data['name'] ?></span><span class="black">(以下簡稱甲方)</span></li>
                    <li>出租人：<span class="red"><?= $data['device_owner_name'] ?></span><span class="black">(以下簡稱乙方)</span></li>
                </ul>
                <h4 class="sub-title">承租品明細：</h4>
                <ul class="list">
                    <li>出租品項：<span class="red"><?= $data['device_name'] ?></span></li>
                    <li>承租起迄日：<span class="black">自</span>
                        <span class="red"> 民國<?= date('Y', strtotime($data['start_time'])) - 1911 ?>年<?= date('m月d日', strtotime($data['start_time'])) ?></span>
                        <span class="black">至</span>
                        <span class="red"> 民國<?= date('Y', strtotime($data['start_time'])) - 1911 ?>年<?= date('m月d日', strtotime($data['end_time'])) ?></span>
                        <span class="black">， 共</span>
                        <span class="red"><?= count_date($data['start_time'], $data['end_time']) ?></span>
                        <span class="black">天</span>
                    </li>
                    <li>租金：
						<span class="red">新臺幣
							<span><?= $data['rent'] ?></span>
						</span>
						<span class="black">元</span></li>
                    <li>押金：
						<span class="red">新臺幣
							<span><?= $data['deposit'] ?></span>
						</span>
						<span class="black">元</span></li>
                    <li>面交時租公收取租金+押金， 共<span class="red">新臺幣<?= $data['rent'] + $data['deposit'] ?></span><span class="black">元</span></li>
                    <li>歸還時確認無誤後，現場退還押金</li>
                </ul>
                <h4 class="sub-title">茲因甲乙雙方進行家電租賃乙事，雙方特立本契約書，其協議範圍為本契約書指定品項之承租事宜，雙方同意按下列之約定進行各項事宜：</h4>
                <h4 class="sub-title">第一條 租賃協議：</h4>
                <ul class="list">一、產品租金
					<li>平日（週二～週五）新臺幣<?= $data['normal'] ?>元</li>
					<li>假日（週六～次週一）新臺幣<?= $data['week'] ?>元</li>
                </ul>
                <ul class="list">二、甲方如需延長承租時間，應提前確認產品後續無人承租，方可續租，並需提前於歸還日前12小時提出延長承租之要求，至多以延長兩次為限。(若有不可抗力之因素致無法如期歸還者除外)</ul>
                <ul class="list">三、本協議書乙式貳份，甲乙雙方各執正本乙份。</ul>
                <ul class="list">四、甲方於租賃前,應主動告知乙方所清潔區域是否有人感染法定傳染病,以便乙方後續清潔事宜。</ul>
                <ul class="list">五、如有本協議未盡之事宜，得經由雙方共同合議解決。</ul>
                <h4 class="sub-title">第二條 損壞賠償：</h4>
                <ul class="list">甲方承租前，需確實檢查品項功能正常，乙方亦須主動告知產品現狀，如非正常使用以致損壞，甲方需依實際維修費用照價賠償於乙方。</ul>
                <h4 class="sub-title">第三條 產品點交表：(請務必確認產品功能正常) </h4>
                <ul class="list">
                    <li>
                        <div class="checkedlist hp-form-group col-sm-12">
                            <div class="filter-checkbox display-f">
                                <input type="checkbox" name="" checked disabled>
                                <span class="red">（<?= $data['device_name'] ?>）</span>
                                <span class="black">(本體+配件 共</span>
                                <span class="red"><?= count(json_decode($data['device_accessory'])) + 1 ?></span>
                                <span class="blaxck">項)</span>
                            </div>
                        </div>
                    </li>
                    <li>※若產品含有電子晶片，切勿碰水，如造成損壞依賠償之規定進行。</li>
                    <?php if (!empty($data['notice'])): ?>
                        <li>※其他注意事項：
                            <ul class="black-border">
                                <li><?= $data['notice'] ?></li>
                            </ul>
                        </li>
                    <?php endif; ?>
                </ul>
                <h4 class="sub-title">第四條 違約處理：</h4>
                <ul class="list">甲方租借如逾期，若未提前12小時提出延長承租之要求，則需支付產品租金兩倍罰金於乙方。(若有不可抗力之因素致無法如期歸還者除外)</ul>
                <h4 class="sub-title">第五條  歸還流程:</h4>
                <ul class="list">
                    <span class="black">甲方</span>
                    <span class="red"><?= $data['name'] ?></span>
                    <span class="black">於民國 </span>
                    <span class="red"><?= date('Y', strtotime($data['start_time'])) - 1911 ?>年<?= date('m月d日', strtotime($data['end_time'])) ?></span>
                    <span class="black">歸還產品後，乙方確認無誤即完成歸還流程。</span>
                </ul>
                <h4 class="sub-title">第六條</h4>
                <ul class="list"><span class="black">如因本契約所發生之一切爭議，如有訴訟之必要，雙方合議由臺灣臺北地方法院為第一審管轄法院。</span></ul>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="profile-info-bk">
                            <h4 class="sub-title">甲方</h4>
                            <ul class="list">
                                <li>姓名：<span class="red"><?= $data['name'] ?></span></li>
                                <li>身份證字號：<span class="red"><?= $data['renter_idnumber'] ?> </span></li>
                                <li>地址：<span class="red"><?= $data['renter_address'] ?> </span></li>
                                <li>行動電話：<span class="red"><?= $data['renter_phone'] ?> </span></li>
                                <li>E-MAIL：<span class="red"><?= $data['username'] ?> </span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="profile-info-bk">
                            <h4 class="sub-title">乙方</h4>
                            <ul class="list">
                                <li>姓名：<span class="red"><?= $data['device_owner_name'] ?></span></li>
                                <li>身份證字號：<span class="red"><?= $data['device_owner_idnumber'] ?> </span></li>
                                <li>地址：<span class="red"><?= $data['device_owner_address'] ?> </span></li>
                                <li>行動電話：<span class="red"><?= $data['device_owner_phone'] ?> </span></li>
                                <li>E-MAIL：<span class="red"><?= $data['device_owner_username'] ?> </span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="signUpDate tac">中華民國
                            <span class="year"><?= date('Y', strtotime($data['create_time'])) - 1911 ?> 年 </span>
                            <span class="month"><?= date('m', strtotime($data['create_time'])) ?> 月 </span>
                            <span class="day"><?= date('d', strtotime($data['create_time'])) ?>  日</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function printElem(elem)
    {
        var mywindow = window.open('', 'PRINT');

        mywindow.document.write('<html><head><link rel="stylesheet" href="/vendors/bootstrap/dist/css/bootstrap.min.css"><link rel="stylesheet" href="/css/all.css"><title>' + document.title  + '</title>');
        mywindow.document.write('</head><body>');
        mywindow.document.write('<div class="container width-800" id="rent-request_contact-tenant"><div class="rent-info green-border-box" id="rent-contract">');
        mywindow.document.write(document.getElementById(elem).innerHTML);
        mywindow.document.write('</div></div>');
        mywindow.document.write('</body></html>');

		setTimeout(function () {
//            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10*/
			mywindow.print();
//			mywindow.close();
        }, 200);

        return true;
    }
</script>