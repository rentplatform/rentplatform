<!--main content-->
<div class="page-heading">
    <h2>電電租專欄</h2>
</div>
<div class="container">
    <div class="row">
        <!-- 側欄--分類選單-->
        <div class="aside-block col-xs-12 col-sm-12 col-md-3">
            <button class="collapse-toggle green-border" type="button" data-toggle="collapse"
                    data-target="#collapse__article-categories-menu" aria-expanded="false"
                    aria-controls="collapse__article-categories-menu">文章分類
            </button>
            <div class="collapse-container collapse" id="collapse__article-categories-menu">
                <ul class="article-categories-menu">
                    <?php foreach($type as $row): ?>
                        <li class="<?= $atypeid == $row['atype_id'] ? 'current' : '' ?>"><a href="/article/list/?type=<?= $row['atype_id'] ?>" title="#"><?= $row['name'] ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <!-- 主要內容-->
        <div class="article-list main-block col-xs-12 col-sm-12 col-md-9">

                <?php foreach($list as $k => $row): ?>
					<?= $k % 2 === 0 ? '<div class="row">' : '' ?>
                    <div class="col-xs-12 col-sm-6">
                        <div class="article">
							<div class="article__pic img-container" style="background-image: url('/upload/<?= $row['pic'] ?>');"></div>
                            <div class="article__cate-label"><?= $row['typename'] ?></div>
                            <div class="article__published-date"><?= date('Y-m-d', strtotime($row['release_time'])) ?></div>
                            <div class="article__txt">
                                <div class="article__txt__title"><?= $row['title'] ?></div>
                                <div class="article__txt__content">
                                    <?= $row['introduction'] ?>
                                </div>
                            </div>
                            <div class="readmore-btn-outer">
								<a class="readmore-btn blue-btn-sm btn" href="/article/content/<?= $row['at_id'] ?>">閱讀更多</a>
							</div>
                        </div>
                    </div>
                    <?= $k % 2 > 0 || $k === count($list) - 1 ? '</div>' : '' ?>
                <?php endforeach; ?>
<!--            </div>-->
        </div>
    </div>
    <div class="row">
        <!--頁數-->
        <div class="center-all">
            <?= $pageHtml ?>
        </div>
    </div>
</div>