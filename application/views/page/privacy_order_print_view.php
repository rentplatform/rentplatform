<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>電電租 | 全國首創家電出租共享平台</title>
	<style>
		/* contract style */
		.container {
			/*border: 1px solid #000;*/
			/*padding: 25px 35px;*/
		}

		ul, li {
			list-style: none;
		}

		.container {
			width: 1200px;
			max-width: 1200px;
			margin: 0 auto;
		}

		h3.title {
			font-size: 28px;
			text-align: center;
			display: block;
		}

		.block-title-lg h2 {
			font-size: 20px;
			font-weight: bold;
			margin-top: 30px;
			margin-bottom: 25px;
		}

		}
		.btn-print-contract {
			margin-top: 8px;
			margin-bottom: 8px;
		}

		.rent-info.green-border-box {
			margin-top: 0;
		}

		@media (min-width: 768px) {
			.block-title-lg h2 {
				font-size: 28px;
				margin-top: 30px;
				margin-bottom: 0;
				text-align: left;
			}

			.btn-print-contract {
				float: right;
				margin-top: -40px;
			}

			.pdf-download {
				margin-right: 15px;
			}
		}

		#rent-contract .title {
			font-weight: bold;
			margin-top: 12px;
			margin-bottom: 24px;
		}

		#rent-contract .sub-title {
			font-weight: bold;
			line-height: 28px;
			margin-top: 8px;
			margin-bottom: 8px;
		}

		.list {
			display: block;
			padding-left: 15px;
			font-size: 16px;
			line-height: 28px;
		}

		.list > span {
			font-size: 18px;
			line-height: 30px;
		}

		#rent-contract .checkedlist.hp-form-group {
			padding-right: 0;
			padding-left: 0;
			margin-bottom: 0;
		}

		#rent-contract .black-border {
			border: 1px solid #000;
			margin-top: 12px;
			margin-bottom: 12px;
			padding: 15px 25px;
			padding-left: 40px;
			-webkit-border-radius: 5px;
			border-radius: 5px;
			background-clip: padding-box;
		}

		#rent-contract .signUpDate {
			margin-top: 65px;
			text-align: center;
			font-size: 18px;
			line-height: 30px;

		}

		.row:after {
			content: '.';
			display: block;
			height: 0;
			clear: both;
			visibility: hidden;
		}

		.col-sm-6 {
			width: 50%;
			float: left;
		}

		h2.orderId {
			font-size: 18px;
			color: #00a79e;
		}


	</style>
</head>
<!--<div class="row">-->
<!--	<div class="col-xs-12 col-sm-12">-->
		<!--				<div class="block-title-lg col-sm-12">-->
		<h2 class="orderId">訂單編號： <?= $data['ur_id'] ?></h2>
		<!--				</div>-->
		<h3 class="title">電電租 家電租賃契約</h3>
		<h2 class="sub-title">立契約書人：</h2>
		<dl>
			<dt class="list">
			<dd>承租人：<span class="red"><?= $data['name'] ?></span><span class="black">(以下簡稱甲方)</span></dd>
			<dd>出租人：<span class="red"><?= $data['device_owner_name'] ?></span><span class="black">(以下簡稱乙方)</span>
			</dd>
			</dt>
		</dl>
		<h2 class="sub-title">承租品明細：</h2>
		<dl>
			<dt class="list">
			<dd>出租品項：<span class="red"><?= $data['device_name'] ?></span></dd>
			<dd>承租起迄日：<span class="black">自</span>
				<span class="red"> 民國<?= date('Y', strtotime($data['start_time'])) - 1911 ?>
					年<?= date('m月d日', strtotime($data['start_time'])) ?></span>
				<span class="black">至</span>
				<span class="red"> 民國<?= date('Y', strtotime($data['start_time'])) - 1911 ?>
					年<?= date('m月d日', strtotime($data['end_time'])) ?></span>
				<span class="black">， 共</span>
				<span class="red"><?= count_date($data['start_time'], $data['end_time']) ?></span>
				<span class="black">天</span>
			</dd>
			<dd>租金：<span class="red">新臺幣<?= $data['rent'] ?></span><span class="black">元</span></dd>
			<dd>押金：<span class="red">新臺幣<?= $data['deposit'] ?></span><span class="black">元</span></dd>
			<dd>面交時租公收取租金+押金， 共<span class="red">新臺幣<?= $data['rent'] + $data['deposit'] ?></span><span
						class="black">元</span></dd>
			<dd>歸還時確認無誤後，現場退還押金</dd>
			</dt>
		</dl>
		<ul class="sub-title" style=""><h4>茲因甲乙雙方進行家電租賃乙事，雙方特立本契約書，其協議範圍為本契約書指定品項之承租事宜，雙方同意按下列之約定進行各項事宜：</h4></ul>
		<h2 class="sub-title">第一條 租賃協議：</h2>
		<dl>
			<dt class="list">
			<dd>一、產品租金</dd>
			<dd>平日（週二～週五）新臺幣<?= $data['normal'] ?>元</dd>
			<dd>假日（週六～次週一）新臺幣<?= $data['week'] ?>元</dd>
			</dt>
		</dl>
		<ul class="list">二、甲方如需延長承租時間，應提前確認產品後續無人承租，方可續租，並需提前於歸還日前12小時提出延長承租之要求，至多以延長兩次為限。(若有不可抗力之因素致無法如期歸還者除外)</ul>
		<ul class="list">三、本協議書乙式貳份，甲乙雙方各執正本乙份。</ul>
		<ul class="list">四、甲方於租賃前,應主動告知乙方所清潔區域是否有人感染法定傳染病,以便乙方後續清潔事宜。</ul>
		<ul class="list">五、如有本協議未盡之事宜，得經由雙方共同合議解決。</ul>
		<h2 class="sub-title">第二條 損壞賠償：</h2>
		<ul class="list">甲方承租前，需確實檢查品項功能正常，乙方亦須主動告知產品現狀，如非正常使用以致損壞，甲方需依實際維修費用照價賠償於乙方。</ul>
		<h2 class="sub-title">第三條 產品點交表：(請務必確認產品功能正常) </h2>
		<dl>
			<dt class="list">
			<dd>
				<span class="red">（<?= $data['device_name'] ?>）</span>
				<span class="black">(本體+配件 共</span>
				<span class="red"><?= count(json_decode($data['device_accessory'])) + 1 ?></span>
				<span class="blaxck">項)</span>
			</dd>
			<dd>※若產品含有電子晶片，切勿碰水，如造成損壞依賠償之規定進行。</dd>
            <?php if (!empty($data['notice'])): ?>
				<dd>※其他注意事項：
					<dl class="black-border">
						<dt>
						<dd><table border="1" cellpadding="5"><tr><td><?= $data['notice'] ?></td></tr></table></dd>
						</dt>
					</dl>
				</dd>
            <?php endif; ?>
			</dt>
		</dl>
		<h2 class="sub-title">第四條 違約處理：</h2>
		<ul class="list">甲方租借如逾期，若未提前12小時提出延長承租之要求，則需支付產品租金兩倍罰金於乙方。(若有不可抗力之因素致無法如期歸還者除外)</ul>
		<h2 class="sub-title">第五條 歸還流程:</h2>
		<ul class="list">
			<span class="black">甲方</span>
			<span class="red"><?= $data['name'] ?></span>
			<span class="black">於民國 </span>
			<span class="red"><?= date('Y', strtotime($data['start_time'])) - 1911 ?>
				年<?= date('m月d日', strtotime($data['end_time'])) ?></span>
			<span class="black">歸還產品後，乙方確認無誤即完成歸還流程。</span>
		</ul>
		<h2 class="sub-title">第六條</h2>
		<ul class="list"><span class="black">如因本契約所發生之一切爭議，如有訴訟之必要，雙方合議由臺灣臺北地方法院為第一審管轄法院。</span></ul>
		<table>
			<tr>
				<td>
					<div class="col-xs-12 col-sm-6">
						<div class="profile-info-bk">
							<h4 class="sub-title">甲方</h4>
							<dl>
								<dt class="list">
								<dd>姓名：<span class="red"><?= $data['name'] ?></span></dd>
								<dd>身份證字號：<span class="red"><?= $data['renter_idnumber'] ?> </span></dd>
								<dd>地址：<span class="red"><?= $data['renter_address'] ?> </span></dd>
								<dd>行動電話：<span class="red"><?= $data['renter_phone'] ?> </span></dd>
								<dd>E-MAIL：<span class="red"><?= $data['username'] ?> </span></dd>
								</dt>
							</dl>
						</div>
					</div>
				</td>
				<td>
					<div class="col-xs-12 col-sm-6">
						<div class="profile-info-bk">
							<h4 class="sub-title">乙方</h4>
							<dl>
								<dt class="list">
								<dd>姓名：<span class="red"><?= $data['device_owner_name'] ?></span></dd>
								<dd>身份證字號：<span class="red"><?= $data['device_owner_idnumber'] ?> </span></dd>
								<dd>地址：<span class="red"><?= $data['device_owner_address'] ?> </span></dd>
								<dd>行動電話：<span class="red"><?= $data['device_owner_phone'] ?> </span></dd>
								<dd>E-MAIL：<span class="red"><?= $data['device_owner_username'] ?> </span></dd>
								</dt>
							</dl>
						</div>
					</div>
				</td>
			</tr>
		</table>
<!--	</div>-->
<!--	<div class="row">-->
<!--		<div class="col-sm-12">-->
			<div class="signUpDate tac" style="text-align: center;">中華民國
				<span class="year"><?= date('Y', strtotime($data['create_time'])) - 1911 ?> 年</span>
				<span class="month"><?= date('m', strtotime($data['create_time'])) ?> 月</span>
				<span class="day"><?= date('d', strtotime($data['create_time'])) ?> 日</span>
			</div>
<!--		</div>-->
<!--	</div>-->
</body>
</html>