
<!--main content-->
<div class="page-heading">
    <h2>常見問題</h2>
</div>
<div class="container" id="qa-content">
    <?php foreach ($list as $arr): ?>
        <div class="row">
        <?php foreach ($arr as $row): ?>
            <div class="col-xs-12 col-sm-6">
                <div class="green-border-box">
                    <div class="block-title-sm"><?= $row['title'] ?></div>
                    <div class="block__normal-content">
                        <?= $row['description'] ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
</div>