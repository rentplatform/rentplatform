
<!--main content-->
<div class="page-heading">
    <h2>電電租專欄</h2>
</div>
<div class="container">
    <div class="row">
        <!-- 側欄--分類選單-->
        <div class="aside-block col-xs-12 col-sm-12 col-md-3">
            <button class="collapse-toggle green-border" type="button" data-toggle="collapse" data-target="#collapse__article-categories-menu" aria-expanded="false" aria-controls="collapse__article-categories-menu">文章分類</button>
            <div class="collapse-container collapse" id="collapse__article-categories-menu">
                <ul class="article-categories-menu">
                    <?php foreach($type as $row): ?>
                        <li class="<?= $data['atype_id'] == $row['atype_id'] ? 'current' : '' ?>"><a href="/article/list/<?= $row['atype_id'] ?>" title="#"><?= $row['name'] ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <!-- 主要內容-->
        <div class="main-block col-xs-12 col-sm-12 col-md-9">
            <div class="row">
                <div class="article-detail col-sm-12">
                    <div class="article__pic"><img src="/upload/<?= $data['pic'] ?>" alt=""></div>
                    <div class="article__cate-label"><?= $data['typename'] ?></div>
                    <div class="article__published-date"><?= $data['release_time'] ? date('Y-m-d', strtotime($data['release_time'])) : '預覽' ?></div>
                    <div class="article__txt">
                        <div class="article__txt__title"><?= $data['title'] ?></div>
                        <div class="article__txt__content editor-content">
                            <?= $data['description'] ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="hp-button-block center-all"><a class="button-cancel green-btn-md btn" href="/article/list/<?= $data['atype_id'] ?>">返回	</a></div>
        </div>
    </div>
</div>