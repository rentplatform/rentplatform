<!--main content-->
<div class="page-heading">
    <h2>E-mail 驗證</h2>
</div>
<div class="container width-800">
    <div id="email-auth">
        <div class="block-title-md">請收 E-mail 完成電子信箱驗證!</div>
        <div class="resend-email tac"><i class="icon_warning icon-sprite"> </i><span class="icon-txt">沒有收到? 請點選下方按鈕重新發送驗證信</span></div>
        <div class="center-all">
            <button id="resendMail" class="green-btn-md btn" type="button">重新發送</button>
        </div>
    </div>
</div>
<script>
    $(function () {
        var sendTime = 0;
        $('#resendMail').on('click', function () {
            var $btn = $(this);
            var now = (new Date()).getTime() / 1000;
            if (sendTime && (now - sendTime) < 300) {
                alert('請於5分鐘後再試');
                return false;
            }
            sendTime = (new Date()).getTime() / 1000;
            $btn.button('loading');
            $.request('/api/User_api/resendMailValid', {})
                .always(function(res) {
                    if (res && res.valid) {
                        alert('驗證信已寄出，若還是未收到請5分鐘後再試');
                    } else {
                        alert(res && res.msg ? res.msg : '請於5分鐘後再試');
                    }
                    $btn.button('reset');
                });

        });
    });
</script>