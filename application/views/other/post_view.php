<!DOCTYPE html>
<html>
    <head></head>
    <body>
        <form id="postForm" method="post" action="<?= $url ?>">
            <?php
            foreach ($data as $name => $value) {
                echo "<input type='hidden' name='{$name}' value='{$value}'/>";
            }
            ?>
        </form>
        <script>
            document.getElementById('postForm').submit();
        </script>
    </body>
</html>
