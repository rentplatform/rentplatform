<div class="page-heading">
    <h2>登入</h2>
</div>
<div class="container">
    <?php if ($sign ?? false): ?>
        <div class="login-block row" style="text-align: center;font-size: 30px;">恭喜您已完成信箱驗證</div>
    <?php endif; ?>
    <div class="login-block row">
        <div class="col-xs-12 col-sm-6">
            <div class="login__left-block">
                <form class="login-form formValidation" method="POST" action="/api/User_api/login" data-redirect="<?= $redirect ?? '/' ?>">
                    <filedset>
                        <div class="hp-form-group">
                            <div class="row">
                                <div class="col-xs-12 col-sm-2">
                                    <label class="control-label" for="email">email:</label>
                                </div>
                                <div class="col-xs-12 col-sm-10">
                                    <input class="form-content-green-border" type="email" name="username" required>
                                </div>
                            </div>
                        </div>
                        <div class="hp-form-group">
                            <div class="row">
                                <div class="col-xs-12 col-sm-2">
                                    <label class="control-label">密碼:</label>
                                </div>
                                <div class="col-xs-12 col-sm-10">
                                     <input class="form-content-green-border" type="password" name="password" required>
                                </div>
                            </div>
                        </div>
                        <div class="hp-form-group">
                            <?php if ($sign ?? false): ?>
                                <input type="hidden" name="uid" value="<?= $uid ?>">
                            <?php endif; ?>
                            <button class="login-button green-btn-md btn" type="submit">登入</button>
                        </div>
                    </filedset>
                </form>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="login__right-block green-border-box" style="margin-top: 8px">
                <div class="link__register-now">
                    <i class="icon_warning icon-sprite"></i>
                    <span>還不是會員</span>
                    <a href="/register">立刻註冊</a>
                </div>
                <div class="link__forget-password">
                    <i class="icon_question icon-sprite"></i>
                    <a href="/guest/forgetPassword">忘記密碼</a>
                </div>
            </div>
        </div>
    </div>
</div>
