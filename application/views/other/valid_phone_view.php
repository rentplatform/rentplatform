<!--main content-->
<div class="page-heading">
    <h2>手機驗證</h2>
</div>
<div class="container width-700">
    <div id="mobile-auth">
        <div class="block-title-md">完成手機驗證就可以開始租電器囉!</div>
        <!--表單驗證-->
        <div id="email-auth-form-outer">
            <form class="email-auth-form formValidation" method="POST" action="/api/User_api/validPhone" data-redirect="/user/validPhoneSign">
                <div class="hp-form-group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <label class="control-label" for="email">請輸入您的手機號碼:</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <input class="form-content-green-border" type="text" value="<?= $profile['phone_number'] ?? '' ?>" name="phone_number" phone="true">
                        </div>
                    </div>
                    
                    
                </div>
                <div class="hp-button-block center-all">
                    <button class="green-btn-md btn" type="submit">送出</button>
                </div>
                <div class="tac">*系統將發送驗證到您的手機</div>
            </form>
        </div>
    </div>
</div>