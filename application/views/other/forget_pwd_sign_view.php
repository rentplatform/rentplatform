
<!--main content-->
<div class="page-heading">
    <h2>重設密碼</h2>
</div>
<div class="container">
    <div class="reset-password-block row">
        <div class="col-xs-12 col-sm-12">
            <form class="reset-password-form formValidation" method="post" action="/api/Guest_api/resetPwd" data-redirect="/login" data-msg="修改密碼成功">
                <div class="hp-form-group center-block">
                    <label class="col-sm-5">輸入信箱驗證碼:</label>
                    <input class="col-sm-7" type="text" name="sign" data-validate-length="4" maxlength="4" required>
                </div>
                <div class="hp-form-group center-block">
                    <label class="col-sm-5">輸入新密碼:</label>
                    <input id="passwordInput" class="col-sm-7" type="text" name="password" rangelength="5,20" required>
                </div>
                <div class="hp-form-group center-block">
                    <label class="col-sm-5">再一次輸入新密碼:</label>
                    <input class="col-sm-7" type="text" equalTo="#passwordInput" required>
                </div>
                <div class="note-block">
                    <div class="note-block__inner">
                        <span class="note-txt">*沒有收到密碼重設簡訊?</span>
                        <a id="resendMail" class="link-txt" href="javascript:void(0)">再送一次</a>
                    </div>
                </div>
                <div class="center-all">
                    <input type="hidden" name="uid" value="<?= $uid ?>">
                    <button class="green-btn-md btn" type="submit">送出</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(function () {
        var sendTime = 0;
        var loading = false;
        var email = '<?= $email ?>';
        $('#resendMail').on('click', function () {
            var $btn = $(this);
            var now = (new Date()).getTime() / 1000;
            if (loading) {
                return false;
            }
            if (sendTime && (now - sendTime) < 300) {
                alert('請於5分鐘後再試');
                return false;
            }
            sendTime = (new Date()).getTime() / 1000;
            loading = true;
            $.post('/api/Guest_api/validResetPwdMail', {email:email}, function(){}, 'json')
                .always(function(res) {
                    if (res && res.valid) {
                        alert('驗證信已寄出，若還是未收到請5分鐘後再試');
                    } else {
                        alert(res && res.msg ? res.msg : '請於5分鐘後再試');
                    }
                    loading = false;
                });

        });
    });
</script>