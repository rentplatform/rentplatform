<!--main content-->
<div class="page-heading">
    <h2>手機驗證</h2>
</div>
<div class="container width-800">
    <div id="mobile-auth__success">
        <div class="block-title-md">感謝您完成手機驗證</div>
        <div class="block-title-md black">您可以開始借電器來體驗嘍</div>
        <div class="center-all"><a class="green-btn-md btn" href="/device/search">搜尋電器</a></div>
        <div class="block-title-md black">想賺外快？ 只要再一個步驟就可以成為電租公！</div>
        <div class="center-all"><a class="green-btn-md btn" href="/master/validCredit">立刻前往</a></div>
    </div>
</div>