<!--main content-->
<div class="page-heading">
    <h2>註冊會員</h2>
</div>
<div class="container width-800">
    <div class="register-block row">
        <div class="col-xs-12 col-sm-12">
            <form class="register-form formValidation" method="POST" action="/api/User_api/register" data-redirect="/register/success">
                <filedset>
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-3">
                                <label class="control-label" for="user-name">姓名:*</label>
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <input class="form-content-green-border" type="text" name="name" id="user-name" rangelength="2,10" required>
                            </div>
                        </div>
                    </div>
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="control-label">性別:*</label>
                            </div>
                            <div class="col-sm-9">
                                <label class="radio-inline gender" for="gender">
                                    <input type="radio" name="sex" id="male" value="1" checked>男性
                                </label>
                                <label class="radio-inline gender" for="gender">
                                    <input type="radio" name="sex" id="female" value="2">女性
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-3">
                                <label class="control-label">暱稱:*</label>
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <input class="form-content-green-border" type="text" name="nickname" rangelength="2,10" required>
                            </div>
                        </div>
                    </div>
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-3">
                                <label class="control-label">身分證字號:*</label>
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <input class="form-content-green-border" type="text" name="id_number" required>
                            </div>
                        </div>
                    </div>
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-3">
                                <label class="control-label">Email:*</label>
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <input class="form-content-green-border" type="email" name="username" required>
                            </div>
                        </div>
                    </div>
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-3">
                                <label class="control-label">設定密碼:*</label>
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <input id="passwordInput" class="form-content-green-border" type="password" name="password" rangelength="5,20" required>
                            </div>
                        </div>
                    </div>
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-3">
                                <label class="control-label">重複輸入密碼:*</label>
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <input class="form-content-green-border" type="password" equalTo="#passwordInput" required>
                            </div>
                        </div>
                    </div>
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-3">
                                <label class="control-label">手機:*</label>
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <input class="form-content-green-border" type="text" name="phone_number" maxlength="15" phone="true" required>
                            </div>
                        </div>
                    </div>
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-3">
                                <label class="control-label">地址:</label>
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <input class="form-content-green-border" type="text" name="address">
                            </div>
                        </div>
                    </div>
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-3">
                                <label class="control-label">Line ID:</label>
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <input class="form-content-green-border" type="text" name="line_id">
                            </div>
                        </div>
                    </div>
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-3"></div>
                            <div class="col-xs-12 col-sm-9">
                                <input type="checkbox" id="service-policy" value="1" name="policy" required>
                                <label for="service-policy" style="vertical-align: super;">
									同意
									<a href="/service">服務條款</a>與
									<a href="/privacy">隱私權政策</a>
								</label>
                            </div>
                        </div>
                    </div>
                    <div class="hp-button-block center-all mutiple-btn">
                        <a class="button-cancel green-btn-md btn" href="/login">取消</a>
                        <input class="submit button-register green-btn-md btn" type="submit" value="註冊會員">
                    </div>
                </filedset>
            </form>
        </div>
    </div>
</div>