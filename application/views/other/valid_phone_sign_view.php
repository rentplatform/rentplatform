<!--main content-->
<div class="page-heading">
    <h2>手機驗證</h2>
</div>
<div class="container width-700">
    <div id="mobile-auth__auth-code">
        <div class="block-title-md">請輸入您手機收到的驗證碼</div>
        <!--表單驗證-->
        <div id="email-auth-form-outer">
            <form class="email-auth-form formValidation" method="POST" action="/api/User_api/validPhoneSign" data-redirect="/user/validPhoneSuccess">
                <div class="hp-form-group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <label class="control-label" for="mobile-auth-code">請輸入手機驗證碼:</label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <input class="form-content-green-border" type="text" value="" name="sign" minlength="6" maxlength="6"  digits="true">
                        </div>
                    </div>
                </div>



                <div class="hp-button-block center-all">
                    <button class="green-btn-md btn" type="submit">送出</button>
                </div>
                <div class="tac">*系統將發送驗證到您的手機</div>
            </form>
        </div>
    </div>
</div>