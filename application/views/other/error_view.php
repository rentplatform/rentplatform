
<!--main content-->
<div class="page-heading">
    <h2>發生錯誤</h2>
</div>
<div class="container">
    <div class="forget-password-block row">
        <form class="mobile-auth-form formValidation" method="post" action="/api/Guest_api/validResetPwdMail" data-redirect="/guest/forgetPasswordSign">
            <div class="hp-form-group center-block">
                <label class="col-sm-6">請輸入註冊用的信箱:</label>
                <input class="col-sm-6" type="email" name="email" required>
            </div>
            <div class="note-block"><span class="note-txt">*系統將發送驗證碼到您的信箱</span></div>
        </form>
    </div>
</div>