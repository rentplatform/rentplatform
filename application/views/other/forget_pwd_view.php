
<!--main content-->
<div class="page-heading">
    <h2>重設密碼</h2>
</div>
<div class="container width-700">
    <div class="forget-password-block row">
        <form class="mobile-auth-form formValidation" method="post" action="/api/Guest_api/validResetPwdMail" data-redirect="/guest/forgetPasswordSign">
            <div class="hp-form-group center-block">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                         <label class="control-label">請輸入註冊用的信箱:</label>
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <input class="form-content-green-border" type="email" name="email" required>
                    </div>
                </div>
               
               
            </div>
            <div class="note-block"><span class="note-txt">*系統將發送驗證碼到您的信箱</span></div>
            <div class="center-all">
                <!-- 送出後進 forgot-password-reset.html-->
                <button class="green-btn-md btn" type="submit">送出</button>
            </div>
        </form>
    </div>
</div>