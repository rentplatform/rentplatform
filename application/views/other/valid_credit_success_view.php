<!--main content-->
<div class="page-heading">
    <h2>信用卡驗證</h2>
</div>
<div class="container width-800">
    <div id="mobile-auth__success">
        <?php if ($isSuccess): ?>
        <div class="block-title-md">感謝您完成信用卡驗證</div>
        <div class="block-title-md black">您已通過信用卡驗證，立刻開始出借電器吧</div>
            <div class="center-all"><a class="green-btn-md btn" href="/master/deviceList">前往電器管理</a></div>
        <?php else: ?>
            <div class="block-title-md">信用卡驗證失敗</div>
            <div class="block-title-md black">您的信用卡驗證失敗，請重新確認信用卡資訊並再次驗證</div>
            <div class="center-all"><a class="green-btn-md btn" href="/api/trans_api/valid">重新驗證</a></div>
        <?php endif; ?>
    </div>
</div>