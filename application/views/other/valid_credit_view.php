<!--main content-->
<div class="page-heading">
    <h2>輸入信用卡資訊</h2>
</div>
<div class="container width-800">
    <div class="center-block" id="credit-auth">
        <div class="icon-credit-card center-all"><img class="img-responsive" src="/images/icons/icon-credit-card.png" alt=""></div>
        <div class="block-title-md black tac">系統會驗證您的信用卡是否正確</div>
        <div class="block__normal-content red tac">我們會先刷30元作為驗證之用，驗證完成後會立刻退刷，因此過程中您不會被實際收取任何費用。</div>
        <div class="hp-button-block center-all">
            <a class="green-btn-md btn" href="/api/trans_api/valid">前往</a>
        </div>
    </div>
</div>