<!--alert-window手機驗證-->
<div class="alert-window modal fade" id="mobileAuth">
    <div class="alert-window__inner modal-dialog green-border">
        <div class="alert-window__heading">
            <div class="center-all">
                <div class="icon_mobile-auth"></div>
            </div>
            <div class="alert-window-title">您需要完成手機認證</div>
            <div class="alert-window-title" style="font-weight: normal;">才能開始租借電器</div>
        </div>
        <div class="alert-window__content">
            <div class="row center-all-pc">
                <div class="col-sm-12"><a class="green-btn-md btn" href="/user/validPhone">立刻認證</a></div>
            </div>
        </div>
    </div>
</div>
