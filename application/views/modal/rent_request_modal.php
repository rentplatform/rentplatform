
<div class="alert-window modal fade " id="rentRequest">
    <div class="alert-window__inner modal-dialog green-border">
        <div class="alert-window__heading">
            <div class="center-all">
                <div class="icon_alert_window_success"></div>
            </div>
            <div class="alert-window-title">您的租借申請已經送出</div>
            <div class="alert-window-title" style="font-weight: normal;">請等待電租公回應</div>
        </div>
        <div class="alert-window__content">
            <div class="row center-all-pc">
                <div class="col-sm-12"><a class="green-btn-md btn" href="#" data-dismiss="modal">關閉</a></div>
            </div>
        </div>
    </div>
</div>