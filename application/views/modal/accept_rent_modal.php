<?php global $urid;?>
<div class="alert-window modal fade" id="accept-rent">
    <div class="alert-window__inner modal-dialog">
        <div class="alert-window__heading">
            <div class="center-all">
                <div class="icon_system-success"></div>
            </div>
            <div class="alert-window-title">是否確認接單</div>
            <div class="alert-window-sub-title">確認後進入付款畫面，完成後即可接單</div>
        </div>
        <div class="alert-window__content">
            <div class="hp-button-block row center-all-pc">
                <div class="col-sm-12">
                    <button class="green-btn-md btn" type="button" data-dismiss="modal">取消</button>
                    <a class="green-btn-md btn" href="/api/Trans_api/orderPay/<?= $urid ?>">確定</a>
                </div>
            </div>
        </div>
    </div>
</div>