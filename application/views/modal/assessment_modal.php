<!--alert-window: 評價-->
<div class="alert-window modal fade" id="vote-window">
    <div class="alert-window__inner modal-dialog">
        <div class="alert-window__heading">
            <div class="block-title-lg">您覺得這位電租公/婆...</div>
        </div>
        <div class="alert-window__content">
            <form id="commentForm" class="vote-form formValidation" method="post" action="/api/User_api/setAssessment">
                <div class="evaluation-block">
                    <div class="evaluate-button icon_positive_3x icon-sprite good selected">
                        <input class="hidden" type="radio" name="type" value="1" checked>
                    </div>
                    <div class="evaluate-button icon_negative_3x icon-sprite bad">
                        <input class="hidden" type="radio" name="type" value="2">
                    </div>
                </div>
                <div class="hp-form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="tal" for="">想說的話:</label>
                        </div>
                        <div class="col-sm-12">
                            <textarea class="form-content-green-border" name="comment" rows="5" col="250" placeholder="請輸入使用心得或評價" value="" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="hp-button-block row center-all-pc">
                    <div class="col-sm-12">
                        <input type="hidden" name="urid" value=""/>
                        <button class="green-btn-md btn js-close-alertWindow" type="button" data-dismiss="modal">取消</button>
                        <button class="green-btn-md btn" type="submit">確定</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(function() {
        //.evaluate-button:not(.not)
        $(document).on('click', '.evaluate-button:not(.not)', function(e){
            $(this).parent().find('.selected').removeClass('selected');
            $(this).addClass('selected');
            $(this).find('input').prop('checked', true);
        })
    });
</script>