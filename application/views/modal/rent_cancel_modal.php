
<!--alert-window: 取消訂單-->
<div class="alert-window modal fade" id="cancel-order">
    <div class="alert-window__inner modal-dialog">
        <div class="alert-window__heading tac">
            <div class="icon_system-failed icon-sprite"></div>
            <div class="alert-window-title">確定取消訂單?</div>
        </div>
        <div class="alert-window__content">
            <form class="reject-rent-form formValidation" method="POST" action="/api/User_api/rentCancel">
                <div class="alert-window-sub-title">請輸入原因:</div>
                <div class="radio hp-form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <label for="reject-rent_reson">
                                <input type="radio" name="reason" value="時間無法配合" checked>時間無法配合
                            </label>
                        </div>
                    </div>
                </div>
                <div class="radio hp-form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <label for="reject-rent_reson">
                                <input type="radio" name="reason" value="已經借到">已經借到
                            </label>
                        </div>
                    </div>
                </div>
                <div class="radio hp-form-group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3">
                            <label>
                                <input type="radio" name="reason" value="other">其他
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <input class="form-content-green-border" type="text" name="txt" value="">
                        </div>
                    </div>
                </div>
                <div class="hp-button-block row center-all-pc">
                    <div class="col-sm-12">
                        <input type="hidden" name="id" value=""/>
                        <button class="green-btn-md btn" type="button" data-dismiss="modal">取消</button>
                        <button class="green-btn-md btn" type="submit">確定</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>