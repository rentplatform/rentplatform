
<div id="deleteLocationModal" class="alert-window modal fade">
    <div class="alert-window__inner modal-dialog">
        <div class="center-all">
            <div class="alert-window__delet-icon icon-sprite"></div>
        </div>
        <div class="alert-window__heading">
            <div class="block-title-md black">確認刪除此地點?</div>
        </div>
        <div class="hp-button-block row center-all-pc">
            <div class="col-sm-12">
                <button class="green-btn-md btn" type="button" data-dismiss="modal">取消</button>
                <button id="delLocationBtn" class="green-btn-md btn" type="button">刪除</button>
            </div>
        </div>
    </div>
</div>