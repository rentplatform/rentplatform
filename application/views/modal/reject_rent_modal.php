<?php global $urid; ?>
<div class="alert-window modal fade" id="reject-rent">
    <div class="alert-window__inner modal-dialog">
        <div class="alert-window__heading tac">
            <div class="icon_system-failed icon-sprite"></div>
            <div class="alert-window-title">確認無法接單?</div>
        </div>
        <div class="alert-window__content">
            <form id="rejectForm" class="reject-rent-form formValidation" method="post" action="/api/Master_api/rejectRent" data-redirect="/user/rentRecord">
                <div class="alert-window-sub-title">請輸入原因:</div>

                <div class="radio hp-form-group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <label>
                                <input type="radio" name="reason" value="家電故障" checked>家電故障
                            </label>
                        </div>
                    </div>
                </div>

                <div class="radio hp-form-group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <label>
                                <input type="radio" name="reason" value="臨時無法外借">臨時無法外借
                            </label>
                        </div>
                    </div>
                </div>



                <div class="radio hp-form-group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <label>
                                <input id="otherReason" type="radio" name="reason" value="other">其他
                            </label>
                        </div>
                        
                        <div class="col-xs-12 col-sm-8">
                            <input type="text" name="reasontxt" value="" class="form-content-green-border">
                        </div>
                    </div>
                </div>

                
                <div class="hp-button-block row center-all-pc">
                    <div class="col-sm-12">
                        <input type="hidden" name="urid" value="<?= $urid ?>" />
                        <button class="green-btn-md btn" type="button" data-dismiss="modal">取消</button>
                        <button class="green-btn-md btn" type="submit">確定</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(function() {
        $('#rejectForm input[type=radio]').on('click', function () {
            if ($(this).val() === 'other') {
                $('#rejectForm input[name=reasontxt]').attr('required', true);
            } else {
                $('#rejectForm input[name=reasontxt]').removeAttr('required');
            }
        });
    });

</script>