
<!--alert-window手機驗證==========================================//-->
<div id="mobileValidTip" class="alert-window modal" id="mobile-auth">
    <div class="alert-window__inner modal-dialog green-border">
        <div class="alert-window__heading">
            <div class="center-all">
                <div class="icon_mobile-auth"></div>
            </div>
            <div class="alert-window-title">您需要完成手機認證</div>
            <div class="alert-window-title" style="font-weight: normal;">才能開始租借電器</div>
        </div>
        <div class="alert-window__content">
            <div class="row center-all-pc">
                <div class="col-sm-12"><a class="green-btn-md btn" href="/user/validPhone">立刻認證</a>&nbsp;&nbsp;<a class="green-btn-md btn" href="#" data-dismiss="modal">我知道了</a></div>
            </div>
        </div>
    </div>
</div>
<!--alert-window手機驗證==========================================//-->

<!--footer-->
<footer id="footer">
    <div class="container">
        <div class="footer__menu">
            <a href="/brand">品牌合作</a>
            <a href="/service">服務條款</a>
            <a href="/privacy">隱私權政策</a>
            <a <?= $logged ? 'href="/user/arbitration"' : 'class="loginRedirect" href="javascript:void(0)" data-uri="/user/arbitration"' ?>>意見反應</a>
        </div>
        <div class="footer__contact-info__outer row">
            <div class="footer__contact-info col-xs-12 col-sm-6">
                <div class="footer__contact-info__logo"><a href="#"><img class="img-responsive" src="/images/icons/header__logo.png" alt=""></a></div>
                <div class="footer__contact-info__list">
                    <ul>
                        <li>02-27123335</li>
                        <li><a href="mailto:service@homeapp123.com">service@homeapp123.com</a></li>
                        <li><a href="https://line.me/R/ti/p/%40vov2586x" target="_blank">Line@線上客服</a></li>
                        <li>台北市松山區南京東路4段1號11樓</li>
                    </ul>
                </div>
            </div>
            <div class="footer__fb col-xs-12 col-sm-6 hidden-xs">
                 <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FHomeApp123%2F&tabs=timeline&width=450&height=200&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1481386098811399" width="450" height="200" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
            </div>
        </div>
        <div class="footer_copyright"><span>©電電租股份有限公司 2017 All right reserved.</span></div>
    </div>
</footer>
<script src="/js/slick.min.js"></script>
<script src="/js/exif.js"></script>

<script>
    $(function(){
        $('.home-page__main-slider').slick({
            dots: true,
            infinite: true,
            speed: 1000,
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 3500
        });


        $('.home-page__new-product__slider').slick({
            dots: false,
            infinite: true,
            slidesToShow: 1,
            autoplay: false,
        });


        $('.home-page__hot-product__slider').slick({
            dots: false,
            infinite: true,
            slidesToShow: 1,
            autoplay: false,
        });
        //- tab
        $('.home-page__recommend__tab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show')
        });
        <?php if ($firstLogin ?? false): ?>
            $('#mobileValidTip').modal('show');
        <?php endif; ?>
        $('button, input[type=button]').attr('data-loading-text', '載入中');

        <?php if ($setting->getGaCode()): ?>
			<!-- Google Analytics -->
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			ga('create', '<?= $setting->getGaCode() ?>', 'auto');
			ga('send', 'pageview');
			<!-- End Google Analytics -->
		<?php endif; ?>

    });

</script>


</body>
</html>