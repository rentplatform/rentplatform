<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>電電租 | 全國首創家電出租共享平台</title>
	<meta name="description" content="<?= $setting->getSsoDescription() ?>">
	<meta name="keywords" content="<?= $setting->getSsoKeyword() ?>">
    <!--bootstrap v3.3.6-->
    <link rel="stylesheet" href="/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/all.css?2">
    <!--js library-->
    <script src="/vendors/jquery/dist/jquery.min.js"></script>
    <script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="/vendors/jquery-validation/dist/localization/messages_zh_TW.js"></script>
    <script src="/js/js_extension.js?3"></script>
    <script src="/js/homeapp.js"></script>
    <script src="/js/common.js?v2"></script>
	<script>
        <?php if ($setting->getGtCode()): ?>
        <!-- Global Site Tag (gtag.js) - Google Analytics -->
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','<?= $setting->getGtCode() ?>');
        <?php endif; ?>
	</script>
</head>
<body>
<?php if ($setting->getGtCode()): ?>
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?= $setting->getGtCode() ?>"
					  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<?php endif; ?>
<!--navigator-->
<header id="header">
    <div class="container" id="header__inner">
        <h1 id="nav__logo"><a href="/"><img class="img-responsive" src="/images/icons/header__logo.png" alt=""></a>
        </h1>
        <nav class="nav">
            <div class="nav__slogan"><img class="img-responsive hidden-xs" src="/images/header__slogan--pc.png"><img
                        class="img-responsive hidden-sm hidden-md hidden-lg" src="/images/header__slogan--mobile.png">
            </div>
            <button class="nav__toggle navbar-toggle pull-right" type="button"><span
                        class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                        class="icon-bar"></span><span class="icon-bar"></span></button>
            <div class="nav__bg-mask">
                <div class="close-menu"></div>
            </div>
            <div class="collapse-bk">
                <div class="nav__list">
                    <div class="nav__list__login">
                        <?php if ($logged): ?>
                            <a class="status-login login__button" href="javascript:;" id="login"><?= $profile['nickname'] ?></a>
                            <div class="login__menu">
                                <a href="/user/profile">-個人資料</a>
                                <a href="/user/notifications">-訊息通知</a>
                                <a href="/user/rentRecord">-租借記錄</a>
    <!--                            一般會員不會顯示電氣管理-->
								<?php if ($profile['rent_master']): ?>
									<a href="/master/deviceList">-電器管理</a>
									<a href="/master/location">-面交地點</a>
								<?php endif; ?>
                                <a class="logout-button" href="/logout">-登出</a>
                            </div>
                        <?php else: ?>
                            <a class="status-logout login__button" href="/login" id="login">登入/註冊</a>
                        <?php endif; ?>
                    </div>
                    <div class="nav__list__item">
                        <a href="/device/search">要租家電</a>
                        <a class="current" href="/master/apply">成為電租公</a>
                        <a href="/article/list">電電租專欄</a>
                        <a href="/question">常見問題</a>
                        <a href="/aboutus">關於我們</a>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>