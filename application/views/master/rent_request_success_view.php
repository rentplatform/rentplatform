
<!--main content-->
<?php if ($isSuccess): ?>
<div class="page-heading">
    <h2>恭喜，您的電小孩要出門賺錢囉!</h2>
</div>
<div class="container width-800" id="rent-request_contact-tenant">
    <div class="system-msg-block">
        <div class="system-icon-success center-all">
            <div class="icon_system-success icon-sprite"></div>
        </div>
        <div class="system-msg center-all">您已經完成接單，請趕快與租客聯絡!</div>
        <div class="hp-button-block center-all"><a class="green-btn-md btn" href="/master/contactRenter/<?= $urid ?>">聯絡租客</a></div>
    </div>
</div>
<?php else: ?>
    <div class="page-heading">
        <h2>抱歉，接單失敗!</h2>
    </div>
    <div class="container main-block width-800" id="rent-request_contact-tenant">
        <div class="system-msg-block">
            <div class="center-all">
                <div class="icon_system-failed-lg icon-sprite"></div>
            </div>
            <div class="block-title-md">付款失敗</div>
            <div class="system-msg center-all block__normal-content">信用卡號錯誤無法成功付款，請再次執行付款流程</div>
            <div class="hp-button-block center-all mutiple-btn">
                <a class="green-btn-md btn" href="/api/Trans_api/orderPay/<?= $urid ?>">前往付款</a>
                <a class="green-btn-md btn" href="user/rentRecord/1">取消</a></div>
            </div>
        </div>
    </div>

<?php endif; ?>
