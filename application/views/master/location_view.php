<?php load_modal('del_location'); ?>
<!--main content-->
<div class="page-heading">
    <h2>設定面交地點</h2>
</div>
<div class="container width-800">
    <div class="block-title-md">新建面交地點</div>
    <!--新增地點 -->
    <form class="formValidation" method="post" action="/api/Master_api/locationInsert" data-redirect="/master/location">
        <div class="add-deal-location__form green-border-box">
            <div class="row">
                <div class="col-xs-12 col-sm-10">
                    <div class="hp-form-group">
                        <input class="add-location-name form-content-green-border" type="text" name="placename"
                               placeholder="地點名稱" maxlength="20" required>
                    </div>
                    <div class="hp-form-group">
                        <select class="county-select home-app-search__select selCountry form-content-green-border"
                                name="cid" data-target-city-sel=".selCity" required>
                            <option value="">請選擇縣市</option>
                            <?php
                            foreach ($countryList as $row) {
                                echo sprintf('<option value="%s">%s</option>', $row['cid'], $row['name']);
                            }
                            ?>
                        </select>
                    </div>
                    <div class="hp-form-group">
                        <select class="town-select home-app-search__select selCity form-content-green-border"
                                name="ct_id" required>
                            <option value="">請選擇鄉鎮市區</option>
                        </select>
                    </div>
                    <div class="hp-form-group">
                        <input class="add-road-name form-content-green-border" type="text" name="address"
                               placeholder="路名" required>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2">
                    <button class="blue-btn-sm btn js-addLocation" type="submit">新增</button>
                </div>
            </div>
        </div>
        <!--現有面交地點			-->
        <?php if (!empty($list)): ?>
            <div class="block-title-md locationDiv">現有面交地點</div>
            <div class="my-deal-location green-border-box locationDiv">
                <ul class="my-deal-location__list">
                    <?php foreach ($list as $row): ?>
                        <li class="my-deal-location__list-item row">
                            <div class="col-xs-10 col-sm-10">
                                <span class="location-name"><?= $row['placename'] ?></span>
                                <span class="location-address"><?= $row['countryName'] . $row['cityName'] . $row['address'] ?></span>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <span class="location-delete button_delete_normal deletePlace"
                                      data-id="<?= $row['tp_id'] ?>"></span>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>
    </form>
</div>
<script>
    var deleteId;
    $(function () {
        $('.deletePlace').on('click', function () {
            deleteId = $(this).data('id');
            if ($('.my-deal-location__list li').length < 2) {
                alert('至少需保留一筆面交點');
                return;
            }
            $('#deleteLocationModal').modal({backdrop: 'static', keyboard: false, show: true});
        });
        $('#delLocationBtn').on('click', function () {
            var $btn = $(this);
            $btn.button('loading');
            $.post('/api/Master_api/locationDelete', {
                'tp_id': deleteId
            }).always(function (res) {
                $btn.button('reset');
                $('.deletePlace[data-id='+deleteId+']').parents('li').remove();
                $('#deleteLocationModal').modal('hide');
                if ($('.deletePlace').length < 1) {
                    $('.locationDiv').remove();
                }
            });
        });
    });


    //- 假刪除面交地點
    function delete_location() {
        $(".selected-to-delete").remove();
        $("#deleteLocationModal").modal('hide');
        $.post('/api/Master_api/locationDelete', {id: deleteId});
    }
</script>