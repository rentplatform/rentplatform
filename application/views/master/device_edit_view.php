<!--main content-->
<div class="page-heading">
    <h2><?= isset($data) ? '編輯' : '新增' ?>電器</h2>
</div>
<div class="container width-700">
    <div class="block-title-md">請輸入電器資料</div>
    <div class="">
        <form class="add-home-app-form formValidation" method="post"
              action="/api/Master_api/deviceEdit/<?= $data['device_id'] ?? '' ?>" data-redirect="/master/deviceList">
            <div class="hp-form-group">
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <select class="home-app-search__select form-content-green-border" name="dt_id" required>
                            <option value="">請選擇電器類型</option>
                            <?php
                            foreach ($deviceTypeList as $row) {
                                $seleted = isset($data) && $data['dt_id'] == $row['dt_id'] ? 'selected' : '';
                                echo sprintf('<option value="%s" %s>%s</option>', $row['dt_id'], $seleted, $row['name']);
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="hp-form-group">
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <select class="home-app-search__select form-content-green-border" name="bid" required>
                            <option value="">請選擇品牌</option>
                            <?php
                            foreach ($brandList as $row) {
                                $seleted = isset($data) && $data['bid'] == $row['bid'] ? 'selected' : '';
                                echo sprintf('<option value="%s" %s>%s</option>', $row['bid'], $seleted, $row['name']);
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="hp-form-group">
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <input class="form-content-green-border" type="text" name="name" placeholder="請輸入電器名稱"
                               value="<?= $data['name'] ?? '' ?>" required>
                    </div>
                </div>
            </div>
            <div class="hp-form-group upload-img-block row">
                <label class="col-xs-12 col-sm-2" for="">上傳照片:</label>
                <div class="col-xs-12 col-sm-10">
                    <div class="row">
                        <div class="upload-area-outer">
                            <?php for ($i = 0; $i < 3; $i++): ?>
                                <div class="upload-area col-xs-4">
                                    <div class="img-container img-uploaded"
                                         style="background-image: url('<?= !empty($data['pic'][$i]['filename']) ? '/upload/' . $data['pic'][$i]['filename'] : '/images/icons/upload-image.png' ?>')"></div>
                                    <input class="upload-button" type="file"
                                           name="file[]" <?= !isset($data) ? 'required' : '' ?>>
                                    <?php if (!empty($data['pic'][$i]['filename'])): ?>
                                        <i class="delete-img-button"></i>
                                        <input type="hidden" name="oldpic[]"
                                               value="<?= $data['pic'][$i]['filename'] ?>">
                                    <?php endif; ?>
                                </div>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <!-- 上傳尺寸說明 -->
                    <div class="row">
                      <div class="note-bk col-sm-12">
						  上傳電器照片須知：<br>
						  (1) 尺寸建議大於1024x1024 pixel，不然會模糊；顯示比例為正方形，若能事先裁切為正方形後上傳，畫面效果最好。<br>
						  (2) 單一照片檔案大小建議不超過3MB。<br>
						  (3) 檔案格式為jpg或png (手機拍照即為此2種格式)

                      </div>
                    </div>
                </div>
            </div>
            <div class="hp-form-group">
                <div class="row">
                    <div class="col-xs-12 col-sm-2">
                        <label class="control-label ">設定狀態:</label>
                    </div>
                    <div class="col-xs-12 col-sm-10">
                        <label class="radio-inline" for="launch">
                            <input type="radio" name="status"
                                   value="1" <?= ($data['status'] ?? 1) == 1 ? 'checked' : '' ?>>上架
                        </label>
                        <label class="radio-inline" for="discontinue">
                            <input type="radio" name="status"
                                   value="0" <?= ($data['status'] ?? 1) == 0 ? 'checked' : '' ?>>暫停
                        </label>
                    </div>
                </div>
            </div>
            <div class="hp-form-group ">
                <div class="row">
                    <div class="col-xs-12 col-sm-2">
                        <label class="control-label ">設定押金:</label>
                    </div>
                    <div class="col-xs-12 col-sm-10">
                        <input class="form-content-green-border" type="number" name="deposit" value="<?= $data['deposit'] ?? '' ?>" placeholder="請輸入押金金額"
                               required>
                    </div>
                </div>
            </div>
            <div class="hp-form-group ">
                <div class="row">
                    <div class="col-xs-12 col-sm-2">
                        <label class="control-label ">輸入描述:</label>
                    </div>
                    <div class="col-xs-12 col-sm-10">
                        <textarea class="form-content-green-border" rows="8" cols="250" name="description" placeholder="請輸入電器描述"
                                  required><?= $data['description'] ?? '' ?></textarea>
                    </div>
                </div>
            </div>
			<div class="hp-form-group">
				<div class="row">
					<div class="col-xs-12 col-sm-2">
						<label class="control-label ">注意事項:(將出現於家電租賃契約中)</label>
					</div>
					<div class="col-xs-12 col-sm-10">
						<textarea class="form-content-green-border" rows="8" cols="250" name="notice" placeholder="請輸入注意事項(將出現於家電租賃契約中)"><?= $data['notice'] ?? '' ?></textarea>
					</div>
				</div>
			</div>
            <div class="hp-form-group ">
                <div class="row">
                    <div class="col-xs-12 col-sm-2">
                        <label class="control-label ">配件:</label>
                    </div>
                    <div class="col-xs-12 col-sm-10">
                        <div class="add-accessory-input__outer">
                            <input id="add-accessory-input" class="form-content-green-border" type="text" placeholder="請輸入配件名稱" value="">
                        </div>
                        <div class="add-accessory-button__outer">
                            <a id="add-accessory-button" href="javascript:;">+</a>
                        </div>
                    </div>
                </div>
            </div>

           <div class="hp-form-group">
                <div class="row">
                    <div class="col-sm-12" id="accessory-list">
                        <!--配件加入後塞這裡-->
                        <?php foreach ($data['accessory'] ?? array() as $k => $text): ?>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2"></div>
                                <div class="col-xs-12 col-sm-10">
                                    <div class="accessory-item hp-form-group">
                                        <i class="delete-accessory">X</i>
                                        <input class="accessory-item__input" name="accessory[]" type="text"
                                               value="<?= $text ?>">
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>


            <div class="hp-button-block btn-combin center-all mutiple-btn">
                <?php if (isset($data)): ?>
                    <button id="deleteDevice" class="warning-btn button-delete green-btn-md btn " type="button" <?= $isRenting ? 'disabled data-toggle="tooltip" data-placement="top" title="電器使用中無法刪除"' : '' ?>>刪除</button>
                <?php endif; ?>
                <a class="button-cancel green-btn-md btn" href="/master/deviceList">取消</a>
                <button class="button-register green-btn-md btn" type="submit">儲存</button>
            </div>
        </form>
    </div>
</div>
<script>
    $(function () {
        var deviceId = <?= $data['device_id'] ?? '0' ?>;
        $('[data-toggle="tooltip"]').tooltip();
        // $(".upload-button").change(function () {
        //     var $parent = $(this).parent();
        //     var $img = $parent.find(".img-container");
        //     if (this.files && this.files[0]) {
        //         var reader = new FileReader();
        //         reader.onload = function (e) {
        //             $img.attr('style', 'background-image: url('+e.target.result+')');
        //             $parent.append('<i class="delete-img-button"></i>');
        //             $parent.find('input[type=hidden]').remove();
        //         };
        //         reader.readAsDataURL(this.files[0]);
        //     }
        // });

        $(".upload-button").change(function () {
                var $parent = $(this).parent();
                var $img = $parent.find(".img-container");

                if (this.files && this.files[0]) {
                    var fr = new FileReader();
                    fr.onload = function (e) {
                        var data = fr.result;
                        var imgTag = $img.attr('style', 'background-image: url('+ data +')');
                        var image = new Image();
                        image.src = data;
                        image.onload = function() {
                            EXIF.getData(image, function() {
                                  var orientation = EXIF.getTag(this, "Orientation");
                                  switch(orientation) {
                                    case 3:
                                      imgTag.css('transform', 'rotate(180deg)');
                                      break;
                                    case 6:
                                      imgTag.css('transform', 'rotate(90deg)');
                                      break;
                                    case 8:
                                      imgTag.css('transform', 'rotate(-90deg)');
                                      break;
                                  }
                            });

                        $parent.find('.delete-img-button').remove();
                        $parent.append('<i class="delete-img-button"></i>');
                        $parent.find('input[type=hidden]').remove();

                    };
                    };
                    fr.readAsDataURL(this.files[0]);
                }
            });

        $('.add-home-app-form').on('click', '.delete-img-button', function (e) {

            var target = $(this).parent();
            target.find(".img-uploaded").attr("style", "background-image: url('/images/icons/upload-image.png')");
            target.find(".upload-button").val('');
            target.find('input[type=hidden]').remove();
            $(this).remove();
        });

        // add accessory handler
        $('#add-accessory-button').click(function (e) {
            var input_value = $('#add-accessory-input').val();
            if (!$('#add-accessory-input').val()) {
                alert("請輸入配件名稱!");
            } else {
                $('#add-accessory-input').val('');
                $('#accessory-list').append('<div class="row">'+
                                '<div class="col-xs-12 col-sm-2"></div>'+
                                '<div class="col-xs-12 col-sm-10">'+
                                    '<div class="accessory-item hp-form-group">'+
                                        '<i class="delete-accessory">X</i>'+
                                        '<input class="accessory-item__input" name="accessory[]" type="text"'+
                                               'value="'+input_value+'">'+
                                    '</div>'+
                                '</div></div>');

            }
        });

        $('.add-home-app-form').on('click', '.delete-accessory', function (e) {
            $(this).parent().remove();
        });

        $('#deleteDevice:enabled').on('click', function () {
            if (!confirm('確認是否刪除此電器?')) {
                return false;
            }
            $.post('/api/Master_api/deviceDelete', {'id':deviceId}, function (res) {
                if (res && res.valid) {
                    window.location.href = '/master/deviceList';
                    return ;
                }
                alert('刪除失敗');
            }, 'json');
        });

    });
</script>