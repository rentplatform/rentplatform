<!--main content-->
<div class="page-heading">
    <h2>電器清單</h2>
</div>
<div class="my-app-heading container">
    <div class="row">
        <div class="my-app_money-display col-xs-12 col-sm-8">
            <?php if (!$reward > 0): ?>
                <div class="my-app_money-display__icon icon_money icon-sprite"></div>
                <div class="my-app_money-display__txt">
                    <h3>電小孩已幫你賺進$<?= $reward ?>元囉!</h3>
                </div>
            <?php endif; ?>
        </div>
        <div class="button-add col-xs-12 col-sm-4">
            <a class="blue-btn-sm btn " href="<?= !$canEditDevice ? '/master/location' : '/master/deviceEdit'?>"
               <?= !$canEditDevice ? 'data-toggle="tooltip" data-placement="top" title="請先建立面交地點才能新增電器"': '' ?>
            >新增電器</a>
        </div>
    </div>
</div>
<div class="my-app-list container">
    <!-- 可出租 data-toggle="available" -->
    <!-- 暫停出租 data-toggle="unavailable"-->
    <!-- 出租中 data-toggle="renting"-->
    <?php foreach ($list as $row): ?>
        <div class="home-app-list__item row green-border-box">
            <div class="home-app-list__item__pic col-sm-3">
                <div class="img-container" style="background-image: url('/upload/<?= $row['filename'] ?>')"></div>
            </div>
            <div class="home-app-list__item__info col-sm-6">
				<a href="/device/detail/<?= $row['device_id'] ?>">
                	<div class="product-name"><?= $row['name'] ?></div>
				</a>
                <div class="display-f">
                    <div class="product-category"><?= $row['device_type_name'] ?></div>
                    <div class="product-status" data-toggle="<?= $row['status'] > 0 ? ($row['status'] > 1 ? 'available' : 'renting' ) : 'unavailable' ?>">
                        <?= $row['status'] > 0 ? ($row['status'] > 1 ? '出租中' : '可出租' ) : '暫停出租' ?>
                    </div>
                </div>
            </div>
            <div class="home-app-list__item__action col-xs-12 col-sm-3"><a class="green-btn-md btn" href="/master/deviceEdit/<?= $row['device_id'] ?>" type="button">編輯</a></div>
        </div>
    <?php endforeach; ?>
<!--    <div class="my-app-list home-app-list__item row green-border-box">-->
<!--        <div class="home-app-list__item__pic col-sm-3"><img src="/images/demo-img/app-2.jpg" alt="" img="img-responsive"></div>-->
<!--        <div class="home-app-list__item__info col-sm-6">-->
<!--            <div class="product-name">Dyson DC48 吸塵器</div>-->
<!--            <div class="display-f">-->
<!--                <div class="product-category">清潔打掃-->
<!--                </div>-->
<!--                <div class="product-status" data-toggle="renting">出租中</div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="home-app-list__item__action col-xs-12 col-sm-3"><a class="green-btn-md btn" href="my-settings-app_edite.html" type="button">編輯</a></div>-->
<!--    </div>-->
</div>

<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    })
</script>