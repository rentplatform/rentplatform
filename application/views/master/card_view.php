<?php
load_modal('phone_valid');
?>
<!--main content-->
<div class="page-heading">
    <h2>會員資料</h2>
</div>
<div class="container">
    <!--大頭像-->
    <div class="user-pic center-all">
        <div class="icon_user-profile"></div>
    </div>
    <div class="user-profile row">
        <div class="col-xs-12 col-sm-12">
            <!--狀態欄-->
            <div class="user-profile-data green-border-box">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="user-profile-data__nickname display-f">
                            <div class="data-title">暱稱:</div>
                            <div class="data-content"><?= $usercard['nickname'] ?></div>
                        </div>
                        <div class="user-profile-data__gendar display-f">
                            <div class="data-title">性別:</div>
                            <div class="data-content"><?= $usercard['sex'] > 1 ? '女' : '男' ?></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="user-profile-data__my-evaluation display-f">
                            <div class="data-title">我的評價:</div>
                            <div class="data-content product-evaluation display-f">
                                <div class="product-evaluation--good display-f">
                                    <div class="icon_positive icon-sprite"> </div>
                                    <div class="product-evaluation__num"><?= $usercard['good'] ?></div>
                                </div>
                                <div class="product-evaluation--bad display-f">
                                    <div class="icon_negative icon-sprite"></div>
                                    <div class="product-evaluation__num"><?= $usercard['bad'] ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="user-profile-data__my-status display-f">
                            <div class="data-title">我的狀態:</div>
                            <div class="data-content">正常</div>
                        </div>
                        <div class="user-profile-data__no-reply-num display-f">
                            <div class="data-title">未回應次數:</div>
                            <div class="data-content"><?= $usercard['miss'] ?></div>
                        </div>
                        <div class="user-profile-data__cancel-num display-f">
                            <div class="data-title">取消次數:</div>
                            <div class="data-content"><?= $usercard['reject'] ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--電器清單-->
        <div class="landlord-product-list col-xs-12 col-sm-12">
            <!-- 家電列表-->
            <div class="home-app-list">
                <!-- 可出租 data-toggle="available" -->
                <!-- 暫停出租 data-toggle="unavailable"-->
                <!-- 出租中 data-toggle="renting"-->
                <?php foreach ($deviceList as $row): ?>
                    <div class="home-app-list__item row green-border-box">
                        <div class="home-app-list__item__pic col-sm-2"><img src="/upload/<?= $row['filename'] ?>" alt="" img="img-responsive"></div>
                        <div class="home-app-list__item__info col-sm-7">
                            <div class="product-name"><a href="/device/detail/<?= $row['device_id'] ?>"><?= $row['name'] ?></a></div>
                            <div class="display-f">
                                <div class="product-category"><?= $row['device_type_name'] ?></div>
                                <div class="product-status" data-toggle="<?= $row['status'] > 0 ? ($row['status'] > 1 ? 'available' : 'renting' ) : 'unavailable' ?>">
                                    <?= $row['status'] > 0 ? ($row['status'] > 1 ? '出租中' : '可出租' ) : '暫停出租' ?>
                                </div>
                            </div>
                        </div>
                        <div class="home-app-list__item__action col-xs-12 col-sm-3"><a class="green-btn-md btn <?= $logged ? '' : 'loginRedirect' ?>" <?= $logged ? ($profile['phone_valid'] ? "href=\"/device/rent/{$row['device_id']}\"" : 'data-toggle="modal" data-target="#mobileAuth"') : "data-uri=\"/device/rent/{$row['device_id']}\"" ?>>租借</a></div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>