
<!--main content-->
<div class="page-heading">
    <h2>恭喜，您的電小孩要出門賺錢囉!</h2>
</div>
<div class="container width-800" id="rent-request_contact-tenant">
    <div class="block-title-lg">租客聯絡資訊</div>
    <div class="system-msg block-title-md red">請盡快聯絡對方喔!</div>
    <div class="rent-info green-border-box">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="rent-info__product row">
                    <div class="rent-info__product-pic col-xs-12 col-sm-5">
                        <img class="img-responsive" src="/upload/<?= $data['filename'] ?>" alt="<?= $data['device_name'] ?>" title="<?= $data['device_name'] ?>"></div>
                    <div class="rent-info__product-name col-xs-12 col-sm-7"><?= $data['device_name'] ?></div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <!--租客暱稱-->
<!--                <a class="product-tenant display-f">-->
                    <div class="product-tenant__icon icon-sprite"></div>
                    <ul class="product-tenant__detail">
                        <li><?= $renter['name'] ?></li>
                        <li><?= $renter['phone_number'] ?></li>
                        <li><?= $renter['username'] ?></li>
                    </ul>
<!--                </a>-->
                <!--摘要-->
                <div class="rent-result">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 display-f">
                            <div class="rent-result__rent-icon label_rent_term icon-sprite"></div>
                            <div class="rent-result__rent-info">
                                <div class="rent-date rent-info-item">
                                    <div class="title">借期:</div>
                                    <div class="content"><?= date('Y/m/d', strtotime($data['start_time'])) ?> ~ <?= date('Y/m/d', strtotime($data['end_time'])) ?> (共<?= $countDate ?>天)</div>
                                </div>
                                <div class="deal-location rent-info-item">
                                    <div class="title">希望面交地點:</div>
                                    <div class="content"><?= $data['placename'] ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 display-f">
                            <div class="rent-result__rent-icon label_rent_money icon-sprite"></div>
                            <div class="rent-result__rent-info">
                                <div class="rent-price rent-info-item">
                                    <div class="title">租金:</div>
                                    <div class="content"><?= $data['rent'] ?>元 (週間x<?= $normal ?> 週末x<?= $weekend ?>)</div>
                                </div>
                                <div class="rent-deposit rent-info-item">
                                    <div class="title">押金:</div>
                                    <div class="content"><?= $data['deposit'] ?>元 (歸還時退還)</div>
                                </div>
                            </div>
                        </div>
						<div class="col-xs-12 col-sm-12 display-f">
							<div class="rent-result__rent-icon label_rent_contract"></div>
							<div class="rent-result__rent-info">
								<div class="rent-info-item">
									<div class="title"> 瀏覽合約:</div>
									<div class="content">
										<span>若有需要可</span>
										<a class="link-txt-underline" href="/privacy/order?e=<?= $privacyStr ?>" target="_blank">瀏覽合約</a>
										<span>，列印一式雙份面交時攜帶</span>
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>