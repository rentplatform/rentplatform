<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>電電租 | 全國首創家電出租共享平台</title>
    <!--bootstrap v3.3.6-->
    <link rel="stylesheet" href="/vendors/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="/css/all.css">
    <!--js library-->
    <script src="/vendors/jquery/dist/jquery.min.js"></script>
    <script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/js/homeapp.js"></script>
</head>
<body>
<!--navigator-->
<header id="header">
    <div class="container" id="header__inner">
        <h1 id="nav__logo"><a href="index.html"><img class="img-responsive" src="images/icons/header__logo.png" alt=""></a></h1>
        <nav class="nav">
            <div class="nav__slogan"><img class="img-responsive hidden-xs" src="images/header__slogan--pc.png"><img class="img-responsive hidden-sm hidden-md hidden-lg" src="images/header__slogan--mobile.png"></div>
            <button class="nav__toggle navbar-toggle pull-right" type="button"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            <div class="nav__bg-mask">
                <div class="close-menu"></div>
            </div>
            <div class="collapse-bk">
                <div class="nav__list">
                    <div class="nav__list__login">
                    </div>
                    <div class="nav__list__item">
                        <a href="/device/search">要租家電</a>
                        <a class="current" href="/master/apply">成為電租公</a>
                        <a href="/article/list">電電租專欄</a>
                        <a href="/question">常見問題</a>
                        <a href="/aboutus">關於我們</a>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>
<!--main content-->
<div class="page-heading">
    <h2>找不到網頁</h2>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="block-title-md">Hello 你迷路了嗎?</div>
        </div>
        <div class="col-sm-12">
            <div class="center-all" style="margin-top: 5%;"><img class="img-responsive" src="/images/icons/icon-404.png" alt="" style="max-width: 230px;"></div>
        </div>
        <div class="col-sm-12">
            <div class="hp-button-block center-all-pc"><a class="green-btn-md btn" href="/">回首頁</a></div>
        </div>
    </div>
</div>
<!--footer-->
<footer id="footer">
    <div class="container">
        <div class="footer__menu">
            <a href="/brand">品牌合作</a>
            <a href="/service">服務條款</a>
            <a href="/privacy">隱私權政策</a>
            <a href="/user/arbitration">意見反應</a>
        </div>
        <div class="footer__contact-info__outer row">
            <div class="footer__contact-info col-xs-12 col-sm-6">
                <div class="footer__contact-info__logo"><a href="#"><img class="img-responsive" src="/images/icons/header__logo.png" alt=""></a></div>
                <div class="footer__contact-info__list">
                    <ul>
                        <li>02-27123335</li>
                        <li><a href="mailto:service@homeapp123.com">service@homeapp123.com</a></li>
                        <li>台北市松山區南京東路4段1號11樓</li>
                    </ul>
                </div>
            </div>
            <div class="footer__fb col-xs-12 col-sm-6 hidden-xs">
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FHomeApp123%2F&tabs=timeline&width=450&height=200&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1481386098811399" width="450" height="200" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
            </div>
        </div>
        <div class="footer_copyright"><span>©電電租股份有限公司 2017 All right reserved.</span></div>
    </div>
</footer>
</body>
</html>