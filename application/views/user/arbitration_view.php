
<!--main content-->
<div class="page-heading">
    <h2>意見反應</h2>
</div>
<div class="container width-800">
    <div class="main-block">
        <div class="block-title-md" style="margin-bottom: 35px;">請填寫訂單編號和您的問題，電電租客服將儘速協助您處理。</div>
        <form id="" class="formValidation" method="post" action="/api/User_api/arbitration" data-msg="意見反應已送出">
            <filedset>
                <div class="hp-form-group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-2 col-md-2">
                            <label class="control-label">訂單編號:</label>
                        </div>
                        <div class="col-xs-12 col-sm-10 col-md-10">
                            <input class="form-content-green-border" type="text" name="urid" required>
                        </div>
                    </div>
                </div>
                <div class="hp-form-group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-2 col-md-2">
                            <label class="control-label">意見事由:</label>
                        </div>
                        <div class="col-xs-12 col-sm-10 col-md-10">
                            <textarea class="form-content-green-border" name="text" cols="250" rows="8" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="hp-button-block">
                    <div class="row">
                        <div class="col-xs-12 col-sm-2 col-md-2"></div>
                        <div class="col-xs-12 col-sm-10 col-md-10">
                            <button class="green-btn-md btn" type="submit">送出</button>
                        </div>
                    </div>
                </div>
            </filedset>
        </form>
    </div>
</div>