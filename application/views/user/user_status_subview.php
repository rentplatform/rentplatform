
<!--狀態欄-->
<div class="user-profile-data green-border-box">
    <div class="user-profile-data__my-evaluation display-f">
        <div class="data-title">我的身份:</div>
        <div class="data-content product-evaluation display-f">
            <div class="product-evaluation--good display-f">
                <div class="product-evaluation__num"><?= !$profile['phone_valid'] ? '基本會員' : ($profile['rent_master'] ? '租公' : '租客') ?></div>
            </div>
        </div>
    </div>
    <?php if ($profile['rent_master'] > 0 && $profile['ad_percent'] > 0 && strtotime($profile['ad_percent_date']) > time()): ?>
        <div class="user-profile-data__my-discount display-f">
            <div class="data-title">優惠狀態:</div>
            <div class="data-content">手續費<?= $profile['ad_percent'] ?>%優惠（至<?= date('Y/m/d', strtotime($profile['ad_percent_date'])) ?>）</div>
        </div>
    <?php endif; ?>
    <div class="user-profile-data__my-evaluation display-f">
        <div class="data-title">我的評價:</div>
        <div class="data-content product-evaluation display-f">
            <div class="product-evaluation--good display-f">
                <div class="icon_positive icon-sprite"> </div>
                <div class="product-evaluation__num"><?= $profile['good'] ?? 0 ?></div>
            </div>
            <div class="product-evaluation--bad display-f">
                <div class="icon_negative icon-sprite"></div>
                <div class="product-evaluation__num"><?= $profile['bad'] ?? 0 ?></div>
            </div>
        </div>
    </div>
    <div class="user-profile-data__my-status display-f">
        <div class="data-title">我的狀態:</div>
        <div class="data-content">正常</div>
    </div>
    <?php if ($profile['rent_master'] > 0): ?>
        <div class="user-profile-data__no-reply-num display-f">
            <div class="data-title">未回應次數:</div>
            <div class="data-content"><?= $noresponse ?></div>
        </div>
        <div class="user-profile-data__cancel-num display-f">
            <div class="data-title">取消次數:</div>
            <div class="data-content"><?= $cancel ?></div>
        </div>
    <?php endif; ?>
</div>