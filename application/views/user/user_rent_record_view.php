<?php load_modal('assessment', 'rent_cancel') ?>
<!--main content-->
<div class="page-heading">
    <h2>租借記錄</h2>
</div>
<div class="container">
    <div class="rent-record">
        <!--tab-->
        <div class="rent-record__tab center-all">
            <ul role="tablist">
                <li class="<?= $type == '0' ? 'active' : '' ?>" role="presentation"><a href="/user/rentRecord"
                                                                                       aria-controls="tenant-record">租借記錄</a>
                </li>
                <li class="<?= $type == '1' ? 'active' : '' ?>" role="presentation"><a href="/user/rentRecord/1"
                                                                                       aria-controls="landlord-record">出借記錄</a>
                </li>
            </ul>
        </div>
        <!--tab content-->
        <div class="hp-table-container tab-content">
            <?php if (empty($data)): ?>
                <div class="tab-pane active fade in active" id="tenant-record">
                    <table class="hp-table <?= $type < 1 ? 'blue' : 'green' ?>-table">
                        <thead>
                        <tr class="hidden">
                            <th>主旨</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="detail">
                            <td data-th="主旨">目前沒有紀錄</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            <?php else: ?>
                <?php foreach ($data as $row): ?>
                    <!--租借記錄-->
                    <div class="tab-pane active fade in active" id="tenant-record">
                        <!--日期-->
                        <div class="date">
                            <i class="icon_calendar icon-sprite"></i>
                            <?= $row['text'] ?>
                        </div>
                        <table class="hp-table <?= $type < 1 ? 'blue' : 'green' ?>-table">
                            <thead>
                            <tr class="">
                                <th>訂單編號</th>
                                <th><?= $type > 0 ? '租客暱稱' : '電租公暱稱' ?></th>
                                <th>家電名稱</th>
                                <th>狀態</th>
                                <th>我的評價</th>
                                <th>我給的評價</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($row['list'] as $k => $row): ?>
                                <tr class="detail" data-status="<?= $type == 0 && in_array($row['status'], [2]) && $row['renter_assessment'] < 1 || $type == 1 && in_array($row['status'], [2]) && $row['owner_assessment'] < 1 ? '' : 'already-reply' ?>">
                                    <td data-th="訂單編號"><a href="/privacy/order?e=<?= $row['privacyStr'] ?>"><?= $row['ur_id'] ?></a></td>
                                    <td data-th="電租公暱稱"><?= $row['nickname'] ?></td>
                                    <td data-th="家電名稱"><?= $row['device_name'] ?></td>
                                    <td data-th="狀態">
                                        <?= $row['statustxt'] ?>
                                        <?php if ($row['status'] < 1 && $type < 1): ?>
                                            <button class="green-btn-sm btn cancelBtn" type="button" data-id="<?= $row['ur_id'] ?>">取消</button>
                                        <?php elseif ($row['status'] < 1 && $type > 1): ?>
                                            <button class="green-btn-sm btn" type="button" onclick="javascript:window.location.href='/master/rentRequest/<?= $row['ur_id'] ?>'">前往</button>
                                        <?php elseif ($type > 0 && in_array($row['status'], [1, 6])): ?>
                                            <button class="green-btn-sm btn" type="button" onclick="javascript:window.location.href='/master/contactRenter/<?= $row['ur_id'] ?>'">查看</button>
                                        <?php endif; ?>

                                    </td>
                                    <td data-th="我的評價">
                                        <?php
                                            if ((int)$type === 1 && in_array($row['status'], [2])) {
                                                if ($row['renter_assessment'] > 0) {
                                                    $class = $row['renter_assessment'] > 1 ? 'icon_negative' : 'icon_positive';
                                                    echo "<span class=\"icon-sprite {$class}\"></span>";
                                                    echo "<span class=\"icon_msg icon-sprite showAccessment \" data-type=\"{$row['renter_assessment']}\" data-comment=\"{$row['renter_comment']}\"></span>";
                                                } else {
                                                    echo '等待評價';
                                                }
                                            } elseif ((int)$type === 0 && in_array($row['status'], [2])) {
                                                if ($row['owner_assessment'] > 0) {
                                                    $class = $row['owner_assessment'] > 1 ? 'icon_negative' : 'icon_positive';
                                                    echo "<span class=\"icon-sprite {$class}\"></span>";
                                                    echo "<span class=\"icon_msg icon-sprite showAccessment \"  data-type=\"{$row['owner_assessment']}\" data-comment=\"{$row['owner_comment']}\"></span>";
                                                } else {
                                                    echo '等待評價';
                                                }
                                            } else {
                                                echo '-';
                                            }
                                        ?>
                                    </td>
                                    <td class="js-vote-block vote deep-blue-table" data-th="我給的評價">
                                        <span class="js-vote-button <?= $type == 0 && in_array($row['status'], [2]) && $row['renter_assessment'] < 1 || $type == 1 && in_array($row['status'], [2]) && $row['owner_assessment'] < 1 ? 'setAccessment' : ''  ?>"
                                            data-id="<?= $row['ur_id'] ?>">
                                            請給評價
                                        </span>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <!--頁數-->
        <div class="center-all">
            <?= $pageHtml ?>
        </div>
    </div>
</div>
<script>
    $(function() {
        $('.setAccessment').on('click', function () {
            $('#vote-window').find('input[name=urid]').val($(this).data('id'));
            $('#vote-window').find('textarea').val('').removeAttr('disabled');
            $('#vote-window .evaluate-button').removeClass('selected not');
            $('#vote-window .good').addClass('selected');
            $('#vote-window .good input').prop('checked', true);
            $('#vote-window button[type=submit]').show();
            $('#vote-window button[type=button]').text('取消');
            $('#vote-window').modal('show');
        });
        $('.showAccessment').on('click', function () {
            var type = $(this).data('type');
            $('#vote-window .evaluate-button').removeClass('selected').addClass('not');
            if (type > 1) {
                $('#vote-window .good').addClass('selected');
            } else {
                $('#vote-window .bad').addClass('selected');
            }
            $('#vote-window textarea').val($(this).data('comment')).attr('disabled', 'disabled');
            $('#vote-window button[type=submit]').hide();
            $('#vote-window button[type=button]').text('關閉');
            $('#vote-window').modal('show');
        });

        $('.cancelBtn').on('click', function() {
            var id = $(this).data('id');
            $('#cancel-order').find('input[name=id]').val(id);
            $('#cancel-order').modal('show');
        })

    });
</script>