
<!--訊息通知的訊息變更如下：-->
<!---->
<!--租借訊息-->
<!--(僅文字訊息，不可連結)XXX 租公拒絕接單通知 > 帶入原因( 該交易轉為交易取消 )-->
<!--(僅文字訊息，不可連結) XXX 租公同意租借 XXX電器 通知，交易單號：0000000(交易單號)-->
<!--(僅文字訊息，不可連結)準備可以租用 XXX電器，請準備赴約，交易單號：0000000(交易單號)  *-->
<!--(僅文字訊息，不可連結)XXX 電器租用期快到了，請準備歸還，交易單號：0000000(交易單號)-->
<!--(僅文字訊息，不可連結)XXX 租公 給 0000000(交易單號) 評價了-->
<!--(僅文字訊息，不可連結)XXX 租公 對 0000000(交易單號) 提出了申訴，如有疑慮請直接聯繫客服人員 *-->
<!---->
<!--出借訊息-->
<!--有租客想要租借你的 XXX 電器囉，交易單號：0000000(交易單號)   > 點擊操作回覆流程-->
<!--(僅文字訊息，不可連結)要準備出租XXX電器囉，請準備赴約，交易單號：0000000(交易單號) *-->
<!--(僅文字訊息，不可連結)XXX 電器租用期快到了，請記得跟租客聯繫收回，交易單號：0000000(交易單號)-->
<!--(僅文字訊息，不可連結)租客 給 0000000(交易單號) 評價了-->
<!--(僅文字訊息，不可連結)租客 對 0000000(交易單號) 提出了申訴，如有疑慮請直接聯繫客服人員  *-->

<!--main content-->
<div class="page-heading">
    <h2>訊息通知</h2>
</div>
<div class="container">
    <div id="news-center">
        <!--tab-->
        <div class="rent-record__tab center-all">
            <ul role="tablist">
                <li class="<?= $type == '0' ? 'active' : '' ?>" role="presentation"><a href="/user/notifications" aria-controls="tenant-record" >租借訊息</a></li>
                <li class="<?= $type == '1' ? 'active' : '' ?>" role="presentation"><a href="/user/notifications/1" aria-controls="landlord-record" >出借訊息</a></li>
            </ul>
        </div>
        <!--tab content-->
        <div class="hp-table-container tab-content">
            <?php if (empty($data)): ?>
                <div class="tab-pane active fade in active" id="tenant-record">
                    <table class="hp-table <?= $type < 1 ? 'blue' : 'green' ?>-table">
                        <thead>
                        <tr class="hidden">
                            <th>主旨</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr class="detail">
                                <td data-th="主旨">目前沒有訊息</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            <?php else: ?>
                <?php foreach ($data as $row): ?>
                    <!--租借記錄-->
                    <div class="tab-pane active fade in active" id="tenant-record">
                        <!--日期-->
                        <div class="date">
                            <i class="icon_calendar icon-sprite"></i>
                            <?= $row['text'] ?>
                        </div>
                        <table class="hp-table <?= $type < 1 ? 'blue' : 'green' ?>-table">
                            <thead>
                            <tr class="hidden">
                                <th>主旨</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($row['list'] as $k => $content): ?>
                                    <tr class="detail">
                                        <td data-th="主旨"> <a href="<?= $content['id'] && !$content['status'] ? '/master/rentRequest/' . $content['id'] : 'javascript:void(0)' ?>" title="[系統訊息] <?= $content['content'] ?>">[系統訊息] <?= $content['content'] . ($content['status'] ? ($content['status'] === 3 ? '(未回應)' : '(已回覆)') : '') ?></a></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <!--頁數-->
        <div class="center-all">
            <?= $pageHtml ?>
        </div>
    </div>
</div>