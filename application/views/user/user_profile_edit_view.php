
<!--main content-->
<div class="page-heading">
    <h2>編輯會員資料</h2>
</div>
<div class="container" id="user-progfile-edite">
    <!--大頭像-->
    <div class="user-pic center-all">
        <div class="icon_user-profile"></div>
    </div>
    <div class="user-profile row">
        <div class="col-xs-12 col-sm-5 col-sm-push-7">
            <!--狀態欄-->
            <?php require_once 'user_status_subview.php'; ?>
        </div>
        <!--基本資料-->
        <div class="user-profile-basic-info col-xs-12 col-sm-7 col-sm-pull-5">
            <form class="user-profile-form formValidation" method="POST" action="/api/User_api/editProfile" data-redirect="/user/profile">
                <div class="user-profile-basic-info__list">
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <label class="control-label">姓名:</label>
                            </div>
                            <div class="col-xs-12 col-sm-8">
                                <input class="form-content-green-border" type="text" name="name" value="<?= $profile['name'] ?>" rangelength="2,10" required>
                            </div>
                        </div>
                    </div>
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <label class="control-label">性別:</label>
                            </div>
                            <div class="col-xs-12 col-sm-8">
                                <label class="gender radio-inline" for="gender">
                                    <input type="radio" name="sex" value="1" <?= (int)$profile['sex'] === 1 ? 'checked' : '' ?>>男
                                </label>
                                <label class="gender radio-inline" for="gender">
                                    <input type="radio" name="sex" value="2" <?= (int)$profile['sex'] === 2 ? 'checked' : '' ?>>女
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <label class="control-label">暱稱:</label>
                            </div>
                            <div class="col-xs-12 col-sm-8">
                                <input class="form-content-green-border" type="text" name="nickname" value="<?= $profile['nickname'] ?>" rangelength="2,10" required>
                            </div>
                        </div>

                    </div>
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <label class="control-label">身分證字號:</label>
                            </div>
                            <div class="col-xs-12 col-sm-8">
                                <input class="form-content-green-border" type="text" name="id_number" value="<?= $profile['id_number'] ?>" required>
                            </div>
                        </div>
                    </div>

                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <label class="control-label">E-mail:</label>
                            </div>
                            <div class="col-xs-12 col-sm-8">
                                <?= $profile['username'] ?>
                            </div>
                        </div>


                    </div>
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <label class="control-label">設定密碼:</label>
                            </div>
                            <div class="col-xs-12 col-sm-8">
                                <input class="form-content-green-border" id="passwordInput" type="password" name="password" value="">
                            </div>
                        </div>
                    </div>
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <label class="control-label">重複輸入密碼:</label>
                            </div>
                            <div class="col-xs-12 col-sm-8">
                                <input class="form-content-green-border" type="password" value="" equalTo="#passwordInput">
                            </div>
                        </div>
                    </div>
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <label class="control-label">手機:</label>
                            </div>
                            <div class="col-xs-12 col-sm-8">
                                <?php if ($profile['phone_valid'] > 0): ?>
                                    <?= $profile['phone_number'] ?>
                                <?php else: ?>
                                    <input class="form-content-green-border" type="text" name="phone_number" value="<?= $profile['phone_number'] ?>" maxlength="15" phone="true" required>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <label class="control-label">地址:</label>
                            </div>
                            <div class="col-xs-12 col-sm-8">
                                <input class="form-content-green-border" type="text" name="address" value="<?= $profile['address'] ?>">
                            </div>
                        </div>
                    </div>
                    <div class="hp-form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <label class="control-label">Line ID:</label>
                            </div>
                            <div class="col-xs-12 col-sm-8">
                                <input class="form-content-green-border" type="text" name="line_id" value="<?= $profile['line_id'] ?>">
                            </div>
                        </div>
                    </div>
                    </ul>
                    <div class="hp-button-block clearfix mutiple-btn"><a class="green-btn-md btn jsBack" href="/user/profile">取消</a>
                        <button class="green-btn-md btn" type="submit">儲存</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>