<!--main content-->
<div class="page-heading">
    <h2>成為電租公</h2>
</div>
<div class="container">
    <h3 class="block-title-md">我要如何成為電租公？</h3>
    <div class="how-to-become-landlord row">
        <div class="col-xs-12 col-sm-6">
            <div class="how-to-become-landlord__pic"><img class="img-responsive" src="/images/icons/process-rentout.png" alt=""></div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="how-to-become-landlord__txt block__normal-content">
                首先您要上架您的家電在電電租平台，我們提供詳細的線上合約讓租客完成線上合約及完整的電租公養成影音，讓您輕鬆透過閒置家電創造被動收入。

            </div>
            <div class="how-to-become-landlord__button">
				<a class="btn green-btn-md <?= $logged ? '' : 'loginRedirect' ?>" <?= !empty($profile) && $profile['rent_master'] > 0 ? 'href="/master/deviceList"' : ($logged ? 'href="/master/validCredit"' : 'data-uri="/master/validCredit"') ?>>加入電租公</a>
			</div>
        </div>
    </div>
    <h3 class="block-title-md">電租公評價</h3>
    <div class="landlord-recommend row">
        <?php foreach ($masterGood as $row): ?>
            <div class="landlord-recommend__article col-xs-12 col-sm-4">
                <div class="landlord-recommend__pic"><img class="img-responsive" src="/upload/<?= $row['pic'] ?>" alt=""></div>
                <div class="landlord-recommend__txt break-word">
                    <h4 class="blcok-title--blue"><?= $row['title'] ?></h4>
                    <p class="block__normal-content"><?= $row['content'] ?></p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>