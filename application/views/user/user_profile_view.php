
<!--main content-->
<div class="page-heading">
    <h2>會員資料</h2>
</div>
<div class="container">
    <!--大頭像-->
    <div class="user-pic center-all">
        <div class="icon_user-profile"></div>
    </div>
    <div class="user-profile row">
        <div class="col-xs-12 col-sm-6 col-sm-push-6">
            <!--狀態欄-->
            <?php require_once 'user_status_subview.php'; ?>
        </div>
        <!--基本資料-->
        <div class="user-profile-basic-info col-xs-12 col-sm-6 col-sm-pull-6">
            <ul class="user-profile-basic-info__list">
                <li>
                    <label>姓名:</label>
                    <p><?= $profile['name'] ?></p>
                </li>
                <li>
                    <label>性別:</label>
                    <p><?= (int)$profile['sex'] === 1 ? '男' : '女' ?></p>
                </li>
                <li>
                    <label>暱稱:</label>
                    <p><?= $profile['nickname'] ?></p>
                </li>
                <li>
                    <label>身分證字號:</label>
                    <p><?= $profile['id_number'] ?></p>
                </li>
                <li>
                    <label>E-mail:</label>
                    <p><?= $profile['username'] ?></p>
                </li>
                <li>
                    <label>設定密碼:</label>
                    <p>*************</p>
                </li>
                <li>
                    <label>重複輸入密碼:</label>
                    <p> </p>
                </li>
                <li>
                    <label>手機:</label>
                    <p><?= $profile['phone_number'] ?></p>
                </li>
                <li>
                    <label>地址:</label>
                    <p><?= $profile['address'] ?? '' ?></p>
                </li>
                <li>
                    <label>Line ID:</label>
                    <p><?= $profile['line_id'] ?? '' ?></p>
                </li>
            </ul>
            <div class="hp-button-block clearfix"><a class="green-btn-md btn" href="/user/editProfile">編輯</a></div>
        </div>
    </div>
</div>