<?php

/**
 *
 */
class Ext_Controller extends CI_Controller {

    protected $mOutput = array();
    protected $mShowAd = TRUE;

    public function __construct() {
        parent::__construct();
        $this->mOutput['logged'] = $this->session->userdata('isLogged') === true;
        $this->mOutput['profile'] = $this->session->userdata('profile') ? : [];
        $this->mOutput['setting'] = new params\Websetting_params();
    }

    /**
     * 檢查登入，未登入則導回首頁
     * @param bool $return 是否需要回傳或直接導頁
     * @return bool
     */
    protected function checkLogin($return = false) {
        if (!$this->mOutput['logged']) {
            if ($return) {
                return false;
            }
            redirect('/');
        }
        return true;
    }

    /**
     * 暫時沒用到
     * @param $url
     * @param $msg
     */
    protected function msgRedirect($url, $msg) {
        $this->mOutput['url'] = $url;
        $this->mOutput['msg'] = $msg;
        $this->loadView('other/msg_redirect');
    }

    /**
     * 載入頁面
     */
    protected function loadView($view) {
        header('content-type:text/html;charset=UTF-8');

        $this->load->view('inc/header_view.php', $this->mOutput);
        $this->load->view("{$view}_view.php", $this->mOutput);
        $this->load->view('inc/footer_view.php', $this->mOutput);
    }

//    protected function error(string $msg, string $redirect = '/') {
//        $this->mOutput['msg'] = $msg;
//        $this->mOutput['url'] = $redirect;
//        $this->loadView('other/error');
//        die;
//    }

    /**
     * 載入分頁功能
     */
    protected function loadPagenation($url, $pageCount, $limit = 10) {
        $this->load->library('pagination');
        $page = $pageCount % $limit === 0 ? $pageCount / $limit : floor($pageCount / $limit) + 1;
        $config = array();
        $config['base_url'] = $url;
        //page count
        $config['total_rows'] = $pageCount;
        //data count of one page
        $config['per_page'] = $limit;
        $config['num_links'] = $this->agent->is_mobile() ? 1 : 3;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = true;
        $config['next_link'] = '>';
        $config['prev_link'] = '<';
        $config['first_link'] = '1';
        $config['last_link'] = $page;
        $config['full_tag_open'] = '<div class="pagination center-all display-f"><ul>';
        $config['full_tag_close'] = '</ul></div>';
//        $config['first_tag_open'] = '<div href="#" class="jump">';
//        $config['first_tag_close'] = '</div>';
        $config['first_tag_open'] = '<li class="hide">';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="hide">';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a class="current"> ';
        $config['cur_tag_close'] = '</a></li>';
//        $config['num_tag_open'] = '<span class="text_pc">';
//        $config['num_tag_close'] = '</span>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $this->mOutput['pageHtml'] = $this->pagination->create_links();
    }

}
