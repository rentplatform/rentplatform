<?php
require_once 'PHPMailer5.2.4/class.phpmailer.php';

class Mail_sender {

    private $mMailer;

    function __construct() {
        $this->mMailer = new PHPMailer(true);
        $this->mMailer->CharSet = 'UTF-8';
        $this->mMailer->Encoding = 'quoted-printable';
        $this->mMailer->IsSMTP();
        $this->mMailer->Host = "smtp.gmail.com";
        $this->mMailer->Port = 465;
        $this->mMailer->SMTPAuth = true;
        $this->mMailer->Username = "service@homeapp123.com";
        $this->mMailer->Password = "arkfree2016";
        $this->mMailer->From = "service@homeapp123.com";
        $this->mMailer->FromName = "service";
//        $this->mMailer->SMTPDebug = 2;
        $this->mMailer->SMTPSecure = 'ssl';
        $this->SetHtml();
    }

    public function AddReceiver($receiver) {
        $this->mMailer->AddAddress($receiver);
    }

    public function AddBCC($receiver) {
        $this->mMailer->AddBCC($receiver);
    }

    public function SetMessage($message) {
        $this->mMailer->Body = $message;
    }

    public function SetSubject($subject) {
        $this->mMailer->Subject = '=?UTF-8?B?' . base64_encode($subject) . '?=';
    }

    public function SetFrom($email) {
        $this->mMailer->From = $email;
    }

    public function SetFromName($name) {
        $this->mMailer->FromName = $name;
    }

    public function SetHtml() {
        $this->mMailer->IsHTML(TRUE);
    }

    public function ResetHtml() {
        $this->mMailer->IsHTML(FALSE);
    }

    public function Send() {
        $res = $this->mMailer->Send();
        $this->mMailer->ClearAddresses();
        return $res;
    }

}