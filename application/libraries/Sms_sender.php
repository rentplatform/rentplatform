<?php

require_once "Http_request.php";

/**
 * 發送簡訊類別
 *
 * @author eric
 * @create 2013/06/28
 */
class Sms_sender {

    private $mCi;

    /**
     * 帳號密碼
     */
    private $mAccount = '0901272983';
    private $mPasswd = 'Mijudy123';

    /**
     * @param string $mAlertNumber 餘額不足時，通知對象的手機號碼，以逗號隔開!
     */
    private $mAlertNumber = '';

    /**
     * @param string $mSendSMSUrl 簡訊傳送url
     * @param string $mGetCreditUrl 查詢餘額url
     * @param string $mGetStatus 查詢簡訊發送狀態url
     */
    private $mSendSMSUrl = "http://api.every8d.com/API21/HTTP/sendSMS.ashx";
    private $mGetCreditUrl = "http://api.every8d.com/API21/HTTP/getCredit.ashx";
    private $mGetStatus = "http://api.every8d.com/API21/HTTP/getDeliveryStatus.ashx";


    function __construct() {
        $this->mCi = &get_instance();
    }

    /**
     * 發送簡訊
     *
     * @param string $msg 簡訊傳送內容
     * @param string $phoneNumber 電話號碼
     * @param string $sendTime 簡訊預定發送時間 ， ex: yyyyMMddHHmnss 20130131153000
     */
    public function sendMsg($msg, $phoneNumber, $sendTime = '') {
//        $balance = $this->getCredit();
//        if ($balance <= 8.0) {
//            throw new Exception('餘額不足!');
//        }

//        if ($balance <= 10.0 && $balance > 8.0) {
//            $this->sendMsgToAdmin();
//            return;
//        }

        $this->sendMsgToUser($msg, $phoneNumber, $sendTime);
    }

    private function sendMsgToUser($msg, $phoneNumber, $sendTime = '') {
        try {
            $data = array(
                'UID' => $this->mAccount,
                'PWD' => $this->mPasswd,
                'SB' => '',
                'MSG' => $msg,
                'DEST' => $phoneNumber,
                'ST' => $sendTime,
                'url' => $this->mSendSMSUrl
            );

            $return_msg = $this->getRequest($data);
            $this->logWrite($return_msg);
        } catch (Exception $e) {
            write_log('sms_error', $e->getMessage());
        }
    }

    private function sendMsgToAdmin() {
        $data = array(
            'UID' => $this->mAccount,
            'PWD' => $this->mPasswd,
            'SB' => '',
            'MSG' => '提醒您，電電租平台簡訊餘額不足10元，請盡速補儲!謝謝。',
            'DEST' => $this->mAlertNumber,
            'ST' => '',
            'url' => $this->mSendSMSUrl
        );

        if ($this->mAlertNumber === '') {
            return ;
        }

        $return_msg = $this->getRequest($data);
        $this->logWrite($return_msg);
    }

    private function logWrite($logMsg) {
        $msg_ary = explode(',', $logMsg);
        $msg_ary = array(
            'balance' => $msg_ary[0],
            'sended' => $msg_ary[1],
            'cost' => $msg_ary[2],
            'unsend' => $msg_ary[3],
            'bid' => $msg_ary[4]
        );
        $status_ary = $this->getStatus($msg_ary['bid']);
        foreach ($status_ary as $row) {
            $this->mCi->db->insert('sms_log', array(
                'bid' => $msg_ary['bid'],//220478cc-8506-49b2-93b7-2505f651c12e
                'phone_number' => $row['number'],
                'status' => $row['status_id'],
                'cost' => $row['cost'],
                'unsend' => $msg_ary['unsend'],
                'send_date' => $row['send_date']
            ));
        }
    }

    /**
     * 取得簡訊傳送狀態
     *
     * @param string $bid 發送代碼
     */
    private function getStatus($bid) {
        $data = array(
            'UID' => $this->mAccount,
            'PWD' => $this->mPasswd,
            'BID' => $bid,
            'PNO' => '',
            'url' => $this->mGetStatus
        );
        do {
            $return_msg = $this->getRequest($data);
            $msg_ary = explode("\t", $return_msg);
        } while ($msg_ary[0] <= '0');
        $msg_list = array();
        for ($s = 1; $s <= (count($msg_ary) - 4); $s += 4) {
            $msg_list[] = array(
                'number' => $msg_ary[$s],
                'send_date' => $msg_ary[$s + 1],
                'cost' => $msg_ary[$s + 2],
                'status_id' => $msg_ary[$s + 3]
            );
        }
        return $msg_list;
    }

    /**
     * 取得帳號餘額
     */
    public function getCredit() {
        $data = array(
            'UID' => $this->mAccount,
            'PWD' => $this->mPasswd,
            'url' => $this->mGetCreditUrl
        );
        $return_msg = $this->getRequest($data);
        return $return_msg;
    }

    private function getRequest($data) {
        $http_request = new Http_request($data);
        return $http_request->sendRequest('GET', 'return');
    }

}

?>
