<?php
/**
 * CodeIgniter PDF Library
 *
 * Generate PDF's in your CodeIgniter applications.
 *
 * @package            CodeIgniter
 * @subpackage        Libraries
 * @category        Libraries
 * @author            Chris Harvey
 * @license            MIT License
 * @link            https://github.com/chrisnharvey/CodeIgniter-PDF-Generator-Library
 */
include_once(__DIR__ . '/tcpdf/tcpdf.php');

class Pdf extends TCPDF {
    function __construct() {
        parent::__construct(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

//        $this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        $this->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
        $this->SetHeaderMargin(0);
        $this->SetFooterMargin(0);

        // set image scale factor
        $this->setImageScale(1.3);

        // set some language-dependent strings (optional)
//        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
//            require_once(dirname(__FILE__).'/lang/eng.php');
//            $this->setLanguageArray($l);
//        }

        // ---------------------------------------------------------

        // set font
        $this->SetFont('DroidSansFallback', '', 10);
    }
}