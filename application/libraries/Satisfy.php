<?php

/**
 * 滿足類別，檢查各類参數是否符合
 * 
 */
class Satisfy {

    /**
     * realname
     * @param string $name
     * @return bool
     */
    public function name($name) {
        $realname = str_replace(' ', '', $name);
        if (mb_strlen($realname, 'UTF-8') > 10) {
            return false;
        }

        if (mb_strlen($realname, 'UTF-8') < 2 && !empty($name)) {
            return false;
        }

        return true;
    }

    /**
     * 檢查 email 格式
     * 
     * @param   string  $email 
     * @return	bool
     */
    public function email($email) {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        return true;
    }

    /**
     * 檢查是否重覆帳號
     * 
     * @param   string  $username 
     * @return	bool
     */
    public function noDuplicateUsername($username) {
        $ci = &get_instance();
        $ci->load->model('User_model');
        if ($ci->User_model->checkUsername($username)) {
            return false;
        }

        return true;
    }

    /**
     * 檢查性別
     * 
     * @param   string  $sex 
     * @return	bool
     */
    public function sex($sex) {
        if ($sex != 1 && $sex != 2) {
            return false;
        }

        return true;
    }

    /**
     * 檢查手機
     * 
     * @param   string  $phone 
     * @return	bool
     */
    public function phone($phone) {
        if (count(preg_grep('/^\d+$/', array($phone))) == 0) {
            return false;
        }

        return true;
    }

    /**
     * 檢查身分證
     * @param $number
     * @return	bool
     */
    public function idnumber($number) {
        $number = ucfirst($number);
        if (!preg_match("/[A-Z]{1}\d{9}/",$number)){
            return false;
        }

        return true;
    }

    /**
     * 驗證是否都為數字
     * @param $string
     * @return bool
     */
    public function number($string) {
        if (count(preg_grep('/^\d+$/', array($string))) == 0) {
            return false;
        }

        return true;
    }

}
