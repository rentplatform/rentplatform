<?php

/**
 */
class Data_api extends ApiBase {

    public function getCityList() {
        $this->checkData('cid:number');
        $this->load->model('Common_model');
        $city = $this->Common_model->getCityList($this->mData['cid']);
        if (empty($city)) {
            $this->error();
        }
        $this->success(array(
            'list' => $city
        ));
    }
}