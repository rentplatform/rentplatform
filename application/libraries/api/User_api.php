<?php

/**
 * 帳號相關API
 *
 * @property User_model userModel
 * @property Mail_sender mail_sender
 * @property Msg_template msg_template
 */
class User_api extends ApiBase {

    public function __construct() {
        parent::__construct();
        $this->load->model('User_model', 'userModel');
    }

    /**
     * 註冊
     */
    public function register() {
        $this->checkData('name:name', 'nickname:name', 'username:email', 'password', 'sex:sex', 'id_number:idnumber', 'phone_number:phone', array('address', 'line_id'));
        if (!$this->satisfy->noDuplicateUsername($this->mData['username'])) {
            $this->error(array('msg' => 'email已註冊過'));
        }
        $uid = $this->userModel->register($this->mData);
        if (!$uid) {
            $this->error();
        }

        $sign = md5($uid . $this->mData['username'] . '_mail_valid_sign');
        $this->userModel->insertUserValidSign($uid, $sign);

        $this->load->library('Msg_template');
        $mail_content = $this->msg_template->register($uid, $this->mData['name'], $sign);

        $this->load->library('Mail_sender');
        $this->mail_sender->AddReceiver($this->mData['username']);
        $this->mail_sender->SetSubject(REGISTER_MAIL_TITLE);
        $this->mail_sender->SetMessage($mail_content);
        $this->mail_sender->send();

        $this->session->set_userdata(array(
            'valid_mail_data' => array(
                'uid' => $uid,
                'content' => $mail_content,
                'username' => $this->mData['username']
            )
        ));

        $this->success();
    }

    /**
     * 重寄信箱驗證碼
     */
    public function resendMailValid() {
        $valid_mail_data = $this->session->userdata('valid_mail_data');
        if (empty($valid_mail_data)) {
            $this->error();
        }
        if (!empty($valid_mail_data['time']) && (time() - $valid_mail_data['time']) < 300) {
            $this->error(array('msg' => '請於5分鐘後再試!'));
        }

        $valid_mail_data['time'] = time();
        $this->session->set_userdata('valid_mail_data', $valid_mail_data);

        $this->load->library('Mail_sender');
        $this->mail_sender->AddReceiver($valid_mail_data['username']);
        $this->mail_sender->SetSubject(REGISTER_MAIL_TITLE);
        $this->mail_sender->SetMessage($valid_mail_data['content']);
        $this->mail_sender->send();

        $this->success();
    }

    /**
     * 發送驗證手機簡訊
     */
    public function validPhone() {
        $this->checkLogin();
        $this->checkData('phone_number:phone');
        $this->load->helper('string');
        $userprofile = $this->session->userdata('profile');
        if (empty($userprofile) || $userprofile['phone_valid'] > 0) {
            $this->error();
        }
        $valid_phone_data = $this->session->userdata('valid_phone_data');
        if (!empty($valid_phone_data) && (time() - $valid_phone_data['time']) < 900) {
            $this->error(array('msg' => '請於15分鐘後再試!'));
        }
        $sign = random_string('numeric', 6);
        $this->session->set_userdata('valid_phone_data', array(
            'phone' => $this->mData['phone_number'],
            'uid' => $userprofile['uid'],
            'sign' => $sign,
            'time' => time()
        ));

        $this->userModel->insertUserValidSign($userprofile['uid'], $sign);

        $this->load->library('Msg_template');
        $message = $this->msg_template->message($userprofile['name'], $sign);

        $this->load->library('Sms_sender');
        $this->sms_sender->sendMsg($message, $this->mData['phone_number']);

        $this->success();
    }

    /**
     * 驗證會員輸入的手機驗證碼，並更新會員資料的手機驗證為已驗證、手機號碼
     */
    public function validPhoneSign() {
        $this->checkLogin();
        $this->checkData('sign:number');
        $valid_phone_data = $this->session->userdata('valid_phone_data');
        if (empty($valid_phone_data)) {
            $this->error();
        }
        if ($this->mData['sign'] !== $valid_phone_data['sign']) {
            $this->error(array('驗證碼輸入錯誤'));
        }
        //修改會員資料的手機為已驗證
        $this->userModel->updateProfile($valid_phone_data['uid'], array(
            'phone_number' => $valid_phone_data['phone'],
            'phone_valid' => 1
        ));
        $this->userModel->deleteValidSignByUid($valid_phone_data['uid']);
        $this->setLogin($valid_phone_data['uid']);
        $this->success();
    }

    /**
     * 登入
     */
    public function login() {
        $this->checkData('username', 'password', array('uid'));
        $user = $this->userModel->checkLogin($this->mData['username'], $this->mData['password']);
        if (empty($user)) {
            $this->error(array('msg' => '帳號或密碼錯誤'));
        }

        if ((int)$user['email_valid'] === 0) {
            $sign = md5($user['uid'] . $user['username'] . '_mail_valid_sign');
            $this->userModel->insertUserValidSign($user['uid'], $sign);

            $this->load->library('Msg_template');
            $mail_content = $this->msg_template->register($user['uid'], $user['name'], $sign);

            $this->session->set_userdata(array(
                'valid_mail_data' => array(
                    'uid' => $user['uid'],
                    'content' => $mail_content,
                    'username' => $user['username']
                )
            ));
            $this->error(array('msg' => '信箱尚未驗證成功，無法登入', 'redirect' => '/register/success'));
        }

        if ((int)$user['status'] > 0) {
            switch ((int)$user['status']) {
                case 1:
                case 2:
                    $locktime = date('Y-m-d H:i', strtotime($user['lock_time']));
                    $msg = "帳號因為部分原因停權中，將於 {$locktime} 恢復，如有問題可聯絡客服人員";
                    break;
                case 3:
                    $msg = '帳號已永久停權，如有問題可聯絡客服人員';
                    break;
                case 4:
                    $msg = '查無此帳號';
                    break;
            }
            if ($msg ?? false) {
                $this->error(array('msg' => $msg));
            }
        }

        //如果是信箱驗證回來後做的登入，刪除相關session和資料表驗證sign
        if (isset($this->mData['uid']) && $user['uid'] == $this->mData['uid']) {
            $this->session->set_userdata('first_login', true);
            $this->session->unset_userdata('valid_mail_data');
            $this->userModel->deleteValidSignByUid($user['uid']);
        }

        $this->setLogin($user['uid']);
        $this->success();
    }

    /**
     * 修改個人資料
     */
    public function editProfile() {
        $this->checkLogin();
        $this->checkData(array('name:name', 'nickname:name', 'password', 'sex:sex', 'id_number:idnumber', 'phone_number:phone', 'address', 'line_id'));
        $uid = $this->session->userdata('profile')['uid'];
        $this->userModel->updateProfile($uid, $this->mData);
        //更新session內容
        $this->setLogin($uid, false);
        $this->success();
    }

    /**
     * 租借申請
     */
    public function rentRequest() {
        $this->checkLogin();
        $this->checkData('start', 'end', 'location', 'device_id');
        $this->load->model('Device_model');
        $this->load->model('Rent_model');
        $this->load->model('Notification_model');
        $this->load->model('User_model');

        $user = $this->session->userdata('profile');
        $device = $this->Device_model->getDevice($this->mData['device_id']);
        if (empty($device)) {
            $this->error(['msg' => '裝置不存在']);
        }

        if (!$user['phone_valid']) {
            $this->error(['msg' => '抱歉，尚未驗證電話無法租借']);
        }

        if ((int)$device['uid'] === (int)$user['uid']) {
            $this->error(array('msg' => '無法租借自己的電器'));
        }

        if ($this->Rent_model->checkRentDateValid($this->mData['device_id'], $this->mData['start'], $this->mData['end'])) {
            $this->error(['msg' => '抱歉, 所選日期已有人使用!']);
        }

        //定義開始與結束日期是一星期中的哪一天
        $startd = date('N', strtotime($this->mData['start']));
        $endd = date('N', strtotime($this->mData['end']));

        //天數
        $datecount = count_date($this->mData['start'], $this->mData['end']);
        if (!in_array($startd, [2, 6]) || !in_array($endd, [1, 5]) || $datecount < 2) {
            $this->error(array('msg' => '日期區間不合法'));
        }

        $setting = new \params\Websetting_params();
        $rentamount = 0;
        //caculate rent amount
        for ($i = 1;$i<=$datecount;$i++) {
            $_d = new DateTime($this->mData['start']);
            $_d->add(new DateInterval("P{$i}D"));
            switch ($_d->format('N')) {
                case 1:
                    $rentamount += $setting->getWeekendRent();
                    break;
                case 5:
                    $rentamount += $setting->getNormalRent();
                    break;
            }
            unset($_d);
        }

        $master = $this->User_model->getProfile($device['uid']);
        $ad_percent = !$setting->isUseGlobalAd() && $master['ad_percent'] > 0 && (strtotime($master['ad_percent_date']) > time()) ? $master['ad_percent'] : $setting->getAdPercent();
        $ad_fee = $rentamount * ($ad_percent / 100);

        //新增訂單
        $urid = $this->Rent_model->insertRentOrder(array(
            'renter_uid' => $user['uid'],
            'device_owner' => $device['uid'],
            'device_id' => $device['device_id'],
            'tp_id' => $this->mData['location'],
            'device_name' => $device['name'],
            'device_accessory' => $device['accessory'],
            'rent' => $rentamount,
            'deposit' => $device['deposit'],
            'adfee' => $ad_fee,
            'reward' => ($rentamount - $ad_fee),
            'start_time' => $this->mData['start'],
            'end_time' => $this->mData['end'],
        ));


        if (!$urid) {
            $this->error(['msg' => '新增失敗，如有問題請聯絡客服人員']);
        }

        //發送通知訊息給租公
        $this->Notification_model->insert($device['uid'], sprintf('有租客想要租借你的 %s 電器囉，交易單號：%s', $device['name'], $urid), 1, $urid);

        //build mail content
        $this->load->library('Msg_template');
        $mail = $this->msg_template->rentRequest($device['realname'], $urid);

        //send mail
        $this->load->library('Mail_sender');
        $this->mail_sender->AddReceiver($device['username']);
        $this->mail_sender->SetSubject(RENT_REQUEST_MAIL_TITLE);
        $this->mail_sender->SetMessage($mail);
        $this->mail_sender->send();

        $this->success([
            'id' => $urid
        ]);
    }

    public function rentCancel() {
        $this->checkLogin();
        $this->checkData('reason', 'txt', 'id');
        $this->load->model('Rent_model');
        $data = $this->Rent_model->getRentRequestOrder($this->mData['id']);
        if (empty($data)) {
            $this->error();
        }
        $uid = (int)$this->session->userdata('profile')['uid'];
        if ($data['renter_uid'] != $uid) {
            $this->error();
        }
        $save = [
            'reason' => $this->mData['reason'],
            'status' => RENT_RECORD_STATUS['RENT_CANCEL']
        ];
        if ($save['reason'] === 'other') {
            $save['reason'] = $this->mData['txt'] ? : '其他';
        }

        $this->Rent_model->updateRecord($this->mData['id'], $save);

        $this->success();
    }

    /**
     * 新增評價
     */
    public function setAssessment() {
        $this->checkLogin();
        $this->checkData('urid:number', 'type:number', 'comment');
        if (!in_array($this->mData['type'], [1,2])) {
            $this->error();
        }
        $this->load->model('Rent_model');
        $this->load->model('Notification_model');
        $data = $this->Rent_model->getRentRequestOrder($this->mData['urid']);
        if (empty($data)) {
            $this->error();
        }

        $uid = (int)$this->session->userdata('profile')['uid'];
        $vals = [];
        $key = $this->mData['type'] > 1 ? 'bad' : 'good';
        if ((int)$data['renter_uid'] === $uid) {
            $vals = [
                'renter_assessment' => $this->mData['type'],
                'renter_comment' => $this->mData['comment']
            ];
            //租客 給 0000000(交易單號) 評價了
            $this->Notification_model->insert($data['device_owner'], sprintf('租客 給 交易單號:%s 評價了', $data['ur_id']), 1);
            $this->db->query("update user set {$key} = {$key} + 1 where uid = {$data['device_owner']}");
        } elseif ((int)$data['device_owner'] === $uid) {
            $vals = [
                'owner_assessment' => $this->mData['type'],
                'owner_comment' => $this->mData['comment']
            ];
            $this->Notification_model->insert($data['renter_uid'], sprintf('%s 租公 給 交易單號:%s 評價了', $data['device_owner_name'], $data['ur_id']));
            $this->db->query("update user set {$key} = {$key} + 1 where uid = {$data['renter_uid']}");
        } else {
            $this->error();
        }

        $this->Rent_model->updateRecord($this->mData['urid'], $vals);
        $this->success();
    }

    /**
     * 新增申訴
     */
    public function arbitration() {
        $this->checkLogin();
        $this->checkData('urid', 'text');

        $this->load->model('Rent_model');
        $this->load->model('Notification_model');
        $data = $this->Rent_model->getRentRequestOrder($this->mData['urid']);
        if (empty($data)) {
            $this->error();
        }
        if ($data['status'] < 1) {
            $this->error(['msg' => '未接單訂單無法反應']);
        }
        $arbitration = $this->Rent_model->getArbitrationData($data['ur_id']);
        if (!empty($arbitration)) {
            $this->error(['msg' => ($arbitration['status'] > 0 ? '此單已反應過，無法二次反應，如有疑慮請洽客服人員' : '此單已進入處理流程, 電電租客服將儘速協助您處理')]);
        }
        $uid = (int)$this->session->userdata('profile')['uid'];
        if ((int)$data['renter_uid'] === $uid) {
            $this->Notification_model->insert($data['device_owner'], sprintf('租客 對 交易單號:%s 提出了意見反應，如有疑慮請直接聯繫客服人員', $data['ur_id']), 1);
        } elseif ((int)$data['device_owner'] === $uid) {
            $this->Notification_model->insert($data['renter_uid'], sprintf('%s 租公 對 交易單號:%s 提出了意見反應，如有疑慮請直接聯繫客服人員', $data['device_owner_name'], $data['ur_id']));
        } else {
            $this->error();
        }
        $vals = [
            'ur_id' => $data['ur_id'],
            'uid' => $uid,
            'reason' => $this->mData['text']
        ];
        $this->Rent_model->insertArbitration($vals);
        $this->Rent_model->updateRecord($data['ur_id'], [
            'status' => RENT_RECORD_STATUS['ARBITRATION']
        ]);

        $this->success();
    }

    /**
     * 設定登入狀態
     */
    private function setLogin($uid, $resetTime = true) {
        $Profile = $this->userModel->getProfile($uid);
        if ($resetTime) {
            $this->userModel->updateLastLoginTime($uid);
        }
        $this->session->set_userdata(array(
            'isLogged' => TRUE,
            'uid' => $uid,
            'profile' => $Profile
        ));
    }

}
