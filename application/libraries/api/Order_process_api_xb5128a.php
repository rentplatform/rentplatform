<?php

/**
 * server 自動執行腳本
 * 更新租借訂單紀錄、發信通知
 *
 * @property Rent_model rent
 * @property Notification_model notifi
 * @property Device_model device
 * @property Msg_template msg_template
 * @property Mail_sender mail_sender
 */
class Order_process_api_xb5128a extends ApiBase {

    private $mLimit = 5000;

    public function __construct() {
        set_time_limit(0);
        parent::__construct();
        $this->load->model('Device_model', 'device');
        $this->load->model('Rent_model', 'rent');
        $this->load->model('Notification_model', 'notifi');
        $this->load->library('Msg_template');
        $this->load->library('Mail_sender');
    }

    public function run() {
        try {
            $this->checkWaitingOrder();
            $this->checkProcessOrder();
            $this->checkRentingOrder();
            $this->success();
        } catch (Exception $e) {
            write_log('processFail', $e->getMessage());
            write_log('processFail', $e->getTraceAsString());
        }
    }

    /**
     * 檢查所有等待租公接單的訂單，超過3天未接單狀態改為漏單並發送漏單通知
     * @param int $page
     */
    private function checkWaitingOrder($page = 1) {
        $data = $this->getOrder(RENT_RECORD_STATUS['WAIT'], $page);
        if (empty($data)) {
            return;
        }
        $today = date('Ymd');
        foreach ($data as $row) {
            if (count_date($row['create_time'], $today) > 3) {
                //狀態轉漏單
                $this->rent->updateRecord($row['ur_id'], [
                    'status' => RENT_RECORD_STATUS['MISS']
                ]);
                //時間到前一天給租客租公發送通知
                $this->notifi->insert($row['renter_uid'], sprintf('您想租的電器：%s 申請已取消。原因： 租公逾時未回覆', $row['device_name']));
                $this->notifi->insert($row['device_owner'], '您有一筆租用訊息超過三天未回覆，系統已自動取消該筆交易，如有疑問請洽客服人員', 1);
            }
        }
        $this->checkWaitingOrder(++$page);

    }

    /**
     * 檢查所有待開始的租借訂單(租公已接單)，更改狀態為租借中，或是發送通知.email提醒即將交易
     * @param int $page
     */
    private function checkProcessOrder($page = 1) {
        $data = $this->getOrder(RENT_RECORD_STATUS['IN_PROCESS'], $page);
        if (empty($data)) {
            return;
        }
        $today = date('Ymd');
        foreach ($data as $row) {
            $start = date('Ymd', strtotime($row['start_time']));
            $before_start = new DateTime($row['create_time']);
            $before_start->sub(new DateInterval("P1D"));
            if ($start <= $today) {
                //狀態轉租借中
                $this->rent->updateRecord($row['ur_id'], [
                    'status' => RENT_RECORD_STATUS['RENTING']
                ]);
                //電器狀態改為租借中
                $this->device->updateDevice($row['device_id'], ['status' => DEVICE_STATUS['RENTING']]);
            } else if ($before_start->format('Ymd') == $today) {
                //時間到前一天給租客租公發送通知
                $this->notifi->insert($row['renter_uid'], sprintf('準備可以租用 %s，請準備赴約，交易單號： %s', $row['device_name'], $row['ur_id']));
                $this->notifi->insert($row['device_owner'], sprintf('要準備出租 %s 囉，請準備赴約，交易單號： %s', $row['device_name'], $row['ur_id']), 1);

                $order = $this->rent->getRentRequestOrder($row['ur_id']);
                //寄信
                $renter_mail = $this->msg_template->noticeStartRentingForRenter($order);
                $this->mail_sender->AddReceiver($order['username']);
                $this->mail_sender->SetSubject(RENT_NOTICE_START_TITLE);
                $this->mail_sender->SetMessage($renter_mail);
                $this->mail_sender->send();

                $master_mail = $this->msg_template->noticeStartRentingForMaster($order);
                $this->mail_sender->AddReceiver($order['device_owner_username']);
                $this->mail_sender->SetSubject(RENT_NOTICE_START_TITLE);
                $this->mail_sender->SetMessage($master_mail);
                $this->mail_sender->send();
            }
        }
        $this->checkProcessOrder(++$page);
    }

    /**
     * 檢查所有租借中訂單，更改狀態為結案或是發送通知.email提醒交易時間即將到期
     * @param int $page
     */
    private function checkRentingOrder($page = 1) {
        $data = $this->getOrder(RENT_RECORD_STATUS['RENTING'], $page);
        if (empty($data)) {
            return;
        }
        $yestoday = date('Ymd', strtotime('-1 days'));
        $today = date('Ymd');
        foreach ($data as $row) {
            $end = date('Ymd', strtotime($row['end_time']));
            $before_end = new DateTime($row['end_time']);
            $before_end->sub(new DateInterval("P1D"));
            if ($end < $today) {
                //狀態轉結案
                $this->rent->updateRecord($row['ur_id'], [
                    'status' => RENT_RECORD_STATUS['COMPLETE']
                ]);
                //電器狀態改為可租借
                $this->device->updateDevice($row['device_id'], ['status' => DEVICE_STATUS['OPEN']]);
            } else if ($before_end->format('Ymd') == $yestoday) {
                $order = $this->rent->getRentRequestOrder($row['ur_id']);
                //時間到前一天給租客租公發送通知
                $this->notifi->insert($row['renter_uid'], sprintf(' %s 租用時間即將到期，請準備赴約返還電器，交易單號： %s', $row['device_name'], $row['ur_id']));
                $this->notifi->insert($row['device_owner'], sprintf('%s 租用時間即將到期，請準備赴約收回電器，交易單號： %s', $row['device_name'], $row['ur_id']), 1);
                //寄信
                $renter_mail = $this->msg_template->noticeEndRentingForRenter($order);
                $this->mail_sender->AddReceiver($order['username']);
                $this->mail_sender->SetSubject(RENT_NOTICE_END_TITLE);
                $this->mail_sender->SetMessage($renter_mail);
                $this->mail_sender->send();

                $master_mail = $this->msg_template->noticeEndRentingForMaster($order);
                $this->mail_sender->AddReceiver($order['device_owner_username']);
                $this->mail_sender->SetSubject(RENT_NOTICE_END_TITLE);
                $this->mail_sender->SetMessage($master_mail);
                $this->mail_sender->send();
            }
        }
        $this->checkProcessOrder(++$page);
    }

    /**
     * @param $status 0待接單 1租借中 2結案 3租供未回應(漏單) 4租公拒絕 5租客取消 6租公已接單(預約時間未到) 7申訴中
     * @return array
     */
    private function getOrder($status, $page) {
        //交易狀態, 0待接單1租借中2結案3租供未回應(漏單)4租公拒絕5租客取消6租公已接單(預約時間未到)7申訴中
        return $this->db
            ->where('status', $status)
            ->limit($this->mLimit, ($page - 1) * $this->mLimit)
            ->get('rent_record')
            ->result_array();
    }

}