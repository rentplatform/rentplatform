<?php

/**
 * server 自動執行腳本
 * 更新會員狀態
 *
 */
class User_process_api_xb5321a extends ApiBase {

    public function __construct() {
        set_time_limit(0);
        parent::__construct();
    }

    public function run() {
        try {
            $this->checkUserStatus();
            $this->checkPrivacyOrder();
            $this->success();
        } catch (Exception $e) {
            write_log('processFail', $e->getMessage());
            write_log('processFail', $e->getTraceAsString());
        }
    }

    /**
     * 檢查所有被停權會員狀態是否到期
     */
    private function checkUserStatus() {
        $this->db->query('update user set status = 0, lock_time = null where status in (1,2) and lock_time <= CURDATE()');
    }

    /**
     * 檢查合約檔案是否過期並刪除
     */
    private function checkPrivacyOrder() {
        if ($handle = opendir(FILE_PATH)) {
            $today = date('Ymd');
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    list($filename, $ext) = explode('.', $entry);
                    list($orderId, $str, $date) = explode('_', $filename);
                    if ($today > $date && count_date(date('Y-m-d'), $date) > 7) {
                        unlink(FILE_PATH . $entry);
                    }
                }
            }
            closedir($handle);
        }
    }

}