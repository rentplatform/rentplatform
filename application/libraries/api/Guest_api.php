<?php

/**
 * @property User_model userModel
 */
class Guest_api extends ApiBase {

    public function __construct() {
        parent::__construct();
        $this->load->model('User_model', 'userModel');
    }

    public function validResetPwdMail() {
        $this->checkData('email:email');
        $user = $this->userModel->getProfileByMail($this->mData['email']);
        if (empty($user)) {
            $this->error(array('msg' => '查無此會員'));
        }

        $valid_pwd_data = $this->session->userdata('valid_pwd_data');
        if (empty($valid_pwd_data)) {
            $valid_pwd_data = array('user' => $user);
        } elseif (!empty($valid_pwd_data['time']) && (time() - $valid_pwd_data['time']) < 300) {
//            $this->error(array('msg' => '請於5分鐘後再試!'));
        }

        $this->load->library('Msg_template');
        $this->load->library('Mail_sender');
        $this->load->helper('string');

        $valid_pwd_data['time'] = time();
        $this->session->set_userdata('valid_pwd_data', $valid_pwd_data);

        //產生驗證碼
        $sign = random_string('numeric', 4);
        $this->userModel->insertUserValidSign($user['uid'], $sign);

        //產生信件內容
        $mail_content = $this->msg_template->forgetPwd($user['name'], $sign);

        //寄信
        $this->mail_sender->AddReceiver($user['username']);
        $this->mail_sender->SetSubject(FORGET_PWD_MAIL_TITLE);
        $this->mail_sender->SetMessage($mail_content);
        $this->mail_sender->send();

        $this->success();
    }

    public function resetPwd() {
        $this->checkData('uid:number', 'sign', 'password');
        if (!$this->userModel->checkUserSign($this->mData['uid'], $this->mData['sign'])) {
            $this->error(array('msg' => '驗證碼輸入錯誤'));
        }
        $this->userModel->updateProfile($this->mData['uid'], array(
            'password' => $this->mData['password']
        ));
        $this->session->unset_userdata('valid_pwd_data');

        $this->success();
    }


}