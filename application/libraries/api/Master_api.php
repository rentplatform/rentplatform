<?php

/**
 * @property Master_model $master
 * @property Device_model $device
 * @property File $file
 */
class Master_api extends ApiBase {

    private $mUid;

    public function __construct() {
        parent::__construct();
        $this->load->model('Master_model', 'master');
        $this->load->model('Device_model', 'device');
        $this->checkLogin();
        $this->mUid = $this->session->userdata('uid');
        $profile = $this->session->userdata('profile');
        if ($profile['rent_master'] < 1) {
            $this->error();
        }
    }

    /**
     * 新增或編輯電器
     * @param int $deviceId
     */
    public function deviceEdit(int $deviceId = 0) {
        $this->checkData('dt_id:number', 'bid:number', 'name', 'status:number', 'deposit:number', 'description', 'notice', array('accessory', 'oldpic'));
        $this->mData['accessory'] = !empty($this->mData['accessory']) ? json_encode($this->mData['accessory']) : '[]';
        if ($this->mData['status'] > 2) {
            $this->error(array('msg' => '狀態錯誤'));
        }
        $locationlist = $this->master->getLocationList($this->mUid);
        if (count($locationlist) < 1) {
            $this->error(['msg' => '請先建立面交地點才能新增電器']);
        }
        $this->load->model('Rent_model');
        if ($this->mData['status'] < 1 && $this->Rent_model->checkOrderStatusByDevice($deviceId)) {
            $this->error(['msg' => '電器租借中或有待租借單子無法編輯狀態']);
        }

        $this->mData['uid'] = $this->mUid;
        $keepfiles = $this->mData['oldpic'] ?? array();
        unset($this->mData['oldpic']);

        $this->db->trans_begin();
        //$deviceId > 0 => 編輯狀態,檢查電器是否存在並且是擁有者,狀態也非出租中
        if ($deviceId) {
            $device = $this->device->getDevice($deviceId);
            if (empty($device) || $device['uid'] != $this->mUid || $device['status'] > 1) {
                $this->error();
            }
            $this->device->updateDevice($deviceId, $this->mData);
        } else {
            if (empty($_FILES['file'])) {
                $this->error();
            }
            $deviceId = $this->device->insertDevice($this->mData);
        }


        $oldpics = $this->device->getDevicePic($deviceId);
        //刪掉舊的被刪除的圖，必須有舊的圖被保留或是有新的圖上傳!才會執行刪除舊圖動作
        if (!empty($oldpics) && ((!empty($keepfiles) && count($oldpics) != count($keepfiles)) || !empty($_FILES['file']))) {
            foreach ($oldpics as $row) {
                if (file_exists(PHOTO_PATH . $row['filename']) && array_search($row['filename'], $keepfiles) === false) {
                    $this->device->deleteDevicePic($deviceId, $row['filename']);
                    unlink(PHOTO_PATH . $row['filename']);
                }
            }
        }

        //圖片處理
        if (!empty($_FILES['file'])) {
            $this->load->library('File');
            list($uploadResult, $fileName, $errMsg) = $this->file->upload();
            if (!$uploadResult || empty($fileName['file'])) {
                $this->db->trans_rollback();
                $this->error(array(
                    'msg' => $errMsg
                ));
            }
            $dataset = array_map(function ($v) use($deviceId) {
                return array(
                    'device_id' => $deviceId,
                    'filename' => $v
                );
            }, $fileName['file']);
            //寫入新上傳的圖
            $this->device->insertDevicePic($dataset);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->error(['msg' => '新增失敗, 請聯絡客服人員']);
        }
        $this->db->trans_commit();

        $this->success();
    }

    public function deviceDelete() {
        $this->checkData('id:number');
        $device = $this->device->getDevice($this->mData['id']);
        if (empty($device)) {
            $this->error(['msg' => '電器不存在']);
        }
        $this->load->model('Rent_model');
        if ($this->Rent_model->checkOrderStatusByDevice($this->mData['id'])) {
            $this->error(['msg' => '使用中電器無法刪除']);
        }
        $this->device->deleteDevice($this->mData['id']);
        //刪除電器圖片
        foreach ($device['pic'] as $row) {
            if (file_exists(PHOTO_PATH . $row['filename'])) {
                unlink(PHOTO_PATH . $row['filename']);
            }
        }

        $this->success();

    }

    public function rejectRent() {
        $this->checkData('reason', 'urid:number', array('reasontxt'));
        $reason = $this->mData['reason'];
        if ($reason === 'other') {
            $reason = $this->mData['reasontxt'];
        }
        if (empty($reason)) {
            $this->error();
        }

        $this->load->model('Rent_model');
        $data = $this->Rent_model->getRentRequestOrder($this->mData['urid']);
        if (empty($data) || $this->mUid != $data['device_owner']) {
            $this->error();
        }

        $this->Rent_model->updateRecord($this->mData['urid'], [
            'status' => 4,
            'reason' => $reason
        ]);

        /**
         * 寄拒絕信給租客和發送通知
         */
        $this->load->model('Notification_model');
        $this->Notification_model->insert($data['renter_uid'], sprintf('%s 拒絕接單通知, 原因: %s', $data['device_name'], $reason));

        //build mail content
        $this->load->library('Msg_template');
        $mail = $this->msg_template->rentFail($data['name'], $data['device_name'], $data['device_owner_nickname'], $reason);

        //send mail
        $this->load->library('Mail_sender');
        $this->mail_sender->AddReceiver($data['username']);
        $this->mail_sender->SetSubject(RENT_FAIL_MAIL_TITLE);
        $this->mail_sender->SetMessage($mail);
        $this->mail_sender->send();

        $this->success();
    }

    public function locationInsert() {
        $this->checkData('placename', 'cid', 'ct_id', 'address');
        $this->mData['uid'] = $this->mUid;
        $this->master->insertLocation($this->mData);
        $this->success();
    }

    public function locationDelete() {
        $this->checkData('tp_id');
        $location = $this->master->getLocation($this->mData['tp_id']);
        if ($this->mUid != $location['uid']) {
            $this->error();
        }
        $user_location_list = $this->master->getLocationList($this->mUid);
        if (count($user_location_list) < 2) {
            $this->error(['msg' => '至少需保留一筆面交點']);
        }
        if ($this->master->checkPlaceIsUsingByRenting($this->mData['tp_id'])) {
            $this->error(['msg' => '此面交點正在使用中無法刪除']);
        }
        $this->master->deleteLocation($this->mData['tp_id']);
        $this->success();

    }

}