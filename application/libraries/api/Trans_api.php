<?php
require_once __DIR__ . "/../Http_request.php";

use params\Trad_params;

/**
 * 綠界交易API
 * 後台帳密
 * https://vendor.ecpay.com.tw/
 *      easyrentapp123
 *      Mijudy2017
 *
 * @property Trans_model transModel
 * @property User_model user
 * @property Rent_model rent
 */
class Trans_api extends ApiBase {

    const TRANS_URL = array(
        'test' => 'https://payment-stage.ecpay.com.tw/Cashier/AioCheckOut/V5',
        'production' => 'https://payment.ecpay.com.tw/Cashier/AioCheckOut/V5',
        'resetOrder' => 'https://payment.ecpay.com.tw/CreditDetail/DoAction'
    );

    /**
     * 合作特店編號
     */
    const MERCHANT_ID = '3038253';

    /**
     * mac value 鑰匙
     */
    const HASHKEY = 'MOBddNi7BPP1m0hx';
    const HASHIV = '3Htu8XAIONrCB7Y0';

    const TRAD_TYPE = array(
        'success' => 1,
        'fail' => 2
    );

    private $userProfile;

    public function __construct() {
        parent::__construct();
        $this->load->model('Trans_model', 'transModel');
        $this->load->model('User_model', 'user');
        $this->load->model('Rent_model', 'rent');
        $this->userProfile = $this->session->userdata('profile');
    }

    /**
     * 驗證信用卡API
     */
    public function valid() {
        try {
            //未登入或已驗證者導回首頁
            if (!$this->checkLogin(true) || $this->userProfile['rent_master'] > 0) {
                redirect('/');
                die;
            }
            $params = new Trad_params();
            $params->amount = 30;
            $params->item = '信用卡驗證';
            $params->desc = '測試是否持有有效信用卡';
            $params->returnUrl = WEBSITE . '/master/validCreditSuccess';
            $params->isValid = 1;

            $this->trad($params);

        } catch (Exception $ex) {
            show_404();
        }
    }

    /**
     * 租公接單付款API
     */
    public function orderPay(int $urId = 0) {
        try {
            if (!$this->checkLogin(true) || $this->userProfile['rent_master'] < 1) {
                redirect('/');
                die;
            }
            $order = $this->rent->getRentRequestOrder($urId);
            if (empty($order) || $order['status'] > 0 || $order['device_owner'] != $this->userProfile['uid']) {
                redirect('/');
                die;
            }
            $params = new Trad_params();
            $params->urId = $order['ur_id'];
            $params->amount = $order['adfee'];
            $params->item = $order['device_name'];
            $params->desc = 'id';
            $params->returnUrl = WEBSITE . '/master/rentSuccess';

            $this->trad($params);

        } catch (Exception $e) {
            show_404();
        }
    }

    /**
     * 接收綠界交易結果
     * 回覆格式為: 狀態(1成功,0失敗)|訊息
     * @todo 是否只允許綠界對外IP
     *    175.99.72.1
     *    175.99.72.11
     *    175.99.72.24
     *    175.99.72.28
     *    175.99.72.32
     */
    public function tradReturn() {
        try {
            $paramsName = array(
                'MerchantID', 'MerchantTradeNo', 'StoreID', 'RtnCode', 'RtnMsg', 'TradeNo',
                'TradeAmt', 'PaymentDate', 'PaymentType', 'PaymentTypeChargeFee', 'TradeDate', 'SimulatePaid', 'CheckMacValue',
                'CustomField1', 'CustomField2', 'CustomField3', 'CustomField4'
            );
            write_log('transaction', $this->mData);
            $data = array();
            //檢查傳入參數
            foreach ($paramsName as $k => $key) {
                if (!isset($this->mData[$key])) {
                    throw new Exception("缺少 {$key} 資訊");
                }
                $data[$key] = $this->mData[$key];
            }

            $macvalue = $data['CheckMacValue'];
            unset($data['CheckMacValue']);
            if ($macvalue !== $this->getMacvalue($data)) {
                throw new Exception('檢查碼錯誤');
            }

            if (!$this->transModel->checkOrderExist($data['MerchantTradeNo'])) {
                throw new Exception('訂單不存在或已處裡完成');
            }

            //更新訂單狀態
            $isupdate = $this->transModel->update($data['MerchantTradeNo'], array(
                'tradno' => $data['TradeNo'],
                'rtncode' => $data['RtnCode'],
                'rtnmsg' => $data['RtnMsg'],
                'payfee' => $data['PaymentTypeChargeFee'],
                'status' => (int)$data['RtnCode'] === 1 ? self::TRAD_TYPE['success'] : self::TRAD_TYPE['fail']
            ));

            if (!$isupdate) {
                throw new Exception('訂單更新失敗');
            }

            //判斷訂單是否為交易成功
            if ((int)$data['RtnCode'] === 1) {
                $orderdata = $this->transModel->getOrder($data['MerchantTradeNo']);
                if ($orderdata['is_valid'] > 0) {
                    $vals = [
                        'credit_card_valid' => 1,
                        'rent_master' => 1
                    ];
                    $this->load->model('Common_model');
                    $ad_discount = $this->Common_model->getDiscountPricePercent();
                    //判斷有無廣告優惠
                    if (!empty($ad_discount)) {
                        //計算廣告優惠到期時間
                        $arrival = new DateTime();
                        $arrival->add(new DateInterval("P{$ad_discount['days']}D"));

                        $vals['ad_percent'] = $ad_discount['percent'];
                        $vals['ad_percent_date'] = $arrival->format('Y-m-d H:i:s');
                        //參加人數加1
                        $this->db->query('update ad_price_list set count = count + 1 where ad_id = ?', $ad_discount['ad_id']);
                    }

                    //信用卡驗證訂單，成功後修改會員資料的信用卡驗證欄位為已驗證、身分改為租公、廣告優惠趴數
                    $this->user->updateProfile($orderdata['uid'], $vals);
                } else {
                    $order = $this->rent->getRentRequestOrder($orderdata['ur_id']);
                    //租公付款完成後更改狀態為已接單
                    $this->rent->updateRecord($order['ur_id'], [
                        'status' => RENT_RECORD_STATUS['IN_PROCESS']
                    ]);

                    //更新電器租借次數
                    $this->load->model('Device_model');
                    $this->load->model('Notification_model');
                    $this->Device_model->updateDeviceRentTime($order['device_id']);

                    //增加租客租借次數
                    $this->db->query('update user set rent_time = rent_time + 1 where uid = ?', $order['renter_uid']);
                    //增加租公出租次數
                    $this->db->query('update user set master_time = master_time + 1 where uid = ?', $order['renter_uid']);
                    //增加面交點使用次數
                    $this->db->query('update transaction_place set usetimes = usetimes + 1 where tp_id = ?', $order['tp_id']);

                    //寄信通知租客
                    $this->load->library('Msg_template');
                    $this->load->library('Mail_sender');
                    $mail_content = $this->msg_template->rentSuccess($order);

                    //新增通知
                    $this->Notification_model->insert($order['renter_uid'], sprintf('%s 租公同意租借 %s 電器，交易單號： %s', $order['device_owner_name'], $order['device_name'], $order['ur_id']));

                    //寄信
                    $this->mail_sender->AddReceiver($order['username']);
                    $this->mail_sender->SetSubject(RENT_DETAIL_MAIL_TITLE);
                    $this->mail_sender->SetMessage($mail_content);
                    $this->mail_sender->send();

                }
            }

            echo '1|OK';
        } catch (Exception $e) {
            write_log('transaction', 'fail->' . $e->getMessage());
            echo '0|' . $e->getMessage();
        }
    }

    /**
     * 系統排程自動退刷API
     */
    public function resetTransaction() {
        $data = $this->transModel->getValidSuccessOrder();
        $resetData = $this->transModel->getRentResetOrder();
        $data = array_merge($data, $resetData);
        foreach ($data as $row) {
            $params = array(
                'MerchantID' => self::MERCHANT_ID,
                'MerchantTradeNo' => $row['transid'],
                'TradeNo' => $row['tradno'],
                'Action' => 'R',
                'TotalAmount' => (int)$row['amount']
            );
            $params['CheckMacValue'] = $this->getMacvalue($params);
            $params['url'] = self::TRANS_URL['resetOrder'];

            $http_request = new Http_request($params);
            $res = $http_request->sendRequest('POST', 'return');

            //string to array
            parse_str($res, $res);

            if ((int)$res['RtnCode'] === 1 && $res['MerchantTradeNo'] === $row['transid']) {
                $this->transModel->update($row['transid'], array(
                    'is_valid' => 2
                ));
            } else {
                write_log('resetValidOrder', $res);
            }
        }

    }

    public function test() {
        $transid = 'RPT00000000000000005';
        $params = array(
            'MerchantID' => self::MERCHANT_ID,
            'MerchantTradeNo' => $transid,
            'TradeNo' => '1711112233493717',
            'Action' => 'N',
            'TotalAmount' => 30
        );
        $params['CheckMacValue'] = $this->getMacvalue($params);
        $params['url'] = self::TRANS_URL['resetOrder'];

        $http_request = new Http_request($params);
        $res = $http_request->sendRequest('POST', 'return');
        //string to array
        parse_str($res, $res);
        if ((int)$res['RtnCode'] === 1 && $res['MerchantTradeNo'] === $transid) {
            $this->transModel->update($transid, array(
                'is_valid' => 2
            ));
            echo 'success';
        } else {
            write_log('resetValidOrder', $res);
            echo 'fail';
        }
    }

    /**
     * 產生訂單
     * @param $amount 金額
     * @param $isValid 是否為驗證信用卡的訂單
     * @return string 訂單編號
     */
    private function transId($amount, $isValid, $urId) {
        $id = $this->transModel->create($this->userProfile['uid'], $amount, $isValid, $urId);
        //訂單編號, 共20碼, ex: RPT00000000000000001
        $transid = 'RPT' . sprintf('%017d', $id);
        $this->transModel->updateTransId($id, $transid);
        return $transid;
    }

    /**
     * 產生交易參數並導向綠界
     * @param Trad_params $params
     */
    private function trad(Trad_params $params) {
        $transid = $this->transId($params->amount, $params->isValid, $params->urId);
        $data = array(
            'MerchantID' => self::MERCHANT_ID,
//            'PlatformID' => self::MERCHANT_ID,
//            'PlatformID' => '',
            //電電租平台交易編號
            'MerchantTradeNo' => $transid,
            'MerchantTradeDate' => date('Y/m/d H:i:s'),
            //交易類型，固定
            'PaymentType' => 'aio',
            'TotalAmount' => $params->amount,
            //交易描述，租公接單則放訂單編號
            'TradeDesc' => $params->desc === 'id' ? $transid : $params->desc,
            //商品名稱
            'ItemName' => $params->item,
            //預設付款方式
            'ChoosePayment' => 'Credit',
            //傳送交易結果網址
            'ReturnURL' => WEBSITE . '/api/Trans_api/tradReturn',
            //用戶刷卡結束導向網址
            'ClientBackURL' => WEBSITE,
            'OrderResultURL' => $params->returnUrl,
            //加密類型，SHA256
            'EncryptType' => 1,
            //記憶卡號
            'BindingCard' => 1,
            //記憶卡號識別碼
            'MerchantMemberID' => self::MERCHANT_ID . $this->userProfile['uid'],
            'CustomField1' => $params->urId
        );
        $macvalue = $this->getMacvalue($data);
        $data['CheckMacValue'] = $macvalue;

        $this->post(self::TRANS_URL['production'], $data);
    }

    /**
     * 產生驗證碼
     * @param array $data
     * @return string sha256 hash string
     */
    private function getMacvalue(array $data) : string {
        //按照key值排序，從小到大
        ksort($data);

        $datastr = '';
        foreach ($data as $k => $v) {
            $datastr .= $k . '=' . $v . '&';
        }
        $datastr = substr($datastr, 0, -1);
        $datastr = sprintf('HashKey=%s&%s&HashIV=%s', self::HASHKEY, $datastr, self::HASHIV);
        $datastr = urlencode($datastr);
        $datastr = strtolower($datastr);
        $datastr = str_replace('%2d', '-', $datastr);
        $datastr = str_replace('%5f', '_', $datastr);
        $datastr = str_replace('%2e', '.', $datastr);
        $datastr = str_replace('%21', '!', $datastr);
        $datastr = str_replace('%2a', '*', $datastr);
        $datastr = str_replace('%28', '(', $datastr);
        $datastr = str_replace('%29', ')', $datastr);
        $datastr = str_replace('%20', '+', $datastr);

        $encrypt = hash('sha256', $datastr);
        return strtoupper($encrypt);
    }

    /**
     * post資料到綠界交易網址
     * @param string $url
     * @param array $data
     */
    private function post(string $url, array $data) : void {
        $this->load->view('other/post_view.php', array(
            'url' => $url,
            'data' => $data
        ));
    }

}