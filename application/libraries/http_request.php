<?php

/**
 * Description of http_request
 *
 */
class Http_request {

    private $_mCh   = null;
    private $_mOpts = array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_TIMEOUT_MS     => 15000, //連線等待15秒
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false
    );
    private $_mData;

    function __construct($data) {
        $this->_mData = $data;
        $this->_mCh   = curl_init();
    }

    function __destruct() {
        curl_close($this->_mCh);
    }

    private function makeQuery() {
        $query = $this->_mData;
        unset($query['url']);
        return http_build_query($query);
    }

    public function sendRequest($method = 'GET', $decode = 'json') {

        $opts = $this->_mOpts;

        if ($method == 'GET') {
            $opts[CURLOPT_URL] = $this->_mData['url'] . '?' . $this->makeQuery();
        } else {
            $opts[CURLOPT_URL]        = $this->_mData['url'];
            $opts[CURLOPT_POSTFIELDS] = $this->makeQuery();
            $opts[CURLOPT_POST]       = true;
        }
        curl_setopt_array($this->_mCh, $opts);

        $result = curl_exec($this->_mCh);

        if ($decode == 'json') {
            return json_decode($result, true);
        } else if ($decode == 'xml') {
            return $this->parseXml($result);
        } else {
            return $result;
        }
    }

    private function parseXml($result) {
        $str   = strip_tags($result);
        $agent = explode(',', $str);
        foreach ($agent as $val) {
            list($code, $name) = explode('|', $val);
            $xml[$code] = $name;
        }
        return $xml;
    }

    public function debug() {
        echo curl_error($this->_mCh);
    }

}
