<?php

/**
 * 寄信模版控制器
 *
 * User: eric.chung
 * Date: 2017/11/3
 */
class Msg_template {

    const REGISTER = 1;
    const FORGET_PWD = 2;
    const RENT_REQUEST = 3;
    const RENT_SUCCESS = 4;
    const NOTICE_RENTER_END = 5;
    const NOTICE_RENTER_START = 6;
    const NOTICE_MASTER_END = 9;
    const NOTICE_MASTER_START = 10;
    const RENT_FAIL = 8;
    const MESSAGE = 7;

    private $mCi;

    function __construct() {
        $this->mCi = &get_instance();
        $this->mCi->load->model('Common_model');
    }

    /**
     * 取得註冊信
     * @param int $uid
     * @param string $name 會員姓名
     * @param string $sign 驗證簽名
     * @return string
     */
    public function register(int $uid, string $name, string $sign) : string {
        $template = $this->getTemplate(self::REGISTER);
        $template = str_replace("{name}", $name, $template);
        $template = str_replace("{a}", sprintf('<a href="%s/login/sign/%s/%s">', WEBSITE, $uid, $sign), $template);
        $template = str_replace("{/a}", '</a>', $template);
        return $template;
    }

    /**
     * 取得忘記密碼驗證信
     * @param string $name 會員姓名
     * @param string $sign
     * @return string
     */
    public function forgetPwd(string $name, string $sign) : string {
        $template = $this->getTemplate(self::FORGET_PWD);
        $template = str_replace("{name}", $name, $template);
        $template = str_replace("{sign}", $sign, $template);
        return $template;
    }

    /**
     * 租借申請信
     * @param string $name 租公名稱
     * @param int $urId 訂單編號
     * @return string
     */
    public function rentRequest(string $name, int $urId) : string {
        $template = $this->getTemplate(self::RENT_REQUEST);
        $template = str_replace("{name}", $name, $template);
        $template = str_replace("{a}", sprintf('<a href="%s/master/rentRequest/%s">', WEBSITE, $urId), $template);
        $template = str_replace("{/a}", '</a>', $template);
        return $template;
    }

    /**
     * 租公接單給租客的租借成功信
     * @param array $data 訂單資訊
     *      '{name}: 租客', '{ur_id}: 訂單編號', '{device_name}: 電器名稱', '{rent}: 租金', '{deposit}: 押金', '{total}: 費用總計：租金 + 押金', '{accessory}: 配件',
     *      '{device_owner}: 出租人', '{phone_number}: 出租人聯絡電話', '{placename}: 地點', '{address}: 地址', '{start_time}: 租借起始日', '{end_time}: 租借結束日',
     * @return string
     */
    public function rentSuccess(array $data) : string {
        $template = $this->getTemplate(self::RENT_SUCCESS);
        return $this->generateOrderContent($template, $data, $data['name']);
    }

    /**
     * 租公拒絕接單信
     * @param string $name 租客
     * @param string $ownerNickname 租公暱稱
     * @param string $reason 拒絕原因
     * @return string
     */
    public function rentFail(string $name, string $deviceName, string $ownerNickname, string $reason) : string {
        $template = $this->getTemplate(self::RENT_FAIL);
        $template = str_replace("{name}", $name, $template);
        $template = str_replace("{device_name}", $deviceName, $template);
        $template = str_replace("{device_owner_nickname}", $ownerNickname, $template);
        $template = str_replace("{reason}", $reason, $template);

        return $template;
    }

    /**
     * 提醒租客交易
     * @param array $data
     * @return string
     */
    public function noticeStartRentingForRenter(array $data) : string {
        $template = $this->getTemplate(self::NOTICE_RENTER_START);
        return $this->generateOrderContent($template, $data, $data['name']);
    }

    /**
     * 提醒租公交易
     * @param array $data
     * @return string
     */
    public function noticeStartRentingForMaster(array $data) : string {
        $template = $this->getTemplate(self::NOTICE_MASTER_START);
        return $this->generateOrderContent($template, $data, $data['device_owner_name']);
    }

    /**
     * 提醒租客返還
     * @param array $data
     * @return string
     */
    public function noticeEndRentingForRenter(array $data) : string {
        $template = $this->getTemplate(self::NOTICE_RENTER_END);
        return $this->generateOrderContent($template, $data, $data['name']);
    }

    /**
     * 提醒租公返還
     * @param array $data
     * @return string
     */
    public function noticeEndRentingForMaster(array $data) : string {
        $template = $this->getTemplate(self::NOTICE_MASTER_END);
        return $this->generateOrderContent($template, $data, $data['device_owner_name']);
    }

    /**
     * 取得手機驗證簡訊
     * @param string $name 會員姓名
     * @param string $sign
     * @return string
     */
    public function message(string $name, string $sign) : string {
        $template = $this->getTemplate(self::MESSAGE);
        $template = str_replace("{name}", $name, $template);
        $template = str_replace("{sign}", $sign, $template);
        return $template;
    }

    private function generateOrderContent($template, $data, $reciveName) {
        $this->load->library('encryption');
        $privacyStr = urlencode($this->encryption->encrypt($data['ur_id']));
        $template = str_replace("{name}", $reciveName, $template);
        foreach ($data as $k => $v) {
            if ($k === 'start_time' || $k === 'end_time') {
                $v = date('Y/m/d', strtotime($v));
            }
            $template = str_replace("{{$k}}", $v, $template);
        }
        if (!empty($data['device_accessory'])) {
            $accessory = json_decode($data['device_accessory'], true);
            $template = str_replace('{accessory}', implode('、', $accessory), $template);
        }
        $template = str_replace("{renter_name}", $data['name'], $template);
        $template = str_replace("{phone_number}", $data['device_owner_phone'], $template);
        $template = str_replace("{total}", $data['rent'] + $data['deposit'], $template);
        $template = str_replace("{privacy_str}", sprintf('<a href="%s/privacy/order?e=%s">', WEBSITE, $privacyStr), $template);
        $template = str_replace("{/privacy_str}", '</a>', $template);

        return $template;
    }

    /**
     * @param int $typeId 模版ID
     * @return string
     */
    private function getTemplate(int $typeId) : string {
        return $this->mCi->Common_model->getTemplate($typeId);
    }
}