<?php
namespace params;

/**
 * 網站全域設定
 */
class Websetting_params {

    private $adPercent;
    private $gaCode;
    private $gtCode;
    private $indexCarouselSecond;
    private $normalRent;
    private $weekendRent;
    private $ssoDescription;
    private $ssoKeyword;
    private $useGlobalAd;
    private $globalAdStart;
    private $globalAdEnd;

    function __construct() {
        $ci = &get_instance();
        $ci->load->model('Common_model', 'common_model');
        $data = $ci->common_model->getCommonSetting();
        $this->adPercent = $data['ad_percent'];
        $this->gaCode = $data['gacode'];
        $this->gtCode = $data['gtcode'];
        $this->indexCarouselSecond = $data['index_carousel_second'];
        $this->normalRent = $data['normal_rent'];
        $this->weekendRent = $data['weekend_rent'];
        $this->ssoDescription = $data['sso_description'];
        $this->ssoKeyword = $data['sso_keyword'];
        $this->useGlobalAd = $data['use_global_ad'];
        $this->globalAdStart= $data['global_ad_date_start'];
        $this->globalAdEnd = $data['global_ad_date_end'];
    }

    /**
     * @return int 全域廣告趴數
     */
    public function getAdPercent() {
        return $this->adPercent;
    }

    /**
     * @return string google analytics追蹤碼
     */
    public function getGaCode() {
        return $this->gaCode;
    }

    /**
     * @return string google tag manager
     */
    public function getGtCode() {
        return $this->gtCode;
    }

    /**
     * @return int 首頁輪播秒數
     */
    public function getIndexCarouselSecond() {
        return $this->indexCarouselSecond;
    }

    /**
     * @return int 平日租金
     */
    public function getNormalRent() {
        return $this->normalRent;
    }

    /**
     * @return int 週末租金
     */
    public function getWeekendRent() {
        return $this->weekendRent;
    }

    /**
     * @return string 網站描述
     */
    public function getSsoDescription() {
        return $this->ssoDescription;
    }

    /**
     * @return string 網站關鍵字
     */
    public function getSsoKeyword() {
        return $this->ssoKeyword;
    }

    public function isUseGlobalAd() {
        if ($this->useGlobalAd) {
            if (empty($this->globalAdStart)) {
                return true;
            }
            $today = strtotime('now');
            $start = strtotime($this->globalAdStart . ' 00:00:00');
            $end = strtotime($this->globalAdEnd . ' 23:59:59');
            if ($today >= $start && $today < $end) {
                return true;
            }
        }
        return false;
    }



}