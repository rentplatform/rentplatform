<?php
/**
 */

namespace params;

class Device_search_params {

    private $bid;
    private $dtid;
    private $cid;
    private $ctid;
    private $address;
    private $keyword;
    private $allow;

    function __construct() {
        $input = &get_instance()->input;
        $this->bid = $input->get('bid');
        $this->dtid = $input->get('dtid');
        $this->cid = $input->get('cid');
        $this->ctid = $input->get('ctid');
        $this->address = $input->get('address');
        $this->keyword = $input->get('keyword');
        $this->allow = $input->get('allow');

    }

    /**
     * @return int 品牌ID
     */
    public function getBid() {
        return (int)$this->bid ? : 0;
    }

    /**
     * @return int 電器類型ID
     */
    public function getDtid() {
        return (int)$this->dtid ? : 0;
    }

    /**
     * @return int country ID
     */
    public function getCid() {
        return (int)$this->cid ? : 0;
    }

    /**
     * @return int city ID
     */
    public function getCtid() {
        return (int)$this->ctid ? : 0;
    }

    /**
     * @return string 地址
     */
    public function getAddress() {
        return $this->address ? : '';
    }

    /**
     * @return string 關鍵字
     */
    public function getKeyword() {
        return $this->keyword ? : '';
    }

    /**
     * @return bool 是否僅限現在可租
     */
    public function getAllow() {
        return (int)$this->allow === 1;
    }

}