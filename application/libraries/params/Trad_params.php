<?php

namespace params;

/**
 * Date: 2017/11/6
 */
class Trad_params {
    public $isValid = 0;
    public $amount;
    public $desc;
    public $item;
    public $returnUrl;
    public $urId = 0;
}