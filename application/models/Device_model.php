<?php
use params\Device_search_params;

/**
 * Created by PhpStorm.
 * User: eric
 * Date: 2017/11/8
 * Time: 下午10:56
 */
class Device_model extends CI_Model {

    /**
     * 電器列表(含搜尋
     * @param Device_search_params $params
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getDeviceList(Device_search_params $params, $page = 1, $limit = 10) : array {
        $this->db->start_cache();
        if ($params->getBid()) {
            $this->db->where('d.bid', $params->getBid());
        }
        if ($params->getDtid()) {
            $this->db->where('d.dt_id', $params->getDtid());
        }
        if ($params->getCid()) {
            $this->db->where('tp.cid', $params->getCid());
        }
        if ($params->getCtid()) {
            $this->db->where('tp.ct_id', $params->getCtid());
        }
        if ($params->getAddress()) {
            $this->db->like('tp.address', $params->getAddress());
        }
        if ($params->getKeyword()) {
            $this->db->like('d.name', $params->getKeyword());
        }
        if ($params->getAllow()) {
            $this->db->where('(select count(*) from rent_record r where r.device_id = d.device_id and status = 1) <', 1);
        }

        $statement = $this->db->select('d.*, u.nickname, u.good, u.bad, dt.name as device_type_name, b.name as brand_name, dp.filename')
            ->join('user as u', 'd.uid = u.uid', 'left')
            ->join('device_type_list as dt', 'd.dt_id = dt.dt_id', 'left')
            ->join('brand_list as b', 'd.bid = b.bid', 'left')
            ->join('device_pic as dp', 'd.device_id = dp.device_id', 'left')
            ->join('transaction_place as tp', 'd.uid = tp.uid', 'left')
            //租公狀態不能被停權
            ->where('u.status < ', 2)
            ->where('d.status > ', 0)
            ->group_by('d.device_id')
            ->order_by('d.update_time', 'desc')
        ->from('device_list as d');
        $data = array(
            'count' => $statement->count_all_results(),
            'list' => $statement->limit($limit, ($page - 1) * $limit)->get()->result_array()
        );

        $this->db->flush_cache();
        $this->db->stop_cache();
        return $data;
    }

    /**
     * 取得最新上架電器
     * @param int $num 筆數
     * @return array
     */
    public function getNewestDevice(int $num = 3) : array {
        return $this->db->select('d.device_id, d.name, dp.filename')
            ->join('device_pic as dp', 'd.device_id = dp.device_id', 'left')
            ->group_by('d.device_id')
            ->order_by('d.create_time', 'desc')
            ->where('d.status', 1)
            ->limit($num)
            ->get('device_list as d')
            ->result_array();
    }

    /**
     * 取得最熱門電器
     * @param int $num 筆數
     * @return array
     */
    public function gethotDevice(int $num = 3) : array {
        return $this->db->select('d.device_id, d.name, dp.filename')
            ->join('device_pic as dp', 'd.device_id = dp.device_id', 'left')
            ->group_by('d.device_id')
            ->order_by('d.rent_time', 'desc')
            ->where('d.status', 1)
            ->limit($num)
            ->get('device_list as d')
            ->result_array();
    }

    /**
     * 取得租公的電器列表，圖片只會取一張
     * @param $uid
     * @return array
     */
    public function getDeviceListByUid(int $uid) : array {
        return $this->db->select('d.*, dt.name as device_type_name, b.name as brand_name, dp.filename')
            ->where('uid', $uid)
            ->join('device_type_list as dt', 'd.dt_id = dt.dt_id', 'left')
            ->join('brand_list as b', 'd.bid = b.bid', 'left')
            ->join('device_pic as dp', 'd.device_id = dp.device_id', 'left')
            ->group_by('d.device_id')
            ->get('device_list as d')
            ->result_array();
    }

    /**
     * 取得電器資訊
     * @param int $deviceId
     * @return array
     */
    public function getDevice(int $deviceId) : array {
        $data = $this->db->select('d.*, u.name as realname, u.username,u.id_number,u.phone_number,
         u.nickname, u.address, u.good, u.bad, dt.name as device_type_name, b.name as brand_name')
            ->where('d.device_id', $deviceId)
            ->join('user as u', 'd.uid = u.uid', 'left')
            ->join('device_type_list as dt', 'd.dt_id = dt.dt_id', 'left')
            ->join('brand_list as b', 'd.bid = b.bid', 'left')
            ->get('device_list as d')
            ->row_array();
        if (empty($data)) {
            return [];
        }
        $data['pic'] = $this->db->where('device_id', $deviceId)->get('device_pic')->result_array();
        return $data;
    }

    /**
     * 取得電器全部圖片
     * @param int $deviceId
     * @return array
     */
    public function getDevicePic(int $deviceId) : array {
        return $this->db->where('device_id', $deviceId)->get('device_pic')->result_array();
    }

    /**
     * @param int $deviceId
     * @return bool 電器是否存在
     */
    public function checkDeviceExist(int $deviceId) : bool {
        return $this->db->where(array(
                'device_id' => $deviceId,
            ))->count_all_results('device_list') > 0;
    }

    /**
     * 刪除電器圖片
     * @param int $deviceId
     * @param string $filename
     */
    public function deleteDevicePic(int $deviceId, string $filename) {
        $this->db->where(array(
            'device_id' => $deviceId,
            'filename' => $filename
        ))->delete('device_pic');
    }

    public function deleteDevice(int $deviceId) {
        $this->db->where('device_id', $deviceId)->delete('device_list');
        $this->db->where(array(
            'device_id' => $deviceId,
        ))->delete('device_pic');
    }

    /**
     * 寫入電器新圖
     * @param array $data
     * @return bool
     */
    public function insertDevicePic(array $data) : bool {
        $this->db->insert_batch('device_pic', $data);
        return $this->db->affected_rows();
    }

    /**
     * 新增電器
     * @param array $data
     * @return int
     */
    public function insertDevice(array $data) : int {
        $this->db->insert('device_list', $data);
        return $this->db->insert_id();
    }

    /**
     * 更新電器
     * @param int $deviceId
     * @param array $data
     */
    public function updateDevice(int $deviceId, array $data) {
        $this->db->where('device_id', $deviceId)->update('device_list', $data);
    }

    public function updateDeviceRentTime(int $deviceId) {
        $this->db->query('update device_list set rent_time = rent_time + 1 where device_id = ?', $deviceId);
    }

}