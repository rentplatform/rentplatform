<?php

/**
 * 訂單交易相關處理
 *
 */
class trans_model extends CI_Model {

    const TABLE = 'transaction_log';

    /**
     * @param string $transId
     * @return array 訂單資訊
     */
    public function getOrder(string $transId) : array {
        return $this->db->where('transid', $transId)->get(self::TABLE)->row_array();
    }

    /**
     * 取得前一天晚上10點前所有交易成功的驗證信用卡訂單
     * @return array
     */
    public function getValidSuccessOrder() {
        return $this->db->where(array(
            'is_valid' => 1,
            'status' => 1,
            'update_time <' => date('Y-m-d 22:00:00', strtotime('-1 days'))
        ))->get(self::TABLE)->result_array();
    }

    /**
     * 取得申訴後銷單的訂單
     * @return array
     */
    public function getRentResetOrder() {
        return $this->db
            ->select('t.*')
            ->join('rent_record r', 't.ur_id = r.ur_id', 'left')
            ->where('r.status', 8)
            ->where('t.status', 1)
            ->where('t.is_valid', 0)
            ->get(self::TABLE . ' t')
            ->result_array();
    }

    /**
     * 查詢訂單是否存在
     * @param string $transId
     * @return bool
     */
    public function checkOrderExist(string $transId) : bool {
        return $this->db->where(array(
           'transid' => $transId,
            'status' => 0
        ))->count_all_results(self::TABLE) > 0;
    }

    /**
     * 新增訂單
     * @param int $uid
     * @param int $amount
     * @param int $isValid 是否為驗證信用卡訂單
     * @param int $urId 租借編號
     * @return int
     */
    public function create(int $uid, int $amount, int $isValid, int $urId = 0) : int {
        $data = array(
            'uid' => $uid,
            'amount' => $amount,
            'is_valid' => $isValid
        );
        if (!$isValid) {
            $data['ur_id'] = $urId;
        }
        $this->db->insert(self::TABLE, $data);
        return $this->db->insert_id();
    }

    /**
     * 更新訂單編號
     * @param int $id 訂單流水號
     * @param string $transId 由流水號產生的訂單編號
     */
    public function updateTransId(int $id, string $transId) {
        $this->db->where('id', $id)->update(self::TABLE, array('transid' => $transId));
    }

    /**
     * 更新訂單資料
     * @param string $transId
     * @param array $data
     * @return bool
     */
    public function update(string $transId, array $data) : bool {
        $this->db->where('transid', $transId)->update(self::TABLE, $data);
        return $this->db->affected_rows() > 0;
    }

}
