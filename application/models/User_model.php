<?php

/**
 * 會員資料相關操作
 */
class User_model extends CI_Model {

    /**
     * 以UID取得個人資料
     * @param $uid
     * @return array
     */
    public function getProfile($uid) {
        return $this->db->where('uid', $uid)->get('user')->row_array();
    }

    /**
     * 以信箱取得個人資料
     * @param string $username
     * @return array
     */
    public function getProfileByMail(string $username) : array {
        return $this->db->where('username', $username)->get('user')->row_array();
    }

    /**
     * 登入檢查
     * @param string $username 
     * @param string $password 
     * @return array user profile
     */
    public function checkLogin($username, $password) : array {
        $user = $this->db
                        ->select('*')
                        ->where(array(
                            'username' => $username,
                            'password' => md5($password),
                        ))->get('user')->row_array() ?? array();
        return $user;
    }

    /**
     * 登入密碼檢查
     * @param int $uid 
     * @param string $password 
     * @return array 
     */
    public function checkPassword($uid, $password) : bool {
        return $this->db
                        ->where(array(
                            'uid' => $uid,
                            'password' => $password
                        ))->count_all_results('user') > 0;
    }

    /**
     * 帳號檢查
     * @param string $username email
     * @return bool
     */
    public function checkUsername($username) : bool {
        return $this->db
                ->where(array(
                    'username' => $username
                ))->count_all_results('user') > 0;
    }

    /**
     * 檢查驗證碼是否存在，驗證完順便刪除3天前的資料
     * @param int $uid UID
     * @param string $sign 驗證碼
     * @return bool
     */
    public function checkUserSign(int $uid, string $sign) : bool {
        $exist = $this->db->where(array(
                'uid' => $uid,
                'sign' => $sign
            ))->count_all_results('user_sign_valid') > 0;
        if ($exist) {
            $this->db->where('create_time < ', date('Y-m-d', strtotime('-3 day')))->delete('user_sign_valid');
        }
        return $exist;
    }

    /**
     * 註冊
     * @param array $data user register profile
     * @return  int UID or 0
     */
    public function register($data) : int {
        $data['password'] = md5($data['password']);
        $this->db->insert('user', $data);
        if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        }
        return 0;
    }
    
    /**
     * 更新個人資料(姓名、暱稱、行動電話)
     * @param int $uid 
     * @param array $data
     */
    public function updateProfile(int $uid, array $data) : void {
        if (!empty($data['password'])) {
            $data['password'] = md5($data['password']);
        }
        $this->db->where('uid', $uid)->update('user', $data);
    }

    public function updateLastLoginTime($uid) : void {
        $this->db->where('uid', $uid)->update('user', array(
            'last_login' => date('Y-m-d H:i:s')
        ));
    }

    /**
     * 新增或更新會員信箱驗證碼
     * @param int $uid 會員ID
     * @param string $sign 驗證簽名
     */
    public function insertUserValidSign(int $uid, string $sign) {
        $this->db->replace('user_sign_valid', array(
            'uid' => $uid,
            'sign' => $sign
        ));
    }

    /**
     * 刪除會員驗證碼
     * @param int $uid
     */
    public function deleteValidSignByUid(int $uid) : void {
        $this->db->where('uid', $uid)->delete('user_sign_valid');
    }
    
}
