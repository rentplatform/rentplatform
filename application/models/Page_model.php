<?php

class Page_model extends CI_Model {

    public function getArticleTypeList() {
        return $this->db->get('article_type_list')->result_array();
    }

    public function getArticle(int $atid = 0, $ignoreStatus = false) {
        if (!$ignoreStatus) {
            $this->db->where('a.status', 1);
        }
        return $this->db->where('a.at_id', $atid)
            ->select('a.*,l.name as typename')
            ->join('article_type_list l', 'a.atype_id = l.atype_id', 'left')
            ->get('article a')
            ->row_array() ? : [];
    }

    public function getArticleList(int $type, int $page, int $limit = 10): array {
        $this->db->start_cache();
        $statement = $this->db
            ->where([
                'a.atype_id' => $type,
                'a.status' => 1
            ])
            ->select('a.*,l.name as typename')
            ->join('article_type_list l', 'a.atype_id = l.atype_id', 'left')
            ->from('article a');

        $data = array(
            'count' => $statement->count_all_results()
        );

        $statement->order_by('displayorder', 'desc');
        $data['list'] = $statement->limit($limit, ($page - 1) * $limit)->get()->result_array();

        $this->db->flush_cache();
        $this->db->stop_cache();
        return $data;
    }

    public function getNewestArticle($type, $limit = 3) {
        return $this->db
            ->where([
                'atype_id' => $type,
                'status' => 1
            ])
            ->order_by('release_time', 'desc')
            ->limit($limit)
            ->get('article')
            ->result_array();
    }

    public function getQuestion() {
        return $this->db->where('status', 1)->order_by('displayorder', 'desc')->get('question_list')->result_array();
    }

}