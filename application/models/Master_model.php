<?php

/**
 * Created by PhpStorm.
 * User: eric
 * Date: 2017/11/6
 * Time: 下午9:54
 */
class Master_model extends CI_Model {

    /**
     * @param $uid
     * @return int 租公賺取總額
     */
    public function getRewardCount(int $uid): int {
        return $this->db->select_sum('reward')->where('device_owner', $uid)->get('rent_record')->row_array()['reward'] ?: 0;
    }

    /**
     * 取得面交地點列表
     * @param int $uid
     * @return array
     */
    public function getLocationList(int $uid): array {
        return $this->db->select('t.*,c.name as countryName, ct.name as cityName')
            ->where('uid', $uid)
            ->join('country c', 't.cid = c.cid', 'left')
            ->join('city ct', 't.ct_id = ct.ct_id', 'left')
            ->get('transaction_place t')
            ->result_array();
    }

    /**
     * 取得面交地點資料
     * @param int $tpId
     * @return array
     */
    public function getLocation(int $tpId): array {
        return $this->db->where('tp_id', $tpId)
            ->select('t.*,c.name as countryName, ct.name as cityName')
            ->join('country c', 't.cid = c.cid', 'left')
            ->join('city ct', 't.ct_id = ct.ct_id', 'left')
            ->get('transaction_place t')
            ->row_array() ?: [];
    }

    /**
     * 檢查面交點是否被使用中，待接單、租借中、預約中、申訴 都算
     * @param int $tpId
     * @return bool
     */
    public function checkPlaceIsUsingByRenting(int $tpId = 0) {
        return $this->db
            ->where('tp_id', $tpId)
            ->where_in('status', [
                RENT_RECORD_STATUS['WAIT'],
                RENT_RECORD_STATUS['RENTING'],
                RENT_RECORD_STATUS['IN_PROCESS'],
                RENT_RECORD_STATUS['ARBITRATION'],
            ])
            ->count_all_results('rent_record') > 0;

    }

    /**
     * 新增面交地點
     * @param array $data
     * @return bool
     */
    public function insertLocation(array $data): bool {
        $this->db->insert('transaction_place', $data);
        return $this->db->affected_rows();
    }

    /**
     * 刪除面交地點
     * @param int $id
     */
    public function deleteLocation(int $id): void {
        $this->db->where('tp_id', $id)->delete('transaction_place');
    }

}