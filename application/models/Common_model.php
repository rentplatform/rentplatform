<?php

class Common_model extends CI_Model {

    /**
     * 取得寄信模版
     * @param $typeId int 1=email註冊驗證, 2=email忘記密碼, 3=email租借申請, 4=email租借結果, 5=email提醒返還, 6=email提醒交易, 7=簡訊通知
     * @return string template html content
     */
    public function getTemplate($typeId) : string {
        return $this->db->where('type', $typeId)->get('template')->row_array()['content'] ?? '';
    }

    /**
     * 取得網站全域設定
     */
    public function getCommonSetting() {
        $data = $this->db->get('common_setting')->result_array();
        $keyvalue = array();
        foreach ($data as $row) {
            $keyvalue[$row['key']] = $row['value'];
        }
        return $keyvalue;
    }

    /**
     * @return array 品牌列表
     */
    public function getBrandList() {
        return $this->db->get('brand_list')->result_array();
    }

    /**
     * @return array 電器類型列表
     */
    public function getDeviceTypeList() {
        return $this->db->get('device_type_list')->result_array();
    }

    public function getCountryList() {
        return $this->db->get('country')->result_array();
    }

    public function getCityList(int $cid) {
        return $this->db->where('cid', $cid)->get('city')->result_array();
    }

    /**
     * 取得目前時間的廣告優惠趴數
     * @return int
     */
    public function getDiscountPricePercent() {
        $today = date('Y-m-d');
        $data = $this->db->where(array(
            'start_time <=' => $today,
            'end_time >=' => $today
        ))->get('ad_price_list')->row_array() ? : [];
        return $data;
    }

    /**
     * 取得輪播圖列表
     */
    public function getCarouselList() {
        $today = date('Y-m-d H:i:s');
        return $this->db
            ->group_start()
            ->where('status', 1)
            ->where('alwaysshow', 1)
            ->group_end()
            ->group_start('', 'or')
            ->where('status', 1)
            ->where('start_time <=', $today)
            ->where('end_time >=', $today)
            ->group_end()
            ->order_by('displayorder', 'asc')
            ->get('carousel')
            ->result_array();
    }

    public function getPreviewCarousel(int $id = 0) {
        return $this->db->where('caid', $id)->get('carousel')->result_array();
    }

    public function getNewArticle($count = 3) {
        return $this->db
            ->where('status', 1)
            ->order_by('release_time', 'desc')
            ->limit($count)
            ->get('article')
            ->result_array();
    }

    public function getStaticData(int $type = 1, bool $isMulti = false) {
        $statement = $this->db->where('type', $type)->get('static_page');
        return $isMulti ? $statement->result_array() : ($statement->row_array() ? : []);
    }

}