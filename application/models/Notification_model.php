<?php

class notification_model extends CI_Model {

    public function getList(int $uid, int $type, int $page, int $limit = 10): array {
        $this->db->start_cache();
        $statement = $this->db
            ->where([
                'type' => $type,
                'uid' => $uid
            ])
            ->from('notification_list');

        $data = array(
            'count' => $statement->count_all_results()
        );

        $statement->order_by('create_time', 'desc');
        $data['list'] = $statement->limit($limit, ($page - 1) * $limit)->get()->result_array();

        $this->db->flush_cache();
        $this->db->stop_cache();
        return $data;
    }

    /**
     * 新增通知
     * @param int $uid
     * @param string $content
     * @param int $type
     * @param int $id
     */
    public function insert(int $uid, string $content, int $type = 0, int $id = 0): void {
        $data = [
            'uid' => $uid,
            'content' => $content,
            'type' => $type
        ];
        if ($id) {
            $data['id'] = $id;
        }
        $this->db->insert('notification_list', $data);
    }

}