<?php

/**
 * 租借記錄相關
 */
class Rent_model extends CI_Model {

    /**
     * 依照電器持有人及租借訂單狀態取得訂單總數
     * @param int $uid
     * @param int $status
     * @return int
     */
    public function getDeviceOwnerRecordCountByStatus(int $uid, int $status): int {
        return $this->db->where(array(
            'device_owner' => $uid,
            'status' => $status
        ))->count_all_results('rent_record');
    }

    /**
     * 取得訂單內容
     * @param int $urId 訂單編號
     * @return array
     */
    public function getRentRequestOrder(int $urId): array {
        return $this->db
            ->select('r.*, du.name as device_owner_name, du.nickname as device_owner_nickname, 
            du.id_number as device_owner_idnumber, u.id_number as renter_idnumber, du.address as device_owner_address,
            u.address as renter_address, d.notice, 
            du.phone_number as device_owner_phone, u.name, u.nickname, u.username, u.phone_number as renter_phone
            , u.good, u.bad, tp.placename, tp.address, dp.filename, du.username as device_owner_username'
            )
            ->where('r.ur_id', $urId)
            ->join('transaction_place tp', 'r.tp_id = tp.tp_id', 'left')
            ->join('user u', 'r.renter_uid = u.uid', 'left')
            ->join('user du', 'r.device_owner = du.uid', 'left')
            ->join('device_pic as dp', 'r.device_id = dp.device_id', 'left')
            ->join('device_list as d', 'r.device_id = d.device_id', 'left')
            ->limit(1)
            ->get('rent_record r')
            ->row_array() ?: [];
    }

    /**
     * 取得租借紀錄列表
     * @param int $type
     * @param int $uid
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getRentRecordList(int $type, int $uid, int $page = 1, int $limit = 10) : array {
        $this->db->start_cache();
        $uk = !$type ? 'device_owner' : 'renter_uid';
        $statement = $this->db
            ->select("r.*, u.nickname as nickname, r.status")
            ->join('user u', "r.{$uk} = u.uid", 'left')
            ->where($type ? 'device_owner' : 'renter_uid', $uid)
            ->from('rent_record r');
        $data = [
            'count' => $statement->count_all_results()
        ];
        $statement->order_by('create_time', 'desc');
        $data['list'] = $statement->limit($limit, ($page - 1) * $limit)->get()->result_array();
        $this->db->flush_cache();
        $this->db->stop_cache();
        return $data;
    }

    /**
     * 依租借編號取得申訴單
     * @param int $urId
     * @return array
     */
    public function getArbitrationData(int $urId) {
        return $this->db->where('ur_id', $urId)->get('arbitration_list')->row_array() ? : [];
    }

    /**
     * 確認電器的日期是否被預約(狀態待接單、租借中、已接單未開始、申訴中都算)
     * @param int $deviceId
     * @param string $start
     * @param string $end
     * @return bool
     */
    public function checkRentDateValid(int $deviceId, string $start, string $end): bool {
        $data = $this->db->query('select count(*) as c from rent_record 
            where device_id = ? and 
            status in (0, 1, 6, 7) and
            (
                start_time <= ? and 
                end_time >= ? or 
                start_time <= ? and 
                end_time >= ? or 
                start_time > ? and 
                end_time < ?
            )', [
            $deviceId, $start, $start, $end, $end, $start, $end
        ])->row_array();
        return $data['c'] > 0;
    }

    /**
     * 確認租借請求是否已回應
     * @param int $urId
     * @return int
     */
    public function checkOrderStatus(int $urId): int {
        return $this->db->where('ur_id', $urId)->get('rent_record')->row_array()['status'];
    }

    /**
     * 確認電器是否被租借中或已預約或待接單
     * @param int $deviceId
     * @return bool true代表租借中或已預約
     */
    public function checkOrderStatusByDevice(int $deviceId) : bool {
        return $this->db->where_in('status', [0, 1, 6])->where('device_id', $deviceId)->count_all_results('rent_record') > 0;
    }

    /**
     * 更新訂單
     * @param $urId
     * @param $data
     * @return mixed
     */
    public function updateRecord($urId, $data) {
        $this->db->where('ur_id', $urId)->update('rent_record', $data);
        return $this->db->affected_rows();
    }

    /**
     * 新增租借訂單
     * @param array $data
     * @return int ur_id
     */
    public function insertRentOrder(array $data): int {
        $this->db->insert('rent_record', $data);
        return $this->db->insert_id();
    }

    /**
     *
     * @param array $data
     */
    public function insertArbitration(array $data) {
        $this->db->insert('arbitration_list', $data);
        return $this->db->insert_id();
    }

}