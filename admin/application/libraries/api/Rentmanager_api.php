<?php

/**
 * 租公相關
 * @property Rentmanager_model Rentmanager_model
 */
class Rentmanager_api extends AdminApiBase {

    public function __construct() {
        parent::__construct();
        $this->checkLogin();
        $this->load->model('Rentmanager_model');
    }

    /**
     * 編輯品牌
     */
    public function editBrand() {
        $this->checkPerm('brandEdit');
        $this->checkData('name', array('bid'));

        if (empty($this->mData['bid'])) {
            $res = $this->Rentmanager_model->insertBrand($this->mData['name']);
        } else {
            $res = $this->Rentmanager_model->updateBrand($this->mData['bid'], $this->mData['name']);
        }

        if ($res) {
            $this->success();
        }
        $this->error();
    }

    /**
     * 刪除品牌
     * @param int $bid
     */
    public function deleteBrand(int $bid = 0) {
        $this->checkPerm('brandEdit');
        if (!$bid) {
            $this->error();
        }

        if ($this->Rentmanager_model->deleteBrand($bid)) {
            $this->success();
        }
        $this->error();
    }

    /**
     *
     * 編輯電器類型
     */
    public function editDeviceType() {
        $this->checkPerm('deviceTypeEdit');
        $this->checkData('name', array('dtid'));

        if (empty($this->mData['dtid'])) {
            $res = $this->Rentmanager_model->insertDeviceType($this->mData['name']);
        } else {
            $res = $this->Rentmanager_model->updateDeviceType($this->mData['dtid'], $this->mData['name']);
        }

        if ($res) {
            $this->success();
        }
        $this->error();
    }

    /**
     * 刪除電器類型
     * @param int $dtid
     */
    public function deleteDeviceType(int $dtid = 0) {
        $this->checkPerm('deviceTypeEdit');
        if (!$dtid) {
            $this->error();
        }

        if ($this->Rentmanager_model->deleteDeviceType($dtid)) {
            $this->success();
        }
        $this->error();
    }

    /**
     * 取得租借列表
     */
    public function getRentTableList() {
        $this->checkPerm('transactionList');
        $get = $this->input->get(null, true);
        $data = $this->Rentmanager_model->getRentRecordList();

        $list = [];
        foreach ($data['list'] as $k => &$row) {
            switch ($row['status']) {
                case 0:
                    $row['statustxt'] = '待接單';
                    break;
                case 1:
                    $row['statustxt'] = '租借中';
                    break;
                case 2:
                    $row['statustxt'] = '結案';
                    break;
                case 3:
                    $row['statustxt'] = '租供未回應';
                    break;
                case 4:
                    $row['statustxt'] = '租公拒絕';
                    break;
                case 5:
                    $row['statustxt'] = '租客取消';
                    break;
                case 6:
                    $row['statustxt'] = '租公已接單(預約時間未到)';
                    break;
                case 7:
                    $row['statustxt'] = '意見反應中';
                    break;
                case 8:
                    $row['statustxt'] = '已銷單';
                    break;
                default:
                    $row['statustxt'] = '';
            }
            $row['accessorytxt'] = implode('<br>', json_decode($row['device_accessory'], true));
            $d = [];
            $d[] = $row['ur_id'];
            $d[] = $row['transid'];
            $d[] = sprintf('<a class="showUser" href="javascript:void(0)" data-uid="%s"><i class="fa fa-share-square-o"></i> %s</a>', $row['device_owner'], $row['device_owner_name']);
            $d[] = sprintf('<a class="showUser" href="javascript:void(0)" data-uid="%s"><i class="fa fa-share-square-o"></i> %s</a>', $row['renter_uid'], $row['name']);
            $d[] = $row['create_time'];
            $d[] = date('Y/m/d', strtotime($row['start_time'])) . ' ~ ' . date('Y/m/d', strtotime($row['end_time']));
            $d[] = $row['device_id'];
            $d[] = $row['device_name'];
            $d[] = $row['accessorytxt'];
            $d[] = $row['statustxt'];
            $d[] = $row['rent'];
            $d[] = $row['deposit'];
            $d[] = in_array($row['status'], [2,7]) ? sprintf('<a class="showAssessment btn btn-primary" href="javascript:void(0)" data-id="%s">評價</a>', $row['ur_id']) : '';
            $list[] = $d;
        }

        $output = array(
            "draw" => $get['draw'],
            "recordsTotal" => $data['total'],
            "recordsFiltered" => $data['total'],
            "data" => $list,
        );

        echo json_encode($output);
    }

    /**
     * 電器列表
     */
    public function getDeviceList() {
        $this->checkPerm('deviceList');
        $get = $this->input->get(null, true);
        $data = $this->Rentmanager_model->getDeviceList();

        $list = [];
        foreach ($data['list'] as $k => &$row) {
            switch ($row['status']) {
                case 0:
                    $row['statustxt'] = '暫停';
                    break;
                case 1:
                    $row['statustxt'] = '上架';
                    break;
                case 2:
                    $row['statustxt'] = '租借中';
                    break;
                default:
                    $row['statustxt'] = '';
            }
            $row['accessorytxt'] = implode('<br>', json_decode($row['accessory'], true));
            $d = [];
            $d[] = sprintf('<img src="/upload/%s" style="width: 50px;">', $row['filename']);
            $d[] = $row['brand_name'];
            $d[] = $row['device_type_name'];
            $d[] = $row['name'];
            $d[] = $row['accessorytxt'];
            $d[] = $row['description'];
            $d[] = $row['deposit'];
            $d[] = $row['rent'];
            $d[] = $row['uname'];
            $d[] = $row['statustxt'];

            $edit = "<a class=\"btn btn-primary\" href=\"" . base_url() . "rentmanager/deviceEdit/{$row['device_id']}\">編輯</a>";
            $del = "<a class=\"btn btn-primary listDataDelete\" data-url='" . base_url() . "api/Rentmanager_api/deleteDevice/{$row['device_id']}' >刪除</a>";

            $d[] = $this->checkPerm('deviceEdit', true) ? $edit . $del  : '';
            $list[] = $d;
        }

        $output = array(
            "draw" => $get['draw'],
            "recordsTotal" => $data['total'],
            "recordsFiltered" => $data['total'],
            "data" => $list,
        );

        echo json_encode($output);
    }

    /**
     * 新增或編輯電器
     * @param int $deviceId
     */
    public function editDevice(int $deviceId = 0) {
        $this->checkPerm('deviceEdit');

        $this->checkData('dt_id', 'bid', 'name', 'status', 'deposit', 'description', 'uid', array('accessory', 'oldpic'));
        $this->mData['accessory'] = !empty($this->mData['accessory']) ? json_encode(explode(',', $this->mData['accessory'])) : '[]';
        if ($this->mData['status'] > 2) {
            $this->error(array('msg' => '狀態錯誤'));
        }
        $locationlist = $this->Rentmanager_model->getLocationList($this->mData['uid']);
        if (count($locationlist) < 1) {
            $this->error(['msg' => '缺少面交地點無法新增電器']);
        }
        if ($this->Rentmanager_model->checkOrderStatusByDevice($deviceId)) {
            $this->error(['msg' => '電器租借中或有待租借單子無法編輯']);
        }

        $keepfiles = $this->mData['oldpic'] ?? array();
        unset($this->mData['oldpic']);

        $this->db->trans_begin();
        //$deviceId > 0 => 編輯狀態,檢查電器是否存在並且是擁有者,狀態也非出租中
        if ($deviceId) {
            $device = $this->Rentmanager_model->getDeviceData($deviceId);
            if (empty($device) || $device['uid'] != $this->mData['uid'] || $device['status'] > 1) {
                $this->error();
            }
            $this->Rentmanager_model->updateDevice($deviceId, $this->mData);
        } else {
            if (empty($_FILES['file'])) {
                $this->error();
            }
            $deviceId = $this->Rentmanager_model->insertDevice($this->mData);
        }


        $oldpics = $this->Rentmanager_model->getDevicePic($deviceId);
        //刪掉舊的被刪除的圖，必須有舊的圖被保留或是有新的圖上傳!才會執行刪除舊圖動作
        if (!empty($oldpics) && ((!empty($keepfiles) && count($oldpics) != count($keepfiles)) || !empty($_FILES['file']))) {
            foreach ($oldpics as $row) {
                if (file_exists(PHOTO_PATH . $row['filename']) && array_search($row['filename'], $keepfiles) === false) {
                    $this->Rentmanager_model->deleteDevicePic($deviceId, $row['filename']);
                    unlink(PHOTO_PATH . $row['filename']);
                }
            }
        }

        //圖片處理
        if (!empty($_FILES['file'])) {
            $this->load->library('File');
            list($uploadResult, $fileName, $errMsg) = $this->file->upload();
            if (!$uploadResult || empty($fileName['file'])) {
                $this->db->trans_rollback();
                $this->error(array(
                    'msg' => $errMsg
                ));
            }
            $dataset = array_map(function ($v) use($deviceId) {
                return array(
                    'device_id' => $deviceId,
                    'filename' => $v
                );
            }, $fileName['file']);
            //寫入新上傳的圖
            $this->Rentmanager_model->insertDevicePic($dataset);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->error(['msg' => '新增失敗, 請聯絡客服人員']);
        }
        $this->db->trans_commit();

        $this->success();
    }

    /**
     * 刪除電器
     * @param int $deviceId
     */
    public function deleteDevice(int $deviceId = 0) {
        $this->checkPerm('deviceEdit');
        if (!$deviceId) {
            $this->error();
        }

        if ($this->Rentmanager_model->checkOrderStatusByDevice($deviceId)) {
            $this->error(['msg' => '電器租借中或已預約無法刪除']);
        }

        if ($this->Rentmanager_model->deleteDevice($deviceId)) {
            $this->success();
        }
        $this->error();
    }

    /**
     * 取得意見反應列表資料
     */
    public function getArbitrationTableList() {
        $this->checkPerm('arbitration');
        $get = $this->input->get(null, true);
        $data = $this->Rentmanager_model->getArbitrationList();

        $list = [];
        foreach ($data['list'] as $k => &$row) {
            switch ($row['status']) {
                case 0:
                    $row['statustxt'] = '待接單';
                    break;
                case 1:
                    $row['statustxt'] = '租借中';
                    break;
                case 2:
                    $row['statustxt'] = '結案';
                    break;
                case 3:
                    $row['statustxt'] = '租供未回應';
                    break;
                case 4:
                    $row['statustxt'] = '租公拒絕';
                    break;
                case 5:
                    $row['statustxt'] = '租客取消';
                    break;
                case 6:
                    $row['statustxt'] = '租公已接單(預約時間未到)';
                    break;
                case 7:
                    $row['statustxt'] = '意見反應中';
                    break;
                case 8:
                    $row['statustxt'] = '已銷單';
                    break;
                default:
                    $row['statustxt'] = '';
            }
            $row['accessorytxt'] = implode('<br>', json_decode($row['device_accessory'], true));
            $d = [];
            $d[] = $row['ab_id'];
            $d[] = $row['ur_id'];
            $d[] = sprintf('<a class="showUser" href="javascript:void(0)" data-uid="%s"><i class="fa fa-share-square-o"></i> %s</a>', $row['device_owner'], $row['device_owner_name']);
            $d[] = sprintf('<a class="showUser" href="javascript:void(0)" data-uid="%s"><i class="fa fa-share-square-o"></i> %s</a>', $row['renter_uid'], $row['name']);
            $d[] = $row['create_time'];
            $d[] = date('Y/m/d', strtotime($row['start_time'])) . ' ~ ' . date('Y/m/d', strtotime($row['end_time']));
            $d[] = $row['device_id'];
            $d[] = $row['device_name'];
            $d[] = $row['accessorytxt'];
            $d[] = $row['statustxt'];
            $d[] = $row['rent'];
            $d[] = $row['deposit'];

            $assessment_btn = sprintf('<a class="showAssessment btn btn-primary" href="javascript:void(0)" data-id="%s">評價</a>', $row['ur_id']);
            $info_btn = sprintf('<a class="showInfo btn btn-primary" href="javascript:void(0)" data-id="%s">詳細</a>', $row['ab_id']);
            $d[] = in_array($row['status'], [2,7]) ? $assessment_btn . $info_btn : '';
            $list[] = $d;
        }

        $output = array(
            "draw" => $get['draw'],
            "recordsTotal" => $data['total'],
            "recordsFiltered" => $data['total'],
            "data" => $list,
        );

        echo json_encode($output);
    }

    /**
     * 顯示租借訂單的評價資訊 modal view
     */
    public function getRentAssessment() {
        $this->checkPerm('transactionList');
//        $this->checkData('id:number');
        $data = $this->Rentmanager_model->getRentData($this->mData['id']);
        if (empty($data)) {
            show_404();
            return ;
        }

        $this->load->view('rentmanager/subview/rent_accessment_subview', [
            'data' => $data
        ]);
    }

    /**
     * 顯示使用者資訊
     */
    public function getUserData() {
        $this->checkPerm('transactionList');
        $this->checkData('uid');
        $this->load->model('User_model');
        $data = $this->User_model->getUserById($this->mData['uid']);
        if (empty($data)) {
            show_404();
            return;
        }

        $noresponse = $this->User_model->getMasterRentInfoByStatus($this->mData['uid'], 3);
        $cancel = $this->User_model->getUserRentInfoByStatus($this->mData['uid'], 5);

        $this->load->view('rentmanager/subview/user_card_subview', [
            'data' => $data,
            'nores' => $noresponse,
            'cancel' => $cancel
        ]);
    }

    /**
     * 顯示意見反應單的明細
     */
    public function getArbitrationInfo() {
        $this->checkPerm('arbitration');
        $this->checkData('abid');
        $data = $this->Rentmanager_model->getArbitrationData($this->mData['abid']);
        if (empty($data)) {
            show_404();
            return;
        }

        $this->load->view('rentmanager/subview/arbitration_subview', [
            'data' => $data
        ]);
    }

    /**
     * 審核意見反應單
     */
    public function updateArbitrationInfo() {
        $this->checkPerm('arbitration');
        $this->checkData('id', 'renter', 'owner', 'comment', 'type');
        if (!is_numeric($this->mData['type']) || !in_array($this->mData['type'], [1,2])) {
            $this->error(['msg' => '錯誤的狀態']);
        }
        $data = $this->Rentmanager_model->getArbitrationData($this->mData['id']);
        //處理過的單子不可重複處理
        if (empty($data) || $data['abstatus'] > 0) {
            $this->error();
        }
        if ($this->Rentmanager_model->updateArbitration($this->mData['id'], [
            'owner_reason' => $this->mData['owner'],
            'renter_reason' => $this->mData['renter'],
            'comment' => $this->mData['comment'],
            'status' => $this->mData['type']
        ])) {
            $this->success();
        }

        $this->error(['msg' => '更新失敗']);
    }


}