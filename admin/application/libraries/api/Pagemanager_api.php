<?php

/**
 * Class User
 * @property Page_model $page
 */
class Pagemanager_api extends AdminApiBase {

    public function __construct() {
        parent::__construct();
        $this->checkLogin();
        $this->load->model('Page_model', 'page');
    }

    /**
     * 編輯輪播圖
     * @param int $caid
     */
    public function edit(int $caid = 0) {
        $this->load->library('file');
        $this->checkPerm('carouselEdit');
        $this->checkData('url', 'start_time', 'end_time', 'status', 'displayorder', array('alwaysshow'));
        $this->mData['start_time'] = $this->mData['start_time'] . ' 00:00:00';
        $this->mData['alwaysshow'] = !empty($this->mData['alwaysshow']) ? 1 : 0;
        $this->mData['end_time'] = !empty($this->mData['end_time']) ? $this->mData['end_time'] . ' 23:59:59' : null;
        if (!$this->mData['alwaysshow'] && empty($this->mData['end_time'])) {
            $this->error(['msg' => '請選擇下架日期']);
        }
        if ($caid) {
            $c = $this->page->getCarousel($caid);
            if (empty($c)) {
                $this->error();
            }
        }
        if (!empty($_FILES['pcimg'])) {
            if (!empty($c['pcimg'])) {
                unlink(PHOTO_PATH . $c['pcimg']);
            }
            list($uploadResult, $fileName, $errMsg) = $this->file->upload('pcimg');
            if (!$uploadResult || empty($fileName['pcimg'])) {
                $this->error(array(
                    'msg' => $errMsg
                ));
            }
            $this->mData['pcimg'] = $fileName['pcimg'];
        }
        if (!empty($_FILES['mobileimg'])) {
            if (!empty($c['mobileimg'])) {
                unlink(PHOTO_PATH . $c['mobileimg']);
            }
            list($uploadResult, $fileName, $errMsg) = $this->file->upload('mobileimg');
            if (!$uploadResult || empty($fileName['mobileimg'])) {
                $this->error(array(
                    'msg' => $errMsg
                ));
            }
            $this->mData['mobileimg'] = $fileName['mobileimg'];
        }
        if ((empty($c) && $this->mData['status'] > 0) || (!empty($c) && $c['status'] < 1 && $this->mData['status'] > 0)) {
            $this->mData['release_time'] = date('Y-m-d H:i:s');
        }
        if ($caid) {
            $this->page->updateCarousel($caid, $this->mData);
        } else {
            $caid = $this->page->insertCarousel($this->mData);
        }
        $this->success(['id' => $caid]);
    }

    /**
     * 編輯專欄文章
     * @param int $atid
     */
    public function editArticle(int $atid = 0) {
        $this->load->library('file');
        $this->checkPerm('articleEdit');
        $this->checkData('title', 'introduction', 'description', 'status', 'displayorder', 'atype_id');
        //預設會使用xss clean，這裡避免html attribute 被xss clean掉，so 另外抓一次
        $this->mData['description'] = $this->input->post('description');
        if ($atid) {
            $c = $this->page->getArticle($atid);
            if (empty($c)) {
                $this->error();
            }
        }
        if (!empty($_FILES['pic'])) {
            if (!empty($c['pic'])) {
                unlink(PHOTO_PATH . $c['pic']);
            }
            list($uploadResult, $fileName, $errMsg) = $this->file->upload('pic');
            if (!$uploadResult || empty($fileName['pic'])) {
                $this->error(array(
                    'msg' => $errMsg
                ));
            }
            $this->mData['pic'] = $fileName['pic'];
        }
        if ((empty($c) && $this->mData['status'] > 0) || (!empty($c) && $c['status'] < 1 && $this->mData['status'] > 0)) {
            $this->mData['release_time'] = date('Y-m-d H:i:s');
        }
        if ($atid) {
            $this->page->updateArticle($atid, $this->mData);
        } else {
            $atid = $this->page->insertArticle($this->mData);
        }
        $this->success(['id' => $atid]);
    }

    public function editArticleType(int $atypeid = 0) {
        $this->checkPerm('articleEdit');
        $this->checkData('name');
        if ($atypeid) {
            $c = $this->page->getArticleTypeList($atypeid);
            if (empty($c)) {
                $this->error();
            }
        }
        if ($atypeid) {
            $this->page->updateArticleType($atypeid, $this->mData);
        } else {
            $this->page->insertArticleType($this->mData);
        }
        $this->success();
    }

    public function editMasterSourceData(int $type = 1, int $id = 0) {
        $this->load->library('file');
        $this->checkPerm('staticpageEdit');
        $this->checkData('title', 'content');
        if ($type > 2) {
            $this->error();
        }
        $typeData = $this->page->getStaticData($type, true);
        if (count($typeData) >= 3 && !$id) {
            $this->error();
        }
        if ($id) {
            $c = $this->page->getStaticDataById($id);
            if (empty($c)) {
                $this->error();
            }
        }

        if (!empty($_FILES['pic'])) {
            if (!empty($c['pic'])) {
                unlink(PHOTO_PATH . $c['pic']);
            }
            list($uploadResult, $fileName, $errMsg) = $this->file->upload('pic');
            if (!$uploadResult || empty($fileName['pic'])) {
                $this->error(array(
                    'msg' => $errMsg
                ));
            }
            $this->mData['pic'] = $fileName['pic'];
        }
        if ($id) {
            $this->page->updateMasterSource($id, $this->mData);
        } else {
            $this->mData['type'] = $type;
            $this->page->insertMasterSource($this->mData);
        }
        $this->success();
    }

    /**
     * 新增修改常見問題
     * @param int $id
     */
    public function editQuestion(int $id = 0) {
        $this->checkPerm('questionEdit');
        $this->checkData('title', 'description', 'displayorder', 'status');
        if ($id) {
            $c = $this->page->getQuestion($id);
            if (empty($c)) {
                $this->error();
            }
        }
        if ((empty($c) && $this->mData['status'] > 0) || (!empty($c) && $c['status'] < 1 && $this->mData['status'] > 0)) {
            $this->mData['release_time'] = date('Y-m-d H:i:s');
        }
        if ($id) {
            $this->page->updateQuestion($id, $this->mData);
        } else {
            $this->page->insertQuestion($this->mData);
        }
        $this->success();
    }

    public function editStaticPageData() {
        $this->checkPerm('staticpageEdit');
        $this->checkData('content', 'type');
        //預設會使用xss clean，這裡避免html attribute 被xss clean掉，so 另外抓一次
        $this->mData['content'] = $this->input->post('content');
        if ($this->mData['type'] > 7 || $this->mData['type'] < 3) {
            $this->error();
        }
        $this->page->updateStaticData($this->mData['type'], $this->mData);
        $this->success();
    }

    /**
     *
     * 刪除輪播圖
     * @param int $caid
     */
    public function delete(int $caid = 0) {
        $this->checkPerm('carouselEdit');
        if (!$caid) {
            $this->error();
        }
        if ($this->page->deleteCarousel($caid)) {
            $this->success();
        }
        $this->error();
    }

    /**
     * 刪除專欄文章
     * @param int $atid
     */
    public function deleteArticle(int $atid = 0) {
        $this->checkPerm('articleEdit');
        if (!$atid) {
            $this->error();
        }
        if ($this->page->deleteArticle($atid)) {
            $this->success();
        }
        $this->error();
    }

    /**
     * 刪除專欄文章
     * @param int $id
     */
    public function deleteArticleType(int $id = 0) {
        $this->checkPerm('articleEdit');
        if (!$id) {
            $this->error();
        }
        if ($this->page->deleteArticleType($id)) {
            $this->success();
        }
        $this->error();
    }

    /**
     * 刪除常見問題
     * @param int $id
     */
    public function deleteQuestion(int $id = 0) {
        $this->checkPerm('questionEdit');
        if (!$id) {
            $this->error();
        }
        if ($this->page->deleteQuestion($id)) {
            $this->success();
        }
        $this->error();
    }

}