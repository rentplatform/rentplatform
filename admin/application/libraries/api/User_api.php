<?php

/**
 * Class UserApi
 * 前台會員相關API
 *
 * @property User_model $user
 */
class User_api extends AdminApiBase {

    public function __construct() {
        parent::__construct();
        $this->checkLogin();
        $this->load->model('User_model', 'user');
    }

    /**
     * 新增會員(租公)
     */
    public function insert() {
        $this->checkData('name', 'nickname', 'sex', 'id_number', 'phone_number', 'username', 'password', array('address', 'line_id', 'is_renter'));
        if (empty($this->mData['is_renter'])) {
            $this->checkPerm('managerEdit');
        } else {
            $this->checkPerm('masterEdit');
        }

        $this->mData['password'] = md5($this->mData['password']);

        if (!empty($this->mData['is_renter']) || !empty($this->mData['rent_master'])) {
            $this->mData['rent_master'] = 1;
            $this->mData['credit_card_valid'] = 1;
        }
        if ($this->user->checkUsernameExists($this->mData['username'])) {
            $this->error(['msg' => 'email已被註冊']);
        }
        unset($this->mData['is_renter']);
        if ($this->user->insertUser($this->mData)) {
            $this->success();
        }

        $this->error();
    }

    /**
     * 修改會員(租公)
     */
    public function edit() {
        $this->checkData('uid', array('name', 'nickname', 'sex', 'id_number', 'phone_number', 'username', 'password', 'address', 'line_id', 'status', 'lock_time', 'rent_master', 'is_renter'));
        if (empty($this->mData['is_renter'])) {
            $this->checkPerm('managerEdit');
        } else {
            $this->checkPerm('masterEdit');
        }

        $uid = $this->mData['uid'];
        unset($this->mData['uid']);
        $user = $this->user->getUserById($uid);
        if (empty($user)) {
            $this->error(array(
                'msg' => '該帳號不存在'
            ));
        }

        if (!empty($this->mData['password'])) {
            $this->mData['password'] = md5($this->mData['password']);
        } else {
            unset($this->mData['password']);
        }
        if (empty($this->mData['status']) || !in_array($this->mData['status'], [1,2])) {
            $this->mData['lock_time'] = null;
        }
        if (!empty($this->mData['is_renter']) || !empty($this->mData['rent_master'])) {
            $this->mData['rent_master'] = 1;
            $this->mData['credit_card_valid'] = 1;
        }
        if ($this->mData['username'] != $user['username'] && $this->user->checkUsernameExists($this->mData['username'], $uid)) {
            $this->error(['msg' => 'email已被註冊']);
        }

        unset($this->mData['is_renter']);
        $this->user->updateUser($uid, $this->mData);
        $this->success();
    }

    /**
     * 會員列表
     */
    public function getUserList() {
        $this->checkPerm('managerList');
        $get = $this->input->get(null, true);
        $data = $this->user->getUserList();

        $list = [];
        foreach ($data['list'] as $k => &$row) {
            $d = [];
            $d[] = $row['uid'];
            $d[] = $row['id_number'];
            $d[] = $row['name'];
            $d[] = $row['phone_number'];
            $d[] = $row['username'];
            $d[] = $row['last_login'];
            $d[] = $row['rent_time'];

            $canedit = $this->checkPerm('managerEdit', true);
            $edit = $canedit ? "<a class='btn btn-primary' href=\"" . base_url() . "user/userEdit/{$row['uid']}\" >編輯</a>" : '';
            $rent = "<a class='btn btn-primary' href='" . base_url() . "user/rentRecord/{$row['uid']}' target='_blank'>租借紀錄</a>";
            $detail = "<a class='btn btn-primary showUserInfo' href='javascript:void(0)' data-id='{$row['uid']}'>詳細狀態</a>";
            $del = $canedit ? "<a class=\"btn btn-primary deleteUser\" data-id='{$row['uid']}' >刪除</a>" : '';
            $d[] = $edit. $rent . $detail . $del;

            $list[] = $d;
        }

        $output = array(
            "draw" => $get['draw'],
            "recordsTotal" => $data['total'],
            "recordsFiltered" => $data['total'],
            "data" => $list,
        );

        echo json_encode($output);
    }

    /**
     * 會員列表
     */
    public function getMasterList() {
        $this->checkPerm('masterList');
        $get = $this->input->get(null, true);
        $data = $this->user->getMasterList();

        $list = [];
        foreach ($data['list'] as $k => &$row) {
            $d = [];
            $d[] = $row['uid'];
            $d[] = $row['id_number'];
            $d[] = $row['name'];
            $d[] = $row['phone_number'];
            $d[] = $row['username'];
            $d[] = $row['last_login'];

            $canedit = $this->checkPerm('masterEdit', true);
            $edit = $canedit ? "<a class='btn btn-primary' href=\"" . base_url() . "user/masterEdit/{$row['uid']}\" >編輯</a>" : '';
            $location = "<a class='btn btn-primary' href=\"" . base_url() . "user/locationList/{$row['uid']}\" target='_blank'>面交點</a>";
            $rent = "<a class='btn btn-primary' href='" . base_url() . "user/masterRecord/{$row['uid']}' target='_blank'>租借紀錄</a>";
            $device = "<a class='btn btn-primary' href='" . base_url() . "user/deviceList/{$row['uid']}' target='_blank'>電器列表</a>";
            $detail = "<a class='btn btn-primary showUserInfo' href='javascript:void(0)' data-id='{$row['uid']}'>詳細狀態</a>";
            $del = $canedit ? "<a class=\"btn btn-primary deleteUser\" data-id='{$row['uid']}' >刪除</a>" : '';
            $d[] = $edit . $location . $rent . $device . $detail . $del;

            $list[] = $d;
        }

        $output = array(
            "draw" => $get['draw'],
            "recordsTotal" => $data['total'],
            "recordsFiltered" => $data['total'],
            "data" => $list,
        );

        echo json_encode($output);
    }

    /**
     * 會員租借紀錄
     * @param int $uid
     */
    public function getUserRentRecord(int $uid = 0) {
        $this->checkPerm('managerList');
        $get = $this->input->get(null, true);
        if (empty($uid)) {
            show_404();
            return ;
        }
        $data = $this->user->getUserRentRecordList($uid);
        if (empty($uid)) {
            show_404();
            return ;
        }

        $list = [];
        foreach ($data['list'] as $k => &$row) {
            switch ($row['status']) {
                case 0:
                    $row['statustxt'] = '待接單';
                    break;
                case 1:
                    $row['statustxt'] = '租借中';
                    break;
                case 2:
                    $row['statustxt'] = '結案';
                    break;
                case 3:
                    $row['statustxt'] = '租供未回應';
                    break;
                case 4:
                    $row['statustxt'] = '租公拒絕';
                    break;
                case 5:
                    $row['statustxt'] = '租客取消';
                    break;
                case 6:
                    $row['statustxt'] = '租公已接單(預約時間未到)';
                    break;
                case 7:
                    $row['statustxt'] = '意見反應中';
                    break;
                case 8:
                    $row['statustxt'] = '已銷單';
                    break;
                default:
                    $row['statustxt'] = '';
            }
            $row['accessorytxt'] = implode('<br>', json_decode($row['device_accessory'], true));

            $d = [];
            $d[] = $row['ur_id'];
            $d[] = date('Y/m/d', strtotime($row['start_time'])) . ' ~ ' . date('Y/m/d', strtotime($row['end_time']));
            $d[] = $row['device_id'];
            $d[] = $row['device_name'];
            $d[] = $row['accessorytxt'];
            $d[] = $row['statustxt'];
            $d[] = $row['rent'];
            $d[] = $row['deposit'];

            $d[] = in_array($row['status'], [2,7]) ? sprintf('<a class="showAssessment btn btn-primary" href="javascript:void(0)" data-id="%s">評價</a>', $row['ur_id']) : '';

            $list[] = $d;
        }

        $output = array(
            "draw" => $get['draw'],
            "recordsTotal" => $data['total'],
            "recordsFiltered" => $data['total'],
            "data" => $list,
        );

        echo json_encode($output);
    }

    /**
     * 取得租公出借列表
     * @param int $uid
     */
    public function getMasterRentRecord(int $uid = 0) {
        $this->checkPerm('masterList');
        $get = $this->input->get(null, true);
        $data = $this->user->getMasterRentRecordList($uid);

        $list = [];
        foreach ($data['list'] as $k => &$row) {
            switch ($row['status']) {
                case 0:
                    $row['statustxt'] = '待接單';
                    break;
                case 1:
                    $row['statustxt'] = '租借中';
                    break;
                case 2:
                    $row['statustxt'] = '結案';
                    break;
                case 3:
                    $row['statustxt'] = '租供未回應';
                    break;
                case 4:
                    $row['statustxt'] = '租公拒絕';
                    break;
                case 5:
                    $row['statustxt'] = '租客取消';
                    break;
                case 6:
                    $row['statustxt'] = '租公已接單(預約時間未到)';
                    break;
                case 7:
                    $row['statustxt'] = '意見反應中';
                    break;
                case 8:
                    $row['statustxt'] = '已銷單';
                    break;
                default:
                    $row['statustxt'] = '';
            }
            $row['accessorytxt'] = implode('<br>', json_decode($row['device_accessory'], true));
            $d = [];
            $d[] = $row['ur_id'];
            $d[] = sprintf('<a class="showUser" href="javascript:void(0)" data-uid="%s"><i class="fa fa-share-square-o"></i> %s</a>', $row['renter_uid'], $row['name']);
            $d[] = $row['create_time'];
            $d[] = date('Y/m/d', strtotime($row['start_time'])) . ' ~ ' . date('Y/m/d', strtotime($row['end_time']));
            $d[] = $row['device_id'];
            $d[] = $row['device_name'];
            $d[] = $row['accessorytxt'];
            $d[] = $row['statustxt'];
            $d[] = $row['rent'];
            $d[] = $row['deposit'];
            $d[] = in_array($row['status'], [2,7]) ? sprintf('<a class="showAssessment btn btn-primary" href="javascript:void(0)" data-id="%s">評價</a>', $row['ur_id']) : '';
            $list[] = $d;
        }

        $output = array(
            "draw" => $get['draw'],
            "recordsTotal" => $data['total'],
            "recordsFiltered" => $data['total'],
            "data" => $list,
        );

        echo json_encode($output);
    }

    /**
     * 會員詳細狀態
     */
    public function userInfo() {
        $this->checkPerm('managerList');
        $this->checkData('uid');
        $user = $this->user->getUserById($this->mData['uid']);
        if (empty($user)) {
            show_404();
            return ;
        }
        $statustxt = '';
        switch ($user['status']) {
            case 0:
                $statustxt = '正常使用中';
                break;
            case 1:
            case 2:
                $statustxt = sprintf('停權中 - 將於 %s 恢復 ', date('Y/m/d', strtotime($user['lock_time'])));
                break;
            case 3:
                $statustxt = '永久停權';
                break;
            case 4:
                $statustxt = '已刪除';
                break;
        }
        $this->load->view('user/subview/user_info_subview',[
            'user' => $user,
            'statustxt' => $statustxt
        ]);
    }

    /**
     * 租公詳細狀態
     */
    public function masterInfo() {
        $this->checkPerm('masterList');
        $this->checkData('uid');
        $user = $this->user->getUserById($this->mData['uid']);
        if (empty($user) || $user['rent_master'] < 1) {
            show_404();
            return ;
        }
        $statustxt = '';
        switch ($user['status']) {
            case 0:
                $statustxt = '正常使用中';
                break;
            case 1:
            case 2:
                $statustxt = sprintf('停權中 - 將於 %s 恢復 ', date('Y/m/d', strtotime($user['lock_time'])));
                break;
            case 3:
                $statustxt = '永久停權';
                break;
            case 4:
                $statustxt = '已刪除';
                break;
        }

        $this->load->view('user/subview/master_info_subview',[
            'user' => $user,
            'statustxt' => $statustxt,
            'reward' => $this->user->getMasterTransactionTotal($this->mData['uid']),
            'reject' => $this->user->getMasterRentInfoByStatus($this->mData['uid'], 4),
            'miss' => $this->user->getMasterRentInfoByStatus($this->mData['uid'], 3)
        ]);
    }

    /**
     *　編輯面交點
     */
    public function locationEdit() {
        $this->checkPerm('masterEdit');
        $this->checkData('tpid', 'placename', 'cid', 'ct_id', 'address');

        $data = $this->user->getLocation($this->mData['tpid']);
        if (empty($data)) {
            $this->error();
            return;
        }

        $this->user->updateLocation($this->mData['tpid'], [
            'placename' => $this->mData['placename'],
            'cid' => $this->mData['cid'],
            'ct_id' => $this->mData['ct_id'],
            'address' => $this->mData['address'],
        ]);
        $this->success();
    }

    /**
     * 刪除面交點
     */
    public function locationDelete() {
        $this->checkPerm('masterEdit');
        $this->checkData('id');
        $this->user->deleteLocation($this->mData['id']);
        $this->success();
    }

    /**
     *　刪除會員（含租公）
     */
    public function deleteUser() {
        $this->checkPerm('managerEdit');
        $this->checkData('uid');
        if ($this->user->updateUser($this->mData['uid'], ['status' => 4])) {
            $this->success();
        }
        $this->error();
    }

}