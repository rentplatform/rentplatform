<?php

/**
 * Class UserApi
 * 模板相關API
 *
 * @property Template_model $template
 */
class Template_api extends AdminApiBase {

    public function __construct() {
        parent::__construct();
        $this->checkLogin();
        $this->load->model('Template_model', 'template');
    }

    /**
     * 修改模板
     */
    public function edit() {
        $this->checkData('type', 'content');
        //預設會使用xss clean，這裡避免html attribute 被xss clean掉，so 另外抓一次
        $this->mData['content'] = $this->input->post('content');

        if (!$this->template->update($this->mData['type'], $this->mData['content'])) {
            $this->error();
        }
        $this->success();


    }
}