<?php

/**
 * Class Group
 * @property Group_model        $group
 * @property Permission_model   $perm
 */
class Group_api extends AdminApiBase {

    public function __construct() {
        parent::__construct();
        $this->checkLogin();
        $this->load->model('Group_model', 'group');
    }

    public function insert() {
        $this->checkPerm('groupEdit');
        $this->checkData('name', 'desc', 'displayorder');

        if ($this->group->checkGroupNameExist($this->mData['name'])) {
            $this->error(array(
                'msg' => '該群組名稱已存在'
            ));
        }

        if ($this->group->insertGroup($this->mData)) {
            $this->success();
        }
        $this->error();
    }

    public function edit() {
        $this->checkPerm('groupEdit');
        $this->checkData('gid', 'name', 'desc', 'displayorder');
        $gid = $this->mData['gid'];
        unset($this->mData['gid']);
        if ($gid < 2) {
            $this->error();
        }
        if ($this->group->updateGroup($gid, $this->mData)) {
            $this->success();
        }
        $this->error();

    }

    public function editPerm() {
        $this->checkPerm('groupEdit');
        $this->checkData('gid', 'permids');
        $gid = $this->mData['gid'];
        unset($this->mData['gid']);
        if ($gid < 2) {
            $this->error();
        }

        $this->load->model('Permission_model', 'perm');
        if ($this->perm->editGroupPerm($gid, $this->mData['permids'])) {
            $this->success();
        }
        $this->error();

    }

    public function delete(int $gid = 0) {
        $this->checkPerm('groupDelete');
        if ($gid < 2) {
            $this->error();
        }
//        $detail = $this->group->getGroupById($gid);
        if ($this->group->deleteGroup($gid)) {
            $this->success();
        }
        $this->error();
    }

}