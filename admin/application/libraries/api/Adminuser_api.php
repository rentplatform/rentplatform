<?php

/**
 * Class User
 * @property Adminuser_model  $user
 * @property File             $file
 */
class Adminuser_api extends AdminApiBase
{

    public function __construct() {
        parent::__construct();
        $this->load->model('Adminuser_model', 'user');
    }

    /**
     * 登入
     */
    public function login() {
        $this->checkData('account', 'password');
        $user = $this->user->getAdminUser($this->mData['account'], md5($this->mData['password']));
        if (empty($user)) {
            $this->error();
            return;
        }
        $this->session->set_userdata(array(
            'aid' => $user['aid'],
            'gid' => $user['gid'],
            'pic' => $user['pic'],
            'name' => $user['name']
        ));
        $this->success();
    }

    /**
     * 新增管理者
     */
    public function insert() {
        $this->checkLogin();
        $this->checkPerm('adminEdit');
        $this->checkData('name', 'gid', 'account', 'password');

        if ($this->user->checkAdminUserExist($this->mData['account'])) {
            $this->error(array(
                'msg' => '該帳號已存在'
            ));
        }

        if (!empty($_FILES['file'])) {
            $this->load->library('file');
            list($uploadResult, $fileName, $errMsg) = $this->file->upload('file');
            if (!$uploadResult || empty($fileName['file'])) {
                $this->error(array(
                    'msg' => $errMsg
                ));
            }
            $this->mData['pic'] = $fileName['file'];
        }

        $this->mData['password'] = md5($this->mData['password']);

        if ($this->user->insertAdminUser($this->mData)) {
            $this->success();
        }
        $this->error();
    }

    /**
     * 編輯管理者
     */
    public function edit() {
        $this->checkLogin();
        $this->checkPerm('adminEdit');
        $this->checkData('aid', 'name', 'gid', array('password'));
        $user = $this->user->getAdminUserById($this->mData['aid']);
        if (empty($user)) {
            $this->error(array(
                'msg' => '該帳號不存在'
            ));
        }
        $aid = $this->mData['aid'];
        unset($this->mData['aid']);

        if (!empty($_FILES['file'])) {
            if ($user['pic'] && file_exists(PHOTO_PATH . $user['pic'])) {
                unlink(PHOTO_PATH . $user['pic']);
            }
            $this->load->library('file');
            list($uploadResult, $fileName, $errMsg) = $this->file->upload('file');
            if (!$uploadResult || empty($fileName['file'])) {
                $this->error(array(
                    'msg' => $errMsg
                ));
            }
            $this->mData['pic'] = $fileName['file'];
        }

        if (!empty($this->mData['password'])) {
            $this->mData['password'] = md5($this->mData['password']);
        } else {
            unset($this->mData['password']);
        }

        if ($this->user->updateAdminUser($aid, $this->mData)) {
            $loginaid = $this->session->userdata('aid');
            //修改的人為自己(登入中的人),則同步修改session內的資訊
            if ($loginaid == $aid) {
                $user = $this->user->getAdminUserById($aid);

                $this->session->set_userdata(array(
                    'aid' => $user['aid'],
                    'gid' => $user['gid'],
                    'pic' => $user['pic'],
                    'name' => $user['name']
                ));
            }
            $this->success();
        }
        $this->error(array('msg' => '無資料更新'));

    }

    /**
     * 刪除管理者
     * @param int $aid
     */
    public function delete(int $aid = 0) {
        $this->checkPerm('adminDelete');
        if (!$aid) {
            $this->error();
        }
        if ($this->user->deleteAdminUser($aid)) {
            $this->success();
        }
        $this->error();
    }

}