<?php

/**
 * Class Common
 * @property Setting_model $setting
 */
class Common_api extends AdminApiBase {

    public function __construct() {
        parent::__construct();
        $this->checkLogin();
        $this->load->model('Setting_model', 'setting');
    }

    /**
     * 修改全域設定API
     */
    public function setting() {
        $perms = array(
            'index_carousel_second' => 'carasolSetting',
            'ad_percent' => 'adPriceSetting',
            'gacode' => 'gaSetting',
            'gtcode' => 'gaSetting',
            'sso_keyword' => 'ssoSetting',
            'sso_description' => 'ssoSetting',
            'normal_rent' => 'rentSetting',
            'weekend_rent' => 'rentSetting',
            'use_global_ad' => 'adPriceSetting',
            'global_ad_date_start' => 'adPriceSetting',
            'global_ad_date_end' => 'adPriceSetting'
        );
//        $this->checkData(array_keys($perms));

        if (isset($this->mData['ad_percent']) && !isset($this->mData['use_global_ad'])) {
            $this->mData['use_global_ad'] = 0;
            $this->mData['global_ad_date_start'] = '';
            $this->mData['global_ad_date_end'] = '';
        }

        foreach ($this->mData as $key => $value) {
            if (!$this->permission->has($perms[$key])) {
                continue;
            }

            $this->setting->update($key, $value);
        }

        $this->success();
    }

    /**
     * 新增修改縣市
     */
    public function areaEdit() {
        $this->checkPerm('areaSetting');
        $this->checkData('name', array('cid'));
        if (isset($this->mData['cid'])) {
            $res = $this->setting->updateCountry($this->mData['cid'], $this->mData['name']);
        } else {
            $res = $this->setting->insertCountry($this->mData['name']);
        }
        if ($res) {
            $this->success();
        }
        $this->error();
    }

    /**
     * 新增修改區域
     */
    public function areaCityEdit() {
        $this->checkPerm('areaSetting');
        $this->checkData('name', 'cid', array('ctid'));
        if (!empty($this->mData['ctid'])) {
            $res = $this->setting->updateCity($this->mData['ctid'], $this->mData['name']);
        } else {
            $res = $this->setting->insertCity($this->mData['cid'], $this->mData['name']);
        }
        $this->success();
    }

    /**
     * 刪除縣市
     * @param int $cid
     */
    public function areaDelete(int $cid = 0) {
        $this->checkPerm('areaSetting');
        if (!$cid) {
            $this->error();
        }
        if ($this->setting->deleteCountry($cid)) {
            $this->success();
        }
        $this->error();
    }

    /**
     * 刪除區域
     * @param int $ctid
     */
    public function areaCityDelete(int $ctid = 0) {
        $this->checkPerm('areaSetting');
        if (!$ctid) {
            $this->error();
        }
        if ($this->setting->deleteCity($ctid)) {
            $this->success();
        }
        $this->error();
    }

    /**
     * 新增修改廣告優惠
     * @param int $adId
     */
    public function adDiscountEdit(int $adId = 0) {
        $this->checkPerm('adDiscountEdit');
        $this->checkData('title', 'percent', 'start_time', 'end_time', 'days');

        if (!$this->setting->checkAdDiscountDateValid($this->mData['start_time'], $this->mData['end_time'], $adId)) {
            $this->error(['msg' => '日期區間與現有優惠活動重複']);
        }
        $this->mData['end_time'] = $this->mData['end_time'] . ' 23:59:59';
        if ($adId) {
            $res = $this->setting->updateAdDiscount($adId, $this->mData);
        } else {
            $res = $this->setting->insertAdDiscount($this->mData);
        }
        $this->success();
    }

    /**
     * 刪除廣告優惠
     * @param int $adId
     */
    public function adDiscountDelete(int $adId = 0) {
        $this->checkPerm('adDiscountEdit');
        if (!$adId) {
            $this->error();
        }
        if ($this->setting->deleteAdDiscount($adId)) {
            $this->success();
        }
        $this->error();
    }

}