<?php

class File {

    private $mConfig = array(
        'ad' => array(
            'width' => 1000,
            'height' => 370,
            'size' => 1024
        ),
    );
    private $mFileNames = array();
    private $mCi;
    private $mType;

    public function __construct() {
        $this->mCi = &get_instance();
        $this->mCi->load->library('upload');
    }

    public function upload($type = '') {
        $this->mType = $type;
//        if (!array_key_exists($type, $this->mConfig)) {
//            $this->error();
//        }

        $files = $_FILES;
        
        foreach ($files as $name => $file) {
            if (is_array($file['name'])) {
                $cnt = count($file['name']);
                for ($i = 0; $i < $cnt; $i ++) {

                    $_FILES [$name] ['name'] = $file ['name'] [$i];
                    $_FILES [$name] ['type'] = $file ['type'] [$i];
                    $_FILES [$name] ['tmp_name'] = $file ['tmp_name'] [$i];
                    $_FILES [$name] ['error'] = $file ['error'] [$i];
                    $_FILES [$name] ['size'] = $file ['size'] [$i];
                    if ($this->doUpload($name)) {
                        $this->mFileNames[$name][] = $this->mCi->upload->data()['file_name'];
                    } else {
                        $this->reset();
                        return array(false, $files [$name] ['name'] [$i], sprintf('圖片上傳失敗，%s 格式錯誤或超過限定大小', $files [$name] ['name'] [$i]));
                    }
                }
            } else {
                if ($this->doUpload($name)) {
                    $this->mFileNames[$name] = $this->mCi->upload->data()['file_name'];
                } else {
                    $this->reset();
                    return array(false, $files [$name] ['name'], sprintf('圖片上傳失敗，%s 格式錯誤或超過限定大小', $files [$name] ['name']));
                }
            }
        }
        return array(true, $this->mFileNames, '');
    }

    private function doUpload($name) {
        $config = array(
            'upload_path' => PHOTO_PATH,
            'allowed_types' => 'gif|jpg|png',
            'encrypt_name' => TRUE,
//            'max_size' => $this->mConfig[$this->mType]['size'],
//            'max_width' => $this->mConfig[$this->mType]['width'],
//            'max_height' => $this->mConfig[$this->mType]['height'],
        );

        $this->mCi->upload->initialize($config);
        return $this->mCi->upload->do_upload($name);
    }

    private function reset() {
        //如果有一隻檔案儲存失敗，就會把剛剛的都刪掉
        foreach ($this->mFileNames as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $_k => $_v) {
                    if (file_exists(PHOTO_PATH . $_v)) {
                        unlink(PHOTO_PATH . $_v);
                    }
                }
            } else if (file_exists(PHOTO_PATH . $v)) {
                unlink(PHOTO_PATH . $v);
            }
        }
    }

}
