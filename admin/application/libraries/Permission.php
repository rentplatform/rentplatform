<?php

/**
 * Created by PhpStorm.
 * User: eric.chung
 * Date: 2017/10/12
 * Time: 下午 05:47
 */
class Permission {
    private $mHandlers = array();
    private $mPerm;
    private $mGid;

    public function __construct()
    {
        $ci = &get_instance();
        $ci->load->model('Permission_model', 'perm');
        $list = $ci->perm->getAllPermList();
        $this->mGid = $ci->session->userdata('gid');
        $this->mPerm = $ci->perm->getGroupPermList($this->mGid);
        foreach ($list as $k => $v) {
            $this->register($v['key'], function ($gid) use ($v) {
                if ((int)$gid === 1) {
                    return true;
                }
                foreach ($this->mPerm as $row) {
                    if ($row['key'] === $v['key']) {
                        return true;
                    }
                }
                return false;
//                return (bool)$this->mPerm->CheckPerm($gid, $v['key']);
            });
        }
    }

    public function register($kind, $handle)
    {
        $this->mHandlers[$kind] = $handle;
    }

    public function has($kind)
    {
        if (!array_key_exists($kind, $this->mHandlers)) {
            return FALSE;
        }

        if (!is_callable($this->mHandlers[$kind])) {
            return FALSE;
        }

        return call_user_func($this->mHandlers[$kind], $this->mGid);
    }

}