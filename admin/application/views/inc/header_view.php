<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>電電租 後台管理系統</title>
    <!-- Bootstrap -->
    <link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="/css/qwer/custom.min.css" rel="stylesheet">
    <link href="/css/qwer/admin.css" rel="stylesheet">

    <!-- jQuery -->
    <script src="/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

</head>

<body class="nav-md">
<!--右側區塊 start-->
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="/admin" class="site_title"><img style="width: 30px;" src="/images/icons/header__logo.png"/> <span> 後台管理</span></a>
                </div>

                <div class="clearfix"></div>
                <br/>

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section active">
<!--                        <h3>系統管理</h3>-->
                        <ul class="nav side-menu">
                            <?= gen_menu() ?>
                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <!--						<div class="sidebar-footer hidden-small">-->
                <!--							<a data-toggle="tooltip" data-placement="top" title="Settings">-->
                <!--								<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>-->
                <!--							</a>-->
                <!--							<a data-toggle="tooltip" data-placement="top" title="FullScreen">-->
                <!--								<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>-->
                <!--							</a>-->
                <!--							<a data-toggle="tooltip" data-placement="top" title="Lock">-->
                <!--								<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>-->
                <!--							</a>-->
                <!--							<a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">-->
                <!--								<span class="glyphicon glyphicon-off" aria-hidden="true"></span>-->
                <!--							</a>-->
                <!--						</div>-->
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                <?= $user['pic'] ? "<img src=\"/upload/{$user['pic']}\" alt=\"\">" : '' ?>
                                <?= $user['name'] ?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <!--								<li><a href="javascript:;"> Profile</a></li>-->
                                <!--								<li>-->
                                <!--									<a href="javascript:;">-->
                                <!--										<span class="badge bg-red pull-right">50%</span>-->
                                <!--										<span>Settings</span>-->
                                <!--									</a>-->
                                <!--								</li>-->
                                <!--								<li><a href="javascript:;">Help</a></li>-->
                                <li><a href="<?= base_url(); ?>logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">