<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><a href="<?= base_url(); ?>user/masterList">租公列表</a> > 面交點列表</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered datatable-admin">
                        <thead>
                        <tr>
                            <th>面交點名稱</th>
                            <th>地址</th>
                            <th>被使用次數</th>
                            <th>功能</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($data as $row): ?>
                            <tr>
                                <td><?= $row['placename'] ?></td>
                                <td><?= $row['countryName'] . $row['cityName'] . $row['address'] ?></td>
                                <td><?= $row['usetimes'] ?></td>
                                <td>
                                    <?php if ($this->permission->has('masterEdit')): ?>
                                        <a class="btn btn-primary" href="<?= base_url(); ?>user/locationEdit/<?= $row['tp_id'] ?>">編輯</a>
                                        <a class="btn btn-primary deleteLocation" data-id="<?= $row['tp_id'] ?>">刪除</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	$(function () {
		$('.deleteLocation').on('click', function () {
			var id = $(this).data('id');
			if (!confirm('確定刪除此面交點?')) {
			    return;
			}
			if ($(this).parents('tbody').find('tr').length < 2) {
			    alert('至少需保留一個面交點');
			    return;
			}
			$.post('<?= base_url(); ?>api/User_api/locationDelete', {id: id}, null, 'json').always(function (res) {
				if (res && res.valid) {
				    window.location.reload();
				    return;
				}
				alert('刪除失敗');
            });
        });
    });
</script>