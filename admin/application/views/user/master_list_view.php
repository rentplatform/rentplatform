<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>租公列表</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered datatable-admin" data-linkurl="<?= base_url(); ?>user/masterInsert" data-ajax="<?= base_url(); ?>api/User_api/getMasterList">
                        <thead>
                        <tr>
                            <th>會員編號</th>
                            <th>身份證號</th>
                            <th>真實姓名</th>
                            <th>電話</th>
                            <th>E-MAIL</th>
                            <th>最後登入時間</th>
                            <th>功能</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="card" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">評價資料</h4>
            </div>
            <div class="modal-body">
                loading...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
            </div>

        </div>
    </div>
</div>
<script>
    $(function() {

        $(document).on('click', '.showUserInfo', function () {
            var id = $(this).data('id');
            var $btn = $(this);
            $btn.button('loading');
            $('#card .modal-body').load('<?= base_url(); ?>api/User_api/masterInfo', {uid: id}, function() {
                $btn.button('reset');
                $('#card .modal-title').text('詳細狀態');
                $('#card').modal('show');
            });
        });
        $(document).on('click', '.deleteUser', function () {
            var id = $(this).data('id');
            var $btn = $(this);
            $btn.button('loading');
            if (confirm('確定刪除此會員？')) {
                $.post('<?= base_url(); ?>api/User_api/deleteUser', {uid:id}, function (res) {
                    if (res && res.valid) {
                        $('.datatable-admin').data('object').ajax.reload();
                    } else {
                        alert('刪除失敗');
                        $btn.button('reset');
                    }

                }, 'json')
            } else {
                $btn.button('reset');
            }
        });
    })
</script>