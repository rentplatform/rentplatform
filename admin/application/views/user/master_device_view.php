<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><a href="<?= base_url(); ?>user/masterList">租公列表</a> > 電器列表</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered datatable-admin">
                        <thead>
                        <tr>
                            <th>電器編號</th>
                            <th>電器名稱</th>
                            <th>電器圖片</th>
                            <th>電器配件</th>
                            <th>電器描述</th>
<!--                            <th>租金</th>-->
                            <th>押金</th>
                            <th>出租次數</th>
                            <th>目前租借狀態</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($data as $row): ?>
                            <tr>
                                <td><?= $row['device_id'] ?></td>
                                <td><?= $row['name'] ?></td>
                                <td><img src="/upload/<?= $row['filename'] ?>" style="width: 50px;"></td>
                                <td><?= implode('<br>', json_decode($row['accessory'], true)) ?></td>
                                <td><?= $row['description'] ?></td>
<!--                                <td>--><?//= $row['rent'] ?><!--</td>-->
                                <td><?= $row['deposit'] ?></td>
                                <td><?= $row['rent_time'] ?></td>
                                <td><?= $row['status'] > 1 ? '出租中' : ($row['status'] > 0 ? '上架中' : '暫停') ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>