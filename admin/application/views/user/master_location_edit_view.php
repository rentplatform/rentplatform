<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><a href="<?= base_url(); ?>user/masterList">租公列表</a> > <a href="<?= base_url(); ?>user/locationList/<?= $data['uid'] ?>">>面交點列表</a> > 編輯面交點</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <form class="form-horizontal form-label-left formValidation" enctype="multipart/form-data" action="<?= base_url(); ?>api/User_api/locationEdit" data-is-update="1" data-redirect="<?= base_url(); ?>user/locationList/<?= $data['uid'] ?>" novalidate>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">地點名稱 <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12"
                                        name="placename" placeholder="" maxlength="20"
                                       required type="text" value="<?= $data['placename'] ?? '' ?>">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gid">縣市 <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control selCountry" name="cid" data-target-city-sel=".selCity" required>
                                    <?php foreach($country as $row): ?>
                                        <option value="<?= $row['cid'] ?>" <?= $row['cid'] == $data['cid'] ? 'selected' : '' ?>><?= $row['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gid">鄉鎮市區 <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control selCity" name="ct_id" required>
                                    <?php foreach($city as $row): ?>
                                        <option value="<?= $row['ct_id'] ?>" <?= $row['ct_id'] == $data['ct_id'] ? 'selected' : '' ?>><?= $row['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">路名 <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12"
                                       name="address" placeholder="" maxlength="100"
                                       required type="text" value="<?= $data['address'] ?? '' ?>">
                            </div>
                        </div>
                        <div class="alert alert-danger alert-dismissible fade" role="alert" style="width: 50%;margin-left: 25%;">
                            <span class="txt"></span>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="hidden" name="tpid" value="<?= $tpid ?>">
                                <a href="<?= base_url(); ?>user/locationList/<?= $data['uid'] ?>" class="btn btn-primary">返回</a>
                                <button id="send" type="submit" class="btn btn-success">送出</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>