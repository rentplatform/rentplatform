<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?= isset($uid) ? '編輯' : '新增' ?>會員</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <form class="form-horizontal form-label-left formValidation"
						  enctype="multipart/form-data"
						  action="<?= base_url(); ?>api/user_api/<?= !isset($data) ? 'insert' : 'edit' ?>"
						  data-redirect="<?= base_url(); ?>user/<?= isset($isRenter) ? 'masterList' : 'uList' ?>"
						  data-is-update="<?= isset($uid) ? '1' : '0' ?>"
                          novalidate>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">真實姓名 <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12" rangelength="2,10"
                                        name="name" placeholder="請輸入2~10個字"
                                       required type="text" value="<?= $data['name'] ?? '' ?>">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nickname">暱稱 <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="nickname" class="form-control col-md-7 col-xs-12" rangelength="2,10"
                                       name="nickname" placeholder="請輸入2~10個字"
                                       required type="text" value="<?= $data['nickname'] ?? '' ?>">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sex">性別 <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div id="gender" class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-default  <?= empty($data) || ($data['sex'] ?? 0) && $data['sex'] == 1 ? 'active' : '' ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                        <input type="radio" name="sex" value="1"  <?= empty($data) || ($data['sex'] ?? 0) && $data['sex'] == 1 ? 'checked' : '' ?>> &nbsp; 男&nbsp;
                                    </label>
                                    <label class="btn btn-default  <?= ($data['sex'] ?? 0) && $data['sex'] == 2 ? 'active' : '' ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                        <input type="radio" name="sex" value="2"  <?= ($data['sex'] ?? 0) && $data['sex'] == 2 ? 'checked' : '' ?>> &nbsp; 女&nbsp;
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_number">
                                身分證號
                                <?= !isset($uid) ? '<span class="required">*</span>' : '' ?>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="id_number" required
                                   class="form-control col-md-7 col-xs-12" placeholder="" maxlength="10" value="<?= $data['id_number'] ?? '' ?>">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone_number">
                                電話
                                <?= !isset($uid) ? '<span class="required">*</span>' : '' ?>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="phone_number" required
                                       class="form-control col-md-7 col-xs-12" placeholder="" maxlength="15" value="<?= $data['phone_number'] ?? '' ?>">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">
                                E-MAIL
                                <?= !isset($uid) ? '<span class="required">*</span>' : '' ?>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="email" id="email" name="username" required
                                       class="form-control col-md-7 col-xs-12" placeholder="" value="<?= $data['username'] ?? '' ?>">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label for="password" class="control-label col-md-3">
                                密碼
                                <?= !isset($uid) ? '<span class="required">*</span>' : '' ?>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="password" type="password" name="password"
                                       class="form-control col-md-7 col-xs-12" rangelength="5,20" <?= !isset($uid) ? 'required' : '' ?>>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">
                                確認密碼
                                <?= !isset($uid) ? '<span class="required">*</span>' : '' ?>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="password2" type="password" equalTo="#password"
                                       class="form-control col-md-7 col-xs-12" <?= !isset($uid) ? 'required' : '' ?>>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">
                                地址
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="address"
                                       class="form-control col-md-7 col-xs-12" placeholder="" maxlength="50" value="<?= $data['address'] ?? '' ?>">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="line_id">
                                LineID
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="line_id"
                                       class="form-control col-md-7 col-xs-12" maxlength="20" value="<?= $data['line_id'] ?? '' ?>">
                            </div>
                        </div>
                        <?php if (!isset($isRenter) && !empty($data) && !$data['rent_master']): ?>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="line_id">
                                    身份切換成租公
                                </label>
                                <div id="gender" class="btn-group col-md-6 col-sm-6 col-xs-12" data-toggle="buttons">
                                    <label class="btn btn-default active" data-toggle-class="btn-primary" data-toggle-ç-class="btn-default">
                                        <input type="radio" name="rent_master" value="0" checked> &nbsp; 否 &nbsp;
                                    </label>
                                    <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                        <input type="radio" name="rent_master" value="1"> &nbsp; 是 &nbsp;
                                    </label>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($data)): ?>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="line_id">
                                    帳號狀態
                                </label>
                                <div id="gender" class="btn-group col-md-6 col-sm-6 col-xs-12" data-toggle="buttons">
                                    <label class="btn btn-default <?= empty($data) || !empty($data) && $data['status'] == 0 ? 'active' : '' ?>" data-toggle-class="btn-primary" data-toggle-ç-class="btn-default">
                                        <input type="radio" name="status" value="0" <?= empty($data) || ($data['status'] ?? 0) && $data['status'] == 0 ? 'checked' : '' ?>> &nbsp; 正常 &nbsp;
                                    </label>
                                    <label class="btn btn-default <?= ($data['status'] ?? 0) && $data['status'] == 1 ? 'active' : '' ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                        <input type="radio" name="status" value="<?= isset($isRenter) ? '2' : '1' ?>" <?= ($data['status'] ?? 0) && $data['status'] == (isset($isRenter) ? '2' : '1') ? 'checked' : '' ?>> &nbsp; 第一次停權 &nbsp;
                                    </label>
                                    <label class="btn btn-default <?= ($data['status'] ?? 0) && $data['status'] == 3 ? 'active' : '' ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                        <input type="radio" name="status" value="3" <?= ($data['status'] ?? 0) && $data['status'] == 3 ? 'checked' : '' ?>> &nbsp; 永久停權 &nbsp;
                                    </label>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">
                                    停權時間
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" class="form-control has-feedback-left" id="single_cal4" name="lock_time" placeholder="停權時間" aria-describedby="inputSuccess2Status4">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status4" class="sr-only">(success)</span>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="alert alert-danger alert-dismissible fade" role="alert" style="width: 50%;margin-left: 25%;">
                            <span class="txt"></span>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="hidden" name="uid" value="<?= $uid ?? '' ?>">
                                <input type="hidden" name="is_renter" value="<?= $isRenter ?? '' ?>">
                                <a href="<?= base_url(); ?>user/<?= isset($isRenter) ? 'masterList' : 'uList' ?>" class="btn btn-primary">返回</a>
                                <button id="send" type="submit" class="btn btn-success">送出</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>