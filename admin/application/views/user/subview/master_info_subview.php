<table class="table table-bordered">
    <tr>
        <td>總營收</td>
        <td><?= number_format($reward) ?></td>
    </tr>
    <tr>
        <td>漏單次數</td>
        <td><?= $miss ?></td>
    </tr>
    <tr>
        <td>取消次數</td>
        <td><?= $reject ?></td>
    </tr>
    <tr>
        <td>出租次數</td>
        <td><?= $user['master_time'] ?></td>
    </tr>
    <tr>
        <td>租用次數</td>
        <td><?= $user['rent_time'] ?></td>
    </tr>
    <tr>
        <td>正評價次數</td>
        <td><?= $user['good'] ?></td>
    </tr>
    <tr>
        <td>負評價次數</td>
        <td><?= $user['bad'] ?></td>
    </tr>
    <tr>
        <td>租公廣告費%數</td>
        <td><?= $user['ad_percent'] . ($user['ad_percent'] > 0 && strtotime($user['ad_percent_date']) < time() ? '(已過期)' : '') ?></td>
    </tr>
    <tr>
        <td>帳號狀態</td>
        <td><?= $statustxt ?></td>
    </tr>
</table>