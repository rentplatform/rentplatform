<table class="table table-bordered">
    <tr>
        <td>總交易次數</td>
        <td><?= $user['rent_time'] ?></td>
    </tr>
    <tr>
        <td>正評價次數</td>
        <td><?= $user['good'] ?></td>
    </tr>
    <tr>
        <td>負評價次數</td>
        <td><?= $user['bad'] ?></td>
    </tr>
    <tr>
        <td>帳號狀態</td>
        <td><?= $statustxt ?></td>
    </tr>
</table>