<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>電器列表</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered datatable-admin" data-linkurl="<?= base_url(); ?>rentmanager/deviceEdit" data-ajax="<?= base_url(); ?>api/Rentmanager_api/getDeviceList">
                        <thead>
                        <tr>
                            <th>電器圖片</th>
                            <th>品牌</th>
                            <th>電器類型</th>
                            <th>電器名稱</th>
                            <th>配件</th>
                            <th>描述</th>
                            <th>押金</th>
                            <th>租金</th>
                            <th>擁有者</th>
                            <th>狀態</th>
                            <th>功能</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>