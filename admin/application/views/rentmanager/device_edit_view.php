<link rel="stylesheet" href="/vendors/jquery.tagsinput/src/jquery.tagsinput.css">
<style>
    .upload-area {
        position: relative;
        display: inline-block;
        border: 0;
        max-width: 150px;
        max-height: 150px;
        cursor: pointer;
        margin-top: 12px;
        padding-left: 0;
        padding-right: 4px;
    }
    .img-uploaded {
        width: 146px;
        height: 146px;
    }
    .img-uploaded {
        width: 100%;
        height: 29.6vw;
        max-height: 146px;
    }
    .img-container {
        background-size: cover;
        background-position: 50% 50%;
        margin: 0 auto;
    }
    .upload-button {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
        z-index: 1;
        cursor: pointer;
    }
    .delete-img-button {
        background-image: url("/images/icons/btn_delete-image.png");
        background-size: cover;
        background-position: 50% 50%;
        background-repeat: no-repeat;
    }
    .delete-img-button {
        display: inline-block;
        width: 32px;
        height: 32px;
        position: absolute;
        top: -12px;
        right: -6px;
        z-index: 10;
    }
</style>
<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?= isset($deviceId) ? '編輯' : '新增' ?>電器</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <form class="form-horizontal form-label-left formValidation" enctype="multipart/form-data"
                          action="<?= base_url(); ?>api/Rentmanager_api/editDevice/<?= $deviceId ?? '' ?>"
                          data-redirect="<?= base_url(); ?>rentmanager/deviceList"
                          data-is-update="<?= isset($deviceId) ? 1 : 0 ?>"novalidate>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="file">圖片<span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php for ($i = 0; $i < 3; $i++): ?>
                                    <div class="upload-area col-xs-4">
                                        <div class="img-container img-uploaded"
                                             style="background-image: url('<?= !empty($data['pic'][$i]['filename']) ? '/upload/' . $data['pic'][$i]['filename'] : '/images/icons/upload-image.png' ?>')"></div>
                                        <input class="upload-button" type="file"
                                               name="file[]" <?= !isset($data) ? 'required' : '' ?>>
                                        <?php if (!empty($data['pic'][$i]['filename'])): ?>
                                            <i class="delete-img-button"></i>
                                            <input type="hidden" name="oldpic[]"
                                                   value="<?= $data['pic'][$i]['filename'] ?>">
                                        <?php endif; ?>
                                    </div>
                                <?php endfor; ?>
                            </div>
                        </div>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="bid">品牌<span
										class="required">*</span>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<select class="form-control selCity" name="bid" required>
                                    <?php foreach($brand as $row): ?>
										<option value="<?= $row['bid'] ?>" <?= isset($deviceId) && $row['bid'] == $data['bid'] ? 'selected' : '' ?>><?= $row['name'] ?></option>
                                    <?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="dt_id">電器類型<span
										class="required">*</span>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<select class="form-control selCity" name="dt_id" required>
                                    <?php foreach($deviceType as $row): ?>
										<option value="<?= $row['dt_id'] ?>" <?= isset($deviceId) && $row['dt_id'] == $data['dt_id'] ? 'selected' : '' ?>><?= $row['name'] ?></option>
                                    <?php endforeach; ?>
								</select>
							</div>
						</div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">電器名稱<span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12"
                                       name="name" placeholder=""
                                       required type="text" value="<?= $data['name'] ?? '' ?>">
                            </div>
                        </div>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="accessory">配件<span
										class="required">*</span>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input id="accessory" type="text" class="tags form-control" name="accessory" value="<?= isset($deviceId) && $data['accessory'] ? implode(',', json_decode($data['accessory'], true)) : '' ?>" data-max-tags="3" />
								<div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;">(最多3個, 輸入完請按enter)</div>
							</div>
						</div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">描述<span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="form-control" rows="3" placeholder="" name="description"><?= $data['description'] ?? '' ?></textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="deposit">押金<span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12"
                                       name="deposit" placeholder=""
                                       required type="number" value="<?= $data['deposit'] ?? '' ?>">
                            </div>
                        </div>
<!--                        <div class="item form-group">-->
<!--                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rent">租金<span-->
<!--                                        class="required">*</span>-->
<!--                            </label>-->
<!--                            <div class="col-md-6 col-sm-6 col-xs-12">-->
<!--                                <input class="form-control col-md-7 col-xs-12"-->
<!--                                       name="rent" placeholder=""-->
<!--                                       required type="number" value="--><?//= $data['rent'] ?? '' ?><!--">-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="uid">擁有者<span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control selCity" name="uid" required>
                                    <?php if (!isset($deviceId)): ?>
                                        <option value="">請選擇擁有者</option>
                                    <?php endif ?>
                                    <?php foreach($masterList as $row): ?>
                                        <option value="<?= $row['uid'] ?>" <?= isset($deviceId) && $row['uid'] == $data['uid'] ? 'selected' : '' ?>><?= $row['name'] ?> / <?= $row['username'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">
                                狀態
                            </label>
                            <div id="gender" class="btn-group col-md-6 col-sm-6 col-xs-12" data-toggle="buttons">
                                <label class="btn btn-default <?= !isset($deviceId) || $data['status'] < 1 ? 'active' : '' ?>" data-toggle-class="btn-primary" data-toggle-ç-class="btn-default">
                                    <input type="radio" name="status" value="0" <?= !isset($deviceId) || $data['status'] < 1 ? 'checked' : '' ?>> &nbsp; 暫停 &nbsp;
                                </label>
                                <label class="btn btn-default <?= isset($deviceId) && $data['status'] > 0 ? 'active' : '' ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                    <input type="radio" name="status" value="1" <?= isset($deviceId) && $data['status'] > 0 ? 'checked' : '' ?>> &nbsp; 上架 &nbsp;
                                </label>
                            </div>
                        </div>
                        <div class="alert alert-danger alert-dismissible fade" role="alert" style="width: 50%;margin-left: 25%;">
                            <span class="txt"></span>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <a href="<?= base_url(); ?>rentmanager/deviceList" class="btn btn-primary">返回</a>
                                <button id="send" type="submit" class="btn btn-success">送出</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
<script>
	$(function() {
        $('#accessory').tagsInput({
            maxTags: 3,
            defaultText: '輸入配件'
        });
        $("input[type='submit']").click(function(){
            var $fileUpload = $("input[type='file']");
            if (parseInt($fileUpload.get(0).files.length)>2){
                alert("You can only upload a maximum of 2 files");
            }
        });
        $("input[type='file']").change(function () {
            var $parent = $(this).parent();
            var $img = $parent.find(".img-container");
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $img.attr('style', 'background-image: url('+e.target.result+')');
                    $parent.append('<i class="delete-img-button"></i>');
                    $parent.find('input[type=hidden]').remove();
                };
                reader.readAsDataURL(this.files[0]);
            }
        });


        $(document).on('click', '.delete-img-button', function (e) {

            var target = $(this).parent();
            target.find(".img-uploaded").attr("style", "background-image: url('/images/icons/upload-image.png')");
            target.find(".upload-button").val('');
            target.find('input[type=hidden]').remove();
            $(this).remove();
        });
    });
</script>