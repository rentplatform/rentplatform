<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>意見反應列表</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered datatable-admin" data-ajax="<?= base_url(); ?>api/Rentmanager_api/getArbitrationTableList">
                        <thead>
                        <tr>
                            <th>意見反應編號</th>
                            <th>交易編號</th>
                            <th>出租人</th>
                            <th>租借人</th>
                            <th>訂單成立時間</th>
                            <th>交易時間</th>
                            <th>電器編號</th>
                            <th>電器名稱</th>
                            <th>配件清單</th>
                            <th>交易狀態</th>
                            <th>租金</th>
                            <th>押金</th>
                            <th>功能</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="card" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">評價資料</h4>
            </div>
            <div class="modal-body">
                loading...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
<div id="assessmentInfo" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">意見反應資訊</h4>
            </div>
            <div class="modal-body">
                loading...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
            </div>

        </div>
    </div>
</div>
<script>
    $(function() {
        $(document).on('click', '.showAssessment', function () {
            var id = $(this).data('id');
            var $btn = $(this);
            $btn.button('loading');
            $('#card .modal-body').load('<?= base_url(); ?>api/Rentmanager_api/getRentAssessment', {id: id}, function() {
                $btn.button('reset');
                $('#card .modal-title').text('評價資料');
                $('#card').modal('show');
            });

        });
        $(document).on('click', '.showUser', function () {
            var id = $(this).data('uid');
            var $btn = $(this);
            $btn.button('loading');
            $('#card .modal-body').load('<?= base_url(); ?>api/Rentmanager_api/getUserData', {uid: id}, function() {
                $btn.button('reset');
                $('#card .modal-title').text('會員資料');
                $('#card').modal('show');
            });

        });
        $(document).on('click', '.showInfo', function () {
            var id = $(this).data('id');
            var $btn = $(this);
            $btn.button('loading');
            $('#assessmentInfo .modal-body').load('<?= base_url(); ?>api/Rentmanager_api/getArbitrationInfo', {abid: id}, function() {
                $btn.button('reset');
                $('#assessmentInfo').modal('show');
            });

        });
    })
</script>