<style>
    .icon_positive {
        width: 34px;
        height: 30px;
        background-image: url(/images/icons/icon-sprite.png);
        background-position: -682px -78px;
    }
    .icon_negative {
        width: 34px;
        height: 30px;
        background-image: url(/images/icons/icon-sprite.png);
        background-position: -716px -78px;
    }
    .icon-sprite {
        background-size: 955px 399px;
        background-repeat: no-repeat;
        display: inline-block;
    }
</style>
<table class="table table-bordered">
    <tr>
        <td>租客</td>
        <td><?= $data['name'] ?></td>
        <?php if ($data['renter_assessment'] > 0): ?>
            <td><span class="icon-sprite <?= $data['renter_assessment'] > 1 ? 'icon_negative' : 'icon_positive' ?>"></span></td>
            <td><?= $data['renter_comment'] ?></td>
        <?php else: ?>
            <td colspan="2">尚未評價</td>
        <?php endif; ?>
    </tr>
    <tr>
        <td>租公</td>
        <td><?= $data['device_owner_name'] ?></td>
        <?php if ($data['owner_assessment'] > 0): ?>
            <td><span class=" icon-sprite <?= $data['owner_assessment'] > 1 ? 'icon_negative' : 'icon_positive' ?>"></span></td>
            <td><?= $data['owner_comment'] ?></td>
        <?php else: ?>
            <td colspan="2">尚未評價</td>
        <?php endif; ?>
    </tr>
</table>