<style>
    #arbitrationForm textarea {
        width: 400px;
        resize: none;
    }
</style>
<ul class="list-unstyled msg_list">
    <li>
        <a>
            <div>
                <h4 class="message">意見反應人: <?= $data['abuid'] == $data['renter_uid']  ? $data['name'] . '(租客)' : $data['device_owner_name'] . '(租公)' ?></h4>
<!--                <span class="time"></span>-->
            </div>
            <h4 class="message">原因: <?= $data['_reason'] ?></h4>
            <h4 class="message">聯絡電話: <?= $data['abuid'] == $data['renter_uid'] ? $data['renter_phone'] : $data['device_owner_phone'] ?></h4>
            <h4 class="message">E-MAIL: <?= $data['abuid'] == $data['renter_uid'] ? $data['username'] : $data['device_owner_username'] ?></h4>
            <h4 class="message">意見反應時間: <?= $data['create_ass_time'] ?></h4>
        </a>
    </li>
    <li>
        <form id="arbitrationForm" class="form-horizontal form-label-left" >
            <div class="form-group">
                <label class="col-sm-2 control-label">租客理由</label>
                <div class="col-sm-9">
                    <div class="input-group">
                        <textarea class="form-control" name="renter" rows="3" placeholder="" required></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">租公理由</label>
                <div class="col-sm-9">
                    <div class="input-group">
                        <textarea class="form-control" name="owner" rows="3" placeholder="" required></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">官方備註</label>
                <div class="col-sm-9">
                    <div class="input-group">
                        <textarea class="form-control" name="comment" rows="3" placeholder="" required></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">處理方式</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div id="gender" class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                            <input type="radio" name="type" value="1" data-parsley-multiple="gender"> &nbsp; 銷單 &nbsp;
                        </label>
                        <label class="btn btn-default active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                            <input type="radio" name="type" value="2" data-parsley-multiple="gender" checked> 駁回
                        </label>
                    </div>
                </div>
            </div>
            <div class="alert alert-danger alert-dismissible fade" role="alert" style="width: 50%;margin-left: 25%;">
                <span class="txt"></span>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-md-2">
                    <div class="input-group">
                        <input type="hidden" name="id" value="<?= $data['ab_id'] ?>"/>
                        <button class="btn btn-success" type="submit" >提交</button>
                    </div>
                </div>
            </div>
        </form>

    </li>
</ul>
<script>
    $(function() {
        $('#arbitrationForm').validate({
            submitHandler:function(form) {
                $(form).find('.alert-danger').removeClass('in').find('.txt').text('');
                $.request('<?= base_url(); ?>api/Rentmanager_api/updateArbitrationInfo', $(form).serializeArray(), undefined)
                    .always(function (res, status) {
                        res = res || {};
                        if (res.valid) {
                            alert('審核完成');
                            window.location.href = '<?= base_url(); ?>rentmanager/arbitrationList';
                            return;
                        } else {
                            $(form).find('.alert-danger').addClass('in').find('.txt').text(res.msg || '表單提交失敗');
                            $(form).find('input').one('focus', function () {
                                $(form).find('.alert-danger').removeClass('in').find('.txt').text('');
                            });
                        }
                    });
            }
        });
    })
</script>
