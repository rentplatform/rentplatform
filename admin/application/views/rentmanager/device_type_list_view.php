

<div class="">

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>類型列表</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered datatable-admin" data-linkurl="<?= base_url(); ?>Rentmanager/deviceTypeEdit">
                        <thead>
                        <tr>
                            <th>編號</th>
                            <th>類型名稱</th>
                            <th>功能</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($list as $row): ?>
                            <tr>
                                <td><?= $row['dt_id'] ?></td>
                                <td><?= $row['name'] ?></td>
                                <td>
                                    <?php if ($this->permission->has('deviceTypeEdit')): ?>
										<a class="btn btn-primary" href="<?= base_url(); ?>Rentmanager/deviceTypeEdit/<?= $row['dt_id'] ?>">編輯</a>
										<a class="btn btn-primary listDataDelete" href="javascript:void(0)" data-url="<?= base_url(); ?>api/Rentmanager_api/deleteDeviceType/<?= $row['dt_id'] ?>">刪除</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>