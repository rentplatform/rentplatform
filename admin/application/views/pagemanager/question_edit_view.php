<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?= isset($data) ? '編輯' : '新增' ?>常見問題</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form class="form-horizontal form-label-left formValidation"
                          enctype="multipart/form-data"
                          action="<?= base_url(); ?>api/Pagemanager_api/editQuestion/<?= $data['qid'] ?? '' ?>"
                          data-redirect="<?= base_url(); ?>pagemanager/question"
                          data-is-update="<?= isset($data) ? '1' : '0' ?>"
                          novalidate>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">
                                標題<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control col-md-7 col-xs-12" name="title" placeholder="標題" value="<?= $data['title'] ?? '' ?>" required>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">
                                內文<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="form-control col-md-7 col-xs-12" name="description" placeholder="內文" required><?= $data['description'] ?? '' ?></textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="displayorder">
                                排序<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" class="form-control col-md-7 col-xs-12" name="displayorder" placeholder="請輸入數字" value="<?= $data['displayorder'] ?? 1 ?>" required>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">
                                狀態
                            </label>
                            <div id="gender" class="btn-group col-md-6 col-sm-6 col-xs-12" data-toggle="buttons">
                                <label class="btn btn-default <?= !isset($data) || $data['status'] < 1 ? 'active' : '' ?>" data-toggle-class="btn-primary" data-toggle-ç-class="btn-default">
                                    <input type="radio" name="status" value="0" <?= !isset($data) || $data['status'] < 1 ? 'checked' : '' ?>> &nbsp; 暫停 &nbsp;
                                </label>
                                <label class="btn btn-default <?= isset($data) && $data['status'] > 0 ? 'active' : '' ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                    <input type="radio" name="status" value="1" <?= isset($data) && $data['status'] > 0 ? 'checked' : '' ?>> &nbsp; 上架 &nbsp;
                                </label>
                            </div>
                        </div>
                        <div class="alert alert-danger alert-dismissible fade" role="alert" style="width: 50%;margin-left: 25%;">
                            <span class="txt"></span>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <a href="<?= base_url(); ?>pagemanager/question" class="btn btn-primary">返回</a>
                                <button id="send" type="submit" class="btn btn-success">送出</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>