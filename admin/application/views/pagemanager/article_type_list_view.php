

<div class="">

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>專欄文章類型列表</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered datatable-admin" data-linkurl="<?= base_url(); ?>pagemanager/articleTypeEdit">
                        <thead>
                        <tr>
                            <th>編號</th>
                            <th>類型</th>
                            <th>功能</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($list as $row): ?>
                            <tr>
                                <td><?= $row['atype_id'] ?></td>
                                <td><?= $row['name'] ?></td>
                                <td>
                                    <?php if ($this->permission->has('brandEdit') && $row['atype_id'] > 1): ?>
										<a class="btn btn-primary" href="<?= base_url(); ?>pagemanager/articleTypeEdit/<?= $row['atype_id'] ?>">編輯</a>
										<a class="btn btn-primary listDataDelete" href="javascript:void(0)" data-url="<?= base_url(); ?>api/Pagemanager_api/deleteArticleType/<?= $row['atype_id'] ?>">刪除</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>