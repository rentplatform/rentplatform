<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?= isset($data) ? '編輯' : '新增' ?>專欄文章類型</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <form class="form-horizontal form-label-left formValidation" enctype="multipart/form-data"
                          action="<?= base_url(); ?>api/Pagemanager_api/editArticleType/<?= $data['atype_id'] ?? '' ?>"
                          data-redirect="<?= base_url(); ?>pagemanager/articleType"
                          data-is-update="<?= isset($data) ? 1 : 0 ?>"novalidate>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">專欄文章類型名稱 <span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12" maxlength="10"
                                       name="name" placeholder="最多10個字"
                                       required type="text" value="<?= $data['name'] ?? '' ?>">
                            </div>
                        </div>
                        <div class="alert alert-danger alert-dismissible fade" role="alert" style="width: 50%;margin-left: 25%;">
                            <span class="txt"></span>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <a href="<?= base_url(); ?>pagemanager/articleType" class="btn btn-primary">返回</a>
                                <button id="send" type="submit" class="btn btn-success">送出</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>