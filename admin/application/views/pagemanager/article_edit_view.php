<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?= isset($data) ? '編輯' : '新增' ?>專欄文章</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <form class="form-horizontal form-label-left formValidation"
                          enctype="multipart/form-data"
                          action="<?= base_url(); ?>api/Pagemanager_api/editArticle/<?= $atid ?? '' ?>"
                          data-redirect="<?= base_url(); ?>pagemanager/article"
                          data-is-update="<?= isset($data) ? '1' : '0' ?>"
                          novalidate>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pic">圖片
                                <?= isset($data) ? '' : '<span class="required">*</span>' ?>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input class="picinput" type="file" name="pic" <?= isset($data) ? '' : 'required' ?>/>
                                <?php if (!empty($data['pic'])): ?>
                                    <img src="/upload/<?= $data['pic'] ?>" style="height:100px;">
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gid">類型 <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="atype_id" required>
                                    <option value="" <?= isset($data) ? '' : 'selected' ?>>請選擇類型</option>
                                    <?php foreach($type as $row): ?>
                                        <option value="<?= $row['atype_id'] ?>" <?= isset($data) && $row['atype_id'] == $data['atype_id'] ? 'selected' : '' ?>><?= $row['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">
                                標題<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control col-md-7 col-xs-12" name="title" placeholder="標題" value="<?= $data['title'] ?? '' ?>" required>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="introduction">
                                簡介<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control col-md-7 col-xs-12" name="introduction" placeholder="簡介" value="<?= $data['introduction'] ?? '' ?>" required>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">
                                內文<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="form-control col-md-7 col-xs-12" name="description" placeholder="內文" required><?= $data['description'] ?? '' ?></textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="displayorder">
                                排序<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" class="form-control col-md-7 col-xs-12" name="displayorder" placeholder="請輸入數字" value="<?= $data['displayorder'] ?? 1 ?>" required>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">
                                狀態
                            </label>
                            <div id="gender" class="btn-group col-md-6 col-sm-6 col-xs-12" data-toggle="buttons">
                                <label class="btn btn-default <?= !isset($data) || $data['status'] < 1 ? 'active' : '' ?>" data-toggle-class="btn-primary" data-toggle-ç-class="btn-default">
                                    <input type="radio" name="status" value="0" <?= !isset($data) || $data['status'] < 1 ? 'checked' : '' ?>> &nbsp; 暫停 &nbsp;
                                </label>
                                <label class="btn btn-default <?= isset($data) && $data['status'] > 0 ? 'active' : '' ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                    <input type="radio" name="status" value="1" <?= isset($data) && $data['status'] > 0 ? 'checked' : '' ?>> &nbsp; 上架 &nbsp;
                                </label>
                            </div>
                        </div>
                        <div class="alert alert-danger alert-dismissible fade" role="alert" style="width: 50%;margin-left: 25%;">
                            <span class="txt"></span>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <a href="<?= base_url(); ?>pagemanager/article" class="btn btn-primary">返回</a>
                                <button id="send" type="submit" class="btn btn-success">送出</button>
                                <button type="button" class="btn btn-success preview">預覽</button>
                                <span>(預覽等同更新文章內容，若狀態為上架，請確認是否可上架)</span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/vendors/ckeditor/ckeditor.js"></script>
<script>
    $(function () {
        CKEDITOR.replace( 'description' ).on('change', function(e) {
            var thisHTML = e.editor.getData();
            $('textarea[name=description]').val(thisHTML);
        });
        $('.picinput').on('change', function () {
            var $input = $(this);
            var reader = new FileReader();

            reader.onload = function (e) {
                // get loaded data and render thumbnail.
                var $img;
                if ($input.parent().find('img').length > 0) {
                    $img = $input.parent().find('img');
                } else {
                    $img = $('<img style="height:100px;">');
                    $img.appendTo($input.parent());
                }
                $img.attr('src', e.target.result);
            };

            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        });
        $('.preview').on('click', function() {
            var newWindow = window.open("", "_blank");
            $(this).parents('form:eq(0)').data('callback', function(res) {
                newWindow.location = '/article/content/' + res.data.id + '/preview1234';
                window.location.href = '<?= base_url(); ?>pagemanager/articleEdit/' + res.data.id;
            });
            $(this).parents('form:eq(0)').find('button[type=submit]').click();
        });
    });
</script>