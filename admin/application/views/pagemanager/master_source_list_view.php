<div class="">

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?= $title ?></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered datatable-admin" <?= count($list) < 3 ? 'data-linkurl="<?= base_url(); ?>pagemanager/masterSourceEdit/' . $type . '"' : '' ?>>
                        <thead>
                        <tr>
                            <th width="15%">圖片</th>
                            <th width="15%">標題</th>
                            <th width="60%">內文</th>
                            <th width="10%">功能</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($list as $row): ?>
                            <tr>
                                <td><img src="/upload/<?= $row['pic'] ?>" width="100"/></td>
                                <td><?= $row['title'] ?></td>
                                <td><?= $row['content'] ?></td>
                                <td>
                                    <a class="btn btn-primary"
                                       href="<?= base_url(); ?>pagemanager/masterSourceEdit/<?= $type ?>/<?= $row['sid'] ?>">編輯</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>