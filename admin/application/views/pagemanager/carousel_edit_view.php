<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?= isset($data) ? '編輯' : '新增' ?>輪播圖</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <form class="form-horizontal form-label-left formValidation"
                          enctype="multipart/form-data"
                          action="<?= base_url(); ?>api/Pagemanager_api/edit/<?= $caid ?? '' ?>"
                          data-redirect="<?= base_url(); ?>pagemanager/carousel"
                          data-is-update="<?= isset($data) ? '1' : '0' ?>"
                          novalidate>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pcimg">寬版圖片
                                <?= isset($data) ? '' : '<span class="required">*</span>' ?>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input class="picinput" type="file" name="pcimg" <?= isset($data) ? '' : 'required' ?>/>
                                <?php if (!empty($data['pcimg'])): ?>
                                    <img src="/upload/<?= $data['pcimg'] ?>" style="height:100px;">
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobileimg">行動版圖片
                                <?= isset($data) ? '' : '<span class="required">*</span>' ?>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input class="picinput" type="file" name="mobileimg" <?= isset($data) ? '' : 'required' ?>/>
                                <?php if (!empty($data['mobileimg'])): ?>
                                    <img src="/upload/<?= $data['mobileimg'] ?>" style="height:100px;">
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="url">
                                連結<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control col-md-7 col-xs-12" name="url" placeholder="連結" value="<?= $data['url'] ?? '' ?>" required>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">
                                開始日期<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control has-feedback-left" id="adstart" name="start_time" placeholder="開始日期" value="" aria-describedby="inputSuccess2Status3" required>
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">
                                結束日期
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control has-feedback-left" id="adend" name="end_time" placeholder="結束日期" value="" aria-describedby="inputSuccess2Status4" >
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status4" class="sr-only">(success)</span>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="displayorder">
                                排序<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" class="form-control col-md-7 col-xs-12" name="displayorder" placeholder="請輸入數字" value="<?= $data['displayorder'] ?? 1 ?>" required>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">
                                狀態
                            </label>
                            <div id="gender" class="btn-group col-md-6 col-sm-6 col-xs-12" data-toggle="buttons">
                                <label class="btn btn-default <?= !isset($data) || $data['status'] < 1 ? 'active' : '' ?>" data-toggle-class="btn-primary" data-toggle-ç-class="btn-default">
                                    <input type="radio" name="status" value="0" <?= !isset($data) || $data['status'] < 1 ? 'checked' : '' ?>> &nbsp; 暫停 &nbsp;
                                </label>
                                <label class="btn btn-default <?= isset($data) && $data['status'] > 0 ? 'active' : '' ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                    <input type="radio" name="status" value="1" <?= isset($data) && $data['status'] > 0 ? 'checked' : '' ?>> &nbsp; 上架 &nbsp;
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">上架後永久顯示
                            </label>

                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <div class="checkbox">
                                    <label>
                                        <input class="flat"  type="checkbox" name="alwaysshow" value="1" <?= isset($data) && $data['alwaysshow'] ? 'checked' : '' ?>>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="alert alert-danger alert-dismissible fade" role="alert" style="width: 50%;margin-left: 25%;">
                            <span class="txt"></span>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <a href="<?= base_url(); ?>pagemanager/carousel" class="btn btn-primary">返回</a>
                                <button id="send" type="submit" class="btn btn-success">送出</button>
                                <button type="button" class="btn btn-success preview">預覽</button>
                                <span>(預覽等同更新文章內容，若狀態為上架，請確認是否可上架)</span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('.picinput').on('change', function () {
            var $input = $(this);
            var reader = new FileReader();

            reader.onload = function (e) {
                // get loaded data and render thumbnail.
                var $img;
                if ($input.parent().find('img').length > 0) {
                    $img = $input.parent().find('img');
                } else {
                    $img = $('<img style="height:100px;">');
                    $img.appendTo($input.parent());
                }
                $img.attr('src', e.target.result);
            };

            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        })
        var today = moment().format('YYYY-MM-DD');
        $('#adstart').datetimepicker({
//            minDate: today,
            format: 'YYYY-MM-DD',
            defaultDate: '<?= $data['start_time'] ?? false ? date('Y-m-d', strtotime($data['start_time'])) : '' ?>'
        });

        $('#adend').datetimepicker({
//            minDate: moment().add(86400000).format('YYYY-MM-DD'),
            useCurrent: false,
            format: 'YYYY-MM-DD',
            defaultDate: '<?= $data['end_time'] ?? false ? date('Y-m-d', strtotime($data['end_time'])) : '' ?>'
        });

        $("#adstart").on("dp.change", function(e) {
            $('#adend').data("DateTimePicker").minDate(e.date);
        });

        $("#adend").on("dp.change", function(e) {
            $('#adstart').data("DateTimePicker").maxDate(e.date);
        });
        $('.preview').on('click', function() {
            var newWindow = window.open("", "_blank");
            $(this).parents('form:eq(0)').data('callback', function(res) {
                newWindow.location = '/main/index//preview2234/' + res.data.id;
                window.location.href = '<?= base_url(); ?>pagemanager/carouselEdit/' + res.data.id;
            });
            $(this).parents('form:eq(0)').find('button[type=submit]').click();
        });
    });
</script>