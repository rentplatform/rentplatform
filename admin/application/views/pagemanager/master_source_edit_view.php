<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?= (isset($data) ? '編輯' : '新增') . $title ?></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form class="form-horizontal form-label-left formValidation"
                          enctype="multipart/form-data"
                          action="<?= base_url(); ?>api/Pagemanager_api/editMasterSourceData/<?= $type ?>/<?= $data['sid'] ?? '' ?>"
                          data-redirect="<?= base_url(); ?>pagemanager/<?= $type > 1 ? 'masterGood' : 'masterRecommend' ?>"
                          data-is-update="<?= isset($data) ? '1' : '0' ?>"
                          novalidate>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pic">圖片
                                <?= isset($data) ? '' : '<span class="required">*</span>' ?>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input class="picinput" type="file" name="pic" <?= isset($data) ? '' : 'required' ?>/>
                                <?php if (!empty($data['pic'])): ?>
                                    <img src="/upload/<?= $data['pic'] ?>" style="height:100px;">
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">
                                標題<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control col-md-7 col-xs-12" name="title" placeholder="標題" value="<?= $data['title'] ?? '' ?>" required>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="content">
                                內文<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="form-control col-md-7 col-xs-12" name="content" placeholder="內文" required><?= $data['content'] ?? '' ?></textarea>
                            </div>
                        </div>
                        <div class="alert alert-danger alert-dismissible fade" role="alert" style="width: 50%;margin-left: 25%;">
                            <span class="txt"></span>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <a href="<?= base_url(); ?>pagemanager/<?= $type > 1 ? 'masterGood' : 'masterRecommend' ?>" class="btn btn-primary">返回</a>
                                <button id="send" type="submit" class="btn btn-success">送出</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('.picinput').on('change', function () {
            var $input = $(this);
            var reader = new FileReader();

            reader.onload = function (e) {
                // get loaded data and render thumbnail.
                var $img;
                if ($input.parent().find('img').length > 0) {
                    $img = $input.parent().find('img');
                } else {
                    $img = $('<img style="height:100px;">');
                    $img.appendTo($input.parent());
                }
                $img.attr('src', e.target.result);
            };

            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        });
    });
</script>