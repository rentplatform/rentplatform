<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>首頁輪播圖列表</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered datatable-admin" data-linkurl="<?= base_url(); ?>pagemanager/carouselEdit">
                        <thead>
                        <tr>
                            <th>圖片</th>
                            <th>上架時間</th>
                            <th>下架時間</th>
                            <th>狀態</th>
                            <th>排序</th>
                            <th>上次發佈時間</th>
                            <th>功能</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($list as $row): ?>
                            <tr>
                                <td><?= $row['pcimg'] ? sprintf('<img src="/upload/%s" style="height:50px">', $row['pcimg']) : '' ?></td>
                                <td><?= date('Y-m-d', strtotime($row['start_time'])) ?></td>
                                <td><?= $row['alwaysshow'] ? '永久顯示' : date('Y-m-d', strtotime($row['end_time'])) ?></td>
                                <td><?= $row['status'] > 0 ? '啟用' : '停用' ?></td>
                                <td><?= $row['displayorder'] ?></td>
                                <td><?= $row['release_time'] ?? '' ?></td>
                                <td>
                                    <?php if ($this->permission->has('carouselEdit')): ?>
                                        <a class="btn btn-primary" href="<?= base_url(); ?>pagemanager/carouselEdit/<?= $row['caid'] ?>">編輯</a>
                                        <a href="javascript:void(0);" class="btn btn-primary listDataDelete" data-url="<?= base_url(); ?>api/Pagemanager_api/delete/<?= $row['caid'] ?>">刪除</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>