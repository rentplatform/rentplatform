<div class="">

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>常見問題列表</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered datatable-admin" data-linkurl="<?= base_url(); ?>pagemanager/questionEdit" data-order-column="2,desc">
                        <thead>
                        <tr>
                            <th>標題</th>
                            <th width="10%">狀態</th>
                            <th width="10%">排序</th>
                            <th width="15%">功能</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($list as $row): ?>
                            <tr>
                                <td><?= $row['title'] ?></td>
                                <td><?= $row['status'] > 0 ? '啟用' : '停用' ?></td>
                                <td><?= $row['displayorder'] ?></td>
                                <td>
                                    <?php if ($this->permission->has('questionEdit')): ?>
                                        <a class="btn btn-primary" href="<?= base_url(); ?>pagemanager/questionEdit/<?= $row['qid'] ?>">編輯</a>
                                        <a href="javascript:void(0);" class="btn btn-primary listDataDelete" data-url="<?= base_url(); ?>api/Pagemanager_api/deleteQuestion/<?= $row['qid'] ?>">刪除</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>