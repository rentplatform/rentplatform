<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>編輯<?= $title ?></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <form class="form-horizontal form-label-left formValidation"
                          enctype="multipart/form-data"
                          action="<?= base_url(); ?>api/Pagemanager_api/editStaticPageData"
                          data-redirect=""
                          data-is-update="1"
                          novalidate>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="content">
                                內文<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="form-control col-md-7 col-xs-12" name="content" placeholder="內文" required><?= $data['content'] ?? '' ?></textarea>
                            </div>
                        </div>
                        <div class="alert alert-danger alert-dismissible fade" role="alert" style="width: 50%;margin-left: 25%;">
                            <span class="txt"></span>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="hidden" name="type" value="<?= $type ?>"/>
                                <button id="send" type="submit" class="btn btn-success">送出</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/vendors/ckeditor/ckeditor.js"></script>
<script>
    $(function () {
        CKEDITOR.replace( 'content' ).on('change', function(e) {
            var thisHTML = e.editor.getData();
            $('textarea[name=content]').val(thisHTML);
        });
    });
</script>