<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>電電租 後台管理系統</title>

    <!-- Bootstrap -->
    <link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/vendors/nprogress/nprogress.css" rel="stylesheet">
	<!-- Animate.css -->
	<link href="/vendors/animate.css/animate.min.css" rel="stylesheet">

	<!-- Custom Theme Style -->
	<link href="/css/qwer/custom.min.css" rel="stylesheet">
</head>

<body class="login">
	<div>

		<div class="login_wrapper">
			<div class="animate form login_form">
				<section class="login_content">
					<form id="login_data">
						<h1>管理者登入</h1>
						<div>
							<input type="text" class="form-control" name="account" placeholder="帳號" required=""/>
						</div>
						<div>
							<input type="password" class="form-control" id="inputPassword" name="password" placeholder="密碼" required=""/>
						</div>
						<div>
							<input id="login_btn" class="btn btn-default submit" name="" type="button" value="送出" data-loading-text="登入"/>
						</div>

						<div class="clearfix"></div>
						<div class="separator">

							<div>
								<h1><img style="width: 30px;" src="/images/icons/header__logo.png"/>  後台管理系統 </h1>
							</div>
						</div>

					</form>
				</section>
			</div>

		</div>
	</div>
</body>
<!-- jQuery -->
<script src="/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script>
    $(function () {
        var status = false;
        var event = {
            login: function () {
                var $this = $(this);
                var data = $("#login_data").serialize();
                $this.button('loading');
                status = true;
                $.post('<?= base_url(); ?>api/adminuser_api/login', data, function (res) {
                    if (!res.valid) {
                        $this.button('reset');
                        status = false;
                        $('#inputPassword').val('').focus();
                        alert('帳號密碼錯誤!!');
                        return;
                    }
                    $(document).off('click', "#login_btn", event.login);
                    window.location.href = '<?= base_url(); ?>';
                }, 'json');

            },
            enter: function (e) {
                if (e.keyCode === 13 && !status) {
                    $('#login_btn').click();
                }
            }
        };

        $(document).on('click', "#login_btn", event.login);
        $(document).on('keydown', "#inputPassword", event.enter);
    });
</script>
</html>
