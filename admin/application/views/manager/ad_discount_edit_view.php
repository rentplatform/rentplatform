<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?= isset($data) ? '編輯' : '新增' ?>廣告優惠</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <form class="form-horizontal form-label-left formValidation" enctype="multipart/form-data"
                          action="<?= base_url(); ?>api/Common_api/adDiscountEdit/<?= $data['ad_id'] ?? '' ?>"
                          data-redirect="<?= base_url(); ?>manager/adDiscount"
                          data-is-update="<?= isset($data) ? 1 : 0 ?>"
                          novalidate>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gid">
                                活動名稱<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control col-md-7 col-xs-12" name="title" placeholder="活動名稱" value="<?= $data['title'] ?? '' ?>" required>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gid">
                                優惠%數<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" class="form-control col-md-7 col-xs-12" name="percent" placeholder="優惠%數" max="100" value="<?= $data['percent'] ?? '' ?>" required>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">
                                開始日期<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control has-feedback-left" id="adstart" name="start_time" placeholder="開始日期" value="" aria-describedby="inputSuccess2Status3" required>
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">
                                結束日期<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control has-feedback-left" id="adend" name="end_time" placeholder="結束日期" value="" aria-describedby="inputSuccess2Status4" required>
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status4" class="sr-only">(success)</span>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">
                                優惠天數<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" class="form-control col-md-7 col-xs-12" name="days" min="1" placeholder="優惠天數" value="<?= $data['days'] ?? '' ?>" aria-describedby="inputSuccess2Status4" required>
                            </div>
                        </div>
                        <div class="alert alert-danger alert-dismissible fade" role="alert" style="width: 50%;margin-left: 25%;">
                            <span class="txt"></span>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <a href="<?= base_url(); ?>manager/adDiscount" class="btn btn-primary">返回</a>
                                <button id="send" type="submit" class="btn btn-success">送出</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        var today = moment().format('YYYY-MM-DD');
        $('#adstart').datetimepicker({
//            minDate: today,
            format: 'YYYY-MM-DD',
        });
        $('#adstart').data("DateTimePicker").defaultDate("<?= $data['start_time'] ?? false ? date('Y-m-d', strtotime($data['start_time'])) : '' ?>");

        $('#adend').datetimepicker({
//            minDate: today,
            useCurrent: false,
            format: 'YYYY-MM-DD',
        });
        $('#adend').data("DateTimePicker").defaultDate("<?= $data['end_time'] ?? false ? date('Y-m-d', strtotime($data['end_time'])) : '' ?>");

        $("#adstart").on("dp.change", function(e) {
            $('#adend').data("DateTimePicker").minDate(e.date);
        });

        $("#adend").on("dp.change", function(e) {
            $('#adstart').data("DateTimePicker").maxDate(e.date);
        });
    });
</script>