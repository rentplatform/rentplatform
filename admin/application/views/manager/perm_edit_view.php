<div class="">

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>編輯權限</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <h2>
                        群組名稱: <?= $group['name'] ?>
                    </h2>
                    <form class="form-horizontal form-label-left formValidation" action="<?= base_url(); ?>api/group_api/editPerm" data-redirect="<?= base_url(); ?>manager/groupList" novalidate>
                        <table class="table table-bordered">
                            <tr>
                                <td style="width: 150px;">大類別</td>
                                <td>小類別</td>
                            </tr>
                            <?php
                            $template = '<div class="checkbox">
                                            <label class="">
                                                <div class="icheckbox_flat-green %s" style="position: relative;">
                                                    <input type="checkbox" class="flat %s" style="position: absolute; opacity: 0;" name="permids[]" value="%s" %s>
                                                </div> %s</label>
                                        </div>';
                            foreach ($permlist as $permId => $row) {
                                echo '<tr>';
                                if (isset($row['list'])) {
                                    $subperm_ids = array_column($row['list'], 'perm_id');
                                    $main_ischecked = count(array_intersect($subperm_ids, $groupperm)) === count($subperm_ids) ? 'checked' : '';
                                    echo '<td>' . sprintf($template, $main_ischecked, 'jAllCheck', '', $main_ischecked, $row['name']) . '</td>';

                                    echo '<td>';
                                    foreach ($row['list'] as $subperm) {
                                        $ischecked = in_array($subperm['perm_id'], $groupperm) ? 'checked' : '';
                                        echo sprintf($template, $ischecked, 'subcheckbox', $subperm['perm_id'], $ischecked, $subperm['name']);
                                    }
                                    echo '</td>';
                                } else {
                                    $ischecked = in_array($row['perm_id'], $groupperm) ? 'checked' : '';
                                    echo '<td></td><td>'.sprintf($template, $ischecked, 'subcheckbox', $row['perm_id'], $ischecked, $row['name']).'</td>';
                                }
                                echo '</tr>';
                            }
                            ?>
                        </table>
                        <div class="alert alert-danger alert-dismissible fade" role="alert" style="width: 50%;margin-left: 25%;">
                            <span class="txt"></span>
                        </div>
                        <input type="hidden" name="gid" value="<?php echo $gid ?>" class="adminId"/>
                        <a href="<?= base_url(); ?>manager/groupList" class="btn btn-primary">返回</a>
                        <input type="submit" class="btn btn-success" value="送出"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $('.jAllCheck').on('ifClicked', function () {
            var ischecked = $(this).prop('checked');
            var $tr = $(this).parents('tr');
            $tr.find('td:eq(1) input[type=checkbox]').iCheck(!ischecked ? 'check' : 'uncheck');
        });

        $('.subcheckbox').on('ifChanged', function () {
            var $this = $(this);
            var ischecked = $(this).prop('checked');
            var $tr = $(this).parents('tr');
            if (ischecked) {
                var $td = $(this).parents('td');
                var isTdAllChecked = true;
                $td.find('input[type=checkbox]').each(function () {
                    var checked = $(this).prop('checked');
                    isTdAllChecked = isTdAllChecked && checked;
                });
                if (isTdAllChecked) {
                    $tr.find('.jAllCheck').iCheck('check');
                }
            } else {
                checkHandle = true;
                $(this).parents('tr').find('.jAllCheck').iCheck('uncheck');
            }
        })
    });
</script>