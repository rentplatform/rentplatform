<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>縣市列表</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered datatable-admin" data-linkurl="<?= base_url(); ?>manager/areaEdit">
                        <thead>
                        <tr>
                            <th>編號</th>
                            <th>縣市名稱</th>
                            <th>區域數量</th>
                            <th>功能</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($country as $row): ?>
                            <tr>
                                <td><?= $row['cid'] ?></td>
                                <td><?= $row['name'] ?></td>
                                <td><?= $row['citycount'] ?></td>
                                <td>
                                    <a class="btn btn-primary" href="<?= base_url(); ?>manager/areaEdit/<?= $row['cid'] ?>">編輯</a>
                                    <a class="btn btn-primary" href="<?= base_url(); ?>manager/areaCitySetting/<?= $row['cid'] ?>">編輯區域</a>
                                    <?php if ($row['citycount'] > 0): ?>
                                        <del class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="有區域指定時無法刪除">刪除</del>
                                    <?php else: ?>
                                        <a href="javascript:void(0);" class="btn btn-primary listDataDelete" data-url="<?= base_url(); ?>api/Common_api/areaDelete/<?= $row['cid'] ?>">刪除</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>