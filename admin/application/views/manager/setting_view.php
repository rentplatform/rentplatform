<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>系統設定</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left formValidation"
                          action="<?= base_url(); ?>api/common_api/setting"
                          data-redirect="<?= base_url(); ?>manager/setting"
                          data-is-update="1" novalidate>

                        <?php if ($carasol): ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">首頁輪播秒數</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input name="index_carousel_second" class="knob" data-width="100" data-height="120" data-cursor=true data-fgColor="#34495E" value="<?= $data['index_carousel_second'] ?>" data-max="60">
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if ($ad): ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">全域廣告費趴數</label>
                                <div class="col-md-2 col-sm-9 col-xs-12">
                                    <select class="select2_multiple form-control" name="ad_percent">
                                        <option value="10" <?= (int)$data['ad_percent'] === 10 ? 'selected' : '' ?>>10%</option>
                                        <option value="15" <?= (int)$data['ad_percent'] === 15 ? 'selected' : '' ?>>15%</option>
                                        <option value="20" <?= (int)$data['ad_percent'] === 20 ? 'selected' : '' ?>>20%</option>
                                        <option value="30" <?= (int)$data['ad_percent'] === 30 ? 'selected' : '' ?>>30%</option>
                                    </select>
                                </div>
								<div class="col-md-4 col-sm-9 col-xs-12 checkbox">
									<label>
										<div id="useGlobalAd" class="icheckbox_flat-green <?= $data['use_global_ad'] ? 'checked' : '' ?>" style="position: relative;">
											<input type="checkbox" class="flat" style="position: absolute; opacity: 0;" name="use_global_ad" <?= $data['use_global_ad'] ? 'checked' : '' ?> value="1">
											<ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
										</div>
										是否強制使用全域廣告費趴數
									</label>
								</div>
                            </div>
							<div class="item form-group useGlobalAdDate <?= $data['use_global_ad'] ? '' : 'hidden' ?>">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">
									強制使用全域廣告費趴數開始日期
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="text" class="form-control has-feedback-left" id="adstart" name="global_ad_date_start" placeholder="開始日期" value="<?= $data['global_ad_date_start'] ?? '' ?>" aria-describedby="inputSuccess2Status3" >
									<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
									<span id="inputSuccess2Status3" class="sr-only">(success)</span>
								</div>
							</div>
							<div class="item form-group useGlobalAdDate <?= $data['use_global_ad'] ? '' : 'hidden' ?>">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">
									強制使用全域廣告費趴數結束日期
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="text" class="form-control has-feedback-left" id="adend" name="global_ad_date_end" placeholder="結束日期" value="<?= $data['global_ad_date_end'] ?? '' ?>" aria-describedby="inputSuccess2Status4" >
									<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
									<span id="inputSuccess2Status4" class="sr-only">(success)</span>
								</div>
							</div>
                        <?php endif; ?>

                        <?php if ($ga): ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">GoogleAnalytics追蹤碼</label>
                                <div class="col-md-3 col-sm-9 col-xs-12">
                                    <input type="text" class=" form-control" name="gacode" value="<?= $data['gacode'] ?>" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">GoogleTagManager</label>
                                <div class="col-md-3 col-sm-9 col-xs-12">
                                    <input type="text" class=" form-control" name="gtcode" value="<?= $data['gtcode'] ?>" />
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if ($sso): ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">網站關鍵字</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" class=" form-control" name="sso_keyword" value="<?= $data['sso_keyword'] ?>" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">網站描述</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <textarea class="resizable_textarea form-control" name="sso_description" placeholder=""><?= $data['sso_description'] ?></textarea>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if ($rent): ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">平日時段租金</label>
                                <div class="col-md-3 col-sm-9 col-xs-12">
                                    <input type="number" min="1" class=" form-control" name="normal_rent" value="<?= $data['normal_rent'] ?>" placeholder="平日時段租金"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">周末時段租金</label>
                                <div class="col-md-3 col-sm-9 col-xs-12">
                                    <input type="number" min="1" class=" form-control" name="weekend_rent" value="<?= $data['weekend_rent'] ?>" placeholder="周末時段租金"/>
                                </div>
                            </div>
                        <?php endif; ?>



                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">送出</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- jQuery Knob -->
<script src="/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- jQuery Tags Input -->
<script src="/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
<script>
    $(function() {
        $('.knob').knob();
        $('#countrySelect').on('change', function() {
            var city = $('#countrySelect option:selected').data('city') || '';
            $('#cityTag').tagsInput().importTags(city);
        });

        $('#useGlobalAd').on('click', function () {
			var checked = !$(this).find('input').prop('checked');
			if (checked) {
			    $('.useGlobalAdDate').removeClass('hidden');
			} else {
                $('.useGlobalAdDate').addClass('hidden');
                $('.useGlobalAdDate input').val('');
			}
        })

        $('#adstart').datetimepicker({
//            minDate: today,
            format: 'YYYY-MM-DD',
        });
        $('#adstart').data("DateTimePicker").defaultDate("<?= $data['start_time'] ?? false ? date('Y-m-d', strtotime($data['start_time'])) : '' ?>");

        $('#adend').datetimepicker({
//            minDate: today,
            useCurrent: false,
            format: 'YYYY-MM-DD',
        });
        $('#adend').data("DateTimePicker").defaultDate("<?= $data['end_time'] ?? false ? date('Y-m-d', strtotime($data['end_time'])) : '' ?>");

        $("#adstart").on("dp.change", function(e) {
            if (e.date) {
                $('#adend').attr('required', 'required')
			} else {
                $('#adend').removeAttr('required')
				$('#adend').removeClass('error');
                $('#adend-error').remove();
			}
            $('#adend').data("DateTimePicker").minDate(e.date);
        });

        $("#adend").on("dp.change", function(e) {
            $('#adstart').data("DateTimePicker").maxDate(e.date);
        });
    })
</script>