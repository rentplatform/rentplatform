<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>訊息模板管理</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form id="templateForm">
                        <label for="message">類型 :</label>
                        <select class="form-control" name="type">
                            <?php if (!$showMsgOnly): ?>
                                <option value="1" <?= $typeId === 1 ? 'selected' : '' ?>>email 註冊驗證</option>
                                <option value="2" <?= $typeId === 2 ? 'selected' : '' ?>>email 忘記密碼通知</option>
                                <option value="3" <?= $typeId === 3 ? 'selected' : '' ?>>email 租借申請</option>
                                <option value="4" <?= $typeId === 4 ? 'selected' : '' ?>>email 租借成功</option>
                                <option value="8" <?= $typeId === 8 ? 'selected' : '' ?>>email 租借失敗</option>
                                <option value="5" <?= $typeId === 5 ? 'selected' : '' ?>>email 提醒返還-租客</option>
                                <option value="6" <?= $typeId === 6 ? 'selected' : '' ?>>email 提醒交易-租客</option>
                                <option value="9" <?= $typeId === 9 ? 'selected' : '' ?>>email 提醒返還-租公</option>
                                <option value="10" <?= $typeId === 10 ? 'selected' : '' ?>>email 提醒交易-租公</option>
                            <?php endif; ?>
                            <option value="7" <?= $typeId === 7 ? 'selected' : '' ?>>簡訊 手機驗證信</option>
                        </select>
                        <br>
                        <label for="message">內容 :</label>
                        <textarea class="resizable_textarea form-control <?= $typeId !== 7 ? 'hidden' : '' ?>" name="content"><?= $template['content'] ?? '' ?></textarea>
                        <br>
                        <h3>可使用標籤 :</h3>
                        <?php
                            $notice = array(
                                1 => array('{name}: 收信人名稱', '{a}xxx{/a}: 信箱驗證地址, xxx可自行更改成文字'),
                                2 => array('{name}: 收信人名稱', '{sign}: 驗證碼'),
                                3 => array('{name}: 收信人名稱', '{a}xxx{/a}: 信箱驗證地址, xxx可自行更改成文字'),
                                4 => array(
                                    '{name}: 租客', '{ur_id}: 訂單編號', '{device_name}: 電器名稱', '{rent}: 租金', '{deposit}: 押金', '{total}: 費用總計：租金 + 押金', '{accessory}: 配件',
                                    '{device_owner_name}: 出租人', '{phone_number}: 出租人聯絡電話', '{placename}: 地點', '{address}: 地址', '{start_time}: 租借起始日', '{end_time}: 租借結束日',
                                    '{privacy_str}xxx{/privacy_str}: 合約連結, xxx可自行更改成文字'
                                ),
                                8 => array('{name}: 收信人名稱', '{device_owner_nickname}: 租公暱稱', '{reason}: 失敗原因'),
                                5 => array('{name}: 收信人名稱', '{device_name}: 電器名稱', '{device_owner_name}: 出租人', '{phone_number}: 出租人聯絡電話', '{start_time}: 租借起始日', '{end_time}: 租借結束日',
                                    '{placename}: 地點', '{address}: 地址', '{rent}: 租金', '{deposit}: 押金', '{total}: 費用總計：租金 + 押金', '{accessory}: 配件', '{privacy_str}xxx{/privacy_str}: 合約連結, xxx可自行更改成文字'
                                ),
                                6 => array('{name}: 收信人名稱', '{device_name}: 電器名稱', '{device_owner_name}: 出租人', '{phone_number}: 出租人聯絡電話', '{start_time}: 租借起始日', '{end_time}: 租借結束日',
                                    '{placename}: 地點', '{address}: 地址', '{rent}: 租金', '{deposit}: 押金', '{total}: 費用總計：租金 + 押金', '{accessory}: 配件', '{privacy_str}xxx{/privacy_str}: 合約連結, xxx可自行更改成文字'
                                ),
                                9 => array('{name}: 收信人名稱', '{device_name}: 電器名稱', '{renter_name}: 租客名稱', '{renter_phone}: 租客電話', '{start_time}: 租借起始日', '{end_time}: 租借結束日',
                                    '{placename}: 地點', '{address}: 地址', '{rent}: 租金', '{deposit}: 押金', '{total}: 費用總計：租金 + 押金', '{accessory}: 配件', '{privacy_str}xxx{/privacy_str}: 合約連結, xxx可自行更改成文字'
                                ),
                                10 => array('{name}: 收信人名稱', '{device_name}: 電器名稱', '{renter_name}: 租客名稱', '{renter_phone}: 租客電話', '{start_time}: 租借起始日', '{end_time}: 租借結束日',
                                    '{placename}: 地點', '{address}: 地址', '{rent}: 租金', '{deposit}: 押金', '{total}: 費用總計：租金 + 押金', '{accessory}: 配件', '{privacy_str}xxx{/privacy_str}: 合約連結, xxx可自行更改成文字'
                                ),
                                7 => array('{name}: 收信人名稱', '{sign}: 驗證碼')
                            );
                            if (!empty($notice[$typeId])) {
                                foreach ($notice[$typeId] as $k => $text) {
                                    echo "<h4>{$text}</h4>";
                                }
                            }
                        ?>
                        <div class="alert alert-danger alert-dismissible fade" role="alert" style="width: 50%;margin-left: 25%;">
                            <span class="txt"></span>
                        </div>
                        <button type="button" class="btn btn-success">送出</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/vendors/ckeditor/ckeditor.js"></script>
<script>
    $(function() {
        <?php if ($typeId !== 7): ?>
            CKEDITOR.replace( 'content' );
        <?php endif; ?>

        $('#templateForm select').on('change', function() {
            var type = $(this).val();
            window.location.href = '<?= base_url(); ?>manager/templateEdit/' + type;
        });

        $('#templateForm button').on('click', function() {
            var type = $('select[name=type]').val();
            var content = $('textarea[name=content]').val();;
            if (type != 7) {
                content = CKEDITOR.instances.content.getData();
            }

            $.post('<?= base_url(); ?>api/Template_api/edit', {
                type: type,
                content: content
            }, function(){}, 'json').always(function (res, status) {
                res = res || {};
                if (res.valid) {
                    alert('資料修改成功');
                    return;
                } else {
                    $('#templateForm').find('.alert-danger').addClass('in').find('.txt').text(res.msg || '表單提交失敗');
                    $('#templateForm').find('input,select, textarea').one('focus', function () {
                        $('#templateForm').find('.alert-danger').removeClass('in').find('.txt').text('');
                    });
                }
            });;

        });

    });
</script>