<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?= isset($gid) ? '編輯' : '新增' ?>群組</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <form class="form-horizontal form-label-left formValidation" enctype="multipart/form-data"
                          action="<?= base_url(); ?>api/group_api/<?= !isset($data) ? 'insert' : 'edit' ?>"
                          data-redirect="<?= base_url(); ?>manager/groupList"
                          data-is-update="<?= isset($gid) ? 1 : 0 ?>" novalidate>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">群組名稱 <span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12" maxlength="20"
                                       name="name" placeholder="最多20個字"
                                       required type="text" value="<?= $data['name'] ?? '' ?>">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gid">描述 <span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea id="message" required="required" class="form-control" name="desc" placeholder="限100字內" ><?= $data['desc'] ?? '' ?></textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="account">排序 <span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="displayorder" required>
                                    <?php for ($i=1;$i<100;$i++): ?>
                                        <option value="<?= $i ?>"><?= $i ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                        </div>
                        <div class="alert alert-danger alert-dismissible fade" role="alert" style="width: 50%;margin-left: 25%;">
                            <span class="txt"></span>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="hidden" name="gid" value="<?= $gid ?? '' ?>">
                                <a href="<?= base_url(); ?>manager/groupList" class="btn btn-primary">返回</a>
                                <button id="send" type="submit" class="btn btn-success">送出</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>