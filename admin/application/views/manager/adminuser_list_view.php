<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>帳號列表</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered datatable-admin" data-linkurl="<?= base_url(); ?>manager/userInsert">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>群組名稱</th>
                            <th>擁有者</th>
                            <th>帳號</th>
                            <th>狀態</th>
                            <th>建立時間</th>
                            <th>功能</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($list as $row): ?>
                            <tr>
                                <td><?= $row['aid'] ?></td>
                                <td><?= $row['groupName'] ?></td>
                                <td><?= $row['name'] ?></td>
                                <td><?= $row['account'] ?></td>
                                <td><?= $row['status'] ? '啟用' : '停用' ?></td>
                                <td><?= $row['create_time'] ?></td>
                                <td>
                                    <?php if ($row['aid'] > 1 && $this->permission->has('adminEdit') || (int)$row['aid'] === 1 && (int)$user['aid'] === 1): ?>
                                        <a class="btn btn-primary" href="<?= base_url(); ?>manager/userEdit/<?= $row['aid'] ?>">編輯</a>
                                        <?php if ($this->permission->has('adminDelete') && $row['aid'] != $user['aid'] && $row['aid'] > 1){ ?>
                                            <a href="javascript:void(0);" class="btn btn-primary listDataDelete" data-url="<?= base_url(); ?>api/Adminuser_api/delete/<?= $row['aid'] ?>">刪除</a>
                                        <?php } ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>