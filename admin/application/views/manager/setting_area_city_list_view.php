<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><a href="<?= base_url(); ?>manager/areaSetting">縣市列表</a> > <?= $country['name'] ?> 區域列表</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered datatable-admin" data-linkurl="<?= base_url(); ?>manager/areaCityEdit/<?= $country['cid'] ?>">
                        <thead>
                        <tr>
                            <th>編號</th>
                            <th>區域名稱</th>
                            <th>功能</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($city as $row): ?>
                            <tr>
                                <td><?= $row['ct_id'] ?></td>
                                <td><?= $row['name'] ?></td>
                                <td>
                                    <a class="btn btn-primary" href="<?= base_url(); ?>manager/areaCityEdit/<?= $country['cid'] ?>/<?= $row['ct_id'] ?>">編輯</a>
                                    <a href="javascript:void(0);" class="btn btn-primary listDataDelete" data-url="<?= base_url(); ?>api/Common_api/areaCityDelete/<?= $row['ct_id'] ?>">刪除</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>