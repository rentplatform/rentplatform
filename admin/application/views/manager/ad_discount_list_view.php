<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>廣告優惠列表</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    提示：<br>
                    一、活動設定完成後，如非必要請勿變更設定值<br>
                    二、活動開始後不得再變動優惠％數、開始日期、優惠天數欄位資訊<br>
                    三、有不同活動請另外建議新活動，勿直接修改原活動資訊<br><br>
                    <table class="table table-striped table-bordered datatable-admin"
                           data-linkurl="<?= base_url(); ?>manager/adDiscountEdit"
                           data-order-column="1,desc">
                        <thead>
                        <tr>
                            <th>活動名稱</th>
                            <th>優惠%數</th>
                            <th>優惠天數</th>
                            <th>參加人數</th>
                            <th>開始日期</th>
                            <th>結束日期</th>
                            <th>建立日期</th>
                            <th>功能</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($list as $row): ?>
                            <tr>
                                <td><?= $row['title'] ?></td>
                                <td><?= $row['percent'] ?>%</td>
                                <td><?= $row['days'] ?></td>
                                <td><?= $row['count'] ?></td>
                                <td><?= date('Y-m-d', strtotime($row['start_time'])) ?></td>
                                <td><?= date('Y-m-d', strtotime($row['end_time'])) ?></td>
                                <td><?= $row['create_time'] ?></td>
                                <td>
                                    <?php if ($this->permission->has('adDiscountEdit')): ?>
                                        <a class="btn btn-primary" href="<?= base_url(); ?>manager/adDiscountEdit/<?= $row['ad_id'] ?>">編輯</a>
                                        <a href="javascript:void(0);" class="btn btn-primary listDataDelete" data-url="<?= base_url(); ?>api/Common_api/adDiscountDelete/<?= $row['ad_id'] ?>">刪除</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>