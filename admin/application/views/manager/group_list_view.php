

<div class="">

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>群組列表</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered datatable-admin" data-linkurl="<?= base_url(); ?>manager/groupInsert">
                        <thead>
                        <tr>
                            <th>群組編號</th>
                            <th>名稱</th>
                            <th>描述</th>
                            <th>排序</th>
                            <th>建立時間</th>
                            <th>功能</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($list as $row): ?>
                            <tr>
                                <td><?= $row['gid'] ?></td>
                                <td><?= $row['name'] ?></td>
                                <td><?= $row['desc'] ?></td>
                                <td><?= $row['displayorder'] ?></td>
                                <td><?= $row['create_time'] ?></td>
                                <td>
                                    <?php if ((int)$row['gid'] !== 1): ?>
                                        <a class="btn btn-primary" href="<?= base_url(); ?>manager/groupEdit/<?= $row['gid'] ?>">編輯群組</a>
                                        <a class="btn btn-primary" href="<?= base_url(); ?>manager/permEdit/<?= $row['gid'] ?>">編輯權限</a>
                                        <a class="btn btn-primary listDataDelete" href="javascript:void(0)" data-url="<?= base_url(); ?>api/group_api/delete/<?= $row['gid'] ?>">刪除</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>