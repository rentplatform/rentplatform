<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?= isset($aid) ? '編輯' : '新增' ?>管理者帳號</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <form class="form-horizontal form-label-left formValidation"
                          enctype="multipart/form-data"
                          action="<?= base_url(); ?>api/Adminuser_api/<?= !isset($data) ? 'insert' : 'edit' ?>"
                          data-redirect="<?= base_url(); ?>manager/userList"
                          data-is-update="<?= isset($data) ? 1 : 0 ?>"
                          novalidate>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">擁有者姓名 <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12" rangelength="2,10"
                                        name="name" placeholder="請輸入2~10個字"
                                       required type="text" value="<?= $data['name'] ?? '' ?>">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gid">群組設定 <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="gid" required>
                                    <option value="" <?= isset($aid) ? '' : 'selected' ?>>請選擇群組</option>
                                    <?php foreach($groups as $row): ?>
                                        <option value="<?= $row['gid'] ?>" <?= isset($aid) && $row['gid'] == $data['gid'] ? 'selected' : '' ?>><?= $row['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="account">
                                帳號
                                <?= !isset($aid) ? '<span class="required">*</span>' : '' ?>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php if (isset($data)): ?>
                                   <?= $data['account'] ?? '' ?>
                                <?php else: ?>
                                    <input type="text" id="email" name="account" rangelength="4,20" required
                                           class="form-control col-md-7 col-xs-12" placeholder="請輸入4~20個字">
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label for="password" class="control-label col-md-3">
                                密碼
                                <?= !isset($aid) ? '<span class="required">*</span>' : '' ?>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="password" type="password" name="password"
                                       class="form-control col-md-7 col-xs-12" <?= !isset($aid) ? 'required' : '' ?>>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">
                                確認密碼
                                <?= !isset($aid) ? '<span class="required">*</span>' : '' ?>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="password2" type="password" equalTo="#password"
                                       class="form-control col-md-7 col-xs-12" <?= !isset($aid) ? 'required' : '' ?>>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">頭像設定</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input class="picinput" type="file" name="file">
                                <?php if (!empty($data['pic'])): ?>
                                    <img src="/upload/<?= $data['pic'] ?>" style="width:100px;">
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="alert alert-danger alert-dismissible fade" role="alert" style="width: 50%;margin-left: 25%;">
                            <span class="txt"></span>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="hidden" name="aid" value="<?= $aid ?? '' ?>">
                                <a href="<?= base_url(); ?>manager/userList" class="btn btn-primary">返回</a>
                                <button id="send" type="submit" class="btn btn-success">送出</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('.picinput').on('change', function () {
            var $input = $(this);
            var reader = new FileReader();

            reader.onload = function (e) {
                // get loaded data and render thumbnail.
                var $img;
                if ($input.parent().find('img').length > 0) {
                    $img = $input.parent().find('img');
                } else {
                    $img = $('<img style="width:100px;">');
                    $img.appendTo($input.parent());
                }
                $img.attr('src', e.target.result);
            };

            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        })
    });
</script>