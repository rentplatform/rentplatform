<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $menu = gen_menu(true);
        foreach ($menu as $r) {
            foreach ($r['list'] as $item) {
                if ($this->permission->has($item['perm'])) {
                    redirect($r['controller'] . $item['url']);
                    return true;
                }
            }
        }
        $this->loadView('index');
    }
    
    public function template() {
        $this->load->view('template.php');
    }

}
