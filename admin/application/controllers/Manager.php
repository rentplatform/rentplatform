<?php

/**
 * 系統管理
 */
class Manager extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Adminuser_model', 'user');
        $this->load->model('Group_model', 'group');
        $this->load->model('Permission_model', 'perm');
    }

    /**
     * 群組列表
     */
    public function groupList() {
        $this->checkPerm('groupList');
        $this->mOutput['list'] = $this->group->getGroupList();
        $this->loadView('manager/group_list');
    }

    /**
     * 新增群組
     */
    public function groupInsert() {
        $this->checkPerm('groupEdit');
        $this->loadView('manager/group_edit');
    }

    /**
     * 編輯群組
     * @param int $gid 群組編號
     */
    public function groupEdit(int $gid = 0) {
        $this->checkPerm('groupEdit');
        if ($gid === 1) {
            show_404();
            return;
        }
        $this->mOutput['gid'] = $gid;
        $this->mOutput['data'] = $this->group->getGroupById($gid);
        if (empty($this->mOutput['data'])) {
            show_404();
        }
        $this->loadView('manager/group_edit');
    }

    /**
     * 群組權限設定
     * @param int $gid 群組編號
     */
    public function permEdit(int $gid) {
        $this->checkPerm('groupEdit');
        if ($gid === 1) {
            show_404();
            return;
        }
        $this->mOutput['gid'] = $gid;
        $permlist = $this->perm->getAllPermList();
        $showlist = array();
        foreach ($permlist as $row) {
            if ((int)$row['parent_perm_id'] === 0 && (int)$row['subcount'] === 0) {
                $showlist[$row['perm_id']] = $row;
            }elseif ((int)$row['parent_perm_id'] === 0 && (int)$row['subcount'] !== 0) {
                $showlist[$row['perm_id']] = $row;
                $showlist[$row['perm_id']]['list'] = array();
            } else {
                $showlist[$row['parent_perm_id']]['list'][] = $row;
            }
        }

        $this->mOutput['groupperm'] = $this->perm->getGroupPerm($gid);
        $this->mOutput['group'] = $this->group->getGroupById($gid);
        $this->mOutput['gid'] = $gid;
        $this->mOutput['permlist'] = $showlist;
        $this->loadView('manager/perm_edit');

    }

    /**
     * 管理員列表
     */
    public function userList() {
        $this->checkPerm('adminList');
        $this->mOutput['list'] = $this->user->getAdminUserList();
        $this->loadView('manager/adminuser_list');
    }

    /**
     * 新增管理員
     */
    public function userInsert() {
        $this->checkPerm('adminEdit');
        $this->mOutput['groups'] = $this->group->getGroupList();
        $this->loadView('manager/adminuser_edit');
    }

    /**
     * 編輯管理員
     * @param int $aid 管理員編號
     */
    public function userEdit(int $aid = 0) {
        $this->checkPerm('adminEdit');
        $this->mOutput['aid'] = $aid;
        $this->mOutput['groups'] = $this->group->getGroupList();
        $this->mOutput['data'] = $this->user->getAdminUserById($aid);
        if (empty($this->mOutput['data'])) {
            show_404();
        }
        $this->loadView('manager/adminuser_edit');
    }

    /**
     * 系統設定頁面
     */
    public function setting() {
        $this->checkPerm(array('carasolSetting', 'gaSetting', 'rentSetting', 'adPriceSetting', 'ssoSetting'));
        $this->load->model('Setting_model', 'setting');

        $this->mOutput['carasol'] = $this->permission->has('carasolSetting');
        $this->mOutput['ga'] = $this->permission->has('gaSetting');
        $this->mOutput['rent'] = $this->permission->has('rentSetting');
        $this->mOutput['ad'] = $this->permission->has('adPriceSetting');
        $this->mOutput['sso'] = $this->permission->has('ssoSetting');
        $this->mOutput['data'] = $this->setting->getAll();
        $this->loadView('manager/setting');

    }

    /**
     * 縣市列表
     */
    public function areaSetting() {
        $this->checkPerm('areaSetting');
        $this->load->model('Setting_model', 'setting');

        $this->mOutput['country'] = $this->setting->getCountry();

        $this->loadView('manager/setting_area_list');
    }

    /**
     * 編輯縣市頁面
     * @param int $cid
     */
    public function areaEdit(int $cid = 0) {
        $this->checkPerm('areaSetting');
        if ($cid) {
            $this->load->model('Setting_model', 'setting');
            $this->mOutput['data'] = $this->setting->getCountry($cid);
            $this->mOutput['cid'] = $cid;
        }
        $this->loadView('manager/setting_area_edit');
    }

    /**
     * 區域列表
     * @param int $cid
     */
    public function areaCitySetting(int $cid = 0) {
        $this->checkPerm('areaSetting');
        $this->load->model('Setting_model', 'setting');
        $this->mOutput['country'] = $this->setting->getCountry($cid);
        $city = $this->setting->getCityByCountry($cid);
        $this->mOutput['city'] = $city;

        $this->loadView('manager/setting_area_city_list');
    }

    /**
     * 編輯區域頁
     * @param int $cid
     * @param int $ctid
     */
    public function areaCityEdit(int $cid = 0, int $ctid = 0) {
        $this->checkPerm('areaSetting');
        $this->load->model('Setting_model', 'setting');
        if ($ctid) {
            $this->mOutput['data'] = $this->setting->getCity($ctid);
            $this->mOutput['ctid'] = $ctid;
        }
        $this->mOutput['cid'] = $cid;
        $this->mOutput['country'] = $this->setting->getCountry($cid);
        if (empty($this->mOutput['country'])) {
            show_404();
            return;
        }
        $this->loadView('manager/setting_area_city_edit');
    }

    public function adDiscount() {
        $this->checkPerm('adDiscountList');
        $this->load->model('Setting_model');
        $this->mOutput['list'] = $this->Setting_model->getAdDiscountList();
        $this->loadView('manager/ad_discount_list');
    }

    public function adDiscountEdit(int $adId = 0) {
        $this->checkPerm('adDiscountEdit');
        if ($adId) {
            $this->load->model('Setting_model');
            $this->mOutput['data'] = $this->Setting_model->getAdDiscountData($adId);
            if (empty($this->mOutput['data'])) {
                show_404();
                return;
            }
        }
        $this->loadView('manager/ad_discount_edit');
    }

    /**
     * 訊息模板管理
     * @param int $typeId
     */
    public function templateEdit(int $typeId = 1) {
        $this->checkPerm(array('emailEdit', 'messageEdit'));
        $this->load->model('Template_model', 'template');

        //預設顯示email 註冊驗證模板 or message notice template
        $typeId = $this->permission->has('emailEdit') ? $typeId : 7;
        $this->mOutput['template'] = $this->template->getTemplate($typeId);
        $this->mOutput['showMsgOnly'] = !$this->permission->has('emailEdit');
        $this->mOutput['typeId'] = $typeId;

        $this->loadView('manager/template_edit');
    }

}