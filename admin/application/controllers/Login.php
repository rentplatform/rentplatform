<?php

/**
 * 登入頁
 */
class Login extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('loginUser')) {
            redirect('/');
        }
    }
    
    public function index() {
        $this->load->view('login_view.php');
    }
}
