<?php

/**
 * 登出控制器
 */
class Logout extends CI_Controller {
    
    public function index() {
        $this->session->unset_userdata(array('aid', 'gid', 'pic', 'name'));
        redirect('/');
    }
    
}
