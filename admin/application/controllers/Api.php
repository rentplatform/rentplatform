<?php

/**
 * Description of api
 *
 * @author asdf
 */
class Api extends CI_Controller {
    
    public function __call($name, $args) {
        $name = ucfirst($name);
        if (!file_exists(__DIR__ . '/../libraries/api/' . $name . '.php')) {
            show_404();
        }
        require __DIR__ . '/../libraries/api/admin_api_base.php';
        $this->load->library("api/$name", NULL, $name);

        $func = $args[0];
        if (!method_exists($this->$name, $func)) {
            show_404();
        }
        $args = array_slice($args, 1);
        call_user_func_array(array($this->$name, $func), $args);
    }

}
