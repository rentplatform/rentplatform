<?php

class User extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('User_model', 'user');
    }

    /**
     * 會員列表
     */
    public function uList() {
        $this->checkPerm('managerList');
        $this->loadView('user/user_list');
    }

    /**
     * 租公列表
     */
    public function masterList() {
        $this->checkPerm('masterList');
        $this->loadView('user/master_list');
    }

    /**
     * 會員租借紀錄
     * @param int $uid
     */
    public function rentRecord(int $uid = 0) {
        $this->checkPerm('managerList');
        if (empty($uid)) {
            show_404();
            return;
        }
        $this->mOutput['uid'] = $uid;
        $this->loadView('user/rent_record');
    }

    /**
     * 租公出租記錄
     * @param int $uid
     */
    public function masterRecord(int $uid = 0) {
        $this->checkPerm('masterList');
        if (empty($uid)) {
            show_404();
            return;
        }
        $this->mOutput['uid'] = $uid;
        $this->loadView('user/master_rent_record');
    }

    /**
     * 會員新增
     */
    public function insert() {
        $this->checkPerm('managerEdit');
        $this->loadView('user/user_edit');
    }

    /**
     * 租公新增
     */
    public function masterInsert() {
        $this->checkPerm('masterEdit');
        $this->mOutput['isRenter'] = 1;
        $this->loadView('user/user_edit');
    }

    /**
     * 會員編輯
     * @param int $uid
     */
    public function userEdit(int $uid) {
        $this->checkPerm('managerEdit');
        $this->mOutput['data'] = $this->user->getUserById($uid);
        if (empty($this->mOutput['data'])) {
            show_404();
            return;
        }
        $this->mOutput['uid'] = $uid;
        $this->loadView('user/user_edit');
    }

    /**
     * 租公編輯
     * @param int $uid
     */
    public function masterEdit(int $uid) {
        $this->checkPerm('masterEdit');
        $this->mOutput['data'] = $this->user->getUserById($uid);
        if (empty($this->mOutput['data'])) {
            show_404();
            return;
        }
        $this->mOutput['uid'] = $uid;
        $this->mOutput['isRenter'] = 1;
        $this->loadView('user/user_edit');

    }

    /**
     * 租公面交點列表
     * @param int $uid
     */
    public function locationList(int $uid = 0) {
        $this->checkPerm('masterList');
        $this->mOutput['data'] = $this->user->getMasterLocationList($uid);
        $this->mOutput['uid'] = $uid;
        $this->loadView('user/master_location');
    }

    /**
     * 編輯租公面交點
     * @param int $tpId
     */
    public function locationEdit(int $tpId = 0) {
        $this->checkPerm('masterEdit');
        $this->load->model('Setting_model');
        $data = $this->user->getLocation($tpId);
        if (empty($data)) {
            show_404();
            return;
        }

        $this->mOutput['tpid'] = $tpId;
        $this->mOutput['data'] = $data;
        $this->mOutput['country'] = $this->Setting_model->getCountry();
        $this->mOutput['city'] = $this->Setting_model->getCityByCountry($data['cid']);
        $this->loadView('user/master_location_edit');
    }

    /**
     * 租公電器列表
     * @param int $uid
     */
    public function deviceList(int $uid = 0) {
        $this->checkPerm('masterList');
        $this->mOutput['data'] = $this->user->getMasterDeviceList($uid);
        $this->mOutput['uid'] = $uid;
        $this->loadView('user/master_device');
    }

}