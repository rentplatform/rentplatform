<?php


/**
 * 強力推薦管理
 */
class Test extends CI_Controller {

    public function index() {
        $this->db->delete('admin_user', array('aid' => 27));
        echo $this->db->last_query();
    }

    public function image() {
        $image = new Imagick(PHOTO_PATH . 'ca6d1eb2a97106c29772e000f2a73273.jpg');
        $orientation = $image->getImageOrientation();

        switch($orientation) {
            case imagick::ORIENTATION_BOTTOMRIGHT:
                $image->rotateimage("#000", 180); // rotate 180 degrees
                break;

            case imagick::ORIENTATION_RIGHTTOP:
                $image->rotateimage("#000", 90); // rotate 90 degrees CW
                break;

            case imagick::ORIENTATION_LEFTBOTTOM:
                $image->rotateimage("#000", -90); // rotate 90 degrees CCW
                break;
        }
        $image->writeImage(PHOTO_PATH . 'test.jpg');
    }

}
