<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property Page_model  $page
 */
class Pagemanager extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Page_model', 'page');
    }

    /**
     * 首頁輪播圖list
     */
    public function carousel() {
        $this->checkPerm('carouselList');
        $this->mOutput['list'] = $this->page->getCarousel();
        $this->loadView('pagemanager/carousel_list');
    }

    public function carouselEdit($caid = 0) {
        $this->checkPerm('carouselEdit');
        if ($caid > 0) {
            $this->mOutput['caid'] = $caid;
            $this->mOutput['data'] = $this->page->getCarousel($caid);
            if (empty($this->mOutput['data'])) {
                show_404();
                return;
            }
        }
        $this->loadView('pagemanager/carousel_edit');
    }

    public function article() {
        $this->checkPerm('articleList');
        $this->mOutput['list'] = $this->page->getArticle();
        $this->loadView('pagemanager/article_list');
    }

    public function articleEdit(int $atid = 0) {
        $this->checkPerm('articleEdit');
        if ($atid) {
            $this->mOutput['atid'] = $atid;
            $this->mOutput['data'] = $this->page->getArticle($atid);
            if (empty($this->mOutput['data'])) {
                show_404();
                return;
            }
        }
        $this->mOutput['type'] = $this->page->getArticleTypeList();
        $this->loadView('pagemanager/article_edit');
    }

    public function articleType() {
        $this->checkPerm('articleEdit');
        $this->mOutput['list'] = $this->page->getArticleTypeList();
        $this->loadView('pagemanager/article_type_list');
    }

    public function articleTypeEdit(int $atypeid = 0) {
        $this->checkPerm('articleEdit');
        if ($atypeid) {
            $this->mOutput['data'] = $this->page->getArticleTypeList($atypeid);
            if (empty($this->mOutput['data'])) {
                show_404();
                return;
            }
        }
        $this->loadView('pagemanager/article_type_edit');
    }

    public function masterRecommend() {
        $this->checkPerm('staticpageEdit');
        $this->mOutput['title'] = '租客推薦';
        $this->mOutput['type'] = STATICPAGE_TYPE['MASTER_RECOMMEND'];
        $this->mOutput['list'] = $this->page->getStaticData(STATICPAGE_TYPE['MASTER_RECOMMEND'], true);
        $this->loadView('pagemanager/master_source_list');
    }

    public function masterGood() {
        $this->checkPerm('staticpageEdit');
        $this->mOutput['title'] = '租客滿意';
        $this->mOutput['type'] = STATICPAGE_TYPE['MASTER_GOOD'];
        $this->mOutput['list'] = $this->page->getStaticData(STATICPAGE_TYPE['MASTER_GOOD'], true);
        $this->loadView('pagemanager/master_source_list');
    }

    public function masterSourceEdit(int $type = 1,int $id = 0) {
        $this->checkPerm('staticpageEdit');
        if ($id) {
            $this->mOutput['data'] = $this->page->getStaticDataById($id);
            if (empty($this->mOutput['data'])) {
                show_404();
                return;
            }
        } else {
            if ($type > 2) {
                show_404();
                return;
            }
            $data = $this->page->getStaticData($type, true);
            if (count($data) >= 3) {
                show_404();
                return;
            }
        }
        $this->mOutput['type'] = $type;
        $this->mOutput['title'] = $type > 1 ? '租公滿意' : '租客推薦';

        $this->loadView('pagemanager/master_source_edit');
    }

    public function question() {
        $this->checkPerm('questionList');
        $this->mOutput['list'] = $this->page->getQuestion();
        $this->loadView('pagemanager/question_list');
    }

    /**
     * 新增修改常見問題頁
     * @param int $id
     */
    public function questionEdit(int $id = 0) {
        $this->checkPerm('questionEdit');
        if ($id) {
            $this->mOutput['data'] = $this->page->getQuestion($id);
            if (empty($this->mOutput['data'])) {
                show_404();
                return;
            }
        }
        $this->loadView('pagemanager/question_edit');

    }

    public function brandCooperation() {
        $this->mOutput['title'] = '品牌合作';
        $this->mOutput['type'] = STATICPAGE_TYPE['BRAND_COOPERATION'];
        $this->mOutput['data'] = $this->page->getStaticData(STATICPAGE_TYPE['BRAND_COOPERATION']);
        $this->loadView('pagemanager/static_page_edit');
    }

    public function service() {
        $this->mOutput['title'] = '服務條款';
        $this->mOutput['type'] = STATICPAGE_TYPE['SERVICE'];
        $this->mOutput['data'] = $this->page->getStaticData(STATICPAGE_TYPE['SERVICE']);
        $this->loadView('pagemanager/static_page_edit');
    }

    public function privacy() {
        $this->mOutput['title'] = '隱私權政策';
        $this->mOutput['type'] = STATICPAGE_TYPE['PRIVACY'];
        $this->mOutput['data'] = $this->page->getStaticData(STATICPAGE_TYPE['PRIVACY']);
        $this->loadView('pagemanager/static_page_edit');
    }

    public function barrister() {
        $this->mOutput['title'] = '法律顧問';
        $this->mOutput['type'] = STATICPAGE_TYPE['BARRISTER'];
        $this->mOutput['data'] = $this->page->getStaticData(STATICPAGE_TYPE['BARRISTER']);
        $this->loadView('pagemanager/static_page_edit');
    }

    public function aboutus() {
        $this->mOutput['title'] = '關於我們';
        $this->mOutput['type'] = STATICPAGE_TYPE['ABOUT_US'];
        $this->mOutput['data'] = $this->page->getStaticData(STATICPAGE_TYPE['ABOUT_US']);
        $this->loadView('pagemanager/static_page_edit');
    }

}
