<?php
/**
 * User: eric
 * Date: 2017/10/31
 * @
 */
class Rentmanager extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Rentmanager_model');
    }

    /**
     * 品牌列表
     */
    public function brandList() {
        $this->checkPerm('brandList');
        $this->mOutput['list'] = $this->Rentmanager_model->getBrand();
        $this->loadView('rentmanager/brand_list');
    }

    /**
     * 品牌新增修改
     * @param int $bid
     */
    public function brandEdit(int $bid = 0) {
        $this->checkPerm('brandEdit');

        if ($bid) {
            $this->mOutput['bid'] = $bid;
            $this->mOutput['data'] = $this->Rentmanager_model->getBrand($bid);
            if (empty($this->mOutput['data'])) {
                show_404();
                die;
            }
        }
        $this->loadView('rentmanager/brand_edit');
    }

    /**
     * 電器類型列表
     */
    public function deviceTypeList() {
        $this->checkPerm('deviceTypeList');
        $this->mOutput['list'] = $this->Rentmanager_model->getDeviceTypeList();
        $this->loadView('rentmanager/device_type_list');
    }

    /**
     * 電器類型新增修改
     * @param int $dtid
     */
    public function deviceTypeEdit(int $dtid = 0) {
        $this->checkPerm('deviceTypeEdit');

        if ($dtid) {
            $this->mOutput['dtid'] = $dtid;
            $this->mOutput['data'] = $this->Rentmanager_model->getDeviceTypeList($dtid);
            if (empty($this->mOutput['data'])) {
                show_404();
                die;
            }
        }
        $this->loadView('rentmanager/device_type_edit');
    }

    /**
     * 租借列表
     */
    public function rentList() {
        $this->checkPerm('transactionList');
        $this->loadView('rentmanager/rent_list');
    }

    /**
     * 意見反應列表
     */
    public function arbitrationList() {
        $this->checkPerm('arbitration');
        $this->loadView('rentmanager/arbitration_list');
    }

    /**
     * 電器列表
     */
    public function deviceList() {
        $this->checkPerm('deviceList');
        $this->loadView('rentmanager/device_list');
    }

    /**
     * 電器編輯
     * @param int $deviceId
     */
    public function deviceEdit(int $deviceId = 0) {
        $this->checkPerm('deviceEdit');
        $uid = 0;
        if ($deviceId) {
            $this->mOutput['data'] = $this->Rentmanager_model->getDeviceData($deviceId);
            $this->mOutput['deviceId'] = $deviceId;
            $uid = $this->mOutput['data']['uid'];
        }
        $this->mOutput['brand'] = $this->Rentmanager_model->getBrand();
        $this->mOutput['deviceType'] = $this->Rentmanager_model->getDeviceTypeList();
        $this->mOutput['masterList'] = $this->Rentmanager_model->getValidMasterList($uid);
        $this->loadView('rentmanager/device_edit');

    }

}