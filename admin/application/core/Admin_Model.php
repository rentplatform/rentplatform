<?php

/**
 * Created by PhpStorm.
 * User: eric
 * Date: 2017/11/21
 * Time: 上午12:35
 */
class Admin_Model extends CI_Model {


    protected function setDataTableQuery($defaultOrder = array(), $column = array(), $order = array()) {
        try {
            $order = $order ? : $column;
            $get = $this->input->get(null, true);

            if (!empty($get['search']['value'])) {
                $count = count($column);
                $text = mb_convert_encoding($get['search']['value'], 'UTF-8');
                foreach ($column as $i => $emp) {
                    if ($get['search']['value']) {
                        if ($i === 0) {
                            $this->db->group_start();
                            $this->db->like($emp, $text);
                        } else {
                            $this->db->or_like($emp, $text);
                        }

                        if ($count - 1 == $i) //last loop
                            $this->db->group_end(); //close bracket
                    }
                }
            }

            if (isset($get['order'])) {
                $this->db->order_by($order[$get['order']['0']['column']], isset($get['order']['0']['dir']) ? $get['order']['0']['dir'] : 'asc');
            } else if (!empty($defaultOrder)) {
                foreach ($defaultOrder as $k => $v) {
                    $this->db->order_by($k, $v);
                }
            }

            if(isset($get['length']) && $get['length'] < 1) {
                $get['length']= '10';
            } else {
                $get['length']= $get['length'];
            }

            if(isset($get['start'])) {
                $this->db->limit($get['length'], $get['start']);
            }
        } catch (Exception $e) {

        }
    }

}