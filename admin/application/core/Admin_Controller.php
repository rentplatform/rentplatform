<?php

/**
 * 
 */
class Admin_Controller extends CI_Controller {

    protected $mOutput = array();

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('aid')) {
            redirect('/login');
            die;
        }
        $this->mOutput['user'] = array(
            'aid' => $this->session->userdata('aid'),
            'gid' => $this->session->userdata('gid'),
            'pic' => $this->session->userdata('pic'),
            'name' => $this->session->userdata('name'),
        );
    }

    protected function checkPerm($key) {
        if (is_array($key)) {
            foreach ($key as $k => $subkey) {
                if ($this->permission->has($subkey)){
                    return true;
                }
            }
            show_404();
            die;
        } else if (!$this->permission->has($key)){
            show_404();
            die;
        }
    }

    protected function loadView($view) {
        $this->load->view('inc/header_view.php', $this->mOutput);
        $this->load->view("{$view}_view.php", $this->mOutput);
        $this->load->view('inc/footer_view.php', $this->mOutput);
    }

    protected function loadPagenation($url, $pageCount, $limit = 10) {
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = $url;
        //page count
        $config['total_rows'] = $pageCount;
        //data count of one page
        $config['per_page'] = $limit;
        $config['num_links'] = 6;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['next_link'] = '下一頁';
        $config['prev_link'] = '上一頁';
        $config['first_link'] = '第一頁';
        $config['full_tag_open'] = '<nav class="pageNav"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = '最後一頁';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
    }

}
