<?php


class Template_model extends CI_Model {

    public function getTemplate($id) {
        return $this->db->where('type', $id)->get('template')->row_array();
    }

    public function update($type, $content) : bool {
        $this->db->replace('template', array('type' => $type, 'content' => $content));

        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }
}