<?php

/**
 */
class Rentmanager_model extends Admin_Model {

    /**
     * 取得品牌列表
     * @param int $bid
     * @return array
     */
    public function getBrand(int $bid = 0) {
        if ($bid) {
            $this->db->where('bid', $bid);
        }
        $object = $this->db->get('brand_list');
        return !$bid ? $object->result_array() : $object->row_array();
    }

    /**
     * 取得電器類型列表
     * @param int $dtid
     * @return array
     */
    public function getDeviceTypeList(int $dtid = 0) {
        if ($dtid) {
            $this->db->where('dt_id', $dtid);
        }
        $object = $this->db->get('device_type_list');
        return !$dtid ? $object->result_array() : $object->row_array();
    }

    /**
     * 取得有面交點的租公列表
     * @param int $uid
     * @return array
     */
    public function getValidMasterList(int $uid = 0) {
        if ($uid) {
            $this->db->where('u.uid', $uid);
        }
        return $this->db
            ->select('u.uid,u.name,u.username')
            ->where('rent_master', 1)
            ->join('transaction_place p', 'u.uid = p.uid', 'inner')
            ->group_by('u.uid')
            ->get('user u')->result_array();
    }

    /**
     * 取得租借紀錄列表
     * @return array
     */
    public function getRentRecordList() {
        $this->db->start_cache();
        $this->setDataTableQuery(
            ['r.create_time'=> 'desc'],
            [
                'r.ur_id', 'l.transid', 'du.name', 'u.name', 'r.device_id', 'r.device_name', 'r.device_accessory', 'r.status', 'r.rent', 'r.deposit'
            ],
            [
                'r.ur_id', 'l.transid', 'du.name', 'u.name', 'r.create_time', 'r.start_time', 'r.device_id', 'r.device_name', 'r.device_accessory', 'r.status', 'r.rent', 'r.deposit'
            ]
        );
        $statement = $this->db
            ->select('r.*, du.name as device_owner_name, du.nickname as device_owner_nickname, 
            du.phone_number as device_owner_phone, u.name, u.nickname, u.username, u.phone_number as renter_phone
            , u.good, u.bad, tp.placename, tp.address, du.username as device_owner_username, l.transid'
            )
            ->join('transaction_place tp', 'r.tp_id = tp.tp_id', 'left')
            ->join('user u', 'r.renter_uid = u.uid', 'left')
            ->join('user du', 'r.device_owner = du.uid', 'left')
            ->join('transaction_log l', 'r.ur_id = l.ur_id and l.rtncode = 1', 'left')
            ->group_by('r.ur_id')
            ->from('rent_record r');

        $query = $statement->get();

        $data = [
            'list' => $query->result_array(),
            'rows' => $query->num_rows(),
            'total' => $statement->count_all_results()
        ];

        $this->db->flush_cache();
        $this->db->stop_cache();

        return $data;
    }

    /**
     * 取得租借紀錄
     * @param int $urId
     * @return array
     */
    public function getRentData(int $urId) {
        return $this->db
            ->select('r.*, du.name as device_owner_name, du.nickname as device_owner_nickname, 
            du.phone_number as device_owner_phone, u.name, u.nickname, u.username, u.phone_number as renter_phone
            , u.good, u.bad, tp.placename, tp.address, dp.filename, du.username as device_owner_username'
            )
            ->join('transaction_place tp', 'r.tp_id = tp.tp_id', 'left')
            ->join('user u', 'r.renter_uid = u.uid', 'left')
            ->join('user du', 'r.device_owner = du.uid', 'left')
            ->join('device_pic as dp', 'r.device_id = dp.device_id', 'left')
            ->group_by('r.ur_id')
            ->where('ur_id', $urId)
            ->get('rent_record r')
            ->row_array();
    }

    public function getDeviceList() {
        $this->db->start_cache();
        $this->setDataTableQuery(
            ['d.create_time', 'desc'],
            [
                'dp.filename', 'b.name', 'dt.name', 'd.name', 'd.accessory', 'd.description', 'd.deposit', 'd.rent', 'd.uid', 'd.status'
            ],
            [
                'dp.pic_id', 'b.name', 'dt.name', 'd.name', 'd.accessory', 'd.description', 'd.deposit', 'd.rent', 'd.uid', 'd.status'
            ]
        );
        $statement = $this->db->select('d.*, u.name as uname, dt.name as device_type_name, b.name as brand_name, dp.filename')
            ->join('user as u', 'd.uid = u.uid', 'left')
            ->join('device_type_list as dt', 'd.dt_id = dt.dt_id', 'left')
            ->join('brand_list as b', 'd.bid = b.bid', 'left')
            ->join('device_pic as dp', 'd.device_id = dp.device_id', 'left')
            ->group_by('d.device_id')
            ->from('device_list as d');

        $query = $statement->get();

        $data = [
            'list' => $query->result_array(),
            'rows' => $query->num_rows(),
            'total' => $statement->count_all_results()
        ];

        $this->db->flush_cache();
        $this->db->stop_cache();

        return $data;
    }

    /**
     * 取得電器資料
     * @param int $deviceId
     * @return array
     */
    public function getDeviceData(int $deviceId = 0) {
        $data = $this->db->select('d.*, u.name as realname, u.username, u.nickname, u.good, u.bad, dt.name as device_type_name, b.name as brand_name')
            ->where('d.device_id', $deviceId)
            ->join('user as u', 'd.uid = u.uid', 'left')
            ->join('device_type_list as dt', 'd.dt_id = dt.dt_id', 'left')
            ->join('brand_list as b', 'd.bid = b.bid', 'left')
            ->get('device_list as d')
            ->row_array();
        if (empty($data)) {
            return [];
        }
        $data['pic'] = $this->db->where('device_id', $deviceId)->get('device_pic')->result_array();
        return $data;
    }

    /**
     * 取得電器全部圖片
     * @param int $deviceId
     * @return array
     */
    public function getDevicePic(int $deviceId) : array {
        return $this->db->where('device_id', $deviceId)->get('device_pic')->result_array();
    }

    /**
     * 取得意見反應列表
     * @return array
     */
    public function getArbitrationList() {
        $this->db->start_cache();
        $this->setDataTableQuery(
            ['a.create_time', 'desc'],
            [
                'r.ur_id', 'du.name', 'u.name', 'r.device_id', 'r.device_name', 'r.device_accessory', 'r.status', 'r.rent', 'r.deposit'
            ],
            [
                'r.ur_id', 'du.name', 'u.name', 'r.create_time', 'r.start_time', 'r.device_id', 'r.device_name', 'r.device_accessory', 'r.status', 'r.rent', 'r.deposit'
            ]
        );
        $statement = $this->db
            ->select('a.ab_id, a.status as abstatus, r.*, du.name as device_owner_name, du.nickname as device_owner_nickname, 
            du.phone_number as device_owner_phone, u.name, u.nickname, u.username, u.phone_number as renter_phone
            , u.good, u.bad, du.username as device_owner_username'
            )
            ->join('rent_record r', 'a.ur_id = r.ur_id', 'left')
            ->join('user u', 'r.renter_uid = u.uid', 'left')
            ->join('user du', 'r.device_owner = du.uid', 'left')
            ->where('a.status', 0)
            ->from('arbitration_list a');

        $query = $statement->get();

        $data = [
            'list' => $query->result_array(),
            'rows' => $query->num_rows(),
            'total' => $statement->count_all_results()
        ];

        $this->db->flush_cache();
        $this->db->stop_cache();

        return $data;
    }

    /**
     * 取得意見反應資料
     * @param $abid
     * @return array
     */
    public function getArbitrationData($abid) : array {
        return $this->db
            ->select('a.ab_id, a.uid as abuid, a.status as abstatus, a.reason as _reason, a.create_time as create_ass_time, 
            r.*, du.name as device_owner_name, du.nickname as device_owner_nickname, 
            du.phone_number as device_owner_phone, du.username as device_owner_username, u.name, u.nickname, u.username, u.phone_number as renter_phone
            , u.good, u.bad, du.username as device_owner_username')
            ->join('rent_record r', 'a.ur_id = r.ur_id', 'left')
            ->join('user u', 'r.renter_uid = u.uid', 'left')
            ->join('user du', 'r.device_owner = du.uid', 'left')
            ->where('ab_id', $abid)
            ->get('arbitration_list a')
            ->row_array() ? : [];

    }

    /**
     * 取得面交地點列表
     * @param int $uid
     * @return array
     */
    public function getLocationList(int $uid): array {
        return $this->db->select('t.*,c.name as countryName, ct.name as cityName')
            ->where('uid', $uid)
            ->join('country c', 't.cid = c.cid', 'left')
            ->join('city ct', 't.ct_id = ct.ct_id', 'left')
            ->get('transaction_place t')
            ->result_array();
    }

    /**
     * 確認電器是否被租借中或已預約或待接單
     * @param int $deviceId
     * @return bool true代表租借中或已預約
     */
    public function checkOrderStatusByDevice(int $deviceId) : bool {
        return $this->db->where_in('status', [0, 1, 6])->where('device_id', $deviceId)->count_all_results('rent_record') > 0;
    }

    /**
     * 更新品牌
     * @param $bid
     * @param $name
     * @return bool
     */
    public function updateBrand($bid, $name) {
        $this->db->where('bid', $bid)->update('brand_list', array('name' => $name));

        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 更新電器類型
     * @param $dtid
     * @param $name
     * @return bool
     */
    public function updateDeviceType($dtid, $name) {
        $this->db->where('dt_id', $dtid)->update('device_type_list', array('name' => $name));

        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 更新電器
     * @param int $deviceId
     * @param array $data
     */
    public function updateDevice(int $deviceId, array $data) {
        $this->db->where('device_id', $deviceId)->update('device_list', $data);
    }

    /**
     * 更新意見反應單
     * @param $abId
     * @param $data
     * @return bool
     */
    public function updateArbitration($abId, $data) : bool {
        $info = $this->getArbitrationData($abId);
        $this->db->trans_start();
        $this->db->where('ab_id', $abId)->update('arbitration_list', $data);
        switch ($data['status']) {
            case 1://銷單
                $status = 8;
                if ($info['renter_assessment'] > 0) {
                    $col = 'good';
                    if ($info['renter_assessment'] > 1) {
                        $col = 'bad';
                    }
                    $this->db->query("update user set {$col} = {$col} - 1 where uid = {$info['device_owner']}");
                }
                if ($info['owner_assessment'] > 0) {
                    $col = 'good';
                    if ($info['owner_assessment'] > 1) {
                        $col = 'bad';
                    }
                    $this->db->query("update user set {$col} = {$col} - 1 where uid = {$info['renter_uid']}");
                }
                break;
            case 2://駁回
                $now = time();
                if ($now >= strtotime($info['start_time']) && $now < strtotime($info['end_time'])) {
                    //租借時間內狀態退回租借中
                    $status = 1;
                } else if ($now < strtotime($info['start_time'])) {
                    //租借時間未到狀態退回待開始
                    $status = 6;
                } else {
                    //結案
                    $status = 2;
                }
                break;
        }
        $this->db->where('ur_id', $info['ur_id'])->update('rent_record', [
            'status' => $status
        ]);

        $this->db->trans_complete();

        return $this->db->trans_status() !== false;
    }

    /**
     * 新增品牌
     * @param $name
     * @return bool
     */
    public function insertBrand($name) {
        $this->db->insert('brand_list', array('name' => $name));
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 新增電器類型
     * @param $name
     * @return bool
     */
    public function insertDeviceType($name) {
        $this->db->insert('device_type_list', array('name' => $name));
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 新增電器
     * @param array $data
     * @return int
     */
    public function insertDevice(array $data) : int {
        $this->db->insert('device_list', $data);
        return $this->db->insert_id();
    }

    /**
     * 寫入電器新圖
     * @param array $data
     * @return bool
     */
    public function insertDevicePic(array $data) : bool {
        $this->db->insert_batch('device_pic', $data);
        return $this->db->affected_rows();
    }

    /**
     * 刪除品牌
     * @param int $bid
     * @return bool
     */
    public function deleteBrand(int $bid) {
        $this->db->delete('brand_list', array('bid' => $bid));
        return $this->db->affected_rows() > 0;
    }

    /**
     * 刪除電器類型
     * @param int $dtid
     * @return bool
     */
    public function deleteDeviceType(int $dtid) {
        $this->db->delete('device_type_list', array('dt_id' => $dtid));
        return $this->db->affected_rows() > 0;
    }

    /**
     * 刪除電器，同時刪除圖片
     * @param int $deviceId
     * @return bool
     */
    public function deleteDevice(int $deviceId = 0) {
        $this->db->delete('device_list', array('device_id' => $deviceId));
        if ($this->db->affected_rows() > 0) {
            $pics = $this->db->where('device_id', $deviceId)->get('device_pic')->result_array();
            foreach ($pics as $row) {
                if (file_exists(PHOTO_PATH . $row['filename'])) {
                    unlink(PHOTO_PATH . $row['filename']);
                }
            }
            $this->db->delete('device_pic', ['device_id', $deviceId]);
            return true;
        }
        return false;
    }

    /**
     * 刪除電器圖片
     * @param int $deviceId
     * @param string $filename
     */
    public function deleteDevicePic(int $deviceId, string $filename) {
        $this->db->where(array(
            'device_id' => $deviceId,
            'filename' => $filename
        ))->delete('device_pic');
    }
}