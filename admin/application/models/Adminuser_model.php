<?php

/**
 * 後台使用者相關資料庫操作
 */
class Adminuser_model extends CI_Model {

    public function getAdminUser($account, $pwd) {
        return $this->db->select('aid, gid, name, pic')
                ->where(array(
                    'account' => "'{$account}'",
                    'password' => "'{$pwd}'"
                ), NULL, FALSE)
                ->get('admin_user')->row_array();
    }

    public function getAdminUserById($aid) {
        return $this->db->select('*')
            ->where('aid', $aid)
            ->get('admin_user')->row_array();
    }

    public function getAdminUserList() {
        return $this->db
            ->select('u.*,g.name as groupName')
            ->join('admin_group as g', 'u.gid = g.gid', 'left')
            ->get('admin_user as u')->result_array();
    }

    public function checkAdminUserExist($account) {
        return $this->db->where('account', $account)->count_all_results('admin_user') > 0;
    }

    public function insertAdminUser($data) {
        $this->db->insert('admin_user', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function updateAdminUser($aid, $data) {
        $this->db->where('aid', $aid)->update('admin_user', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;

    }

    public function deleteAdminUser(int $aid) {
        $this->db->delete('admin_user', array('aid' => $aid));
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

}
