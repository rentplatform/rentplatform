<?php


class Setting_model extends CI_Model {

    /**
     * 取得所有全域設定
     * @return array
     */
    public function getAll() : array {
        $data = $this->db->get('common_setting')->result_array();
        $res = array();
        foreach ($data as $row) {
            $res[$row['key']] = $row['value'];
        }
        return $res;
    }

    /**
     * 取得縣市資料，單筆或所有
     * @param int $cid
     * @return array
     */
    public function getCountry(int $cid = 0) {
        if ($cid) {
            $this->db->where('cid', $cid);
        }
        $res = $this->db->select('c.*, (select count(*) from city as ct where ct.cid = c.cid) as citycount')->get('country as c');
        return !$cid ? $res->result_array() : $res->row_array();
    }

    /**
     * 取得該縣市的所有區域
     * @param int $cid
     * @return array
     */
    public function getCityByCountry(int $cid = 0) {
        if ($cid) {
            $this->db->where('cid', $cid);
        }
        $res = $this->db->get('city');
        return $res->result_array();
    }

    /**
     * 取得某一區域資料
     * @param int $cid
     * @return array
     */
    public function getCity(int $cid = 0) {
        if ($cid) {
            $this->db->where('ct_id', $cid);
        }
        $res = $this->db->get('city');
        return $res->row_array();
    }

    /**
     * 取得廣告優惠列表
     * @return array
     */
    public function getAdDiscountList() {
        return $this->db->order_by('start_time', 'desc')->get('ad_price_list')->result_array();
    }

    /**
     * 取得某一筆的廣告優惠資料
     * @param int $adId
     * @return array
     */
    public function getAdDiscountData(int $adId = 0) {
        return $this->db->where('ad_id', $adId)->get('ad_price_list')->row_array() ? : [];
    }

    public function checkAdDiscountDateValid(string $start, string $end, int $adId) {
        $where = [];
        if ($adId) {
            $where['ad_id !='] = $adId;
        }
        $this->db->group_start();
        $this->db->where(array_merge($where, [
            'start_time <=' => $start,
            'end_time >=' => $start
        ]));
        $this->db->group_end();
        $this->db->group_start('', 'or');
        $this->db->where(array_merge($where, [
            'start_time <=' => $end,
            'end_time >=' => $end
        ]));
        $this->db->group_end();
        $this->db->group_start('', 'or');
        $this->db->where(array_merge($where, [
            'start_time >=' => $start,
            'end_time <=' => $end
        ]));
        $this->db->group_end();
        return $this->db->count_all_results('ad_price_list') < 1;
    }

    /**
     * 修改全域設定
     * @param $key
     * @param $value
     * @return bool
     */
    public function update($key, $value) : bool {
        $this->db->where('key', $key)->update('common_setting', array('value' => $value));

        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 修改縣市
     * @param $cid
     * @param $name
     * @return bool
     */
    public function updateCountry($cid, $name) {
        $this->db->where('cid', $cid)->update('country', array('name' => $name));
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 修改區域
     * @param $ctid
     * @param $name
     * @return bool
     */
    public function updateCity($ctid, $name) {
        $this->db->where('ct_id', $ctid)->update('city', array('name' => $name));
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 修改優惠活動
     * @param int $adid
     * @param array $data
     * @return bool
     */
    public function updateAdDiscount(int $adid, array $data) {
        $this->db->where('ad_id', $adid)->update('ad_price_list', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 新增縣市
     * @param $name
     * @return bool
     */
    public function insertCountry($name) {
        $this->db->insert('country', array('name' => $name));
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 新增區域
     * @param $cid
     * @param $name
     * @return bool
     */
    public function insertCity($cid, $name) {
        $this->db->insert('city', array(
            'name' => $name,
            'cid' => $cid
        ));
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 新增優惠活動
     * @param array $data
     * @return bool
     */
    public function insertAdDiscount(array $data) {
        $this->db->insert('ad_price_list', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 刪除縣市
     * @param int $cid
     * @return bool
     */
    public function deleteCountry(int $cid) {
        $this->db->delete('country', array('cid' => $cid));
        return $this->db->affected_rows() > 0;
    }

    /**
     * 刪除區域
     * @param int $ctid
     * @return bool
     */
    public function deleteCity(int $ctid) {
        $this->db->delete('city', array('ct_id' => $ctid));
        return $this->db->affected_rows() > 0;
    }

    /**
     * 刪除廣告優惠
     * @param int $adId
     */
    public function deleteAdDiscount(int $adId = 0) {
        $this->db->delete('ad_price_list', array('ad_id' => $adId));
        return $this->db->affected_rows() > 0;
    }
}