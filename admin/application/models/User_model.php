<?php

/**
 * 後台使用者相關資料庫操作
 */
class User_model extends Admin_Model {

    /**
     * 取得會員資料
     * @param int $uid
     * @return array
     */
    public function getUserById(int $uid) {
        return $this->db->select('*')
            ->where('uid', $uid)
            ->get('user')->row_array() ? : [];
    }

    /**
     * 取得會員列表
     * @return array
     */
    public function getUserList() {
        $this->db->start_cache();
        $this->setDataTableQuery(
            ['create_time' => 'desc'],
            ['uid', 'id_number', 'name', 'phone_number', 'username'],
            ['uid', 'id_number', 'name', 'phone_number', 'username', 'last_login', 'rent_time']
        );
        $statement = $this->db
            //已刪除的使用者除外
            ->where('status <', 4)
            ->from('user as u');
        $query = $statement->get();

        $data = [
            'list' => $query->result_array(),
            'rows' => $query->num_rows(),
            'total' => $statement->count_all_results()
        ];

        $this->db->flush_cache();
        $this->db->stop_cache();

        return $data;
    }

    /**
     * 取得租公列表
     * @return array
     */
    public function getMasterList() {
        $this->db->start_cache();
        $this->setDataTableQuery(
            ['create_time' => 'desc'],
            ['uid', 'id_number', 'name', 'phone_number', 'username'],
            ['uid', 'id_number', 'name', 'phone_number', 'username', 'last_login']
        );
        $statement = $this->db
            //已刪除的使用者除外
            ->where('status <', 4)
            ->where('rent_master', 1)
            ->from('user as u');
        $query = $statement->get();

        $data = [
            'list' => $query->result_array(),
            'rows' => $query->num_rows(),
            'total' => $statement->count_all_results()
        ];

        $this->db->flush_cache();
        $this->db->stop_cache();

        return $data;
    }

    /**
     * 會員租借紀錄
     * @param int $uid
     * @return array
     */
    public function getUserRentRecordList($uid) {
        $this->db->start_cache();
        $this->setDataTableQuery(
            ['r.start_time' => 'desc'],
            ['r.ur_id', 'r.device_id', 'r.device_name', 'r.device_accessory', 'r.status', 'r.rent', 'r.deposit'],
            ['r.ur_id', 'r.start_time', 'r.device_id', 'r.device_name', 'r.device_accessory', 'r.status', 'r.rent', 'r.deposit']
        );
        $statement = $this->db
            //已刪除的使用者除外
            ->where('r.renter_uid', $uid)
            ->from('rent_record r');
        $query = $statement->get();

        $data = [
            'list' => $query->result_array(),
            'rows' => $query->num_rows(),
            'total' => $statement->count_all_results()
        ];

        $this->db->flush_cache();
        $this->db->stop_cache();

        return $data;
    }

    /**
     * 租公租借紀錄
     * @param int $uid
     * @return array
     */
    public function getMasterRentRecordList(int $uid) {
        $this->db->start_cache();
        $this->setDataTableQuery(
            ['r.create_time'=> 'desc'],
            [
                'r.ur_id', 'u.name', 'r.device_id', 'r.device_name', 'r.device_accessory', 'r.status', 'r.rent', 'r.deposit'
            ],
            [
                'r.ur_id', 'u.name', 'r.create_time', 'r.start_time', 'r.device_id', 'r.device_name', 'r.device_accessory', 'r.status', 'r.rent', 'r.deposit'
            ]
        );
        $statement = $this->db
            ->select('r.*, du.name as device_owner_name, du.nickname as device_owner_nickname, 
            du.phone_number as device_owner_phone, u.name, u.nickname, u.username, u.phone_number as renter_phone
            , u.good, u.bad, tp.placename, tp.address, du.username as device_owner_username'
            )
            ->join('transaction_place tp', 'r.tp_id = tp.tp_id', 'left')
            ->join('user u', 'r.renter_uid = u.uid', 'left')
            ->join('user du', 'r.device_owner = du.uid', 'left')
            ->where('r.device_owner', $uid)
            ->group_by('r.ur_id')
            ->from('rent_record r');
        $query = $statement->get();

        $data = [
            'list' => $query->result_array(),
            'rows' => $query->num_rows(),
            'total' => $statement->count_all_results()
        ];

        $this->db->flush_cache();
        $this->db->stop_cache();

        return $data;
    }

    /**
     * 取得租客某個狀態的租借次數
     * @param $uid
     * @param $status
     * @return int
     */
    public function getUserRentInfoByStatus(int $uid, int $status) {
        return $this->db->where([
            'status' => $status,
            'renter_uid' => $uid
        ])->count_all_results('rent_record');
    }

    /**
     * 取得租公某個狀態的租借次數
     * @param $uid
     * @param $status
     * @return int
     */
    public function getMasterRentInfoByStatus(int $uid, int $status) {
        return $this->db->where([
            'status' => $status,
            'device_owner' => $uid
        ])->count_all_results('rent_record');
    }

    /**
     * user總交易次數
     * @param int $uid
     */
    public function getMasterTransactionTotal(int $uid = 0) {
        return $this->db
            ->select_sum('reward')
            ->where([
            'device_owner' => $uid,
            'status' => 2
        ])->get('rent_record')->row_array()['reward'];
    }

    /**
     * 取得租公面交列表
     * @param int $uid
     * @return array
     */
    public function getMasterLocationList(int $uid = 0) {
        return $this->db->select('t.*,c.name as countryName, ct.name as cityName')
            ->where('uid', $uid)
            ->join('country c', 't.cid = c.cid', 'left')
            ->join('city ct', 't.ct_id = ct.ct_id', 'left')
            ->get('transaction_place t')
            ->result_array();
    }

    /**
     * 取得租公面交點
     * @param int $tpId
     * @return array
     */
    public function getLocation(int $tpId = 0) {
        return $this->db->where('tp_id', $tpId)->get('transaction_place')->row_array();
    }

    /**
     * 取得租公電器列表
     * @param int $uid
     * @return array
     */
    public function getMasterDeviceList(int $uid = 0) {
        return $this->db
            ->select('d.*, p.filename')
            ->join('device_pic p', 'd.device_id = p.device_id')
            ->where('uid', $uid)
            ->group_by('d.device_id')
            ->get('device_list d')
            ->result_array();
    }

    /**
     * 檢查email是否已被使用，如果有傳uid則表示除了自己還有沒有其他人用過
     * @param string $username
     * @param int $uid
     * @return bool
     */
    public function checkUsernameExists(string $username = '', int $uid = 0) {
        if ($uid) {
            $this->db->where('uid !=', $uid);
        }
        return $this->db->where('username', $username)->count_all_results('user') > 0;
    }

    /**
     * insert new user
     * @param $data
     * @return bool
     */
    public function insertUser($data) {
        $this->db->insert('user', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * update user
     * @param $uid
     * @param $data
     * @return bool
     */
    public function updateUser($uid, $data) {
        $this->db->where('uid', $uid)->update('user', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * update user location
     */
    public function updateLocation($tpId, $data) {
        $this->db->where('tp_id', $tpId)->update('transaction_place', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 刪除面交點
     * @param int $tpId
     * @return bool
     */
    public function deleteLocation(int $tpId = 0) {
        $this->db->where('tp_id', $tpId)->delete('transaction_place');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

}
