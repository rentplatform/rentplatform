<?php
/**
 * Created by PhpStorm.
 * User: asdf
 * Date: 2017/10/15
 * Time: 下午 05:03
 */


class Group_model extends CI_Model {


    public function getGroupList() {
        return $this->db->get('admin_group')->result_array();
    }

    public function getGroupById($gid) {
        return $this->db->select('*')
            ->where('gid', $gid)
            ->get('admin_group')->row_array();
    }

    public function checkGroupNameExist($name) {
        return $this->db->where('name', $name)->count_all_results('admin_group') > 0;
    }

    public function updateGroup($gid, $data) {
        $this->db->where('gid', $gid)->update('admin_group', $data);
        return TRUE;
    }

    public function insertGroup($data) {
        $this->db->insert('admin_group', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;

    }

    public function deleteGroup($gid) {
        $this->db->where('gid', $gid)->delete('admin_group');
        return $this->db->affected_rows() > 0;
    }

}