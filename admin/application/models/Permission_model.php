<?php
/**
 * Created by PhpStorm.
 * User: asdf
 * Date: 2017/10/15
 * Time: 下午 05:03
 */


class Permission_model extends CI_Model {

    public function getAllPermList() {
        return $this->db
            ->select('*, (select count(*) from admin_permission_list where parent_perm_id = p.perm_id) as subcount')
            ->order_by('parent_perm_id', 'asc')
            ->order_by('perm_id', 'asc')
            ->get('admin_permission_list as p')->result_array();
    }

    public function getGroupPerm($gid) {
        $data = $this->db->where('gid', $gid)->get('admin_group_permission')->result_array();
        return array_column($data, 'perm_id');
    }

    public function getGroupPermList($gid) {
        return $this->db->where(array(
                'gp.gid' => $gid
            ))->join('admin_permission_list as p', 'p.perm_id = gp.perm_id', 'left')
                ->get('admin_group_permission as gp')->result_array();
    }

    public function checkPerm($gid, $key) {
        return $this->db->where(array(
            'gp.gid' => $gid,
            'p.key' => $key
        ))->join('admin_permission_list as p', 'p.perm_id = gp.perm_id', 'left')
        ->count_all_results('admin_group_permission as gp') > 0;
    }

    public function editGroupPerm($gid, $permIds) {
        $data = array();
        foreach ($permIds as $k => $id) {
            if (empty($id)) continue;
            $data[] = array(
                'gid' => $gid,
                'perm_id' => $id
            );
        }
        $this->db->where('gid', $gid)->delete('admin_group_permission');
        $this->db->insert_batch('admin_group_permission', $data);
        return $this->db->affected_rows() > 0;
    }


}