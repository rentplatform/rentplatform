<?php


class Page_model extends CI_Model {

    /**
     * 取得輪播資料
     * @param int $id
     * @return array
     */
    public function getCarousel(int $id = 0) {
        if ($id) {
            $this->db->where('caid', $id);
        }
        $statement = $this->db->get('carousel');
        return $id ? $statement->row_array() : $statement->result_array();
    }

    /**
     * 取得專欄文章
     * @param int $id
     * @return array
     */
    public function getArticle(int $id = 0) {
        if ($id) {
            $this->db->where('at_id', $id);
        }
        $statement = $this->db
            ->select('a.*,l.name as typename')
            ->join('article_type_list l', 'a.atype_id = l.atype_id', 'left')
            ->get('article a');
        return $id ? $statement->row_array() : $statement->result_array();
    }

    /**
     * 取得專欄文章類型
     * @param int $id
     * @return array
     */
    public function getArticleTypeList(int $id = 0) {
        if ($id) {
            $this->db->where('atype_id', $id);
        }
        $statement = $this->db->get('article_type_list');
        return $id ? $statement->row_array() : $statement->result_array();
    }

    /**
     * 取得常見問題
     * @param int $id
     * @return array
     */
    public function getQuestion(int $id = 0) {
        if ($id) {
            $this->db->where('qid', $id);
        }
        $statement = $this->db->get('question_list');
        return $id ? $statement->row_array() : $statement->result_array();
    }

    /**
     * 取得靜態資料
     * @param $type
     * @param bool $isMulti
     * @return array
     */
    public function getStaticData($type, $isMulti = false) {
        $statement = $this->db->where('type', $type)->get('static_page');
        return $isMulti ? $statement->result_array() : ($statement->row_array() ? : []);
    }

    /**
     * 取得靜態資料 by sid
     * @param int $id
     * @return array
     */
    public function getStaticDataById(int $id = 0) {
        return $this->db->where('sid', $id)->get('static_page')->row_array() ? : [];
    }

    /**
     * 更新輪播圖資料
     * @param int $caid
     * @param array $data
     */
    public function updateCarousel(int $caid, array $data) {
        $this->db->where('caid', $caid)->update('carousel', $data);
    }

    /**
     * 更新專欄文章
     * @param int $atid
     * @param array $data
     */
    public function updateArticle(int $atid, array $data) {
        $this->db->where('at_id', $atid)->update('article', $data);
    }

    /**
     * 更新專欄文章類型
     * @param int $id
     * @param array $data
     */
    public function updateArticleType(int $id, array $data) {
        $this->db->where('atype_id', $id)->update('article_type_list', $data);
    }

    /**
     * 更新租公滿意或推薦資料
     * @param int $id
     * @param array $data
     */
    public function updateMasterSource(int $id, array $data) {
        $this->db->where('sid', $id)->update('static_page', $data);
    }

    /**
     * 更新常見問題
     * @param int $id
     * @param array $data
     */
    public function updateQuestion(int $id, array $data) {
        $this->db->where('qid', $id)->update('question_list', $data);
    }

    /**
     * 更新靜態頁面資料
     * @param int $type
     * @param array $data
     */
    public function updateStaticData(int $type, array $data) {
        $d = $this->getStaticData($type);
        if (empty($d)) {
            $this->db->insert('static_page', $data);
        } else {
            $this->db->where('type', $type)->update('static_page', $data);
        }
    }

    /**
     * 新增輪播圖
     * @param array $data
     * @return int
     */
    public function insertCarousel(array $data) {
        $this->db->insert('carousel', $data);
        return $this->db->insert_id();
    }

    /**
     * 新增專欄文章
     * @param array $data
     * @return int
     */
    public function insertArticle(array $data) {
        $this->db->insert('article', $data);
        return $this->db->insert_id();
    }

    /**
     * 新增專欄文章類型
     * @param array $data
     */
    public function insertArticleType(array $data) {
        $this->db->insert('article_type_list', $data);
    }

    /**
     * 新增租公滿意或推薦資料
     * @param array $data
     */
    public function insertMasterSource(array $data) {
        $this->db->insert('static_page', $data);
    }

    /**
     * 新增常見問題
     * @param array $data
     */
    public function insertQuestion(array $data) {
        $this->db->insert('question_list', $data);
    }

    /**
     * 刪除輪播圖
     * @param int $caid
     * @return bool
     */
    public function deleteCarousel(int $caid = 0) {
        $this->db->where('caid', $caid)->delete('carousel');

        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 刪除專欄文章
     * @param int $atid
     * @return bool
     */
    public function deleteArticle(int $atid = 0) {
        $this->db->where('at_id', $atid)->delete('article');

        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 刪除專欄文章類型
     * @param int $id
     * @return bool
     */
    public function deleteArticleType(int $id = 0) {
        $this->db->where('atype_id', $id)->delete('article_type_list');

        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 刪除常見問題
     * @param int $id
     * @return bool
     */
    public function deleteQuestion(int $id = 0) {
        $this->db->where('qid', $id)->delete('question_list');

        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

}