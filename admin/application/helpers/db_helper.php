<?php

function genTimeWhereString($data, $prefix = '') {
    $output = '';
    $timeWhere = array();
    if (!empty($data['start_time'])) {
        $timeWhere[] = " {$prefix}start_time >= '{$data['start_time']} 00:00:00' ";
    }
    if (!empty($data['end_time'])) {
        $timeWhere[] = " {$prefix}end_time <= '{$data['end_time']} 23:59:59' ";
    }
    if (count($timeWhere) > 0) {
        $output = implode('AND', $timeWhere);
        $output = "($output)";
    }
    return $output;
}

function arrayToSearchString($array) {
    $new = array_walk($array, function($k, $v) {
        return "'{$v}'";
    });
    return implode(',', $new);
}