<?php

define('UTIL_ZIP_EXTRACT_DETECT_ENCODING_ORDER_LIST', 'UTF-8, BIG5, ISO-8859-1'); //請更改你要檢測的編碼。請參考「http://php.net/manual/en/function.mb-detect-order.php」。
//define('UTIL_ZIP_EXTRACT_DETECT_ENCODING_ORDER_LIST', 'UTF-8, GB2312, BIG-5, SHIFT-JIS, ISO-8859-1');
/**
 * ZIP 解壓縮，並偵測檔案名稱編碼，自動轉換成Utf-8。
 *
 * @param string $source_path zip檔案路徑。
 * @param mixed $target_path 解開後的路徑，可以不填。
 * @return string 有發生錯誤，會回傳訊息。
 */

function util_zip_extract($source_path, $target_path = null) {
    if ($target_path === null) {
        $target_path = substr($source_path, 0, -4);
    }
    $target_base_path = dirname($target_path);
    if (file_exists($target_path)) {
        return 'Dir or File is exist!: ' . $target_path;
    }
    if (!file_exists($source_path)) {
        return 'Extract file is not exist!: ' . $source_path;
    }
    $file = zip_open($source_path);
    if (!is_resource($file)) {
        return 'Open zip file not ok!: ' . $source_path;
    }
    while ($entry = zip_read($file)) {
        $file_size = zip_entry_filesize($entry);
        $file_name = zip_entry_name($entry);
        $encode = mb_detect_encoding($file_name, UTIL_ZIP_EXTRACT_DETECT_ENCODING_ORDER_LIST);
        //echo $encode . PHP_EOL;
        if (strtoupper(trim($encode)) !== 'UTF-8') {
            $file_name = iconv($encode, 'UTF-8//IGNORE', $file_name);
            ////$file_name = iconv('Big5', 'UTF-8//IGNORE', $file_name);
            ////$file_name = iconv('Big-5', 'UTF-8//IGNORE', $file_name);
        }
        $target_file_path = $target_base_path . '/' . $file_name;
        if (!$file_size) {
            if (file_exists($target_file_path)) {
                continue;
            }
            mkdir($target_file_path, 0755, true);
            continue;
        } else if (!zip_entry_open($file, $entry)) {
            continue;
        }
        file_put_contents($target_file_path, zip_entry_read($entry, $file_size));
        zip_entry_close($entry);
    }
    zip_close($file);
}
