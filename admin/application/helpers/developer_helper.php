<?php

function debug() {
    $args = func_get_args();
    echo '<pre>';
    foreach ($args as $k => $v) {
        var_dump($v);
    }
    die;
}